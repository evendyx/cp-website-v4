<header class="cd-auto-hide-header">
	<nav class="cd-primary-nav">
		<div class="col-md-6 contact-top">
		<?php 
			global $conection;
			$sql = mysqli_query($conection,"select * from contacts WHERE id='1'");
			$row = mysqli_fetch_assoc($sql);
			$email = $row['email'];
			$phone = $row['phone'];
				if ($email != ''){
					echo "
					<div class='list'>
						<i class='fa fa-envelope'></i> <a href='mailto:".$row['email']."'>".$row['email']."</a>
					</div>";
				}
				if ($email == ''){

					echo "<div class='list'></div>";
				}
				if ($phone != ''){
					echo "
					<div class='list'>
						<i class='fa fa-phone'></i> ".$row['phone']."						
					</div>";
				}
				if ($phone == ''){

					echo "<div class='list'></div>";
				}
		?>
			
		</div>
		<div class="col-md-6 social-top">
			<ul>
				<?php echo getSocial(); ?>				
			</ul>
		</div>
	</nav> <!-- .cd-primary-nav -->
	
	<nav class="cd-secondary-nav">
		<div class="container">
			 <div class="navbar-header col-md-4">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
				<?php
					global $conection;
					$sql = mysqli_query($conection,"select * from logo");
					$row = mysqli_fetch_assoc($sql);
					$image = $row['image'];
					$logo_text = $row['logo_text'];
					
					if ($logo_text == 'logo'){

						echo "
							<a class='navbar-brand page-scroll' href='./'>
								<img src ='assets/img/uploads/other/".$row['image']."' class='img-responsive' />
							</a>";
					}
					if ($logo_text != 'logo'){

						echo "<a class='navbar-brand page-scroll' href='./'>Marketing</a>";
					}
				?>              
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse col-md-8 navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="./"></a>
                    </li>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from settings");
						$row = mysqli_fetch_assoc($sql);
						$about = $row['about'];
						$services = $row['services'];
						$partners = $row['partners'];
						$portfolio = $row['portfolio'];
						$blog = $row['blog'];
						$contacts = $row['contacts'];
						$custom = $row['custom'];
						$calendar = $row['calendar'];
						
						if ($about == '1'){

							echo " 
							<li>
								<a href='about'>".lang('ABOUT')."</a>
							</li>";

						}
						if ($about == ''){
							echo " ";
						}
						if ($services == '1'){

							echo " 
							<li>
								<a href='services'>".lang('SERVICES')."</a>
							</li>";

						}
						if ($services == ''){
							echo " ";
						}
						
						if ($custom == '1'){

							echo showPages();

						}
						if ($custom == ''){
							echo " ";
						}
						
						if ($calendar == '1'){

							echo " 
							<li>
								<a href='calendar'>".lang('CALENDAR')."</a>
							</li>";

						}
						if ($calendar == ''){
							echo " ";
						}
						if ($portfolio == '1'){

							echo " 
							<li>
								<a href='portfolio'>".lang('PORTFOLIO')."</a>
							</li>";

						}
						if ($portfolio == ''){
							echo " ";
						}
						if ($blog == '1'){

							echo " 
							<li>
								<a href='blog'>".lang('BLOG')."</a>
							</li>";

						}
						if ($blog == ''){
							echo " ";
						}
						if ($partners == '1'){

							echo " 
							<li>
								<a href='partners'>".lang('PARTNERS')."</a>
							</li>";

						}
						if ($partners == ''){
							echo " ";
						}
						if ($contacts == '1'){

							echo " 
							<li>
								<a href='contacts'>".lang('CONTACTS')."</a>
							</li>";

						}
						if ($contacts == ''){
							echo " ";
						}
					?>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
		</div>		
	</nav> <!-- .cd-secondary-nav -->
</header> <!-- .cd-auto-hide-header -->


	<!-- Navigation -->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header col-md-4">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    Menu <i class="fa fa-bars"></i>
                </button>
				<?php
					global $conection;
					$sql = mysqli_query($conection,"select * from logo");
					$row = mysqli_fetch_assoc($sql);
					$image = $row['image'];
					$logo_text = $row['logo_text'];
					
					if ($logo_text == 'logo'){

						echo "
							<a class='navbar-brand page-scroll' href='./'>
								<img src ='assets/img/uploads/other/".$row['image']."' class='img-responsive' />
							</a>";
					}
					if ($logo_text != 'logo'){

						echo "<a class='navbar-brand page-scroll' href='./'>Marketing</a>";
					}
				?>              
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse col-md-8 navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="./"></a>
                    </li>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from settings");
						$row = mysqli_fetch_assoc($sql);
						$about = $row['about'];
						$services = $row['services'];
						$partners = $row['partners'];
						$portfolio = $row['portfolio'];
						$blog = $row['blog'];
						$contacts = $row['contacts'];
						$custom = $row['custom'];
						$calendar = $row['calendar'];
						
						if ($about == '1'){

							echo " 
							<li>
								<a href='about'>".lang('ABOUT')."</a>
							</li>";

						}
						if ($about == ''){
							echo " ";
						}
						if ($services == '1'){

							echo " 
							<li>
								<a href='services'>".lang('SERVICES')."</a>
							</li>";

						}
						if ($services == ''){
							echo " ";
						}
						
						if ($custom == '1'){

							echo showPages();

						}
						if ($custom == ''){
							echo " ";
						}
						
						if ($calendar == '1'){

							echo " 
							<li>
								<a href='calendar'>".lang('CALENDAR')."</a>
							</li>";

						}
						if ($calendar == ''){
							echo " ";
						}
						if ($portfolio == '1'){

							echo " 
							<li>
								<a href='portfolio'>".lang('PORTFOLIO')."</a>
							</li>";

						}
						if ($portfolio == ''){
							echo " ";
						}
						if ($blog == '1'){

							echo " 
							<li>
								<a href='blog'>".lang('BLOG')."</a>
							</li>";

						}
						if ($blog == ''){
							echo " ";
						}
						if ($partners == '1'){

							echo " 
							<li>
								<a href='partners'>".lang('PARTNERS')."</a>
							</li>";

						}
						if ($partners == ''){
							echo " ";
						}
						if ($contacts == '1'){

							echo " 
							<li>
								<a href='contacts'>".lang('CONTACTS')."</a>
							</li>";

						}
						if ($contacts == ''){
							echo " ";
						}
					?>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
           
               
		<?php
			global $conection;
			$sql = mysqli_query($conection,"select * from logo");
			$row = mysqli_fetch_assoc($sql);
			$image = $row['image'];
			$logo_text = $row['logo_text'];
			
			if ($logo_text == 'logo'){

				echo "
				<li class='sidebar-brand'>			
					<a href='./' onclick=$('#menu-close').click();><img src ='assets/img/uploads/other/".$row['image']."' /></a>
				</li>";
			}
			if ($logo_text != 'logo'){

				echo "
				<li class='sidebar-brand'>			
					<a href='./' onclick=$('#menu-close').click();>Marketing</a>
				</li>";
			}
		?>
		<?php
			global $conection;
			$sql = mysqli_query($conection,"select * from settings");
			$row = mysqli_fetch_assoc($sql);
			$about = $row['about'];
			$services = $row['services'];
			$partners = $row['partners'];
			$portfolio = $row['portfolio'];
			$blog = $row['blog'];
			$contacts = $row['contacts'];
			$calendar = $row['calendar'];
			
			if ($about == '1'){

				echo " 
				<li>
					<a href='about' onclick=$('#menu-close').click();>".lang('ABOUT')."</a>
				</li>";

			}
			if ($about == ''){
				echo " ";
			}
			if ($services == '1'){

				echo " 
				<li>
					<a href='services' onclick=$('#menu-close').click();>".lang('SERVICES')."</a>
				</li>";

			}
			if ($services == ''){
				echo " ";
			}
			
			if ($custom == '1'){

				echo showPages1();

			}
			if ($custom == ''){
				echo " ";
			}
			
			if ($calendar == '1'){
				echo " 
				<li>
					<a href='calendar' onclick=$('#menu-close').click();>".lang('CALENDAR')."</a>
				</li>";

			}
			if ($calendar == ''){
				echo " ";
			}
			 
			if ($portfolio == '1'){

				echo " 
				<li>
					<a href='portfolio' onclick=$('#menu-close').click();>".lang('PORTFOLIO')."</a>
				</li>";

			}
			if ($portfolio == ''){
				echo " ";
			}
			if ($blog == '1'){

				echo " 
				<li>
					<a href='blog' onclick=$('#menu-close').click();>".lang('BLOG')."</a>
				</li>";

			}
			if ($blog == ''){
				echo " ";
			}
			if ($partners == '1'){

				echo " 
				<li>
					<a href='partners' onclick=$('#menu-close').click();>".lang('PARTNERS')."</a>
				</li>";

			}
			if ($partners == ''){
				echo " ";
			}
			if ($contacts == '1'){

				echo " 
				<li>
					<a href='contacts' onclick=$('#menu-close').click();>".lang('CONTACTS')."</a>
				</li>";

			}
			if ($contacts == ''){
				echo " ";
			}
		?>
		
			
        </ul>
    </nav>
	