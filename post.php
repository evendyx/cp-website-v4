<?php include_once 'header.php'; ?>

	<div class="wrapper">
			<?php 
			
			$id = isset($_GET['id']) ? $_GET['id'] : '';						
			echo post($id);

			?>   
			
			<div id="share-buttons">
    
				<!-- Facebook -->
				<a href="http://www.facebook.com/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank">
					<i class="fa fa-facebook-square fa-fw fa-3x" aria-hidden="true"></i>
				</a>
				
				<!-- Google+ -->
				<a href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank">
					<i class="fa fa-google-plus-square fa-fw fa-3x" aria-hidden="true"></i>
				</a>
				
				<!-- LinkedIn -->
				<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" target="_blank">
					<i class="fa fa-linkedin-square fa-fw fa-3x" aria-hidden="true"></i>
				</a>
				   

			</div>
	
		</div> 
		
	</section>


<?php include('footer.php'); ?>
<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/main.js"></script>
