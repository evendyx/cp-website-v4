<?php include_once 'header.php'; ?>
	
	<?php

		$sql1 = mysqli_query($conection,"select parallax from services_img");
		$row = mysqli_fetch_assoc($sql1);
		$parallax = $row['parallax'];
		
		if (($parallax != '')){
			
			echo "
		<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['parallax'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('SERVICES')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('SERVICES')."</span></p>
					</div>	
				</div>
		<!-- Services -->
		<section id='services' class='services'>
			<div class='container'>
				<div class='descriptions row text-center'>
				";
					showServicesText();
			echo"
				</div>
				<br/>
				<div class='row text-center'>
			";
					showAllServices();
			echo "
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>

			";
		}
		if (($parallax == '')){
			
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('SERVICES')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('SERVICES')."</span></p>
					</div>	
				</div>
		<!-- Services -->
		<section id='services' class='services'>			
			<div class='container'>
				<div class='descriptions row text-center'>
				";
					showServicesText();
			echo"
				</div>
				<br/>
				<div class='row text-center'>
			";
					showAllServices();
			echo "
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
			";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select team from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$team = $row['team'];
				
		if (($team == '1')){
			echo "
				<section id='team' class='team'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('TEAM')."</h2>
								<hr class='small'>
							</div>
							<div class='team'>
			";
						   showTeam();
			echo "
								</div>
							</div>
						</div>
				</section>

			";
		}
		if ($team == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select portfolio from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$portfolio = $row['portfolio'];
		
		$sql2 = mysqli_query($conection,"select type from portfolio_type");
		$row = mysqli_fetch_assoc($sql2);
		$type = $row['type'];
		
	
		if ($portfolio == '1'){
		
			if ($type == 'gallery'){
				echo "
					<!-- 4 Column Portfolio -->
					<section id='portfolio' class='filter-section'>
						<div class='container'>
						<div class='col-lg-12 text-center'>
								<h2>".lang('PORTFOLIO')."</h2>
								<hr class='small'>
							</div>
							<div class='row text-center'>
								<div class='col-sm-12 col-xs-12'>							
									<div class='filter-container isotopeFilters'>
										<ul class='list-inline filter'>
											<li class='active'><a href='#' data-filter='*'>".lang('ALL')."</a><span>/</span></li>
											";
											
											listCategoriesFront();
				echo "  
										</ul>
									</div>
									
								</div>
							</div>
						</div>
					</section>

					<section class='portfolio-section port-col'>
						<div class='container'>
							<div class='row'>
								<div class='isotopeContainer'>
								";
								
								showPortfolioGallery();
								
				echo "
								</div>
							</div>
						</div>
					</section>
					<!-- /4 Column Portfolio -->
				
				";
			}		
			if ($type == 'portfolio'){
				
				echo "
				<!-- 4 Column Portfolio -->
					<section id='portfolio' class='filter-section'>
						<div class='container'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PORTFOLIO')."</h2>
								<hr class='small'>
							</div>
							<div class='row text-center'>
								<div class='col-sm-12 col-xs-12'>							
									<div class='filter-container isotopeFilters'>
										<ul class='list-inline filter'>
											<li class='active'><a href='#' data-filter='*'>All </a><span>/</span></li>
											";
											
											listCategoriesFront();
				echo "  
										</ul>
									</div>
									
								</div>
							</div>
						</div>
					</section>

					<section class='portfolio-section port-col'>
						<div class='container'>
							<div class='row'>
								<div class='isotopeContainer'>
									<div class='content'>
										<div class='grid'>
								";
								
								showPortfolio();
								
				echo "
										</div>
										<!-- /grid -->
										<div class='preview'>
											<button class='action action--close'><i class='fa fa-times'></i><span class='text-hidden'>Close</span></button>
											<div class='description description--preview'></div>
										</div>
										<!-- /preview -->
									</div>
								</div>
							</div>
						</div>
					</section>
				
				";
			}
		}
		if ($portfolio == ''){
			echo " ";
		}
	?>
	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select pricing from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$pricing = $row['pricing'];
				
		if (($pricing == '1')){
			echo "

					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PRICING_TABLES')."</h2>
								<hr class='small'>
							</div>
							<div class='pricing'>
			";
						   showTables();
			echo "
							</div>
							<!-- /PRICING-TABLE -->
						</div>
					</div>


			";
		}
		if ($pricing == ''){
			echo " ";
		}
	?>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		$sql1 = mysqli_query($conection,"select image from counters_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($skills2 == '1')  && ($image != '')){
			echo "
				<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
					<!-- start count stats -->
					<section id='counter-stats' class='wow fadeInRight parallax' data-wow-duration='1.4s'>
						<div class='container'>
							<div class='row' id='counter'>

				";
							   showCounters();
				echo "

							</div>
							<!-- end row -->
						</div>
						<!-- end container -->

					</section>
					<!-- end cont stats -->
				</div>

			";
		}
		if (($skills2 == '1')  && ($image == '')){
			echo "
				<!-- start count stats -->
				<section id='counter-stats' class='wow fadeInRight' data-wow-duration='1.4s'>
					<div class='container'>
						<div class='row' id='counter'>
							
			";
						   showCounters();
			echo "

						</div>
						<!-- end row -->
					</div>
					<!-- end container -->

				</section>
				<!-- end cont stats -->
			";
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select testimonials from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$testimonials = $row['testimonials'];
				
		if ($testimonials == '1'){
			
			echo "
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<h2>".lang('TESTIMONIALS')."</h2>
						<hr class='small'>
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->
			";
		}
		if ($testimonials == ''){
			echo " ";
		}
	?>
	
		<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside id='subscribe' class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
		";
						
						if(isset($_POST))
						{
							$error = 0;
							if (isset($_POST['name']) && !empty($_POST['name'])) {
								$name=mysqli_real_escape_string($conection,trim($_POST['name']));
							}else{
								$error = 1;
							}
							if (isset($_POST['email']) && !empty($_POST['email'])) {
								$email=mysqli_real_escape_string($conection,trim($_POST['email']));
							}else{
								$error = 1;
							}
							
							if(!$error) {
							function valid_email($email) {
									return !!filter_var($email, FILTER_VALIDATE_EMAIL);
								}
								
								if(!empty($_POST)){ 
								$email = $_POST['email'];
								}
								else{
									$email = "";
								}
								if( valid_email($email) ) {
								} else {
									$error = 1;
								}	

								if(!valid_email($email) ) {
									echo "<div class='alert alert-danger fade in'>
													<a href='#' class='close' data-dismiss='alert'>&times;</a>
													<strong>Error!</strong> ".lang('NOT_EMAIL_FORMAT')."
												</div>";
											echo "<script>top.location.href='index.php#subscribe';</script>";
									}	
							}
						

							if(!$error) {
								$sql="select * from clients where (email='$email');";
								$res=mysqli_query($conection,$sql);
								if (mysqli_num_rows($res) > 0) {
								// output data of each row
								$row = mysqli_fetch_assoc($res);
								
								
								if($email==$row['email'])
								{
									echo "<div class='alert alert-danger fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Error!</strong> ".lang('EMAIL_ALREADY_EXISTS')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";
								}
								}else{ 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES ('', '".$name."','".$email."')");
									echo "<div class='alert alert-success fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Success!</strong> ".lang('YOU_ARE_NOW_REGISTERED')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";								
								}
								
							}
						}
		echo "							
						</div>
					</div>
						<div class='input-group clients'>						
							<form action='index.php' method='post' enctype='multipart/form-data'> 
							";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text'  name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>

					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>
	
<?php include_once 'footer.php'; ?>

<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/main.js"></script>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activeservices");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		if ($skills2 == '1'){
			?>
			
				<script src="assets/js/jquery.counterup.js"></script> 
				<script>
				// number count for stats, using jQuery animate
				var a = 0;
				$(window).scroll(function() {

				  var oTop = $('#counter').offset().top - window.innerHeight;
				  if (a == 0 && $(window).scrollTop() > oTop) {
					$('.counter-value').each(function() {
					  var $this = $(this),
						countTo = $this.attr('data-count');
					  $({
						countNum: $this.text()
					  }).animate({
						  countNum: countTo
						},

						{

						  duration: 4000,
						  easing: 'swing',
						  step: function() {
							$this.text(Math.floor(this.countNum));
						  },
						  complete: function() {
							$this.text(this.countNum);
							//alert('finished');
						  }

						});
					});
					a = 1;
				  }

				});
			</script>
		<?php
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>