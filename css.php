
	<?php showCss(); ?>

	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	$font = $row['font'];
	
	if ($font != ""){
		echo "
		body {font-family: '".$row['font']."' !important;}
		.text-vertical-center h1 {color: ".$row['menu_color'].";}
		.text-vertical-center h3 {color: ".$row['menu_color'].";}
		#breadcrumbs {
			background-color:".$row['lcolor'].";
		}
		.footer-bs {
			background-color:".$row['footer_color'].";
		}
			
		/* Tablet Landscape */
		@media only screen and (min-width: 958px) and (max-width: 1079px) {

		}

		@media only screen and (min-width: 958px) and (max-width: 1024px) {
			.services{
				padding-top: 8%;
				padding-bottom: 3%;
			}
		}

		/* Tablet Portrait size to standard 960 (devices and browsers) */
		@media only screen and (min-width: 768px) and (max-width: 959px) {
			.services {
				padding: 40px 0;
			}

		}

		/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
		@media only screen and (min-width: 480px) and (max-width: 767px) {
			.Modern-Slider {
				margin-top: 120px; !important;
			}
			.Modern-Slider .item .img-fill {
				height: 275px !important;
			}
			.Modern-Slider .item h3 {
				font: 22px/22px '".$row['font']."' !important;
				padding-bottom: 10px !important;
			}
			.Modern-Slider .item h5 {
				font: 15px/15px '".$row['font']."' !important;
				height: 135px !important;
				text-transform: initial !important;
			}
			.Modern-Slider .item .img-fill .info {
				line-height: 94vh !important;
				padding: 0 8%;
			}
		}
		
		@media only screen and (max-width: 479px) {
			.Modern-Slider .item h3 {
				font: 22px/22px '".$row['font']."' !important;
				padding-bottom: 10px !important;
			}
			.Modern-Slider .item h5 {
				font: 15px/15px '".$row['font']."' !important;
				height: 135px !important;
				text-transform: initial !important;
			}
			.Modern-Slider .item .img-fill .info {
				line-height: 60vh !important;
				padding: 0 15%;
			}
			.Modern-Slider {
				margin-top: 130px !important;
			}
		}		
		";
	}else{
		echo " ";
	}
	?>
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	$active = $row['id'];
	
	if ($active == 1){
		echo "
			.countdown-section {
				border-radius: ".$row['bradius']."% ".$row['bradius']."%;
				-webkit-border-radius: ".$row['bradius']."% ".$row['bradius']."%;
				-moz-border-radius: ".$row['bradius']."% ".$row['bradius']."%;
				border:solid ".$row['border']."px ".$row['bcolor'].";				
			}
			.countdown-period {
				color:".$row['wcolor'].";
				font-size:".$row['wsize']."px;
				margin-top: -".$row['wsize']."px;
			}
			#defaultCountdown{
				color: ".$row['dcolor'].";
				
			}";
	}else{
		echo " ";
	}
	?>
	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from services WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	$number = $row['number'];
	
	if ($number == 2){
		echo "
			.middle {
				top: 70%;
				left: 80%;
			}
			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
				.middle {
					top: 63% !important;
					left: 77% !important;
				}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
				.middle {
					top: 55% !important;
					left: 70% !important;
				}
			}
			}";
	}
	if ($number == 3){
		echo "
			.middle {
				top: 55%;
				left: 70%;
			}
			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
				.middle {
					top: 51% !important;
					left: 66% !important;
				}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
				.middle {
					top: 39% !important;
					left: 57% !important;
				}
			}

			}";
	}
	if ($number == 4){
		echo "
			.middle {
				top: 44%;
				left: 62%;
			}
			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
				.middle {
					top: 37% !important;
					left: 55% !important;
				}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
				.middle {
					top: 27% !important;
					left: 43% !important;
				}
			}

			/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
			@media only screen and (min-width: 480px) and (max-width: 767px) {
				.middle {
					top: 75% !important;
					left: 81% !important;
				}
			}
			
			@media only screen and (max-width: 479px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
			}	
			";
	}
	if ($number == 6){
		echo "
			.middle {
				top: 28%;
				left: 45%;
			}			
						
			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
				.middle {
					top: 18% !important;
					left: 33% !important;
				}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
				.middle {
					top: 93% !important;
					left: 23% !important;
				}
				h4 strong {
					font-size: 16px;
					letter-spacing: 0px;
				}
				.service-item p {
					font-size: 12px;
				}
			}
			
			/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
			@media only screen and (min-width: 480px) and (max-width: 767px) {
				.middle {
					top: 75% !important;
					left: 81% !important;
				}
			}
			
			@media only screen and (max-width: 479px) {
				.middle {
					top: 56% !important;
					left: 67% !important;
				}
				.text_s a{
					font-size: 12px;
				}
			}";
	}else{
		echo " ";
	}
	?>	
	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select type from background");
	$row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
	if ($type == 'particles'){
		echo "
			#defaultCountdown{
				padding-top: 15% !important;
			}
			.text-vertical-center {
				background-color: transparent;				
			}
			
			";
	}else{
		echo " ";
	}
	?>	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select * from settings");
		$row = mysqli_fetch_assoc($sql);
		$about = $row['about'];
		$contacts = $row['contacts'];
		$services = $row['services'];
		$partners = $row['partners'];
		$portfolio = $row['portfolio'];
		$blog = $row['blog'];
		
		if ($about == '1' || $contacts == '1' || $services == '1' || $partners == '1' || $portfolio == '1' || $blog == '1') {
			echo " ";
		}
		else {
			echo "
			#menu-toggle {
				display: none;
			}";
		}	
	?>	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from progress WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	$active = $row['id'];
	
	if ($active == 1){
		echo "
			.progress {
				height: ".$row['height']."px;
			}
			.progress-bar{
				line-height:".$row['height']."px;
				background-color: ".$row['color'].";
			}";
	}else{
		echo " ";
	}
	?>
	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);

		echo "	
			.member-details > div:after {
				background-image: linear-gradient(45deg, ".$row['background_color']." 50%, transparent 50%);
			}
			.portfolio-section figure .inner-overlay-content.with-icons a{
				background-color: ".$row['background_color'].";
			}
			.action:hover, .action:focus {
				color: ".$row['background_menu_color'].";
			}
			.parallax-window {
				background-color: rgba(0, 0, 0, 0.6);
			}
			#sidebar-wrapper {
				background: ".$row['background_menu_color'].";
			}
			.sidebar-nav li a {
				color: ".$row['items_color'].";
			}
			.bg-primary {
				color: ".$row['background_tcolor'].";
				background-color: ".$row['background_color'].";
			}
			.btn-dark {
				color: ".$row['menu_color'].";
				background-color: ".$row['menu_bcolor'].";
			}
			.btn-dark:hover, .btn-dark:focus, .btn-dark:active {
				color: ".$row['menu_colorhover'].";
				background-color: ".$row['menu_bcolorhover'].";
			}
			.btn-light{
				color: ".$row['menu_color'].";
				background-color: ".$row['menu_bcolor'].";
			}
			.btn-light:hover, .btn-light:focus, .btn-light:active {
				color: ".$row['menu_colorhover'].";
				background-color: ".$row['menu_bcolorhover'].";
			}
			.text-primary {
				color: ".$row['background_color'].";
			}
			footer a {
				color: ".$row['background_color'].";
			}
			footer a:focus, a:hover {
				color: ".$row['menu_bcolorhover'].";
				text-decoration: none;
			}
			.testimonial-name {
				background: ".$row['background_color'].";
			}
			.btn-primary {
				color: #fff;
				background-color: ".$row['menu_bcolorhover'].";
				border-color:  ".$row['menu_bcolorhover'].";
			}
			.btn-primary.disabled:hover{
				color: #fff;
				background-color: ".$row['menu_bcolor'].";
				border-color:  ".$row['menu_bcolorhover'].";
			}
			.btn-primary:hover {
				color: #fff;
				background-color: ".$row['menu_bcolor'].";
				border-color:  ".$row['menu_bcolorhover'].";
			}
			.btn-primary:active:focus, .btn-primary:active:hover{
				color: #fff;
				background-color: ".$row['menu_bcolor'].";
				border-color:  ".$row['menu_bcolorhover'].";
			}			
			input:-webkit-autofill {
				-webkit-box-shadow: 0 0 0 1000px ".$row['background_color']." inset !important;
				-webkit-text-fill-color:".$row['background_tcolor'].";
			}			
			.blog .post-bg:hover .hover-post {
				background-color: ".$row['background_color'].";
			}
			ul.filter > li > a {
				color: ".$row['menu_bcolor'].";
			}
			ul.filter > li > a:hover, ul.filter > li > a:focus {
				color: ".$row['background_color'].";
			}
			.blog .post-bg:hover h3 {
				color: ".$row['menu_bcolor'].";
			}
			.input__label--hoshi-color-1::after {
				border-color: ".$row['background_color'].";
			}
			#breadcrumbs a {
				color: ".$row['menu_bcolor'].";
			}
			.portfolio-section figure .overlay-background .inner {
				background-color: ".$row['background_color']."; 
			}
			.hover-post {
				background-color: ".$row['background_color'].";
			}
			.Modern-Slider .slick-dots li {
				background-color: ".$row['background_color'].";
			}
			.Modern-Slider .item h3 {
				font: 50px/45px '".$row['font']."';
				text-transform: uppercase;
				max-width: 800px;
			}
			.Modern-Slider .item h5 {
				font: 15px/20px '".$row['font']."';
				text-transform: uppercase;
				height: 80px;
				max-width: 800px;
			}
			.pricing-74 .pricing-panel-4 .btn:hover {
				background-color: ".$row['background_color'].";
				border-color: ".$row['background_color'].";
			}
			.pricing-74 .pricing-panel-4 .pricing--body .pricing--list li i {
				color: ".$row['background_color'].";
			}
			.text_s {
				background-color: ".$row['background_color'].";
			}
			.text_s a{
				color: #fff;
			}
			.object{
				background-color: ".$row['lcolor'].";
			}
			.sk-chasing-dots .sk-child {
				background-color: ".$row['lcolor'].";
			}
			.sk-rotating-plane {
				background-color: ".$row['lcolor'].";
			}
			.sk-double-bounce .sk-child {
				background-color: ".$row['lcolor'].";
			}
			.sk-wave .sk-rect {
				background-color: ".$row['lcolor'].";
			}
			.sk-wandering-cubes .sk-cube {
				background-color: ".$row['lcolor'].";
			}
			.sk-spinner-pulse {
				background-color: ".$row['lcolor'].";
			}
			.sk-three-bounce .sk-child {
				background-color: ".$row['lcolor'].";
			}
			.sk-circle .sk-child:before {
				background-color: ".$row['lcolor'].";
			}
			.sk-cube-grid .sk-cube {
				background-color: ".$row['lcolor'].";
			}
			.sk-fading-circle .sk-circle:before {
				background-color: ".$row['lcolor'].";
			}
			.sk-folding-cube .sk-cube:before {
				background-color: ".$row['lcolor'].";
			}
			";

	?>
	
		
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select parallax from services_img");
	$row = mysqli_fetch_assoc($sql1);
	$parallax = $row['parallax'];
	if ($parallax != ''){
	echo "
			
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from counters_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "			
			.stats{
			color: #fff;
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from contactform_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "
			.contact {
			background: none !important;
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from about_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "
			section#aboutfront {
				color: #fff;
			}
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from partners_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "
			#partners {
			background: none !important;
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from blog_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "
			.blog h2 {
			color: #fff;
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql1 = mysqli_query($conection,"select image from testimonials_img");
	$row = mysqli_fetch_assoc($sql1);
	$image = $row['image'];
	if ($image != ''){
	echo "
			.testimonials h2 {
			color: #fff;
		}
	";
	}
	?>
	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from background");
	$row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
	$sql1 = mysqli_query($conection,"select type from slider_type");
	$row = mysqli_fetch_assoc($sql1);
	$type1 = $row['type'];
	
	if ($type == 'static'){
		echo "
			.header {
				background: url(assets/img/uploads/other/"?><?php
				global $conection;
				$sql = mysqli_query($conection,"select * from static");
				$row = mysqli_fetch_assoc($sql);
				echo $row['image'];
				?><?php echo ") no-repeat center center scroll;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
				-o-background-size: cover;
			}";
	}
	if ($type == 'slide'){
		echo "
			.header {
				background: none;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
				-o-background-size: cover;
			}
			#resizeSlider {
				position: absolute !important;
			}
			#home h1, #home h3 {
				color: #fff;
			}
		";
		if ($type1 == 'boxed'){
			echo ".Modern-Slider {
				margin-top: 100px;
				margin-bottom: 4px;
			}
			.Modern-Slider .item .img-fill {
				height: 400px;
			}
			.Modern-Slider .item .img-fill .info {
				line-height: 50vh;
			}";
		}
		if ($type1 == 'fullwidth'){			
			echo".Modern-Slider {
				margin-top: 60px;
			}
			.Modern-Slider .item .img-fill {
				height: 500px;
			}
			.Modern-Slider .item .img-fill .info {
				line-height: 60vh;
			}";
		}
	}	
	if ($type == 'own'){
		echo "
			.header{
				background: none;
				position: absolute;
				top: 0;
			}
			#resizeSlider {
				position: absolute !important;
			}";
	}
	if ($type == 'youtube'){
		echo "
			.header {
				background: none;
				position: relative;
				top: 0;
				z-index: 9;
			}
			#resizeSlider {
				position: absolute !important;
			}";
	}
	if ($type == 'particles'){
		echo "
			.text-vertical-center {
				display: block;
				text-align: center;
				vertical-align: middle;
				margin-top: -35%;
				height: 65%;
			}
			#top{
				height:0;
			}
			#defaultCountdown {
				padding-top: 0% !important;
			}
			";?><?php
				global $conection;
				$sql = mysqli_query($conection,"select * from particles");
				$row = mysqli_fetch_assoc($sql);
				echo "#particles {
				width: 100%;
				height: 100%;
				overflow: hidden;
				z-index: 9;
				background: ".$row['bpcolor'].";
				background-image: -moz-linear-gradient(45deg, ".$row['bpcolor']." 2%, ".$row['dpcolor']." 100%);
				background-image: -webkit-linear-gradient(45deg, ".$row['bpcolor']." 2%, ".$row['dpcolor']." 100%);
				background-image: linear-gradient(45deg, ".$row['bpcolor']." 2%, ".$row['dpcolor']." 100%);
			}
			
			@media screen and (device-aspect-ratio: 4/3) { 
			.text-vertical-center {
				margin-top: -50%;
			}
			}

			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
			.text-vertical-center {
				margin-top: -55% !important;
			}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
			.text-vertical-center {
				margin-top: -55% !important;
			}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
			.text-vertical-center {
				margin-top: -90% !important;
			}
			.services{
				padding-top: 5%;
				padding-bottom: 3%;
			}
			}

			/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
			@media only screen and (min-width: 480px) and (max-width: 767px) {
			.text-vertical-center {
				margin-top: -55% !important;
			}
			.services{
				padding-top: 20%;
				padding-bottom: 5%;
			}
			.text-vertical-center h1 {
				margin-top: 15%;
				font-size: 20px;
			}
			.text-vertical-center h3 {
				font-size: 15px;
			}
			.countdown-section {
				width:80px;
				height:80px;
			}
			}
			@media only screen and (max-width: 479px) {
			.text-vertical-center {
				margin-top: -155% !important;
			}
			.services{
				padding-top: 50%;
				padding-bottom: 10%;
			}
			}
			
			@media only screen and (max-width: 414px) {
			.text-vertical-center {
				margin-top: -165% !important;
			}
			.services{
				padding-top: 60%;
				padding-bottom: 10%;
			}			
			}
			
			@media only screen and (max-width: 360px) {
			.text-vertical-center {
				margin-top: -177% !important;
			}
			.services{
				padding-top: 54%;
				padding-bottom: 10%;
			}
			}
		";
		
	}
	?>
	
	<?php
	//Detect special conditions devices
	$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
	$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
	$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

	//do something with this information
	if( $iPod || $iPhone ){
		//browser reported as an iPhone/iPod touch -- do something here
	}else if($iPad){
		
		global $conection;
			$sql = mysqli_query($conection,"select image from static");
			$row = mysqli_fetch_assoc($sql);
	
		echo "
			.header {
				background: url(assets/img/uploads/other/"?><?php
				global $conection;
				$sql = mysqli_query($conection,"select * from static");
				$row = mysqli_fetch_assoc($sql);
				echo $row['image'];
				?><?php echo ") no-repeat center center scroll;
				-webkit-background-size: cover;
				-moz-background-size: cover;
				background-size: cover;
				-o-background-size: cover;
				position: relative;
				top: 0;
			}
			#map {
				position: relative;
				height: 400px;
			}
			section.slider.clearfix {
				display: none;
			}
			";
	
		
	}else if($Android){
		//browser reported as an Android device -- do something here
	}else if($webOS){
	   
	}else{
		 echo"

		";
	}

	?> 
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select * from menu_type");
		$row = mysqli_fetch_assoc($sql);
		$type = $row['type'];
		
		$sql2 = mysqli_query($conection,"select type from slider_type");
		$row = mysqli_fetch_assoc($sql2);
		$type2 = $row['type'];
		
		if ($type == 'fixed'){
				echo"
				.Modern-Slider {
					margin-top: 0px;
				}
				";
				if ($type2 == 'boxed'){
					echo"
					.Modern-Slider {
						margin-top: 100px;
					}
					";
				}
			}
		
		if ($type == 'topbar'){
			echo "
				nav.navbar.navbar-custom.navbar-fixed-top {
					display: none;
				}

				#sidebar-wrapper {
					display:none;
				}

				#sidebar-wrapper.active {
					display:none;
				}
				.navbar{
					display:none;
				}
				#menu-toggle {
					display:none;
				}	
				
			";
			if ($type2 == 'boxed'){
					echo"
					.Modern-Slider {
						margin-top: 120px;
					}
					";
				}
		}
		if ($type == 'left'){
			echo "
				#menu-toggle {
				z-index: 10;
				position: fixed;
				top: 0;
				left: 0 !important;
			}

				#sidebar-wrapper {
				left: 0px;
				width: 250px;
				transform: translateX(-250px);

			}
			.toggle {
				margin: 5px 0 0 5px;
			}
			.pull-right {
				float: left!important;
			}
			#sidebar-wrapper.active {
				left: 250px !important;
			}
			.navbar{
				display:none;
			}
			header.cd-auto-hide-header {
				display: none;
			}
			.Modern-Slider {
				margin-top: 0px;
			}
			";
			if ($type2 == 'boxed'){
				echo"
				.Modern-Slider {
					margin-top: 100px;
				}
				";
			}
		}
		if ($type == 'right'){
			echo "
				#menu-toggle {
				z-index: 10;
				position: fixed;
				top: 0;
				right: 0 !important;
			}
			.navbar{
				display:none;
			}
			header.cd-auto-hide-header {
				display: none;
			}
			.Modern-Slider {
				margin-top: 0px;
			}
			";
			if ($type2 == 'boxed'){
				echo"
				.Modern-Slider {
					margin-top: 100px;
				}
				";
			}			
		}
		
		$sql1 = mysqli_query($conection,"select * from dashboard WHERE id='1'");
		$row = mysqli_fetch_assoc($sql1);
		
		if ($type == 'topbar'){
			echo "
				.cd-primary-nav {
					background: ".$row['background_color']." !important;
				}
				.cd-secondary-nav {
					background-color: ".$row['background_menu_color']." !important;
				}
				.navbar-toggle {
					background-color: ".$row['background_color']." !important;
				}
				.navbar-toggle .icon-bar {
					background-color: ".$row['background_menu_color']." !important;
				}
				/* Tablet Landscape */
				@media only screen and (min-width: 1080px) {
					.navbar-brand img {
						margin-top: 0px;
					}
					.navbar-nav>li>a {
						padding-top: 10px;
						padding-bottom: 10px;
						line-height: 35px !important;
					}
					.cd-secondary-nav .nav>li>a {
						position: relative;
						display: block;
						padding: 10px 10px !important;
					}
				}
				
				/* Tablet Landscape */
				@media only screen and (min-width: 958px) and (max-width: 1079px) {
					.navbar-nav>li>a {
						line-height: 0px !important;
					}

				}

				@media only screen and (min-width: 958px) and (max-width: 1024px) {
					.navbar-brand {
						float: initial;
					}
					.inner {
						padding-top: 20%;
					}	
					.cd-secondary-nav {
						height: 55px;
					}
				}

				/* Tablet Portrait size to standard 960 (devices and browsers) */
				@media only screen and (min-width: 768px) and (max-width: 959px) {
					.inner {
						padding-top: 20%;
					}

				}

				/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
				@media only screen and (min-width: 480px) and (max-width: 767px) {
					.navbar-brand {
						float: initial;
					}
					.inner {
						padding-top: 30%;
					}
					.cd-secondary-nav {
						height: 75px;
					}
					.navbar-brand img {
						margin-top: -5px;
					}
				}
				@media only screen and (max-width: 479px) {
					.inner {
						padding-top: 50%;
					}
					.cd-secondary-nav {
						height: 100%;
					}
				}
				";
		}
		if ($type == 'fixed'){
			echo "
			.navbar.navbar-custom.navbar-fixed-top.top-nav-collapse{
				background: ".$row['background_menu_color']." !important;
			}
			.in{
				background: ".$row['background_menu_color']." !important;
			}
				.action--close {
				top: 35px;
			}
				#menu-toggle {
				display: none;
			}
			header.cd-auto-hide-header {
				display: none;
			}
			.navbar-nav {
					float: left;
					margin: 10px 0 !important;
				}
			.navbar-custom.top-nav-collapse {
				padding: 0;
				background: ".$row['background_menu_color']." !important;
				border-bottom: 1px solid rgba(255, 255, 255, 0.3);
				box-shadow: 0 0 50px 0 rgba(0,0,0,0.1);
			}
			
			/* Tablet Landscape */
			@media only screen and (min-width: 958px) and (max-width: 1079px) {
				#portfolio, #partners, .about, .services, .testimonials, .contact {
					padding: 60px 0;
				}
			}

			@media only screen and (min-width: 958px) and (max-width: 1024px) {
				#portfolio, #partners, .about, .services, .testimonials, .contact {
					padding: 20px 0;
				}
			}

			/* Tablet Portrait size to standard 960 (devices and browsers) */
			@media only screen and (min-width: 768px) and (max-width: 959px) {
				#portfolio, #partners, .about, .services, .testimonials, .contact {
					padding: 0;
				}
				
			}

			/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */
			@media only screen and (min-width: 480px) and (max-width: 767px) {
				#portfolio, #partners, .about, .services, .testimonials, .contact {
					padding: 0;
				}
				.navbar-fixed-top{
					background: ".$row['background_menu_color']." !important;
				}
				.navbar-brand img {
					margin-top: -40px;
				}
			}
			@media only screen and (max-width: 479px) {
				#portfolio, #partners, .about, .services, .testimonials, .contact {
					padding: 20px 0;
				}
				.navbar-fixed-top{
					background: ".$row['background_menu_color']." !important;
				}
			}
			";
			
		}

	?>