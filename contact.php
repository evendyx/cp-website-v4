<?php
require 'admin/functions/functions.php';
global $conection;
	$sql = mysqli_query($conection,"select * from contact_form WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);

// configure
$from = $row['mailfrom']. '<'.$row['sendyouremail'].'>';
$sendTo = $row['sendto']. '<'.$row['sendyouremail'].'>';
$subject = $row['subject'];
$fields = array('name' => $row['firstname'], 'surname' => $row['lastname'], 'phone' => $row['phone'], 'email' => $row['email'], 'message' => $row['message']); // array variable name => Text to appear in the email
$okMessage = $row['okmessage'];
$errorMessage = $row['errormessage'];

// let's do the sending

try
{
    $emailText = $row['emailtext']."\n=============================\n";

    foreach ($_POST as $key => $value) {

        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }

    $headers = array('Content-Type: text/plain; charset="UTF-8";',
        'From: ' . $from,
        'Reply-To: ' . $from,
        'Return-Path: ' . $from,
    );
    
    mail($sendTo, $subject, $emailText, implode("\n", $headers));

    $responseArray = array('type' => 'success', 'message' => $okMessage);
}
catch (\Exception $e)
{
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
}

if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
else {
    echo $responseArray['message'];
}
