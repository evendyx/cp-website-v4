<?php include_once 'header.php'; ?>

		<div class="wrapper">
			<div id="error">
				<h2>Error 404 - Page Not Found</h2>
				<h3>Sorry, but this page doesn't exist, or your search might lead you into this error page.</h3>
				<img src="assets/img/error.jpg" alt="Error 404 - Page Not Found" />
			</div>
		</div> 
		
	</section>


<?php include('footer.php'); ?>
