	<?php include_once 'header.php'; ?>

	<?php
		global $conection;		
		$sql1 = mysqli_query($conection,"select image from blog_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		$sql = mysqli_query($conection,"select type from blog_type");
		$row = mysqli_fetch_assoc($sql);
		$type = $row['type'];
		
		if ($image != ''){
			if  ($type == 'No Pagination'){
				
				global $conection;		
				$sql1 = mysqli_query($conection,"select image from blog_img");
				$row = mysqli_fetch_assoc($sql1);
				$image = $row['image'];
			
				echo "
					<div id='page_title' class='text-center'>
						<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
							<div class='container inner parallax'>
								<h1>".lang('BLOG')."</h1>
							</div>	
						</div>
					</div>

					<div id='breadcrumbs'>
						<div class='container'>
							<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('BLOG')."</span></p>
						</div>	
					</div>

				 <!-- About -->
				 <section class='blog'>
				  <div class='container'>
				  <div class='descriptions row text-center'>
							";
								showBlogText();
						echo"
							</div>
					  <div class='row-page row'>
				";

								showBlog();
				echo "
					</div>
				  </div>
				</section>
				";
			}
		}
		if ($image == ''){
			
			if($type == 'No Pagination') {
			
				echo "
					<div id='page_title' class='text-center'>
						<div class='container inner parallax'>
							<h1>".lang('BLOG')."</h1>
						</div>	
					</div>

					<div id='breadcrumbs'>
						<div class='container'>
							<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('BLOG')."</span></p>
						</div>	
					</div>
				 <!-- About -->
				<section class='blog'>
				  <div class='container'>
				  <div class='descriptions row text-center'>
							";
								showBlogText();
						echo"
							</div>
					  <div class='row-page row'>		  
				";

								showBlog();
				echo "
					</div>
				  </div>
				</section>
				";
			}
		}
		if ($image == ''){
			if ($type == 'Pagination'){
			
		?>
		
		<?php

        global $conection;
		 
		$sql2 = mysqli_query($conection,"select number from blog_type");
		$row = mysqli_fetch_assoc($sql2);
		$number = $row['number'];
		 
        $rec_limit = $number;
         
         /* Get total number of records */
         $sql2 = "SELECT count(id) FROM post ";
         $retval = mysqli_query($conection,$sql2);
         
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error($conection));
         }
         $row = mysqli_fetch_array($retval, MYSQL_NUM );
         $rec_count = $row[0];
         
         if( isset($_GET{'page'} ) ) {
            $page = $_GET{'page'} + 1;
            $offset = $rec_limit * $page ;
         }else {
            $page = 0;
            $offset = 0;
         }
         
         $left_rec = $rec_count - ($page * $rec_limit);
         $sql2 = "SELECT * ". 
            "FROM post ORDER BY id DESC ".
            "LIMIT $offset, $rec_limit";
            
         $retval = mysqli_query( $conection, $sql2 );
         
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error($conection));
         }	
		echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('BLOG')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('BLOG')."</span></p>
					</div>	
				</div>
			 <!-- About -->
			<section class='blog'>
			  <div class='container'>
			  <div class='descriptions row text-center'>
						";
							showBlogText();
					echo"
						</div>
				  <div class='row-page row'>		  
			";
         
         while($row = mysqli_fetch_array($retval)) {
			$excerpt = getExcerpt(htmlspecialchars_decode($row['description']), 0, 100);
            echo "			
				<div class='col-xs-12 col-sm-6 col-md-3'>
				  <a href='post?id=". $row['id'] . "' class='post-bg'>
					<div class='post-img'>
					  <img class='img-responsive' src='assets/img/uploads/other/".$row['image']."' alt='".$row['title']."' />
					</div>
					<div class='excerpt'>
					  <h3>".$row['title']."</h3>
					  <p>".$excerpt."</p>
					</div>
					<div class='hover-post'>
					 <span>".lang('SEE_MORE')."</span>
					</div>
				  </a>
				</div>			
			";
         }
		echo "
				</div>
			  </div>
			</section>
			";
	?>
		 
	<div id="pagination" class="container">
		<?php
			 if( $page > 0 ) {
				$last = $page - 2;
				echo "<a href = \"blog.php?page=$page\"><< ".lang('OLDER')."</a> | ";
				echo "<a href = \"blog.php?page=$last\">".lang('RECENT')." >></a>";            
			 }else if( $page == 0 ) {
				echo "<a href = \"blog.php?page=$page\"><< ".lang('OLDER')."</a>";
			 }else if( $left_rec < $rec_limit ) {
				$last = $page - 2;
				echo "<a href = \"blog.php?page=$last\">".lang('RECENT')." >></a>";
			 }
		?>
	</div>
		
		<?php
			}
			
		}
		if ($image != ''){
				if($type == 'Pagination'){
			
		?>
		
		<?php

        global $conection;
		 
		$sql3 = mysqli_query($conection,"select number from blog_type");
		$row = mysqli_fetch_assoc($sql3);
		$number = $row['number'];
		 
         $rec_limit = $number;
         
         /* Get total number of records */
         $sql3 = "SELECT count(id) FROM post ";
         $retval = mysqli_query($conection,$sql3);
         
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error($conection));
         }
         $row = mysqli_fetch_array($retval, MYSQL_NUM );
         $rec_count = $row[0];
         
         if( isset($_GET{'page'} ) ) {
            $page = $_GET{'page'} + 1;
            $offset = $rec_limit * $page ;
         }else {
            $page = 0;
            $offset = 0;
         }
         
         $left_rec = $rec_count - ($page * $rec_limit);
         $sql3 = "SELECT * ". 
            "FROM post ORDER BY id DESC ".
            "LIMIT $offset, $rec_limit";
            
         $retval = mysqli_query( $conection, $sql3 );
         
         if(! $retval ) {
            die('Could not get data: ' . mysqli_error($conection));
         }
		 
		global $conection;		
		$sql1 = mysqli_query($conection,"select image from blog_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		echo "
		<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('BLOG')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('BLOG')."</span></p>
					</div>	
				</div>

			 <!-- About -->
			 <section class='blog'>
			  <div class='container'>
			  <div class='descriptions row text-center'>
						";
							showBlogText();
					echo"
						</div>
				  <div class='row-page row'>
			";	 
         
         while($row = mysqli_fetch_array($retval)) {
			$excerpt = getExcerpt(htmlspecialchars_decode($row['description']), 0, 100);
            echo "			
				<div class='col-xs-12 col-sm-6 col-md-3'>
				  <a href='post?id=". $row['id'] . "' class='post-bg'>
					<div class='post-img'>
					  <img class='img-responsive' src='assets/img/uploads/other/".$row['image']."' alt='".$row['title']."' />
					</div>
					<div class='excerpt'>
					  <h3>".$row['title']."</h3>
					  <p>".$excerpt."</p>
					</div>
					<div class='hover-post'>
					 <span>".lang('SEE_MORE')."</span>
					</div>
				  </a>
				</div>			
			";
         }
		echo "
				</div>
			  </div>
			</section>
			";
	?>
		 
	<div id="pagination" class="container">
		<?php
			 if( $page > 0 ) {
				$last = $page - 2;
				echo "<a href = \"blog.php?page=$page\"><< ".lang('OLDER')."</a> | ";
				echo "<a href = \"blog.php?page=$last\">".lang('RECENT')." >></a>";            
			 }else if( $page == 0 ) {
				echo "<a href = \"blog.php?page=$page\"><< ".lang('OLDER')."</a>";
			 }else if( $left_rec < $rec_limit ) {
				$last = $page - 2;
				echo "<a href = \"blog.php?page=$last\">".lang('RECENT')." >></a>";
			 }
		?>
	</div>
		
		<?php
				}
			
		}
		if ($about == ''){
			echo " ";
		}
		
	?>
	
	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activeblog");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	  
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activeblog");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside id='subscribe' class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
		";
						
						if(isset($_POST))
						{
							$error = 0;
							if (isset($_POST['name']) && !empty($_POST['name'])) {
								$name=mysqli_real_escape_string($conection,trim($_POST['name']));
							}else{
								$error = 1;
							}
							if (isset($_POST['email']) && !empty($_POST['email'])) {
								$email=mysqli_real_escape_string($conection,trim($_POST['email']));
							}else{
								$error = 1;
							}
							
							if(!$error) {
							function valid_email($email) {
									return !!filter_var($email, FILTER_VALIDATE_EMAIL);
								}
								
								if(!empty($_POST)){ 
								$email = $_POST['email'];
								}
								else{
									$email = "";
								}
								if( valid_email($email) ) {
								} else {
									$error = 1;
								}	

								if(!valid_email($email) ) {
									echo "<div class='alert alert-danger fade in'>
													<a href='#' class='close' data-dismiss='alert'>&times;</a>
													<strong>Error!</strong> ".lang('NOT_EMAIL_FORMAT')."
												</div>";
											echo "<script>top.location.href='index.php#subscribe';</script>";
									}	
							}
						

							if(!$error) {
								$sql="select * from clients where (email='$email');";
								$res=mysqli_query($conection,$sql);
								if (mysqli_num_rows($res) > 0) {
								// output data of each row
								$row = mysqli_fetch_assoc($res);
								
								
								if($email==$row['email'])
								{
									echo "<div class='alert alert-danger fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Error!</strong> ".lang('EMAIL_ALREADY_EXISTS')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";
								}
								}else{ 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES ('', '".$name."','".$email."')");
									echo "<div class='alert alert-success fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Success!</strong> ".lang('YOU_ARE_NOW_REGISTERED')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";								
								}
								
							}
						}
		echo "							
						</div>
					</div>
						<div class='input-group clients'>						
							<form action='index.php' method='post' enctype='multipart/form-data'>
							";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text'  name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>

					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>
	
<?php include_once 'footer.php'; ?>
<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/main.js"></script>