<?php include_once 'header.php'; ?>	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select testimonials from active");
		$row = mysqli_fetch_assoc($sql);
		$testimonials = $row['testimonials'];
		
		$sql1 = mysqli_query($conection,"select image from testimonials_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($testimonials == '1')  && ($image != '')){
			
			echo "
			<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner'>
							<h1>".lang('TESTIMONIALS')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('TESTIMONIALS')."</span></p>
					</div>	
				</div>
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->

			";
		}
		if (($testimonials == '1')  && ($image == '')){
			
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner'>
						<h1>".lang('TESTIMONIALS')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('TESTIMONIALS')."</span></p>
					</div>	
				</div>
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->
			";
		}
		if ($testimonials == ''){
			echo " ";
		}
	?>

<?php include_once 'footer.php'; ?>