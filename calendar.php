<?php include_once 'header.php'; ?>

	<?php
		global $conection;		
		$sql1 = mysqli_query($conection,"select image from calendar_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if ($image != ''){
			
			echo "
				<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('CALENDAR')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CALENDAR')."</span></p>
					</div>	
				</div>
				

			<!-- Calendar -->
			<section id='calendar' class='calendar'>
				<div class='container'>
					<div id='events'></div>
				</div>
				<!-- /.container -->
			</section>
			";
		}
		if ($image == ''){
			
			echo "
				<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('CALENDAR')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CALENDAR')."</span></p>
					</div>	
				</div>
			<!-- Calendar -->
			<section id='calendar' class='calendar'>
				<div class='container'>
					<div id='events'></div>
				</div>
				<!-- /.container -->
			</section>
			";
		}
		if ($calendar == ''){
			echo " ";
		}
		
	?>
	
		<?php

		global $conection;
		$sql = mysqli_query($conection,"select team from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$team = $row['team'];
		
		$sql1 = mysqli_query($conection,"select * from team");
		$row = mysqli_fetch_assoc($sql1);

		if($team == '1'){
			echo "
				<section id='team' class='team'>
					<div class='container'>	
						<div class='row'>
							<div class='team'>
			";
						   showTeam();
			echo "
								</div>
							</div>
						</div>
				</section>

			";
		}
	
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		$sql1 = mysqli_query($conection,"select image from counters_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($skills2 == '1')  && ($image != '')){
			echo "
				<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
					<!-- start count stats -->
					<section id='counter-stats' class='wow fadeInRight parallax' data-wow-duration='1.4s'>
						<div class='container'>
							<div class='row' id='counter'>

				";
							   showCounters();
				echo "

							</div>
							<!-- end row -->
						</div>
						<!-- end container -->

					</section>
					<!-- end cont stats -->
				</div>

			";
		}
		if (($skills2 == '1')  && ($image == '')){
			echo "
				<!-- start count stats -->
				<section id='counter-stats' class='wow fadeInRight' data-wow-duration='1.4s'>
					<div class='container'>
						<div class='row' id='counter'>
							
			";
						   showCounters();
			echo "

						</div>
						<!-- end row -->
					</div>
					<!-- end container -->

				</section>
				<!-- end cont stats -->
			";
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select services from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$services = $row['services'];
		
		if ($services == '1'){
			
			echo "
		<!-- Services -->
		<section id='services' class='services bg-primary'>
			<div class='container'>
				<div class='row text-center'>
				<div class='col-lg-12 text-center'>
					<h2>".lang('SERVICES')."</h2>
					<hr class='small'>
				</div>
			";
					showAllServices();
			echo "
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>

			";
		}
		if ($services == ''){
			echo " ";
		}
	?>
	

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select pricing from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$pricing = $row['pricing'];
				
		if (($pricing == '1')){
			echo "

					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PRICING_TABLES')."</h2>
								<hr class='small'>
							</div>
							<div class='pricing'>
			";
						   showTables();
			echo "
							</div>
							<!-- /PRICING-TABLE -->
						</div>
					</div>


			";
		}
		if ($pricing == ''){
			echo " ";
		}
	?>	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select testimonials from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$testimonials = $row['testimonials'];
				
		if ($testimonials == '1'){
			
			echo "
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<h2>".lang('TESTIMONIALS')."</h2>
						<hr class='small'>
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->
			";
		}
		if ($testimonials == ''){
			echo " ";
		}
	?>
	
		<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='input-group clients'>
							<form action='index.php' method='post'> 
		";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text' name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>
					<?php
						function simpleRegexpValidation($email) {
							$regex = '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i';
							return preg_match($regex, $email);
						}
						
						if(!empty($_POST)){ 
							$name = $_POST['name'];
							$email = $_POST['email'];
						}
						else{
							$name = '';
							$email = '';
						}
						 if(!empty($_POST)) 
						{ 
							// Ensure that the user fills out field
							if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
							{ 
								echo "<script type='text/javascript'>sweetAlert('Oops...', 'Incorrect email format!', 'error');</script>";	
								echo "<META HTTP-EQUIV=REFRESH CONTENT='100'; '.$url.''>"; 
								die; 
							} 
							 
							// Check if the email is already taken
							$query = ' 
								SELECT 
									1 
								FROM clients 
								WHERE 
									email = :email 
							'; 
							$query_params = array( 
								':email' => $_POST['email'] 
							); 
							try { 
								$stmt = $db->prepare($query); 
								$result = $stmt->execute($query_params); 
							} 
							catch(PDOException $ex){ die('Failed to run query: ' . $ex->getMessage());} 
							$row = $stmt->fetch(); 
							if($row){ 		
										echo "<script type='text/javascript'>swal('Oops...', 'This email address is already registered!', 'error');</script>";
										echo "<meta http-equiv='refresh' content='1; about.php'>"; 
										die(); 
									} 
							 
							// Add row to database 
							$query = " 
								INSERT INTO clients VALUES ('', ''.$name.'',''.$email.'')
							"; 
							 
							// Security measures
							$query_params = array( 
								':email' => $_POST['email']
							); 
							try {  
								$stmt = $db->prepare($query); 
								$result = $stmt->execute($query_params); 
							} 
							catch(PDOException $ex){ die('Failed to run query: ' . $ex->getMessage()); } 
										echo "<script type='text/javascript'>swal('Good job!', 'You Are Now Registered!', 'success');</script>";
										echo "<meta http-equiv='refresh' content='1; index.php'>"; 
										die(); 
						} 
					?>
					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>
	
<?php include_once 'footer.php'; ?>

	<script src="assets/js/jquery.waypoints.js"></script>
	<script src="assets/js/progressbar.js"></script>
	<script src="assets/js/modernizr.custom.js"></script>
	<script src="assets/js/main.js"></script>

	<script>
	
		function animateProgressBar(){
			$(".meter > span").each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({
						width: $(this).data("origWidth")
					}, 1200);
			});
		}

		var waypoint = new Waypoint({
			  element: document.getElementById('skills'),
			  handler: function(direction) {
				animateProgressBar();
				this.destroy();
			  },
			 offset: '100%',
			 
		});
		
		$(document).ready(function(){

		  // hide our element on page load
		  $('#home').css('opacity', 0);
		 
		  $('#home').waypoint(function() {
			  $('#home').addClass('fadeInUp');
		  }, { offset: '35%' });
		 
		});

	</script>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activecalendar");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		if ($skills2 == '1'){
			?>
			
				<script src="assets/js/jquery.counterup.js"></script> 
				<script>
				// number count for stats, using jQuery animate
				var a = 0;
				$(window).scroll(function() {

				  var oTop = $('#counter').offset().top - window.innerHeight;
				  if (a == 0 && $(window).scrollTop() > oTop) {
					$('.counter-value').each(function() {
					  var $this = $(this),
						countTo = $this.attr('data-count');
					  $({
						countNum: $this.text()
					  }).animate({
						  countNum: countTo
						},

						{

						  duration: 4000,
						  easing: 'swing',
						  step: function() {
							$this.text(Math.floor(this.countNum));
						  },
						  complete: function() {
							$this.text(this.countNum);
							//alert('finished');
						  }

						});
					});
					a = 1;
				  }

				});
			</script>
		<?php
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>
	
	<script src='assets/js/moment.min.js'></script>
	<script src='assets/js/fullcalendar.min.js'></script>	

	<?php echo listEvents(); ?>
	<?php echo modalEvents(); ?>
	
	