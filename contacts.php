<?php include_once 'header.php'; ?>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select form from activecontacts");
		$row = mysqli_fetch_assoc($sql);
		$form = $row['form'];

		$sql1 = mysqli_query($conection,"select * from contact_type");
		$row = mysqli_fetch_assoc($sql1);
		$type = $row['type'];
		
		$sql2 = mysqli_query($conection,"select image from contactform_img");
		$row = mysqli_fetch_assoc($sql2);
		$image = $row['image'];
		
		if (($form == '1') && ($type == 'material')  && ($image != '')){
			echo "<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('CONTACTS')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CONTACTS')."</span></p>
					</div>	
				</div>
					<section id='contact' class='contact bg-primary'>
						<div class='container'>
							<div class='descriptions row text-center'>
							";
								showContactsText();
						echo"
							</div>
							<br/>
							<div class='row text-center'>
								<div class='col-lg-8 col-lg-offset-2'>
									<form id='contact-form' method='post' action='contact.php' role='form'>";
										?>
										<?php $csrf->echoInputField(); ?>
										<?php 								
										echo contactFormMaterial();
				echo "				</form>
								</div><!-- /.8 -->
							</div> <!-- /.row-->
						</div> <!-- /.container-->
					</section>
				";
		}
		if (($form == '1') && ($type == 'material')  && ($image == '')){
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('CONTACTS')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CONTACTS')."</span></p>
					</div>	
				</div>
			<section id='contact' class='contact bg-primary'>
						<div class='container'>
							<div class='descriptions row text-center'>
							";
								showContactsText();
						echo"
							</div>
							<br/>
							<div class='row text-center'>
								<div class='col-lg-8 col-lg-offset-2'>
								<form id='contact-form' method='post' action='contact.php' role='form'>";
									?>
									<?php $csrf->echoInputField(); ?>
									<?php 								
									echo contactFormMaterial();
			echo "				</form>
								</div><!-- /.8 -->
							</div> <!-- /.row-->
						</div> <!-- /.container-->
					</section>";
		}
		if (($form == '1') && ($type == 'default')  && ($image != '')){
			echo "<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('CONTACTS')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CONTACTS')."</span></p>
					</div>	
				</div>
					<section id='contact' class='contact bg-primary'>
						<div class='container'>
							<div class='descriptions row text-center'>
							";
								showContactsText();
						echo"
							</div>
							<br/>
							<div class='row text-center'>
								<div class='col-lg-8 col-lg-offset-2'>
								<form id='contact-form' method='post' action='contact.php' role='form'>";								
									?>
									<?php $csrf->echoInputField(); ?>
									<?php 
									echo contactForm();
			echo "				</form>
								</div><!-- /.8 -->
							</div> <!-- /.row-->
						</div> <!-- /.container-->
					</section>
				";
		}
		if (($form == '1') && ($type == 'default')  && ($image == '')){
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('CONTACTS')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('CONTACTS')."</span></p>
					</div>	
				</div>
			<section id='contact' class='contact bg-primary'>
						<div class='container'>
							<div class='descriptions row text-center'>
							";
								showContactsText();
						echo"
							</div>
							<br/>
							<div class='row text-center'>
								<div class='col-lg-8 col-lg-offset-2'>
									<form id='contact-form' method='post' action='contact.php' role='form'>";
										?>
										<?php $csrf->echoInputField(); ?>
										<?php 
										echo contactForm();
			echo "					</form>
								</div><!-- /.8 -->
							</div> <!-- /.row-->
						</div> <!-- /.container-->
					</section>";
		}
		else{
			echo " ";
		}
	?>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select map from activecontacts");
		$row = mysqli_fetch_assoc($sql);
		$map = $row['map'];
		
		if ($map == '1'){
		echo "
		<!-- Map -->
		<section class='map'>
			<span>
				<div id='map'></div>
			</span>
		</section>
		";
		}else{
			echo " ";
		}
	?>	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activecontacts");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activecontacts");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside id='subscribe' class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
		";
						
						if(isset($_POST))
						{
							$error = 0;
							if (isset($_POST['name']) && !empty($_POST['name'])) {
								$name=mysqli_real_escape_string($conection,trim($_POST['name']));
							}else{
								$error = 1;
							}
							if (isset($_POST['email']) && !empty($_POST['email'])) {
								$email=mysqli_real_escape_string($conection,trim($_POST['email']));
							}else{
								$error = 1;
							}
							
							if(!$error) {
							function valid_email($email) {
									return !!filter_var($email, FILTER_VALIDATE_EMAIL);
								}
								
								if(!empty($_POST)){ 
								$email = $_POST['email'];
								}
								else{
									$email = "";
								}
								if( valid_email($email) ) {
								} else {
									$error = 1;
								}	

								if(!valid_email($email) ) {
									echo "<div class='alert alert-danger fade in'>
													<a href='#' class='close' data-dismiss='alert'>&times;</a>
													<strong>Error!</strong> ".lang('NOT_EMAIL_FORMAT')."
												</div>";
											echo "<script>top.location.href='index.php#subscribe';</script>";
									}	
							}
						

							if(!$error) {
								$sql="select * from clients where (email='$email');";
								$res=mysqli_query($conection,$sql);
								if (mysqli_num_rows($res) > 0) {
								// output data of each row
								$row = mysqli_fetch_assoc($res);
								
								
								if($email==$row['email'])
								{
									echo "<div class='alert alert-danger fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Error!</strong> ".lang('EMAIL_ALREADY_EXISTS')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";
								}
								}else{ 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES ('', '".$name."','".$email."')");
									echo "<div class='alert alert-success fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Success!</strong> ".lang('YOU_ARE_NOW_REGISTERED')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";								
								}
								
							}
						}
		echo "							
						</div>
					</div>
						<div class='input-group clients'>						
							<form action='index.php' method='post' enctype='multipart/form-data'> 
							";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text'  name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>

					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>

	<?php echo getGoogleMap(); ?>
		
	
<?php include_once 'footer.php'; ?>
<script src="assets/js/modernizr.custom.js"></script>
<script src="assets/js/main.js"></script>
