
	<?php echo getContacts(); ?>
	
	<?php require "admin/fstats.php" ?>
	
	</div>
	
           
	<a id="to-top" href="#top" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
	
    <!-- jQuery -->
    <script src="assets/js/jquery.js"></script>
	<script src="assets/js/parallax.js"></script>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select * from background");
		$row = mysqli_fetch_assoc($sql);
		$type = $row['type'];
		
		if ($type == 'youtube'){
			echo getVideo(); 
		}
	?>
	
	<script src="assets/js/owl.carousel.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>	
	<script type="text/javascript" src="assets/js/jquery.plugin.js"></script>
    <script type="text/javascript" src="assets/js/jquery.countdown.js"></script>

	<?php
		global $conection;
		$sql = mysqli_query($conection,"select * from background");
		$row = mysqli_fetch_assoc($sql);
		$type = $row['type'];
		
		if ($type == 'particles'){
			echo getParticles(); 
		}
	?>
	
	<script type="text/javascript" src="assets/js/resize-slider-min.js"></script>
    <?php echo listSlideshow(); ?>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
	 //#to-top button appears after scrolling
    var fixed = false;
    $(document).scroll(function() {
        if ($(this).scrollTop() > 250) {
            if (!fixed) {
                fixed = true;
                // $('#to-top').css({position:'fixed', display:'block'});
                $('#to-top').show("slow", function() {
                    $('#to-top').css({
                        position: 'fixed',
                        display: 'block'
                    });
                });
            }
        } else {
            if (fixed) {
                fixed = false;
                $('#to-top').hide("slow", function() {
                    $('#to-top').css({
                        display: 'none'
                    });
                });
            }
        }
    });
	$(document).ready(function noscroll() {
		
    // Disable Google Maps scrolling
    // See http://stackoverflow.com/a/25904582/1607849
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function(event) {
        var that = $(this);
        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('span').css("pointer-events", "none");
    }
    var onMapClickHandler = function(event) {
            var that = $(this);
            // Disable the click handler until the user leaves the map area
            that.off('click', onMapClickHandler);
            // Enable scrolling zoom
            that.find('span').css("pointer-events", "auto");
            // Handle the mouse leave event
            that.on('mouseleave', onMapMouseleaveHandler);
        }
        // Enable map zooming with mouse scroll when the user clicks the map
    $('.map').on('click', onMapClickHandler);

		
	});
   
	$(document).ready(function myFunction() {
		var myVar = setTimeout(showPage, 2000);

		
	});
	function showPage() {
		$('#preloader').css('display','none');
		$('#spinner').css('display','none');
	}
	
</script>

		<script src="assets/js/validator.js"></script>
        <script src="assets/js/contact.js"></script>
		
		<script src="assets/js/isotope.min.js"></script>
		<script src="assets/js/jquery.fancybox.pack.js"></script>
	
	<script>
		jQuery(document).ready(function($) {
			"use strict";
			//  TESTIMONIALS CAROUSEL HOOK
			$('#customers-testimonials').owlCarousel({
				loop: true,
				center: true,
				items: 3,
				margin: 0,
				autoplay: true,
				dots:true,
				autoplayTimeout: 8500,
				smartSpeed: 450,
				responsive: {
				  0: {
					items: 1
				  },
				  768: {
					items: 2
				  },
				  1170: {
					items: 3
				  }
				}
			});
			//  Clients CAROUSEL HOOK
			$("#brand-carousel").owlCarousel({

				loop: true,
				items: 6,
				margin: 0,
				autoplay: true,
				dots:true,
				autoplayTimeout: 4500,
				smartSpeed: 450,
				responsive: {
				  0: {
					items: 1
				  },
				  768: {
					items: 3
				  },
				  1199: {
					items: 5
				  }
				}

			});
		});
	</script>
	<script src="assets/js/classie.js"></script>
	<script>
			(function() {
				// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
				if (!String.prototype.trim) {
					(function() {
						// Make sure we trim BOM and NBSP
						var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
						String.prototype.trim = function() {
							return this.replace(rtrim, '');
						};
					})();
				}

				[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
					// in case the input is already filled..
					if( inputEl.value.trim() !== '' ) {
						classie.add( inputEl.parentNode, 'input--filled' );
					}

					// events:
					inputEl.addEventListener( 'focus', onInputFocus );
					inputEl.addEventListener( 'blur', onInputBlur );
				} );

				function onInputFocus( ev ) {
					classie.add( ev.target.parentNode, 'input--filled' );
				}

				function onInputBlur( ev ) {
					if( ev.target.value.trim() === '' ) {
						classie.remove( ev.target.parentNode, 'input--filled' );
					}
				}
			})();
		</script>	

		
		
	<!-- Fixed top menu script -->
	<script>
		$(document).ready(function () {
			// jQuery to collapse the navbar on scroll
			function collapseNavbar() {
				if ($(".navbar").offset().top > 50) {
					$(".navbar-fixed-top").addClass("top-nav-collapse");
				} else {
					$(".navbar-fixed-top").removeClass("top-nav-collapse");
				}
			}

			$(window).scroll(collapseNavbar);
			$(document).ready(collapseNavbar);

			// Closes the Responsive Menu on Menu Item Click
			$('.navbar-collapse ul li a').click(function() {
				$(".navbar-collapse").collapse('hide');
			});
		});(jQuery);
	</script>
	
	<!-- Slick Slider -->
	<script src="assets/js/slick.js"></script>
	<script>	
		$(document).ready(function () {
		  
		  $(".Modern-Slider").slick({
			autoplay:true,
			autoplaySpeed:10000,
			speed:600,
			slidesToShow:1,
			slidesToScroll:1,
			pauseOnHover:false,
			dots:true,
			pauseOnDotsHover:true,
			cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
			fade:true,
			draggable:true,
			prevArrow:'<button class="PrevArrow"></button>',
			nextArrow:'<button class="NextArrow"></button>', 
		  });
		  
		});(jQuery);
	</script>
	
	<script>
		// For Demo purposes only (show hover effect on mobile devices)
		[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
			el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
		} );
	</script>
		

	<script src='assets/js/moment.min.js'></script>
	<script src='assets/js/fullcalendar.min.js'></script>	

	<?php echo listEvents(); ?>
	<?php echo modalEvents(); ?>

			
</body>

</html>