<?php include_once 'header.php'; ?>

	<?php	
		$sql2 = mysqli_query($conection,"select type from portfolio_type");
		$row = mysqli_fetch_assoc($sql2);
		$type = $row['type'];
		
		$sql1 = mysqli_query($conection,"select image from portfolio_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($image != '') && ($type == 'gallery')){
			echo "
			<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('PORTFOLIO')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('PORTFOLIO')."</span></p>
					</div>	
				</div>
				<!-- 4 Column Portfolio -->
				<section id='portfolio' class='portfolio filter-section'>
					<div class='container'>
						<div class='descriptions row text-center'>
						";
							showPortfolioText();
					echo"
						</div>
						<br/>
						<div class='row text-center'>
							<div class='col-sm-12 col-xs-12'>							
								<div class='filter-container isotopeFilters'>
									<ul class='list-inline filter'>
										<li class='active'><a href='#' data-filter='*'>".lang('ALL')."</a><span>/</span></li>
										";
										
										listCategoriesFront();
			echo "  
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</section>

				<section class='portfolio-section port-col'>
					<div class='container'>
						<div class='row'>
							<div class='isotopeContainer'>
							";
							
							showPortfolioGallery();
							
			echo "
							</div>
						</div>
					</div>
				</section>
				<!-- /4 Column Portfolio -->
			
			";
		}
		if (($image == '') && ($type == 'gallery')){
			
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('PORTFOLIO')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('PORTFOLIO')."</span></p>
					</div>	
				</div>
			<!-- 4 Column Portfolio -->
				<section id='portfolio' class='portfolio filter-section'>
					<div class='container'>
						<div class='descriptions row text-center'>
						";
							showPortfolioText();
					echo"
						</div>
						<br/>
						<div class='row text-center'>
							<div class='col-sm-12 col-xs-12'>							
								<div class='filter-container isotopeFilters'>
									<ul class='list-inline filter'>
										<li class='active'><a href='#' data-filter='*'>All </a><span>/</span></li>
										";
										
										listCategoriesFront();
			echo "  
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</section>

				<section class='portfolio-section port-col'>
					<div class='container'>
						<div class='row'>
							<div class='isotopeContainer'>
							";
							
							showPortfolioGallery();
							
			echo "
							</div>
						</div>
					</div>
				</section>
				<!-- /4 Column Portfolio -->
			
			";
		}
		if (($image != '') && ($type == 'portfolio')){
			echo "
			<div id='page_title' class='text-center'>
					<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
						<div class='container inner parallax'>
							<h1>".lang('PORTFOLIO')."</h1>
						</div>	
					</div>
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('PORTFOLIO')."</span></p>
					</div>	
				</div>
				<!-- 4 Column Portfolio -->
				<section id='portfolio' class='portfolio filter-section'>
					<div class='container'>
						<div class='descriptions row text-center'>
						";
							showPortfolioText();
					echo"
						</div>
						<br/>
						<div class='row text-center'>
							<div class='col-sm-12 col-xs-12'>							
								<div class='filter-container isotopeFilters'>
									<ul class='list-inline filter'>
										<li class='active'><a href='#' data-filter='*'>".lang('ALL')."</a><span>/</span></li>
										";
										
										listCategoriesFront();
			echo "  
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</section>

				<section class='portfolio-section port-col'>
					<div class='container'>
						<div class='row'>
							<div class='isotopeContainer'>
								<div class='content'>
									<div class='grid'>
							";
							
							showPortfolio();
							
			echo "
									</div>
									<!-- /grid -->
									<div class='preview'>
										<button class='action action--close'><i class='fa fa-times'></i><span class='text-hidden'>Close</span></button>
										<div class='description description--preview'></div>
									</div>
									<!-- /preview -->
								</div>
							</div>
						</div>
					</div>
				</section>			
			";
		}
		if (($image == '') && ($type == 'portfolio')){
			
			echo "
			<div id='page_title' class='text-center'>
					<div class='container inner parallax'>
						<h1>".lang('PORTFOLIO')."</h1>
					</div>	
				</div>

				<div id='breadcrumbs'>
					<div class='container'>
						<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ".lang('PORTFOLIO')."</span></p>
					</div>	
				</div>
			<!-- 4 Column Portfolio -->
				<section id='portfolio' class='portfolio filter-section'>
					<div class='container'>
						<div class='descriptions row text-center'>
						";
							showPortfolioText();
					echo"
						</div>
						<br/>
						<div class='row text-center'>
							<div class='col-sm-12 col-xs-12'>							
								<div class='filter-container isotopeFilters'>
									<ul class='list-inline filter'>
										<li class='active'><a href='#' data-filter='*'>All </a><span>/</span></li>
										";
										
										listCategoriesFront();
			echo "  
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</section>

				<section class='portfolio-section port-col'>
					<div class='container'>
						<div class='row'>
							<div class='isotopeContainer'>
								<div class='content'>
									<div class='grid'>
							";
							
							showPortfolio();
							
			echo "
									</div>
									<!-- /grid -->
									<div class='preview'>
										<button class='action action--close'><i class='fa fa-times'></i><span class='text-hidden'>Close</span></button>
										<div class='description description--preview'></div>
									</div>
									<!-- /preview -->
								</div>
							</div>
						</div>
					</div>
				</section>
			
			";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activeportfolio");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activeportfolio");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside id='subscribe' class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
		";
						
						if(isset($_POST))
						{
							$error = 0;
							if (isset($_POST['name']) && !empty($_POST['name'])) {
								$name=mysqli_real_escape_string($conection,trim($_POST['name']));
							}else{
								$error = 1;
							}
							if (isset($_POST['email']) && !empty($_POST['email'])) {
								$email=mysqli_real_escape_string($conection,trim($_POST['email']));
							}else{
								$error = 1;
							}
							
							if(!$error) {
							function valid_email($email) {
									return !!filter_var($email, FILTER_VALIDATE_EMAIL);
								}
								
								if(!empty($_POST)){ 
								$email = $_POST['email'];
								}
								else{
									$email = "";
								}
								if( valid_email($email) ) {
								} else {
									$error = 1;
								}	

								if(!valid_email($email) ) {
									echo "<div class='alert alert-danger fade in'>
													<a href='#' class='close' data-dismiss='alert'>&times;</a>
													<strong>Error!</strong> ".lang('NOT_EMAIL_FORMAT')."
												</div>";
											echo "<script>top.location.href='index.php#subscribe';</script>";
									}	
							}
						

							if(!$error) {
								$sql="select * from clients where (email='$email');";
								$res=mysqli_query($conection,$sql);
								if (mysqli_num_rows($res) > 0) {
								// output data of each row
								$row = mysqli_fetch_assoc($res);
								
								
								if($email==$row['email'])
								{
									echo "<div class='alert alert-danger fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Error!</strong> ".lang('EMAIL_ALREADY_EXISTS')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";
								}
								}else{ 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES ('', '".$name."','".$email."')");
									echo "<div class='alert alert-success fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Success!</strong> ".lang('YOU_ARE_NOW_REGISTERED')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";								
								}
								
							}
						}
		echo "							
						</div>
					</div>
						<div class='input-group clients'>						
							<form action='index.php' method='post' enctype='multipart/form-data'> 
							";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text'  name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>

					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>
	
<?php include_once 'footer.php'; ?>


	<script src="assets/js/modernizr.custom.js"></script>

	<!-- /container -->
	<script src="assets/js/imagesloaded.pkgd.min.js"></script>
	<script src="assets/js/masonry.pkgd.min.js"></script>
	<script src="assets/js/main.js"></script>

	
	<script>
		(function() {
			var support = { transitions: Modernizr.csstransitions },
				// transition end event name
				transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
				onEndTransition = function( el, callback ) {
					var onEndCallbackFn = function( ev ) {
						if( support.transitions ) {
							if( ev.target != this ) return;
							this.removeEventListener( transEndEventName, onEndCallbackFn );
						}
						if( callback && typeof callback === 'function' ) { callback.call(this); }
					};
					if( support.transitions ) {
						el.addEventListener( transEndEventName, onEndCallbackFn );
					}
					else {
						onEndCallbackFn();
					}
				};

			new GridFx(document.querySelector('.grid'), {
				imgPosition : {
					x : -0.5,
					y : 1
				},
				onOpenItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							var delay = Math.floor(Math.random() * 50);
							el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
							el.style.transform = 'scale3d(0.1,0.1,1)';
							el.style.opacity = 0;
						}
					});
				},
				onCloseItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
							el.style.transition = 'opacity .4s, transform .4s';
							el.style.WebkitTransform = 'scale3d(1,1,1)';
							el.style.transform = 'scale3d(1,1,1)';
							el.style.opacity = 1;

							onEndTransition(el, function() {
								el.style.transition = 'none';
								el.style.WebkitTransform = 'none';
							});
						}
					});
				}
			});
		})();
	</script>