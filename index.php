<?php include_once 'header.php'; ?>

<?php
	// DB TABLE INFO
	$GLOBALS['hits_table_name'] = "hits";
	$GLOBALS['info_table_name'] = "info";

	// CONNECT TO DB
	try {   
		$GLOBALS['db'] = new PDO("mysql:host=".$host.";dbname=".$dbname, $username, $password, array(PDO::ATTR_PERSISTENT => false, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => false));  
	}  
	catch(PDOException $e) {  
		echo $e->getMessage();
	}
		
	// CHECK IF PAGE EXISTS IN PAGE HIT TABLE
	function checkPageName($page_name){
		$sql = "SELECT * FROM ".$GLOBALS['hits_table_name']." WHERE page = :page";
		$query = $GLOBALS['db']->prepare($sql);
		$query->execute([':page' => $page_name]);
		if ($query->rowCount() == 0){
			$sql = "INSERT INTO ".$GLOBALS['hits_table_name']." (page, count) VALUES (:page, 0)";
			$query = $GLOBALS['db']->prepare($sql);
			$query->execute([':page' => $page_name]);
		}
	}

	// UPDATE PAGE HIT COUNT
	function updateCounter($page_name){
		checkPageName($page_name);
		$sql = "UPDATE ".$GLOBALS['hits_table_name']." SET count = count+1 WHERE page = :page";
		$query = $GLOBALS['db']->prepare($sql);
		$query->execute([':page' => $page_name]);
	}

	// UPDATE VISITOR INFO
	function updateInfo(){
		$sql = "INSERT INTO ".$GLOBALS['info_table_name']." (ip_address, user_agent) VALUES(:ip_address, :user_agent)";
		$query = $GLOBALS['db']->prepare($sql);
		$query->execute([':ip_address' => $_SERVER["REMOTE_ADDR"], ':user_agent' => $_SERVER["HTTP_USER_AGENT"]]);
	}
		
		updateCounter("MARKETING"); // Updates page hits
		updateInfo(); // Updates hit info
?>


			<?php
			global $conection;
			$sql = mysqli_query($conection,"select type from background");
			$row = mysqli_fetch_assoc($sql);
			$type = $row['type'];			
			
			if ($type == 'own'){
				$sql = mysqli_query($conection,"select * from video");
				$row = mysqli_fetch_assoc($sql);
				$name = $row['video_name'];
				
				echo "
				
				<!-- Slider Section Starts -->
				<section class='slider clearfix'>
					<!-- Easy as hell -->
					<div id='block' style='width: 100%; height: 750px;' data-vide-bg='assets/img/uploads/video/".$row['video_name']."' data-vide-options='position: 0% 50%'></div>
				</section>
				
				";
			}
		?>

		<?php
			global $conection;
			$sql = mysqli_query($conection,"select type from background");
			$row = mysqli_fetch_assoc($sql);
			$type = $row['type'];
			
			if ($type == 'youtube'){
				$sql = mysqli_query($conection,"select type from youtube");
				echo "<div id='background-video' class='background-video'></div>";
			}
		?>
		
		<?php
			global $conection;
			$sql = mysqli_query($conection,"select type from background");
			$row = mysqli_fetch_assoc($sql);
			$type = $row['type'];
			
			$sql1 = mysqli_query($conection,"select type from slider_type");
			$row = mysqli_fetch_assoc($sql1);
			$type1 = $row['type'];
			
			if ($type == 'slide'){
				
				if ($type1 == 'boxed'){
					$sql = mysqli_query($conection,"select type from slide");
					echo "<div class='container'>
					<div class='Modern-Slider'>";
					
						showSlider();
						
					echo"</div></div>";
				}
				if ($type1 == 'fullscreen'){
					$sql = mysqli_query($conection,"select type from slide");
					echo "
					<div class='Modern-Slider'>";
					
						showSlider();
						
					echo"</div>";
				}
				if ($type1 == 'fullwidth'){
					$sql = mysqli_query($conection,"select type from slide");
					echo "
					<div class='Modern-Slider'>";
					
						showSlider();
						
					echo"</div>";
				}
				
			}
			
			if ($type == 'particles')
			{
				?>
					
				<!-- Header -->   
				<header id="top" class="header"></header>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select type from background");
						$row = mysqli_fetch_assoc($sql);
						$type = $row['type'];
						
						if ($type == 'particles'){
							$sql = mysqli_query($conection,"select type from particles");
							echo "<div id='particles'></div>";
						}
					?>
					<div class="text-vertical-center">	
						
						<?php echo showText() ?>
						
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select countdown from comingsoon");
							$row = mysqli_fetch_assoc($sql);
							$countdown = $row['countdown'];
							
							if ($countdown == '1'){

								showCountDown();

							}
							if ($countdown == ''){
								echo " ";
							}
						?>
						
						<div class="container">
							<div class="row">
								<div class="col-lg-12 text-center">
									<?php
										global $conection;
										$sql = mysqli_query($conection,"select progress from comingsoon");
										$row = mysqli_fetch_assoc($sql);
										$progress = $row['progress'];
										
										if ($progress == '1'){

											updateProgressBar();
						
										}
										if ($progress == ''){
											echo " ";
										}
									?>
								</div>
							</div>
						</div>
						
					</div>
					
			<?php
			
			}
			
		
			
			if (($type != 'slide') && ($type != 'particles'))
			{
				?>
					
				<!-- Header -->   
				<header id="top" class="header">
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select type from background");
						$row = mysqli_fetch_assoc($sql);
						$type = $row['type'];
						
						if ($type == 'particles'){
							$sql = mysqli_query($conection,"select type from particles");
							echo "<div id='particles'></div>";
						}
					?>
					<div class="text-vertical-center">	
						
						<?php echo showText() ?>
						
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select countdown from comingsoon");
							$row = mysqli_fetch_assoc($sql);
							$countdown = $row['countdown'];
							
							if ($countdown == '1'){

								showCountDown();

							}
							if ($countdown == ''){
								echo " ";
							}
						?>
						
						<div class="container">
							<div class="row">
								<div class="col-lg-12 text-center">
									<?php
										global $conection;
										$sql = mysqli_query($conection,"select progress from comingsoon");
										$row = mysqli_fetch_assoc($sql);
										$progress = $row['progress'];
										
										if ($progress == '1'){

											updateProgressBar();
						
										}
										if ($progress == ''){
											echo " ";
										}
									?>
								</div>
							</div>
						</div>
						
					</div>
				</header>	
			<?php
			
			}
			
		?>

	<div class="wrapper">
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select services from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$services = $row['services'];
		
		$sql2 = mysqli_query($conection,"select type from services_type");
		$row = mysqli_fetch_assoc($sql2);
		$type = $row['type'];
		
		if (($services == '1') && ($type == 'icon')){
			
			echo "
		<!-- Services -->
		<section id='services' class='services'>
			<div class='container'>
				<div class='row text-center'>
					<h2>".lang('SERVICES')."</h2>
					<hr class='small'>
					<div class='row'>
			";
						showServices();
			echo "
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>

			";
		}
		if (($services == '1') && ($type != 'icon')){
			
			echo "
		<!-- Services -->
		<section id='services' class='services'>
			<div class='container'>
				<div class='row text-center'>
					<h2>".lang('SERVICES')."</h2>
					<hr class='small'>
					<div class='row'>
			";
						showServicesImage();
			echo "
					</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>

			";
		}
		if ($services == ''){
			echo " ";
		}
	?>
		
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select calendar from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$calendar = $row['calendar'];
				
		if (($calendar == '1')){
			echo "
				<section id='calendar' class='calendar'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('CALENDAR')."</h2>
								<hr class='small'>
							</div>
							<div class='container'>
								<div id='events'></div>
							</div>
							</div>
						</div>
				</section>

			";
		}
		if ($calendar == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select about from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$about = $row['about'];
		
		$sql1 = mysqli_query($conection,"select image from about_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($about == '1') && ($image != '')){
			
			echo "
			<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
			 <!-- About -->
			<section id='aboutfront' class='about parallax'>
				<div class='container'>
					<div class='row'>
						<div class='col-lg-12 text-center'>
							<h2>".lang('ABOUT')."</h2>
							<hr class='small'>
						</div>
						<div class='col-md-6'>
						
			";
							showAboutText();
			echo "
						</div>
						<div class='col-md-6'>
						<div id='skills'>
			";

							listSkillsFront();
			echo "
						  </div>
						</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container -->
			</section>
			</div>
			";
		}
		if (($about == '1') && ($image == '')){
			
			echo "
			 <!-- About -->
			<section id='aboutfront' class='about'>
				<div class='container'>
					<div class='row'>
						<div class='col-lg-12 text-center'>
							<h2>".lang('ABOUT')."</h2>
							<hr class='small'>
						</div>
						<div class='col-lg-6'>
						
			";
							showAboutText();
			echo "
						</div>
						<div class='col-md-6'>
							<div id='skills'>
			";

							listSkillsFront();
			echo "

							 </div>
						</div>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container -->
			</section>
			";
		}
		if ($about == ''){
			echo " ";
		}
		
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select team from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$team = $row['team'];
				
		if (($team == '1')){
			echo "
				<section id='team' class='team'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('TEAM')."</h2>
								<hr class='small'>
							</div>
							<div class='team'>
			";
						   showTeam();
			echo "
								</div>
							</div>
						</div>
				</section>

			";
		}
		if ($team == ''){
			echo " ";
		}
	?>
	
		<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		$sql1 = mysqli_query($conection,"select image from counters_img");
		$row = mysqli_fetch_assoc($sql1);
		$image = $row['image'];
		
		if (($skills2 == '1')  && ($image != '')){
			echo "
				<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
					<!-- start count stats -->
					<section id='counter-stats' class='wow fadeInRight parallax' data-wow-duration='1.4s'>
						<div class='container'>
							<div class='row' id='counter'>

				";
							   showCounters();
				echo "

							</div>
							<!-- end row -->
						</div>
						<!-- end container -->

					</section>
					<!-- end cont stats -->
				</div>

			";
		}
		if (($skills2 == '1')  && ($image == '')){
			echo "
				<!-- start count stats -->
				<section id='counter-stats' class='wow fadeInRight parallax' data-wow-duration='1.4s'>
					<div class='container'>
						<div class='row' id='counter'>
							
			";
						   showCounters();
			echo "

						</div>
						<!-- end row -->
					</div>
					<!-- end container -->

				</section>
				<!-- end cont stats -->
			";
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select portfolio from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$portfolio = $row['portfolio'];
		
		$sql2 = mysqli_query($conection,"select type from portfolio_type");
		$row = mysqli_fetch_assoc($sql2);
		$type = $row['type'];
		
	
		if ($portfolio == '1'){
		
			if ($type == 'gallery'){
				echo "
					<!-- 4 Column Portfolio -->
					<section id='portfolio' class='filter-section'>
						<div class='container'>
						<div class='col-lg-12 text-center'>
								<h2>".lang('PORTFOLIO')."</h2>
								<hr class='small'>
							</div>
							<div class='row text-center'>
								<div class='col-sm-12 col-xs-12'>							
									<div class='filter-container isotopeFilters'>
										<ul class='list-inline filter'>
											<li class='active'><a href='#' data-filter='*'>".lang('ALL')."</a><span>/</span></li>
											";
											
											listCategoriesFront();
				echo "  
										</ul>
									</div>
									
								</div>
							</div>
						</div>
					</section>

					<section class='portfolio-section port-col'>
						<div class='container'>
							<div class='row'>
								<div class='isotopeContainer'>
								";
								
								showPortfolioGallery();
								
				echo "
								</div>
							</div>
						</div>
					</section>
					<!-- /4 Column Portfolio -->
				
				";
			}		
			if ($type == 'portfolio'){
				
				echo "
				<!-- 4 Column Portfolio -->
					<section id='portfolio' class='filter-section'>
						<div class='container'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PORTFOLIO')."</h2>
								<hr class='small'>
							</div>
							<div class='row text-center'>
								<div class='col-sm-12 col-xs-12'>							
									<div class='filter-container isotopeFilters'>
										<ul class='list-inline filter'>
											<li class='active'><a href='#' data-filter='*'>All </a><span>/</span></li>
											";
											
											listCategoriesFront();
				echo "  
										</ul>
									</div>
									
								</div>
							</div>
						</div>
					</section>

					<section class='portfolio-section port-col'>
						<div class='container'>
							<div class='row'>
								<div class='isotopeContainer'>
									<div class='content'>
										<div class='grid'>
								";
								
								showPortfolio();
								
				echo "
										</div>
										<!-- /grid -->
										<div class='preview'>
											<button class='action action--close'><i class='fa fa-times'></i><span class='text-hidden'>Close</span></button>
											<div class='description description--preview'></div>
										</div>
										<!-- /preview -->
									</div>
								</div>
							</div>
						</div>
					</section>
				
				";
			}
		}
		if ($portfolio == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select blog from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$blog = $row['blog'];
		
		$sql2 = mysqli_query($conection,"select image from latest_img");
		$row = mysqli_fetch_assoc($sql2);
		$image = $row['image'];
				
		if (($blog == '1')  && ($image != '')){
			echo "
			<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
				<!-- Blog -->
				<section id='blog' class='blog parallax'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('LATEST_POSTS')."</h2>
								<hr class='small'>
							</div>
							<div class='blog'>
			";
						   showLatestPosts();
			echo "
							</div>
						</div>
					</div>
				</section>
			</div>
			";
		}
		if (($blog == '1')  && ($image == '')){
			echo "			
				<!-- Blog -->
				<section id='blog' class='blog parallax  bg-primary'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('LATEST_POSTS')."</h2>
								<hr class='small'>
							</div>
							<div class='blog'>
			";
						   showLatestPosts();
			echo "
							</div>
						</div>
					</div>
				</section>
			";
		}
		if ($blog == ''){
			echo " ";
		}
	?>	
	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select pricing from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$pricing = $row['pricing'];
		
		$sql2 = mysqli_query($conection,"select type from pricing_type");
		$row = mysqli_fetch_assoc($sql2);
		$type = $row['type'];
				
		if (($pricing == '1')){
			if ($type == 'simple'){
				echo "
				<section id='pricing' class='pricing'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PRICING_TABLES')."</h2>
								<hr class='small'>
							</div>
							<div class='pricing'>
			";
						   showTables();
			echo "
							</div>
							<!-- /PRICING-TABLE -->
						</div>
					</div>
				</section>

			"; 
			}		
			if ($type == 'material'){
				
				echo "
				<section id='pricing' class='pricing'>
					<div class='container'>						
						<div class='row'>
							<div class='col-lg-12 text-center'>
								<h2>".lang('PRICING_TABLES')."</h2>
								<hr class='small'>
							</div>
							<div id='pricing75' class='pricing pricing-74 pricing-75 bg-white'>
								<div class='container'>
									<div class='row'>";
											
								showTablesMaterial();
				echo "  
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				";
			}
		}
	?>	
	
	
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select testimonials from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$testimonials = $row['testimonials'];
		
		$sql2 = mysqli_query($conection,"select image from testimonials_img");
		$row = mysqli_fetch_assoc($sql2);
		$image = $row['image'];
				
		if (($testimonials == '1')  && ($image != '')){
			echo "
			<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/parallax/". $row['image'] ."'>
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials parallax'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<h2>".lang('TESTIMONIALS')."</h2>
						<hr class='small'>
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->
			</div>
			";
		}
		if (($testimonials == '1')  && ($image == '')){
			echo "			
			<!-- Testimonials -->
			<section id='testimonials' class='testimonials'>
				<div class='container'>
				  <div class='row text-center'>
					<div class='col-sm-12'>	
						<h2>".lang('TESTIMONIALS')."</h2>
						<hr class='small'>
						<div id='customers-testimonials' class='owl-carousel'>
			";
						   showTestimonials();
			echo "
						</div>
					</div>
				  </div>
				</div>
			</section>
			<!-- END OF TESTIMONIALS -->
			";
		}
		if ($testimonials == ''){
			echo " ";
		}
	?>
	
		<?php
		global $conection;
		$sql = mysqli_query($conection,"select partners from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$partners = $row['partners'];
				
		if ($partners == '1'){
			echo "

			<!-- Partners -->
			<section id='partners' class='brand-carousel'>
				<div class='container'>
					<div  class='row text-center'>
						<div class='col-sm-12'>
							<div id='brand-carousel' class='owl-carousel'>
			";
						   showPartners();
			echo "
						</div>
					</div>
				</div>
			</div>
		</section>

			";
		}
		if ($partners == ''){
			echo " ";
		}
	?>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select subscribe from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$subscribe = $row['subscribe'];
		
		
		if ($subscribe == '1'){
		echo "
			
		 <!-- Call to Action -->
		<aside id='subscribe' class='call-to-action bg-primary'>
			<div class='container'>
				<div class='row'>
					<div class='col-lg-12 text-center'>
						<h3>".lang('REGISTER')."</h3>
						<div class='form-group'>
						<div class='col-sm-8 col-sm-offset-2'>
		";
						
						if(isset($_POST))
						{
							$error = 0;
							if (isset($_POST['name']) && !empty($_POST['name'])) {
								$name=mysqli_real_escape_string($conection,trim($_POST['name']));
							}else{
								$error = 1;
							}
							if (isset($_POST['email']) && !empty($_POST['email'])) {
								$email=mysqli_real_escape_string($conection,trim($_POST['email']));
							}else{
								$error = 1;
							}
							
							if(!$error) {
							function valid_email($email) {
									return !!filter_var($email, FILTER_VALIDATE_EMAIL);
								}
								
								if(!empty($_POST)){ 
								$email = $_POST['email'];
								}
								else{
									$email = "";
								}
								if( valid_email($email) ) {
								} else {
									$error = 1;
								}	

								if(!valid_email($email) ) {
									echo "<div class='alert alert-danger fade in'>
													<a href='#' class='close' data-dismiss='alert'>&times;</a>
													<strong>Error!</strong> ".lang('NOT_EMAIL_FORMAT')."
												</div>";
											echo "<script>top.location.href='index.php#subscribe';</script>";
									}	
							}
						

							if(!$error) {
								$sql="select * from clients where (email='$email');";
								$res=mysqli_query($conection,$sql);
								if (mysqli_num_rows($res) > 0) {
								// output data of each row
								$row = mysqli_fetch_assoc($res);
								
								
								if($email==$row['email'])
								{
									echo "<div class='alert alert-danger fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Error!</strong> ".lang('EMAIL_ALREADY_EXISTS')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";
								}
								}else{ 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES ('', '".$name."','".$email."')");
									echo "<div class='alert alert-success fade in'>
											<a href='#' class='close' data-dismiss='alert'>&times;</a>
											<strong>Success!</strong> ".lang('YOU_ARE_NOW_REGISTERED')."
										</div>";
									echo "<script>top.location.href='index.php#subscribe';</script>";								
								}
								
							}
						}
		echo "							
						</div>
					</div>
						<div class='input-group clients'>						
							<form action='index.php' method='post' enctype='multipart/form-data'> 
							";
							?>
							<?php $csrf->echoInputField(); ?>
							<?php 
		echo "
							<div class='col-md-8'>
								<input type='text' name='name' class='form-control' placeholder='".lang('YOUR_NAME_HERE')."' required value='' />
								<input type='text'  name='email' class='form-control' placeholder='".lang('YOUR_EMAIL_HERE')."' required value='' />
							</div>
							<div class='col-md-2'>
								<input type='submit' class='btn btn-lg btn-light' value='".lang('SUBSCRIBE')."' />
						 </div>
					   </form>
					</div>
		";
					?>

					<?php
		echo "                  
                </div>
            </div>
        </div>
    </aside>		
		";
	}else{
		echo " ";
	}
	?>

<?php include_once 'footer.php'; ?>


	<script src="assets/js/modernizr.custom.js"></script>

	<!-- /container -->
	<script src="assets/js/imagesloaded.pkgd.min.js"></script>
	<script src="assets/js/masonry.pkgd.min.js"></script>
	<script src="assets/js/main.js"></script>

	
	<script>
		(function() {
			var support = { transitions: Modernizr.csstransitions },
				// transition end event name
				transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
				onEndTransition = function( el, callback ) {
					var onEndCallbackFn = function( ev ) {
						if( support.transitions ) {
							if( ev.target != this ) return;
							this.removeEventListener( transEndEventName, onEndCallbackFn );
						}
						if( callback && typeof callback === 'function' ) { callback.call(this); }
					};
					if( support.transitions ) {
						el.addEventListener( transEndEventName, onEndCallbackFn );
					}
					else {
						onEndCallbackFn();
					}
				};

			new GridFx(document.querySelector('.grid'), {
				imgPosition : {
					x : -0.5,
					y : 1
				},
				onOpenItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							var delay = Math.floor(Math.random() * 50);
							el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
							el.style.transform = 'scale3d(0.1,0.1,1)';
							el.style.opacity = 0;
						}
					});
				},
				onCloseItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
							el.style.transition = 'opacity .4s, transform .4s';
							el.style.WebkitTransform = 'scale3d(1,1,1)';
							el.style.transform = 'scale3d(1,1,1)';
							el.style.opacity = 1;

							onEndTransition(el, function() {
								el.style.transition = 'none';
								el.style.WebkitTransform = 'none';
							});
						}
					});
				}
			});
		})();
	</script>

	<script src="assets/js/jquery.waypoints.js"></script>
	<script src="assets/js/progressbar.js"></script>

	<script>
	
		function animateProgressBar(){
			$(".meter > span").each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({
						width: $(this).data("origWidth")
					}, 1200);
			});
		}

		var waypoint = new Waypoint({
			  element: document.getElementById('skills'),
			  handler: function(direction) {
				animateProgressBar();
				this.destroy();
			  },
			 offset: '100%',
			 
		});
		
		$(document).ready(function(){

		  // hide our element on page load
		  $('#home').css('opacity', 0);
		 
		  $('#home').waypoint(function() {
			  $('#home').addClass('fadeInUp');
		  }, { offset: '35%' });
		 
		});

	</script>
	
	<?php
		global $conection;
		$sql = mysqli_query($conection,"select skills2 from activehomepage");
		$row = mysqli_fetch_assoc($sql);
		$skills2 = $row['skills2'];
		
		if ($skills2 == '1'){
			?>
			
				<script src="assets/js/jquery.counterup.js"></script> 
				<script>
				// number count for stats, using jQuery animate
				var a = 0;
				$(window).scroll(function() {

				  var oTop = $('#counter').offset().top - window.innerHeight;
				  if (a == 0 && $(window).scrollTop() > oTop) {
					$('.counter-value').each(function() {
					  var $this = $(this),
						countTo = $this.attr('data-count');
					  $({
						countNum: $this.text()
					  }).animate({
						  countNum: countTo
						},

						{

						  duration: 4000,
						  easing: 'swing',
						  step: function() {
							$this.text(Math.floor(this.countNum));
						  },
						  complete: function() {
							$this.text(this.countNum);
							//alert('finished');
						  }

						});
					});
					a = 1;
				  }

				});
			</script>
		<?php
		}
		if ($skills2 == ''){
			echo " ";
		}
	?>