-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Des 2019 pada 09.23
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `about_img`
--

CREATE TABLE `about_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `about_img`
--

INSERT INTO `about_img` (`id`, `image`) VALUES
(0, '073b559316ad334a4de8cd3b21404c88.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `activeabout`
--

CREATE TABLE `activeabout` (
  `id` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activeabout`
--

INSERT INTO `activeabout` (`id`, `services`, `testimonials`, `partners`, `subscribe`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activeblog`
--

CREATE TABLE `activeblog` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activeblog`
--

INSERT INTO `activeblog` (`id`, `partners`, `subscribe`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activecalendar`
--

CREATE TABLE `activecalendar` (
  `id` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activecalendar`
--

INSERT INTO `activecalendar` (`id`, `services`, `testimonials`, `partners`, `subscribe`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 0, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activecontacts`
--

CREATE TABLE `activecontacts` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `form` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activecontacts`
--

INSERT INTO `activecontacts` (`id`, `partners`, `map`, `form`, `subscribe`) VALUES
(1, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activehomepage`
--

CREATE TABLE `activehomepage` (
  `id` int(11) NOT NULL,
  `about` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `skills2` int(11) NOT NULL,
  `calendar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activehomepage`
--

INSERT INTO `activehomepage` (`id`, `about`, `services`, `testimonials`, `partners`, `subscribe`, `portfolio`, `team`, `pricing`, `blog`, `skills2`, `calendar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activepartners`
--

CREATE TABLE `activepartners` (
  `id` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `activeportfolio`
--

CREATE TABLE `activeportfolio` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activeportfolio`
--

INSERT INTO `activeportfolio` (`id`, `partners`, `subscribe`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activeprivacy`
--

CREATE TABLE `activeprivacy` (
  `id` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activeprivacy`
--

INSERT INTO `activeprivacy` (`id`, `subscribe`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `activeservices`
--

CREATE TABLE `activeservices` (
  `id` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `activeservices`
--

INSERT INTO `activeservices` (`id`, `testimonials`, `partners`, `subscribe`, `portfolio`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `background`
--

CREATE TABLE `background` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `background`
--

INSERT INTO `background` (`id`, `type`) VALUES
(1, 'slide');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog_img`
--

CREATE TABLE `blog_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `blog_img`
--

INSERT INTO `blog_img` (`id`, `image`) VALUES
(0, 'f8fc550a91d2d0e24b7ba58d48f916d8.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog_type`
--

CREATE TABLE `blog_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `blog_type`
--

INSERT INTO `blog_type` (`id`, `type`, `number`) VALUES
(1, 'No Pagination', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `calendar_img`
--

CREATE TABLE `calendar_img` (
  `id` int(11) NOT NULL DEFAULT 0,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `calendar_img`
--

INSERT INTO `calendar_img` (`id`, `image`) VALUES
(0, '1641962411eada27dcc36633d50886f0.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `slug` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`) VALUES
(8, 'Design', 'Design'),
(7, 'Brainstorm', 'Brainstorm'),
(9, 'Comunication', 'Comunication');

-- --------------------------------------------------------

--
-- Struktur dari tabel `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `clients`
--

INSERT INTO `clients` (`id`, `name`, `email`) VALUES
(9, 'John Doe', 'johndoe@ezcode.pt'),
(10, 'Tony Martin', 'tonymartin@apps.com'),
(11, 'Valter Dog', 'valterdog@dascene.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `comingsoon`
--

CREATE TABLE `comingsoon` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `countdown` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `comingsoon`
--

INSERT INTO `comingsoon` (`id`, `progress`, `countdown`) VALUES
(1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `contactform_img`
--

CREATE TABLE `contactform_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contactform_img`
--

INSERT INTO `contactform_img` (`id`, `image`) VALUES
(0, '89722c57bfe91def7910cdf48639cc6b.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `code` text NOT NULL,
  `city` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `title` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `footer` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `linkedin` text NOT NULL,
  `google` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` text NOT NULL,
  `pinterest` text NOT NULL,
  `tumblr` text NOT NULL,
  `vine` text NOT NULL,
  `snapchat` text NOT NULL,
  `reddit` text NOT NULL,
  `flickr` text NOT NULL,
  `soundcloud` text NOT NULL,
  `whatsapp` text NOT NULL,
  `slack` text NOT NULL,
  `xing` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contacts`
--

INSERT INTO `contacts` (`id`, `address`, `code`, `city`, `latitude`, `longitude`, `title`, `email`, `phone`, `footer`, `facebook`, `twitter`, `linkedin`, `google`, `youtube`, `instagram`, `pinterest`, `tumblr`, `vine`, `snapchat`, `reddit`, `flickr`, `soundcloud`, `whatsapp`, `slack`, `xing`) VALUES
(1, 'Kasablanka, 12th floor, unit A&H Jalan Casablanca Raya, Kav. 88, Menteng Dalam, Tebet, Jakarta Selatan', '12870', 'Jakarta Selatan', '-6.2237004', '106.8430123', 'INDIEKOM - IT Solution', 'info@indiekom.com', '021-3950 5432', 'Copyright © Indiekom 2019', 'https://www.facebook.com/', '#', '#', '#', '#', 'https://www.instagram.com/indiekom_tekno/?r=nametag', '', '', '', '', '', '', '', '081360658904', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact_form`
--

CREATE TABLE `contact_form` (
  `id` int(11) NOT NULL,
  `sendyouremail` text NOT NULL,
  `mailfrom` text NOT NULL,
  `sendto` text NOT NULL,
  `subject` text NOT NULL,
  `okmessage` text NOT NULL,
  `errormessage` text NOT NULL,
  `emailtext` text NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `message` text NOT NULL,
  `button` text NOT NULL,
  `fn_holder` text NOT NULL,
  `ln_holder` text NOT NULL,
  `em_holder` text NOT NULL,
  `ph_holder` text NOT NULL,
  `mg_holder` text NOT NULL,
  `fn_error` text NOT NULL,
  `ln_error` text NOT NULL,
  `em_error` text NOT NULL,
  `mg_error` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact_form`
--

INSERT INTO `contact_form` (`id`, `sendyouremail`, `mailfrom`, `sendto`, `subject`, `okmessage`, `errormessage`, `emailtext`, `firstname`, `lastname`, `email`, `phone`, `message`, `button`, `fn_holder`, `ln_holder`, `em_holder`, `ph_holder`, `mg_holder`, `fn_error`, `ln_error`, `em_error`, `mg_error`) VALUES
(1, 'info@indiekom.com', 'Demo contact form1', 'Demo contact form', 'New message from contact form', 'Contact form successfully submitted. Thank you, I will get back to you soon!', 'There was an error while submitting the form. Please try again later', 'You have new message from contact form', 'Your First Name', 'Last Name', 'Email', 'Phone', 'Message', 'Send me a Message', 'Please enter your firstname please *', 'Please enter your lastname *', 'Please enter your email *', 'Please enter your phone *', 'Message for me *', 'Firstname is required.', 'Lastname is required.', 'Valid email is required.', 'Please,leave us a message.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact_type`
--

CREATE TABLE `contact_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `contact_type`
--

INSERT INTO `contact_type` (`id`, `type`) VALUES
(1, 'material');

-- --------------------------------------------------------

--
-- Struktur dari tabel `countdown`
--

CREATE TABLE `countdown` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `bcolor` text NOT NULL,
  `bradius` text NOT NULL,
  `border` text NOT NULL,
  `dcolor` text NOT NULL,
  `dsize` text NOT NULL,
  `wcolor` text NOT NULL,
  `wsize` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `countdown`
--

INSERT INTO `countdown` (`id`, `year`, `month`, `day`, `bcolor`, `bradius`, `border`, `dcolor`, `dsize`, `wcolor`, `wsize`, `active`) VALUES
(1, 2019, 1, 12, '#ffffff', '50', '5', '#ffffff', '5', '#ffffff', '20', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `counter`
--

CREATE TABLE `counter` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `counter`
--

INSERT INTO `counter` (`id`, `number`) VALUES
(1, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `counters`
--

CREATE TABLE `counters` (
  `id` int(11) NOT NULL,
  `color` text NOT NULL,
  `title` text NOT NULL,
  `value` int(11) NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `counters`
--

INSERT INTO `counters` (`id`, `color`, `title`, `value`, `icon`) VALUES
(3, '#13bcb9', 'Lorem Ipsum', 598, 'fa-check'),
(7, '#13bcb9', 'Lorem Ipsum', 100000, 'fa-check'),
(8, '#13bcb9', 'Lorem Ipsum', 20000, 'fa-check'),
(9, '#13bcb9', 'Lorem Ipsum', 400, 'fa-check');

-- --------------------------------------------------------

--
-- Struktur dari tabel `counters_img`
--

CREATE TABLE `counters_img` (
  `id` int(11) NOT NULL DEFAULT 0,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `counters_img`
--

INSERT INTO `counters_img` (`id`, `image`) VALUES
(0, '2ad43d6e59dadb97208602269b6b6173.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `css`
--

CREATE TABLE `css` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `css`
--

INSERT INTO `css` (`id`, `description`) VALUES
(1, '/* Tablet Landscape */\r\n@media only screen and (min-width: 958px) and (max-width: 1079px) {\r\n}\r\n\r\n/* Tablet Portrait size to standard 960 (devices and browsers) */\r\n@media only screen and (min-width: 768px) and (max-width: 959px) {\r\n}\r\n\r\n/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */\r\n@media only screen and (min-width: 480px) and (max-width: 767px) {\r\n}\r\n\r\n/* Vertical Iphone */\r\n@media only screen and (max-width: 479px) {\r\n.cd-primary-nav{\r\nheight:70px;\r\n}\r\n.navbar-brand{\r\nmargin-top: 40px;\r\n}\r\n}\r\n\r\nimg{\r\nmargin: auto;\r\n}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `custom`
--

CREATE TABLE `custom` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `display_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `custom`
--

INSERT INTO `custom` (`id`, `title`, `slug`, `content`, `image`, `display_order`) VALUES
(49, 'The Test', 'the-test', '&lt;p&gt;Completely matrix leading-edge methods of empowerment after ubiquitous initiatives. Assertively plagiarize real-time systems vis-a-vis intuitive paradigms. Quickly facilitate state of the art bandwidth rather than market-driven communities. Intrinsicly facilitate process-centric web-readiness.&lt;/p&gt;&lt;p&gt;Objectively evisculate proactive strategic theme areas with high standards in mindshare. Quickly brand multimedia based solutions through backward-compatible leadership. Completely expedite resource-leveling models for functional alignments. Seamlessly monetize resource maximizing catalysts for change rather than scalable information. Uniquely repurpose interoperable total linkage rather than bleeding-edge e-commerce.&lt;/p&gt;&lt;p&gt;Globally pursue innovative supply chains without ethical methodologies. Quickly disintermediate fully researched infomediaries for go forward bandwidth. Quickly orchestrate virtual portals and empowered networks. Professionally maximize principle-centered convergence with next-generation convergence. Interactively harness sustainable channels rather than out-of-the-box methodologies.&lt;/p&gt;&lt;p&gt;Compellingly maximize economically sound customer service without open-source benefits. Rapidiously generate interoperable benefits through revolutionary resources. Collaboratively incubate real-time solutions after premium total linkage. Compellingly promote mission-critical interfaces rather than client-centered quality vectors. Quickly restore seamless benefits before sustainable vortals.&lt;/p&gt;&lt;p&gt;Assertively communicate quality partnerships through real-time mindshare. Compellingly redefine proactive synergy with sticky best practices. Interactively negotiate alternative vortals after client-centric potentialities. Intrinsicly target team building e-markets for real-time products. Seamlessly optimize superior infomediaries rather than enterprise-wide potentialities.&lt;/p&gt;&lt;p&gt;Assertively target visionary strategic theme areas after transparent outsourcing. Phosfluorescently deliver leveraged services via highly efficient imperatives. Assertively disseminate business value before quality outsourcing. Globally formulate real-time markets without client-centered collaboration and idea-sharing. Synergistically engineer interoperable functionalities rather than proactive bandwidth.&lt;/p&gt;&lt;p&gt;Continually drive multifunctional convergence without premium synergy. Competently harness leveraged intellectual capital for revolutionary alignments. Proactively maximize long-term high-impact total linkage with maintainable models. Progressively reinvent prospective metrics vis-a-vis cooperative intellectual capital. Interactively strategize bleeding-edge applications through magnetic metrics.&lt;/p&gt;&lt;p&gt;Quickly whiteboard exceptional methods of empowerment and client-centered materials. Compellingly transform intermandated solutions for intermandated potentialities. Rapidiously utilize collaborative results and effective best practices. Dramatically fabricate.&lt;/p&gt;', '0337de3e303b62951a5fa600a4d81bc5.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `font` text NOT NULL,
  `lcolor` text NOT NULL,
  `menu_color` text NOT NULL,
  `menu_bcolor` text NOT NULL,
  `menu_colorhover` text NOT NULL,
  `menu_bcolorhover` text NOT NULL,
  `background_color` text NOT NULL,
  `background_tcolor` text NOT NULL,
  `background_menu_color` text NOT NULL,
  `items_color` text NOT NULL,
  `footer_color` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dashboard`
--

INSERT INTO `dashboard` (`id`, `font`, `lcolor`, `menu_color`, `menu_bcolor`, `menu_colorhover`, `menu_bcolorhover`, `background_color`, `background_tcolor`, `background_menu_color`, `items_color`, `footer_color`) VALUES
(1, 'Questrial', '#007db3', '#ffffff', '#333333', '#ffffff', '#005b8f', '#006591', '#ffffff', '#333333', '#ffffff', '#3c3d41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `start`, `end`, `url`) VALUES
(13, 'Marketing', 'test', '2019-01-07 00:00:00', '2019-01-07 00:00:00', 'https://codecanyon.net/user/ezcode');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gdpr`
--

CREATE TABLE `gdpr` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gdpr`
--

INSERT INTO `gdpr` (`id`, `type`) VALUES
(1, 'eupopup eupopup-bottomleft');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hits`
--

CREATE TABLE `hits` (
  `page` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hits`
--

INSERT INTO `hits` (`page`, `count`) VALUES
('MARKETING', 298);

-- --------------------------------------------------------

--
-- Struktur dari tabel `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `time_accessed` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `info`
--

INSERT INTO `info` (`id`, `ip_address`, `user_agent`, `time_accessed`) VALUES
(1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 17:51:59'),
(2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 17:59:39'),
(3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:01:02'),
(4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:17:17'),
(5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:19:49'),
(6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:22:26'),
(7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:23:32'),
(8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:26:14'),
(9, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:26:35'),
(10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:28:57'),
(11, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:30:02'),
(12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:33:58'),
(13, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:38:32'),
(14, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:39:32'),
(15, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:42:55'),
(16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:43:36'),
(17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:47:54'),
(18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:51:51'),
(19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:52:46'),
(20, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:53:33'),
(21, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:54:43'),
(22, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:55:26'),
(23, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:55:59'),
(24, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 18:57:05'),
(25, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:02:50'),
(26, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:03:55'),
(27, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:05:30'),
(28, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:06:17'),
(29, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:07:02'),
(30, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:09:28'),
(31, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:12:58'),
(32, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:14:12'),
(33, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:24:02'),
(34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:25:51'),
(35, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:27:40'),
(36, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:34:41'),
(37, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:35:21'),
(38, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:36:19'),
(39, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:37:06'),
(40, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:40:34'),
(41, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:41:39'),
(42, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:43:03'),
(43, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:43:26'),
(44, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-26 19:50:37'),
(45, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-26 19:50:41'),
(46, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:50:53'),
(47, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:50:56'),
(48, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:51:12'),
(49, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:53:09'),
(50, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-26 19:54:12'),
(51, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:08:11'),
(52, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:17:08'),
(53, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:17:31'),
(54, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:21:03'),
(55, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:25:27'),
(56, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:28:00'),
(57, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:28:38'),
(58, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:28:45'),
(59, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:34:56'),
(60, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:37:38'),
(61, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:38:42'),
(62, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:39:50'),
(63, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:42:59'),
(64, '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1', '2019-12-27 03:45:25'),
(65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 03:59:09'),
(66, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 04:04:40'),
(67, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 04:05:03'),
(68, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 04:07:07'),
(69, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 04:12:45'),
(70, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 04:18:35'),
(71, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:05:29'),
(72, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:10:22'),
(73, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:12:32'),
(74, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:12:42'),
(75, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:15:42'),
(76, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:15:52'),
(77, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36', '2019-12-27 06:16:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `latest_img`
--

CREATE TABLE `latest_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `latest_img`
--

INSERT INTO `latest_img` (`id`, `image`) VALUES
(0, 'a52ae9e2efd6d0e3d9bf61affb6c0f39.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `loader_type`
--

CREATE TABLE `loader_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `loader_type`
--

INSERT INTO `loader_type` (`id`, `type`) VALUES
(1, 'Chasing Dots');

-- --------------------------------------------------------

--
-- Struktur dari tabel `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `logo_text` text NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `logo`
--

INSERT INTO `logo` (`id`, `logo_text`, `image`) VALUES
(0, 'logo', '192fddbd45b7d11b156fab82bbf5d3da.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `map`
--

CREATE TABLE `map` (
  `id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `zoom` text NOT NULL,
  `api` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `map`
--

INSERT INTO `map` (`id`, `latitude`, `longitude`, `title`, `description`, `zoom`, `api`) VALUES
(1, 47.6176679931594, -122.20882885158062, 'Marketing - Corporate &amp; Enterprise Website CMS', 'Marketing - Corporate &amp; Enterprise Website CMS', '15', 'AIzaSyAvOhtbl4wK5vOq3YZuc-gkldJTxI13eY4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `media`
--

INSERT INTO `media` (`id`, `image`) VALUES
(23, '0337de3e303b62951a5fa600a4d81bc5.jpg'),
(24, '5823a1fb1f92629925e3f096d8547a4e.jpg'),
(25, 'cc7770dca3c9a500be8baa353b7ccce3.jpg'),
(26, '9d657e0d3b8b949dea032b7c6c92b70e.jpeg'),
(27, '1281ff6ca525eddf0c0de5d8000c45b4.jpg'),
(28, '8f36b6b416e252188feaa1fc61a73925.jpg'),
(29, '0fbaceb36daddacaf2226e2aae885c60.jpg'),
(30, '000e45bfcab7a806b690788f4d60e31a.jpg'),
(31, 'f4056f50b19e35b531ba8c9a2bda67df.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_type`
--

CREATE TABLE `menu_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu_type`
--

INSERT INTO `menu_type` (`id`, `type`) VALUES
(1, 'topbar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `particles`
--

CREATE TABLE `particles` (
  `id` int(11) NOT NULL,
  `bpcolor` text NOT NULL,
  `dpcolor` text NOT NULL,
  `ppcolor` text NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `particles`
--

INSERT INTO `particles` (`id`, `bpcolor`, `dpcolor`, `ppcolor`, `type`) VALUES
(1, '#13bcb9', '#2cb5e8', '#ffffff', 'particles');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `name` text NOT NULL,
  `website` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `partners`
--

INSERT INTO `partners` (`id`, `image`, `name`, `website`) VALUES
(7, '214122e2cd6a719ee8b4ca496d58f4b4.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(8, '2ded694aee561e0728e10a9da8bb5289.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(9, '6029b7beee3a99512d7b17507d0d973b.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(10, 'db936d49ec2e3199021429bac7152b4c.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(11, '7e78e7dafe61b79d22aa78af19a6211f.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(12, '50f2a50254ac4605e08c5c559001f710.png', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(13, '5783b550b93f66d50acff4ffba04118f.png', 'Lorem Ipsum', 'https://Lorem.Ipsum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partnerspage_img`
--

CREATE TABLE `partnerspage_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `partnerspage_img`
--

INSERT INTO `partnerspage_img` (`id`, `image`) VALUES
(0, '2399d649db2429e34e0d2485d38ac997.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partners_img`
--

CREATE TABLE `partners_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `partners_img`
--

INSERT INTO `partners_img` (`id`, `image`) VALUES
(0, '5fe85a1d0ec25eb1a26ba292f81f9a75.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `category` text NOT NULL,
  `title` text NOT NULL,
  `link` text NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `portfolio`
--

INSERT INTO `portfolio` (`id`, `image`, `category`, `title`, `link`, `description`) VALUES
(20, '2c0077d97c6874d174754dafbe3b7d1b.jpg', 'Design', 'Lorem Ipsum', '', 'Synergistically seize wireless catalysts for change rather than cross-media alignments. Dynamically utilize an expanded array of web-readiness with adaptive ideas. Progressively communicate cooperative deliverables vis-a-vis seamless e-markets. Globally synergize magnetic strategic.'),
(21, '269ef20a92eee85c69de0be01e65e557.jpg', 'Design', 'Lorem Ipsum', '', 'Synergistically seize wireless catalysts for change rather than cross-media alignments. Dynamically utilize an expanded array of web-readiness with adaptive ideas. Progressively communicate cooperative deliverables vis-a-vis seamless e-markets. Globally synergize magnetic strategic.'),
(22, 'fd183615d74f2b06edba243e132b9c6c.jpg', 'Design', 'Lorem Ipsum', '', 'Professionally architect interactive networks vis-a-vis cooperative platforms. Monotonectally fabricate reliable &quot;outside the box&quot; thinking without end-to-end infrastructures. Seamlessly exploit equity invested data through dynamic technologies. Dramatically innovate equity invested results through.'),
(23, '856353943f9658259fef9cc5842fa118.jpg', 'Design', 'Lorem Ipsum', '', 'Quickly fabricate end-to-end potentialities through bleeding-edge networks. Holisticly coordinate multidisciplinary deliverables vis-a-vis customized communities. Collaboratively leverage other&#039;s client-focused total linkage for sticky infomediaries. Quickly unleash intuitive innovation after best-of-breed e-services. Efficiently.'),
(24, 'e4764bfa4f940acb9f3a981cdb16eeea.jpg', 'Design', 'Lorem Ipsum', '', 'Efficiently repurpose just in time expertise with technically sound technologies. Authoritatively implement intermandated information and distinctive customer service. Collaboratively restore global mindshare vis-a-vis installed base methodologies. Efficiently monetize tactical collaboration and.'),
(25, '3c0ef3f93effa6c08e67bab5a75658f4.jpg', 'Design', 'Lorem Ipsum', '', 'Efficiently repurpose just in time expertise with technically sound technologies. Authoritatively implement intermandated information and distinctive customer service. Collaboratively restore global mindshare vis-a-vis installed base methodologies. Efficiently monetize tactical collaboration and.'),
(26, '676941496177800f23d5c0f241d61ac1.jpg', 'Design', 'Lorem Ipsum', '', 'Interactively aggregate orthogonal value after economically sound networks. Dynamically customize focused e-services whereas impactful data. Authoritatively foster functional relationships for wireless platforms. Holisticly formulate highly efficient customer service without accurate data.'),
(27, 'ca6e3d73d647e6c86e02f964f1eed7b0.jpg', 'Design', 'Lorem Ipsum', '', 'Interactively aggregate orthogonal value after economically sound networks. Dynamically customize focused e-services whereas impactful data. Authoritatively foster functional relationships for wireless platforms. Holisticly formulate highly efficient customer service without accurate data.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `portfolio_img`
--

CREATE TABLE `portfolio_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `portfolio_img`
--

INSERT INTO `portfolio_img` (`id`, `image`) VALUES
(0, '5cbfb09fb43063115ae4bea08a93b9d2.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `portfolio_type`
--

CREATE TABLE `portfolio_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `portfolio_type`
--

INSERT INTO `portfolio_type` (`id`, `type`) VALUES
(1, 'portfolio');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `date` text NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pricing`
--

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `hint` text NOT NULL,
  `price` int(11) NOT NULL,
  `description` text NOT NULL,
  `purchase` text NOT NULL,
  `color` text NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pricing`
--

INSERT INTO `pricing` (`id`, `title`, `hint`, `price`, `description`, `purchase`, `color`, `link`) VALUES
(1, 'Business', 'Lorem Ipsum', 299, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#7b5aff', ''),
(2, 'Professional', 'Lorem Ipsum', 99, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#006cba', ''),
(3, 'Personal', 'Lorem Ipsum', 29, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#13bcb9', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pricing_number`
--

CREATE TABLE `pricing_number` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pricing_number`
--

INSERT INTO `pricing_number` (`id`, `number`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pricing_type`
--

CREATE TABLE `pricing_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pricing_type`
--

INSERT INTO `pricing_type` (`id`, `type`) VALUES
(1, 'default');

-- --------------------------------------------------------

--
-- Struktur dari tabel `privacypage_img`
--

CREATE TABLE `privacypage_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `privacypage_img`
--

INSERT INTO `privacypage_img` (`id`, `image`) VALUES
(0, '5969e161a04ec66b3194201f4c0b4d6c.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `progress`
--

CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `percent` text NOT NULL,
  `color` text NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `progress`
--

INSERT INTO `progress` (`id`, `percent`, `color`, `height`) VALUES
(1, '80', '#13bcb9', 50);

-- --------------------------------------------------------

--
-- Struktur dari tabel `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `keywords` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `seo`
--

INSERT INTO `seo` (`id`, `keywords`, `title`, `description`) VALUES
(1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'INDIEKOM - IT Solution', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `logo` text NOT NULL,
  `color` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`id`, `logo`, `color`, `title`, `description`, `type`) VALUES
(1, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(2, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(3, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(4, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(6, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'From T-shirts to the tags that hang on them, we can make it', 'icon'),
(7, 'fa-heart', '#ffffff', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon');

-- --------------------------------------------------------

--
-- Struktur dari tabel `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `services`
--

INSERT INTO `services` (`id`, `number`) VALUES
(1, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `services_img`
--

CREATE TABLE `services_img` (
  `id` int(11) NOT NULL,
  `parallax` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `services_img`
--

INSERT INTO `services_img` (`id`, `parallax`) VALUES
(0, 'c743de97e0768502970fc02b91021c60.jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `services_type`
--

CREATE TABLE `services_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `services_type`
--

INSERT INTO `services_type` (`id`, `type`) VALUES
(1, 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service_images`
--

CREATE TABLE `service_images` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `service_images`
--

INSERT INTO `service_images` (`id`, `image`, `title`, `description`, `type`) VALUES
(1, '2ccf5f7db7e2bc1f1896db9ad1179672.jpg', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets. Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(2, '83ada52e73758e224481917a0eeda63b.jpg', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets. Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(3, 'd023a9dcd2538bbc91d4736c29ca16de.jpg', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets. Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(5, '3ecca8349051aa4442d9d5950357ce6b.jpeg', 'Lorem Ipsum', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets. Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `about` int(11) NOT NULL,
  `contacts` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `privacy` int(11) NOT NULL,
  `calendar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `about`, `contacts`, `services`, `partners`, `portfolio`, `blog`, `custom`, `privacy`, `calendar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `skills`
--

CREATE TABLE `skills` (
  `id` int(11) NOT NULL,
  `color` text NOT NULL,
  `skill` text NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `skills`
--

INSERT INTO `skills` (`id`, `color`, `skill`, `percent`) VALUES
(1, '#00ebdd', 'Lorem Ipsum', 80),
(3, '#42a14c', 'Lorem Ipsum', 100),
(4, '#8fff00', 'Lorem Ipsum', 70),
(5, '#edd13b', 'Lorem Ipsum', 95);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slide`
--

INSERT INTO `slide` (`id`, `title`, `description`, `image`, `type`) VALUES
(1, 'Lorem Ipsum', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.', 'd886d748421fda9ae3e69721f1e66a83.jpg', 'slide'),
(2, 'Lorem Ipsum', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.', '6c658b1dc98dbfab9166602eaa81f147.jpg', 'slide'),
(4, 'Lorem Ipsum', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.', '6f03cb5d26120fc41001a4569e4c1e59.jpg', 'slide'),
(5, 'Lorem Ipsum', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.', '6712bc91eb9f7edaedf07be9ee44fbe4.jpeg', 'slide'),
(6, 'Lorem Ipsum', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.', '92b6b2f9080d053923dd647944abb2d3.jpg', 'slide');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider_type`
--

CREATE TABLE `slider_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slider_type`
--

INSERT INTO `slider_type` (`id`, `type`) VALUES
(1, 'fullscreen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `static`
--

CREATE TABLE `static` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `static`
--

INSERT INTO `static` (`id`, `image`, `type`) VALUES
(0, 'eda5597a4b90a1d0cc674957cb75df0e.jpg', 'static');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stats_day`
--

CREATE TABLE `stats_day` (
  `date` date NOT NULL DEFAULT '0000-00-00',
  `hits` mediumint(8) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stats_day`
--

INSERT INTO `stats_day` (`date`, `hits`) VALUES
('2019-01-05', 7),
('2019-01-06', 189),
('2019-01-07', 81),
('2019-01-08', 9),
('2019-01-22', 1),
('2019-01-23', 13),
('2019-12-26', 73),
('2019-12-27', 47);

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `social_fb` text NOT NULL,
  `social_in` text NOT NULL,
  `social_go` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`id`, `image`, `name`, `description`, `social_fb`, `social_in`, `social_go`) VALUES
(10, 'e9540f35dd8751aa4a31f9466c123063.jpg', 'Lorem Ipsum', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(11, '0f629b70f8cc80011645d7e47d829a26.jpg', 'Lorem Ipsum', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(12, 'e4d3ab39c6f0d1da846b2d77e8abb9ce.jpg', 'Lorem Ipsum', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(13, '4d0473d58522d736f9119c0b3daf0bcc.jpg', 'Lorem Ipsum', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#');

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `name` text NOT NULL,
  `website` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `testimonials`
--

INSERT INTO `testimonials` (`id`, `image`, `description`, `name`, `website`) VALUES
(1, '1e2905dd227f3376a5511334706f7c82.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(2, '54a893adf973eee208a3578fee8be7c2.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(3, 'ed92804a12f41fae081032167d9ff502.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Lorem Ipsum', 'https://Lorem.Ipsum'),
(4, 'af70e85ac56bdb1791bb6df9b30c6ff3.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Lorem Ipsum', 'https://Lorem.Ipsum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `testimonials_img`
--

CREATE TABLE `testimonials_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `testimonials_img`
--

INSERT INTO `testimonials_img` (`id`, `image`) VALUES
(0, 'af2ca0ca6ce8a65cd128d222452a559c.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `text`
--

CREATE TABLE `text` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `text`
--

INSERT INTO `text` (`id`, `title`, `description`) VALUES
(1, 'INDIEKOM - IT Solutions', 'Businesses come to us when they have a problem. We want to help you ?nd a solution. When it comes to choosing the right wholesale supplier for your business, you can count on indiekom for quality products at affordable prices. We offer a wide selection of goods to individuals or businesses, and are located right in the greater Jakarta. Contact us to see what we’ve got in store for you today.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textabout`
--

CREATE TABLE `textabout` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textabout`
--

INSERT INTO `textabout` (`id`, `description`) VALUES
(1, '&lt;p style=&quot;text-align: justify;&quot;&gt;&lt;span style=&quot;color: rgb(24, 24, 24); font-family: proxima-n-w01-reg, sans-serif; font-size: 15px;&quot;&gt;Industry Industrial Engineering Leading Telecommunication Equipment Suppliers, supplying products and individual and company needs. We were established early in 2019 with the work experience of its founders in online stores, IT distributors, IT Services &amp;amp; Solutions at Multi national companies, and have been located in Jakarta ever since . Learn all about Indo and what we offer by calling us. We will be happy to assist you with any information about certain products or services.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textblog`
--

CREATE TABLE `textblog` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textblog`
--

INSERT INTO `textblog` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;Just some interesting information!&lt;/span&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textcontacts`
--

CREATE TABLE `textcontacts` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textcontacts`
--

INSERT INTO `textcontacts` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;color: rgb(24, 24, 24); font-family: proxima-n-w01-reg, sans-serif; font-size: 15px;&quot;&gt;Industry Industrial Engineering Leading Telecommunication Equipment Suppliers, supplying products and individual and company needs. We were established early in 2019 with the work experience of its founders in online stores, IT distributors, IT Services &amp;amp; Solutions at Multi national companies, and have been located in Jakarta ever since . Learn all about Indo and what we offer by calling us. We will be happy to assist you with any information about certain products or services.&lt;/span&gt;&lt;br&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textpartners`
--

CREATE TABLE `textpartners` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textpartners`
--

INSERT INTO `textpartners` (`id`, `description`) VALUES
(1, '&lt;span style=&quot;font-size: 24px;&quot;&gt;Who we work with!&lt;/span&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textportfolio`
--

CREATE TABLE `textportfolio` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textportfolio`
--

INSERT INTO `textportfolio` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;Just check out our work!&lt;/span&gt;&lt;br&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textprivacy`
--

CREATE TABLE `textprivacy` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textprivacy`
--

INSERT INTO `textprivacy` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;p&gt;&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;&lt;span style=&quot;font-size: 36px;&quot;&gt;Privacy Notice&lt;/span&gt;&lt;/strong&gt;&lt;/center&gt;&lt;br&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;&lt;br&gt;&lt;/strong&gt;&lt;/center&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;This privacy notice discloses the privacy practices for &lt;span style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;&quot;&gt;(website address)&lt;/span&gt;. This privacy notice applies solely to information collected by this website. It will notify you of the following:&lt;/p&gt;&lt;ol type=&quot;1&quot; style=&quot;box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style-position: initial; list-style-image: initial; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;What choices are available to you regarding the use of your data.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;The security procedures in place to protect the misuse of your information.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;How you can correct any inaccuracies in the information.&lt;/li&gt;&lt;/ol&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Information Collection, Use, and Sharing&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Your Access to and Control Over Information&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:&lt;/p&gt;&lt;ul style=&quot;box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style: none; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;See what data we have about you, if any.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Change/correct any data we have about you.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Have us delete any data we have about you.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Express any concern you have about our use of your data.&lt;/li&gt;&lt;/ul&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Security&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for &quot;https&quot; at the beginning of the address of the Web page.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Optional Clauses&lt;/strong&gt;&lt;/center&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If your site has a registration page that customers must complete to do business with you, insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Registration&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;In order to use this website, a user must first complete the registration form. During registration a user is required to give certain information (such as name and email address). This information is used to contact you about the products/services on our site in which you have expressed interest. At your option, you may also provide demographic information (such as gender or age) about yourself, but it is not required.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you take and fill orders on your site, insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Orders&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We request information from you on our order form. To buy from us, you must provide contact information (like name and shipping address) and financial information (like credit card number, expiration date). This information is used for billing purposes and to fill your orders. If we have trouble processing an order, we&#039;ll use this information to contact you.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you use cookies or other devices that track site visitors, insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Cookies&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We use &quot;cookies&quot; on this site. A cookie is a piece of data stored on a site visitor&#039;s hard drive to help us improve your access to our site and identify repeat visitors to our site. For instance, when we use a cookie to identify you, you would not have to log in a password more than once, thereby saving time while on our site. Cookies can also enable us to track and target the interests of our users to enhance the experience on our site. Usage of a cookie is in no way linked to any personally identifiable information on our site.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If other organizations use cookies or other devices that track site visitors to your site, insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;Some of our business partners may use cookies on our site (for example, advertisers). However, we have no access to or control over these cookies.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you share information collected on your site with other parties, insert one or more of these paragraphs in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Sharing&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We share aggregated demographic information with our partners and advertisers. This is not linked to any personal information that can identify any individual person.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;And/or: &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We use an outside shipping company to ship orders, and a credit card processing company to bill users for goods and services. These companies do not retain, share, store or use personally identifiable information for any secondary purposes beyond filling your order.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;And/or: &lt;br style=&quot;box-sizing: content-box;&quot;&gt;We partner with another party to provide specific services. When the user signs up for these services, we will share names, or other contact information that is necessary for the third party to provide these services. These parties are not allowed to use personally identifiable information except for the purpose of providing these services.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If your site has links to other sites, you might insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Links&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;This website contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you ever collect data through surveys or contests on your site, you might insert a paragraph like this in your privacy notice:&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Surveys &amp; Contests&lt;/strong&gt; &lt;br style=&quot;box-sizing: content-box;&quot;&gt;From time-to-time our site requests information via surveys or contests. Participation in these surveys or contests is completely voluntary and you may choose whether or not to participate and therefore disclose this information. Information requested may include contact information (such as name and shipping address), and demographic information (such as zip code, age level). Contact information will be used to notify the winners and award prizes. Survey information will be used for purposes of monitoring or improving the use and satisfaction of this site.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `textservices`
--

CREATE TABLE `textservices` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `textservices`
--

INSERT INTO `textservices` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;These are the awesome services we provide!&lt;/span&gt;&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Struktur dari tabel `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `type`
--

INSERT INTO `type` (`id`, `title`) VALUES
(1, 'Marketing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `salt`, `email`, `lang`) VALUES
(5, 'admin', '6324f0554be47328d92a03078e3f3e396bb5e6b67fbdc58d4987337062ab7588', '34fa3861c848add', 'admin@admin.com', 'en');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE `video` (
  `v_id` int(11) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video`
--

INSERT INTO `video` (`v_id`, `video_name`, `type`) VALUES
(0, 'Home - INDIEKOM.mp4', 'own');

-- --------------------------------------------------------

--
-- Struktur dari tabel `youtube`
--

CREATE TABLE `youtube` (
  `id` int(11) NOT NULL,
  `video_id` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `youtube`
--

INSERT INTO `youtube` (`id`, `video_id`, `type`) VALUES
(1, 'B2vmlBXRuBU', 'youtube');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `about_img`
--
ALTER TABLE `about_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `activeblog`
--
ALTER TABLE `activeblog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `activecontacts`
--
ALTER TABLE `activecontacts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `activepartners`
--
ALTER TABLE `activepartners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `activeprivacy`
--
ALTER TABLE `activeprivacy`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `blog_img`
--
ALTER TABLE `blog_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `blog_type`
--
ALTER TABLE `blog_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `calendar_img`
--
ALTER TABLE `calendar_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `contactform_img`
--
ALTER TABLE `contactform_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `counter`
--
ALTER TABLE `counter`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `counters`
--
ALTER TABLE `counters`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `counters_img`
--
ALTER TABLE `counters_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `custom`
--
ALTER TABLE `custom`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gdpr`
--
ALTER TABLE `gdpr`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `latest_img`
--
ALTER TABLE `latest_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `loader_type`
--
ALTER TABLE `loader_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partnerspage_img`
--
ALTER TABLE `partnerspage_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `partners_img`
--
ALTER TABLE `partners_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `portfolio_img`
--
ALTER TABLE `portfolio_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pricing_number`
--
ALTER TABLE `pricing_number`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pricing_type`
--
ALTER TABLE `pricing_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `privacypage_img`
--
ALTER TABLE `privacypage_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `stats_day`
--
ALTER TABLE `stats_day`
  ADD PRIMARY KEY (`date`);

--
-- Indeks untuk tabel `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `testimonials_img`
--
ALTER TABLE `testimonials_img`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textblog`
--
ALTER TABLE `textblog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textcontacts`
--
ALTER TABLE `textcontacts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textpartners`
--
ALTER TABLE `textpartners`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textportfolio`
--
ALTER TABLE `textportfolio`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textprivacy`
--
ALTER TABLE `textprivacy`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `textservices`
--
ALTER TABLE `textservices`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `counters`
--
ALTER TABLE `counters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `custom`
--
ALTER TABLE `custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT untuk tabel `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT untuk tabel `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pricing`
--
ALTER TABLE `pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `skills`
--
ALTER TABLE `skills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
