<?php
require 'admin/functions/functions.php';
// CSRF Protection
require 'admin/functions/CSRF_Protect.php';

echo languageFront();

$csrf = new CSRF_Protect();

// Error Reporting Active
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--SEO META TAGS-->
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from seo");
	$row = mysqli_fetch_assoc($sql);
	
	echo "<title>".$row['title']."</title>
    <meta name='description' content='".$row['description']."'>
    <meta name='keywords' content='".$row['keywords']."'>";
	
	?>
	
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from post");
	$row = mysqli_fetch_assoc($sql);
	?>
	
	<!--FACEBOOK META TAGS-->
	<meta property="og:url"           content="<?php echo 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="<?php echo $row['title']; ?>" />
	<meta property="og:description"   content="<?php echo $row['description']; ?>" />
	<meta property="og:image"         content='<?php echo '/assets/img/uploads/other/'.$row['image']; ?>' />
	
	<link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon" >
	
    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.css">
	<link rel="stylesheet" href="assets/css/animate.css">
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="admin/assets/js/sweetalert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="admin/assets/css/sweetalert.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="../libs/jquery/dist/jquery.min.js"><\/script>')</script>
	<script src="admin/assets/js/jquery.vide.js"></script>
	
	<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css">
	
	<!-- RGPD -->
	<link rel="stylesheet" type="text/css" href="assets/css/jquery-eu-cookie-law-popup.css"/>
	<script src="assets/js/jquery-eu-cookie-law-popup.js"></script>
	
	<link href="admin/assets/css/dataTables.bootstrap.css" rel="stylesheet">
	
	<link href='assets/css/fullcalendar.min.css' rel='stylesheet' />
	
	
	<!-- Custom CSS -->
    <link href="assets/css/styles.css" rel="stylesheet">
	<script>
		jQuery(window).load(function() {
			jQuery("#loading-center").delay(800).fadeOut(800);
			jQuery("#loading").delay(800).fadeOut(800);
		});
	</script>
	<?php
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	$getfont = $row['font'];
	$font = str_replace(' ', '+', $getfont);
	
	if ($getfont != ""){
		echo "<link href='https://fonts.googleapis.com/css?family=".$font."' rel='stylesheet'>";
	}else{
		echo " ";
	}
	?>
	
	<!-- Custom CSS Area -->
	<style>
	
	<?php include('css.php') ;	?>	
	
	</style>
	
</head>

<body class="<?php 
	global $conection;
	$sql = mysqli_query($conection,"select privacy from settings");
	$row = mysqli_fetch_assoc($sql);
	$privacy = $row['privacy'];
	
	$sql1 = mysqli_query($conection,"select type from gdpr");
	$row = mysqli_fetch_assoc($sql1);
	$type = $row['type'];
		
	if ($privacy == '1'){	
		echo $row['type'];
	}
	if ($privacy == '0'){	
		echo '';
	}?>">

 <a id="top" style="visibility:hidden;" class="page-scroll"></a>
 
	
<?php include('preloader.php'); ?>	

	
<?php include('navigation.php'); ?>


	