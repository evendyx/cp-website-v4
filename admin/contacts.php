<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('FOOTER_INFORMATION');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li class="active"><?php echo lang('FOOTER_INFORMATION');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatecontacts" >
								<fieldset>									
								<?php $csrf->echoInputField(); ?>
								<?php echo contacts();?>

								</fieldset>
								
								<br/>
							  
							 <!-- Button -->
							<div class="form-group">
							  <label class="col-md-10 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updatecontacts" class="btn btn-primary" value="Update" />
							  </div>
							</div>
							
							 <?php
						  
							  if(isset($_POST['updatecontacts']))
								{
									update_contacts($_POST['address'], $_POST['code'], $_POST['city'], $_POST['latitude'], $_POST['longitude'], $_POST['title'], $_POST['email'], $_POST['phone'], $_POST['footer'], $_POST['facebook'], $_POST['twitter'], $_POST['linkedin'], $_POST['google'], $_POST['youtube'], $_POST['instagram'], $_POST['pinterest'], $_POST['tumblr'], $_POST['vine'], $_POST['snapchat'], $_POST['reddit'], $_POST['flickr'], $_POST['soundcloud'], $_POST['whatsapp'], $_POST['slack'], $_POST['xing']);
								}
							?>

						</form>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

		</div>
		<!-- /.row -->
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->		

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>