<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_SKILL');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="skills.php"><?php echo lang('SKILLS');?></a></li>
				<li class="active"><?php echo lang('EDIT_SKILL');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					 <form id="editSkill" method="post" enctype="multipart/form-data" class="form-horizontal" name="editSkill">
										
					<?php $csrf->echoInputField(); ?>
					
					<fieldset>
					
					<div class="form-group">
						<label class='col-md-2 control-label' for='color'><?php echo lang('SKILL_COLOR');?></label>
							<div class='col-md-6'>
							  <div id="cp1" class="input-group colorpicker-component">
								<input type="text" name="color" value="<?php echo getSkillColor($_GET['id']); ?>" class="form-control" />
								<span class="input-group-addon"><i></i></span>
							</div>
						</div>
					</div>
						
					<?php echo editSkill($_GET['id']); ?>

					<!-- Button -->
					<div class="form-group">
						<label class="col-md-6 control-label" for="singlebutton"></label>
						<div class="col-md-4">
							<input type="submit" name="editSkill" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
						</div>
					</div>
					
					<script>
						$(function() {
							$('#cp1').colorpicker();
						});
					</script>

					</fieldset>
				</form>
				<?php

				  if(isset($_POST['editSkill']))
				  {
					updateSkill($_GET['id'],$_POST['color'],htmlspecialchars($_POST['skill'], ENT_QUOTES),$_POST['percent']);
				  }

				?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
    </div>
    <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>