<?php

$SQL[] = "

CREATE TABLE IF NOT EXISTS `about_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `about_img` (`id`, `image`) VALUES
(0, '325f0409787292bb66d2e6f557efdfce.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activeabout` (
  `id` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activeabout` (`id`, `services`, `testimonials`, `partners`, `subscribe`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 1, 0, 1, 0, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activeblog` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activeblog` (`id`, `partners`, `subscribe`) VALUES
(1, 1, 0);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activecalendar` (
  `id` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activecalendar` (`id`, `services`, `testimonials`, `partners`, `subscribe`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activecontacts` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `form` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activecontacts` (`id`, `partners`, `map`, `form`, `subscribe`) VALUES
(1, 0, 1, 1, 0);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activehomepage` (
  `id` int(11) NOT NULL,
  `about` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `skills2` int(11) NOT NULL,
  `calendar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activehomepage` (`id`, `about`, `services`, `testimonials`, `partners`, `subscribe`, `portfolio`, `team`, `pricing`, `blog`, `skills2`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activepartners` (
  `id` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activeportfolio` (
  `id` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activeportfolio` (`id`, `partners`, `subscribe`) VALUES
(1, 1, 0);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activeprivacy` (
  `id` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activeprivacy` (`id`, `subscribe`) VALUES
(1, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `activeservices` (
  `id` int(11) NOT NULL,
  `testimonials` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `subscribe` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `team` int(11) NOT NULL,
  `pricing` int(11) NOT NULL,
  `skills2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `activeservices` (`id`, `testimonials`, `partners`, `subscribe`, `portfolio`, `team`, `pricing`, `skills2`) VALUES
(1, 0, 0, 0, 0, 0, 0, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `background` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `background` (`id`, `type`) VALUES
(1, 'slide');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `blog_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `blog_img` (`id`, `image`) VALUES
(0, '13192940684e3529de4dfff0a1d65323.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `blog_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `blog_type` (`id`, `type`, `number`) VALUES
(1, 'Pagination', 4);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `calendar_img` (
  `id` int(11) NOT NULL DEFAULT '0',
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `calendar_img` (`id`, `image`) VALUES
(0, 'cfc25f7b84875946bec994c4ad486ee7.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `slug` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

";

$SQL[] = "

INSERT INTO `categories` (`id`, `name`, `slug`) VALUES
(8, 'Design', 'Design'),
(7, 'Brainstorm', 'Brainstorm'),
(9, 'Comunication', 'Comunication');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

";

$SQL[] = "

INSERT INTO `clients` (`id`, `name`, `email`) VALUES
(9, 'John Doe', 'johndoe@ezcode.pt'),
(10, 'Tony Martin', 'tonymartin@apps.com'),
(11, 'Valter Dog', 'valterdog@dascene.com');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `comingsoon` (
  `id` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `countdown` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `comingsoon` (`id`, `progress`, `countdown`) VALUES
(1, 1, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `contactform_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `contactform_img` (`id`, `image`) VALUES
(0, 'bd65c2e5dbe132738dad666d72b809f0.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `code` text NOT NULL,
  `city` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `title` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `footer` text NOT NULL,
  `facebook` text NOT NULL,
  `twitter` text NOT NULL,
  `linkedin` text NOT NULL,
  `google` text NOT NULL,
  `youtube` text NOT NULL,
  `instagram` text NOT NULL,
  `pinterest` text NOT NULL,
  `tumblr` text NOT NULL,
  `vine` text NOT NULL,
  `snapchat` text NOT NULL,
  `reddit` text NOT NULL,
  `flickr` text NOT NULL,
  `soundcloud` text NOT NULL,
  `whatsapp` text NOT NULL,
  `slack` text NOT NULL,
  `xing` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `contacts` (`id`, `address`, `code`, `city`, `latitude`, `longitude`, `title`, `email`, `phone`, `footer`, `facebook`, `twitter`, `linkedin`, `google`, `youtube`, `instagram`, `pinterest`, `tumblr`, `vine`, `snapchat`, `reddit`, `flickr`, `soundcloud`, `whatsapp`, `slack`, `xing`) VALUES
(1, 'Seattle', '23456-3453', 'Seattle', '47.6060643', '-122.3398535', 'Marketing - Corporate & Enterprise Website CMS', 'geral@marketing.com', 245333444, 'Copyright Â© EZCode 2018', 'https://www.facebook.com/ezcode.pt/', '#', '#', '#', '#', '', '', '', '', '', '', '', '', '', '', '');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `contact_form` (
  `id` int(11) NOT NULL,
  `sendyouremail` text NOT NULL,
  `mailfrom` text NOT NULL,
  `sendto` text NOT NULL,
  `subject` text NOT NULL,
  `okmessage` text NOT NULL,
  `errormessage` text NOT NULL,
  `emailtext` text NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `message` text NOT NULL,
  `button` text NOT NULL,
  `fn_holder` text NOT NULL,
  `ln_holder` text NOT NULL,
  `em_holder` text NOT NULL,
  `ph_holder` text NOT NULL,
  `mg_holder` text NOT NULL,
  `fn_error` text NOT NULL,
  `ln_error` text NOT NULL,
  `em_error` text NOT NULL,
  `mg_error` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `contact_form` (`id`, `sendyouremail`, `mailfrom`, `sendto`, `subject`, `okmessage`, `errormessage`, `emailtext`, `firstname`, `lastname`, `email`, `phone`, `message`, `button`, `fn_holder`, `ln_holder`, `em_holder`, `ph_holder`, `mg_holder`, `fn_error`, `ln_error`, `em_error`, `mg_error`) VALUES
(1, 'geral@marketing.com', 'Demo contact form1', 'Demo contact form', 'New message from contact form', 'Contact form successfully submitted. Thank you, I will get back to you soon!', 'There was an error while submitting the form. Please try again later', 'You have new message from contact form', 'Your First Name', 'Last Name', 'Email', 'Phone', 'Message', 'Send me a Message', 'Please enter your firstname please *', 'Please enter your lastname *', 'Please enter your email *', 'Please enter your phone *', 'Message for me *', 'Firstname is required.', 'Lastname is required.', 'Valid email is required.', 'Please,leave us a message.');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `contact_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `contact_type` (`id`, `type`) VALUES
(1, 'material');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `countdown` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `bcolor` text NOT NULL,
  `bradius` text NOT NULL,
  `border` text NOT NULL,
  `dcolor` text NOT NULL,
  `dsize` text NOT NULL,
  `wcolor` text NOT NULL,
  `wsize` text NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `countdown` (`id`, `year`, `month`, `day`, `bcolor`, `bradius`, `border`, `dcolor`, `dsize`, `wcolor`, `wsize`, `active`) VALUES
(1, 2019, 1, 12, '#13bcb9', '50', '5', '#ffffff', '5', '#ffffff', '20', 0);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `counter` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `counter` (`id`, `number`) VALUES
(1, 4);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `counters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` text NOT NULL,
  `title` text NOT NULL,
  `value` int(11) NOT NULL,
  `icon` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

";

$SQL[] = "

INSERT INTO `counters` (`id`, `color`, `title`, `value`, `icon`) VALUES
(3, '#13bcb9', 'Theme Planning', 598, 'fa-check'),
(7, '#13bcb9', 'Hours Awaken', 100000, 'fa-bed'),
(8, '#13bcb9', 'Cups of Coffee', 20000, 'fa-coffee'),
(9, '#13bcb9', 'Bugs Fixed', 400, 'fa-wrench');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `counters_img` (
  `id` int(11) NOT NULL DEFAULT '0',
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `counters_img` (`id`, `image`) VALUES
(0, '795a28bbd585d35a3568331e487f1b6a.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `css` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `css` (`id`, `description`) VALUES
(1, '/* Tablet Landscape */\r\n@media only screen and (min-width: 958px) and (max-width: 1079px) {\r\n}\r\n\r\n/* Tablet Portrait size to standard 960 (devices and browsers) */\r\n@media only screen and (min-width: 768px) and (max-width: 959px) {\r\n}\r\n\r\n/* Mobile Landscape Size to Tablet Portrait (devices and browsers) */\r\n@media only screen and (min-width: 480px) and (max-width: 767px) {\r\n}\r\n\r\n/* Vertical Iphone */\r\n@media only screen and (max-width: 479px) {\r\n}\r\n');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `custom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `display_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

";

$SQL[] = "

INSERT INTO `custom` (`id`, `title`, `slug`, `content`, `image`, `display_order`) VALUES
(49, 'The Test', 'the-test', '&lt;p&gt;Completely matrix leading-edge methods of empowerment after ubiquitous initiatives. Assertively plagiarize real-time systems vis-a-vis intuitive paradigms. Quickly facilitate state of the art bandwidth rather than market-driven communities. Intrinsicly facilitate process-centric web-readiness.&lt;/p&gt;&lt;p&gt;Objectively evisculate proactive strategic theme areas with high standards in mindshare. Quickly brand multimedia based solutions through backward-compatible leadership. Completely expedite resource-leveling models for functional alignments. Seamlessly monetize resource maximizing catalysts for change rather than scalable information. Uniquely repurpose interoperable total linkage rather than bleeding-edge e-commerce.&lt;/p&gt;&lt;p&gt;Globally pursue innovative supply chains without ethical methodologies. Quickly disintermediate fully researched infomediaries for go forward bandwidth. Quickly orchestrate virtual portals and empowered networks. Professionally maximize principle-centered convergence with next-generation convergence. Interactively harness sustainable channels rather than out-of-the-box methodologies.&lt;/p&gt;&lt;p&gt;Compellingly maximize economically sound customer service without open-source benefits. Rapidiously generate interoperable benefits through revolutionary resources. Collaboratively incubate real-time solutions after premium total linkage. Compellingly promote mission-critical interfaces rather than client-centered quality vectors. Quickly restore seamless benefits before sustainable vortals.&lt;/p&gt;&lt;p&gt;Assertively communicate quality partnerships through real-time mindshare. Compellingly redefine proactive synergy with sticky best practices. Interactively negotiate alternative vortals after client-centric potentialities. Intrinsicly target team building e-markets for real-time products. Seamlessly optimize superior infomediaries rather than enterprise-wide potentialities.&lt;/p&gt;&lt;p&gt;Assertively target visionary strategic theme areas after transparent outsourcing. Phosfluorescently deliver leveraged services via highly efficient imperatives. Assertively disseminate business value before quality outsourcing. Globally formulate real-time markets without client-centered collaboration and idea-sharing. Synergistically engineer interoperable functionalities rather than proactive bandwidth.&lt;/p&gt;&lt;p&gt;Continually drive multifunctional convergence without premium synergy. Competently harness leveraged intellectual capital for revolutionary alignments. Proactively maximize long-term high-impact total linkage with maintainable models. Progressively reinvent prospective metrics vis-a-vis cooperative intellectual capital. Interactively strategize bleeding-edge applications through magnetic metrics.&lt;/p&gt;&lt;p&gt;Quickly whiteboard exceptional methods of empowerment and client-centered materials. Compellingly transform intermandated solutions for intermandated potentialities. Rapidiously utilize collaborative results and effective best practices. Dramatically fabricate.&lt;/p&gt;', '0337de3e303b62951a5fa600a4d81bc5.jpg', 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `dashboard` (
  `id` int(11) NOT NULL,
  `font` text NOT NULL,
  `lcolor` text NOT NULL,
  `menu_color` text NOT NULL,
  `menu_bcolor` text NOT NULL,
  `menu_colorhover` text NOT NULL,
  `menu_bcolorhover` text NOT NULL,
  `background_color` text NOT NULL,
  `background_tcolor` text NOT NULL,
  `background_menu_color` text NOT NULL,
  `items_color` text NOT NULL,
  `footer_color` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `dashboard` (`id`, `font`, `lcolor`, `menu_color`, `menu_bcolor`, `menu_colorhover`, `menu_bcolorhover`, `background_color`, `background_tcolor`, `background_menu_color`, `items_color`,`footer_color`) VALUES
(1, 'Questrial', '#13bcb9', '#ffffff', '#333333', '#ffffff', '#137d7b', '#13bcb9', '#ffffff', '#333333', '#ffffff', '#3c3d41');

";


$SQL[] = "

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `gdpr` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `gdpr` (`id`, `type`) VALUES
(1, 'eupopup eupopup-bottomleft');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `hits` (
  `page` varchar(255) NOT NULL DEFAULT '',
  `count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `hits` (`page`, `count`) VALUES
('MARKETING', 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `time_accessed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4170 ;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `latest_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `latest_img` (`id`, `image`) VALUES
(0, '6fca8b8e89b73839623747d974fddb5f.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `loader_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `loader_type` (`id`, `type`) VALUES
(1, 'Chasing Dots');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `logo` (
  `id` int(11) NOT NULL,
  `logo_text` text NOT NULL,
  `image` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `logo` (`id`, `logo_text`, `image`) VALUES
(0, 'logo', '451ca5c9bf8102dded9a913c3ac8f8f9.png');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `map` (
  `id` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `zoom` text NOT NULL,
  `api` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `map` (`id`, `latitude`, `longitude`, `title`, `description`, `zoom`, `api`) VALUES
(1, 47.6176679931594, -122.20882885158062, 'Marketing - Corporate &amp; Enterprise Website CMS', 'Marketing - Corporate &amp; Enterprise Website CMS', '15', 'AIzaSyAvOhtbl4wK5vOq3YZuc-gkldJTxI13eY4');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

";

$SQL[] = "

INSERT INTO `media` (`id`, `image`) VALUES
(23, '0337de3e303b62951a5fa600a4d81bc5.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `menu_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `menu_type` (`id`, `type`) VALUES
(1, 'topbar');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `particles` (
  `id` int(11) NOT NULL,
  `bpcolor` text NOT NULL,
  `dpcolor` text NOT NULL,
  `ppcolor` text NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `particles` (`id`, `bpcolor`, `dpcolor`, `ppcolor`, `type`) VALUES
(1, '#13bcb9', '#2cb5e8', '#ffffff', 'particles');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `name` text NOT NULL,
  `website` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

";

$SQL[] = "

INSERT INTO `partners` (`id`, `image`, `name`, `website`) VALUES
(7, '214122e2cd6a719ee8b4ca496d58f4b4.png', 'ThemeForest', 'https://themeforest.net/'),
(8, '2ded694aee561e0728e10a9da8bb5289.png', 'CodeCanyon', 'https://codecanyon.net/'),
(9, '6029b7beee3a99512d7b17507d0d973b.png', 'Videohive', 'https://videohive.net/'),
(10, 'db936d49ec2e3199021429bac7152b4c.png', 'AudioJungle', 'https://audiojungle.net/'),
(11, '7e78e7dafe61b79d22aa78af19a6211f.png', 'GraphicRiver', 'https://graphicriver.net/'),
(12, '50f2a50254ac4605e08c5c559001f710.png', 'PhotoDune', 'https://photodune.net/'),
(13, '5783b550b93f66d50acff4ffba04118f.png', '3dOcean', 'https://3docean.net/');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `partnerspage_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `partnerspage_img` (`id`, `image`) VALUES
(0, '9aaa993baac8ec1c731be240fb453d59.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `partners_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `partners_img` (`id`, `image`) VALUES
(0, '5fe85a1d0ec25eb1a26ba292f81f9a75.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `portfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `category` text NOT NULL,
  `title` text NOT NULL,
  `link` text NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

";

$SQL[] = "

INSERT INTO `portfolio` (`id`, `image`, `category`, `title`, `link`, `description`) VALUES
(20, '2c0077d97c6874d174754dafbe3b7d1b.jpg', 'Comunication', 'Photography', 'https://codecanyon.net/user/ezcode', 'Synergistically seize wireless catalysts for change rather than cross-media alignments. Dynamically utilize an expanded array of web-readiness with adaptive ideas. Progressively communicate cooperative deliverables vis-a-vis seamless e-markets. Globally synergize magnetic strategic.'),
(21, '269ef20a92eee85c69de0be01e65e557.jpg', 'Brainstorm', 'Developing Ideas', 'https://codecanyon.net/user/ezcode', 'Synergistically seize wireless catalysts for change rather than cross-media alignments. Dynamically utilize an expanded array of web-readiness with adaptive ideas. Progressively communicate cooperative deliverables vis-a-vis seamless e-markets. Globally synergize magnetic strategic.'),
(22, 'fd183615d74f2b06edba243e132b9c6c.jpg', 'Comunication', 'Design Choices', 'https://codecanyon.net/user/ezcode', 'Professionally architect interactive networks vis-a-vis cooperative platforms. Monotonectally fabricate reliable &quot;outside the box&quot; thinking without end-to-end infrastructures. Seamlessly exploit equity invested data through dynamic technologies. Dramatically innovate equity invested results through.'),
(23, '856353943f9658259fef9cc5842fa118.jpg', 'Design', 'Construction on the way', 'https://codecanyon.net/user/ezcode', 'Quickly fabricate end-to-end potentialities through bleeding-edge networks. Holisticly coordinate multidisciplinary deliverables vis-a-vis customized communities. Collaboratively leverage other&#039;s client-focused total linkage for sticky infomediaries. Quickly unleash intuitive innovation after best-of-breed e-services. Efficiently.'),
(24, 'e4764bfa4f940acb9f3a981cdb16eeea.jpg', 'Brainstorm', 'Ideas coming', 'https://codecanyon.net/user/ezcode', 'Efficiently repurpose just in time expertise with technically sound technologies. Authoritatively implement intermandated information and distinctive customer service. Collaboratively restore global mindshare vis-a-vis installed base methodologies. Efficiently monetize tactical collaboration and.'),
(25, '3c0ef3f93effa6c08e67bab5a75658f4.jpg', 'Design', 'Inovation', 'https://codecanyon.net/user/ezcode', 'Efficiently repurpose just in time expertise with technically sound technologies. Authoritatively implement intermandated information and distinctive customer service. Collaboratively restore global mindshare vis-a-vis installed base methodologies. Efficiently monetize tactical collaboration and.'),
(26, '676941496177800f23d5c0f241d61ac1.jpg', 'Comunication', 'Design Vision', 'https://codecanyon.net/user/ezcode', 'Interactively aggregate orthogonal value after economically sound networks. Dynamically customize focused e-services whereas impactful data. Authoritatively foster functional relationships for wireless platforms. Holisticly formulate highly efficient customer service without accurate data.'),
(27, 'ca6e3d73d647e6c86e02f964f1eed7b0.jpg', 'Comunication', 'Comunication Campaigns', 'https://codecanyon.net/user/ezcode', 'Interactively aggregate orthogonal value after economically sound networks. Dynamically customize focused e-services whereas impactful data. Authoritatively foster functional relationships for wireless platforms. Holisticly formulate highly efficient customer service without accurate data.');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `portfolio_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `portfolio_img` (`id`, `image`) VALUES
(0, '682a1f648f14d70de33f9467f3c8d3d4.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `portfolio_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `portfolio_type` (`id`, `type`) VALUES
(1, 'portfolio');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` text NOT NULL,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

";

$SQL[] = "

INSERT INTO `post` (`id`, `date`, `title`, `slug`, `description`, `image`) VALUES
(49, '2018-07-05 17:11', 'Fantastic admin panel with awesome features!', 'fantastic-admin-panel-with-awesome-features', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '122bf9f5704b4f61e894c083808f13da.jpg'),
(50, '2018-07-05 17:21', 'Easy to Use', 'easy-to-use', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', 'b52f3d36b0c1dc2eb6b90315598a721f.jpg'),
(51, '2018-07-05 17:22', 'Full Customizable', 'full-customizable', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '7b18ae300e1145947bb50cb3ef2eb318.jpg'),
(52, '2018-07-05 17:26', 'Beautiful Design', 'beautiful-design', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '6acd0355a00e64cdf61bb39b0f6bfdd7.jpg'),
(53, '2018-07-05 17:27', 'Fast Learning curve', 'fast-learning-curve', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '640632e01e08a41b983bf4ef9e5dca28.jpg'),
(54, '2018-07-05 17:27', 'Very Fast', 'very-fast', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '9ce24d345edaa5f7529cf32962f00de7.jpg'),
(55, '2018-07-05 17:28', 'Complete Admin Panel', 'complete-admin-panel', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '25f61bc2be50df0c56a20c4761f14c41.jpg'),
(56, '2018-07-05 17:29', 'Completely harness efficient deliverables.', 'completely-harness-efficient-deliverables', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '6ae73d588b576da9bdfedc6abdb2a819.jpg'),
(57, '2018-07-05 17:29', 'Multi-Features', 'multi-features', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '8f519fad98321cce2ae4619b781242eb.jpg'),
(58, '2018-07-05 17:30', 'Sweet as Candy', 'sweet-as-candy', '&lt;p&gt;Competently target just in time strategic theme areas without visionary metrics. Monotonectally foster sustainable niche markets for cost effective catalysts for change. Competently customize multimedia based niche markets with global growth strategies. Credibly recaptiualize customized bandwidth whereas progressive relationships. Proactively productize 2.0 resources whereas resource sucking vortals.&lt;/p&gt;&lt;p&gt;Distinctively build web-enabled sources vis-a-vis functionalized benefits. Enthusiastically exploit superior outsourcing before process-centric web-readiness. Rapidiously reintermediate best-of-breed meta-services before empowered internal or &quot;organic&quot; sources. Compellingly formulate customer directed customer service before ethical infrastructures. Assertively synthesize client-focused paradigms via turnkey imperatives.&lt;/p&gt;&lt;p&gt;Assertively enable covalent portals through enabled technology. Authoritatively target resource-leveling catalysts for change through enterprise-wide e-tailers. Authoritatively drive long-term high-impact scenarios with strategic infomediaries. Energistically supply multidisciplinary manufactured products vis-a-vis parallel models. Objectively iterate customer directed collaboration and idea-sharing before dynamic action items.&lt;/p&gt;&lt;p&gt;Professionally synthesize progressive internal or &quot;organic&quot; sources whereas extensive infrastructures. Rapidiously integrate cost effective resources rather than proactive web-readiness. Authoritatively visualize fully tested methodologies after an expanded array of channels. Uniquely benchmark enterprise-wide best practices with low-risk high-yield results. Enthusiastically provide access to distributed &quot;outside the box&quot; thinking with extensible web services.&lt;/p&gt;&lt;p&gt;Energistically recaptiualize customer directed &quot;outside the box&quot; thinking via client-centered relationships. Interactively transition interactive catalysts for change whereas orthogonal intellectual capital. Completely integrate collaborative e-commerce before go forward services. Objectively supply efficient initiatives through high standards in value. Rapidiously simplify cutting-edge scenarios and installed base outsourcing.&lt;/p&gt;&lt;p&gt;Seamlessly negotiate multidisciplinary relationships after equity invested convergence. Compellingly seize customized bandwidth with viral strategic theme areas. Continually streamline bricks-and-clicks markets before extensive scenarios. Energistically harness unique benefits for extensible content. Dramatically conceptualize vertical partnerships without highly efficient vortals.&lt;/p&gt;&lt;p&gt;Uniquely engage.&lt;/p&gt;', '86f3fbf597b36ff1ed63540b9a63a64d.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `pricing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `hint` text NOT NULL,
  `price` int(11) NOT NULL,
  `description` text NOT NULL,
  `purchase` text NOT NULL,
  `color` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

";

$SQL[] = "

INSERT INTO `pricing` (`id`, `title`, `hint`, `price`, `description`, `purchase`, `color`, `link`) VALUES
(1, 'Business', 'For established business', 299, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#707070', 'https://www.google.com'),
(2, 'Professional', 'Suitable for startups', 99, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#333333', 'https://codecanyon.net/user/ezcode'),
(3, 'Personal', 'Perfect for freelancers', 29, '  &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;100&lt;/span&gt; GB Storge&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free Domain&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;15&lt;/span&gt; Mbps Bandwidth&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Free 24/7 Support&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;&lt;span&gt;10&lt;/span&gt; Accounts&lt;/li&gt;\r\n                                                &lt;li&gt;&lt;i class=&quot;fa fa-check&quot;&gt;&lt;/i&gt;Billing Software&lt;/li&gt;', 'Buy Right Now', '#c2c2c2', 'https://codecanyon.net/user/ezcode');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `pricing_number` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `pricing_number` (`id`, `number`) VALUES
(1, 4);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `pricing_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `pricing_type` (`id`, `type`) VALUES
(1, 'material');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `privacypage_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `privacypage_img` (`id`, `image`) VALUES
(0, '94486c6cd51f945b7d21c0f059a28f26.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `progress` (
  `id` int(11) NOT NULL,
  `percent` text NOT NULL,
  `color` text NOT NULL,
  `height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `progress` (`id`, `percent`, `color`, `height`) VALUES
(1, '80', '#13bcb9', 50);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(11) NOT NULL,
  `keywords` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `seo` (`id`, `keywords`, `title`, `description`) VALUES
(1, 'coming soon, website builder, options panel, featured content, countdown timer, progress bar, bootstrap', 'Marketing - Corporate &amp; Enterprise Website CMS', 'The easiest way to create your Personal or Business Multi-Page Website.');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` text NOT NULL,
  `color` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

";

$SQL[] = "

INSERT INTO `service` (`id`, `logo`, `color`, `title`, `description`, `type`) VALUES
(1, 'fa-heart', '#ffffff', 'Design', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(2, 'fa-desktop', '#ffffff', 'Webdesign', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(3, 'fa-gift', '#ffffff', 'Marketing Campaigns', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(4, 'fa-video-camera', '#ffffff', 'Video Campaigns', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon'),
(6, 'fa-camera', '#ffffff', 'Photography', 'From T-shirts to the tags that hang on them, we can make it', 'icon'),
(7, 'fa-pencil', '#ffffff', 'Logo Design', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. ', 'icon');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `services` (`id`, `number`) VALUES
(1, 4);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `services_img` (
  `id` int(11) NOT NULL,
  `parallax` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `services_img` (`id`, `parallax`) VALUES
(0, '849815854fb147f3ff504933f3c6cd1e.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `services_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `services_type` (`id`, `type`) VALUES
(1, 'image');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `service_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

";

$SQL[] = "

INSERT INTO `service_images` (`id`, `image`, `title`, `description`, `type`) VALUES
(15, 'de54ce3cde8ab398867f2f0868aa60b3.jpg', 'Photography', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(16, 'c1f5070f3b12f9272ec82e25e6e096a3.jpg', 'Design', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(17, 'ced32f1d92451b88b7f2410aaa750f72.jpg', 'Marketing Campaigns', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(18, '6fc75ea1a825c8cac566988cc3bceaaf.jpg', 'Webdesign', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(19, 'ec7b374976b245e106ca1f5002218e87.jpg', 'Video Campaigns', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image'),
(20, '8116ad7f605961797c796ae952e0f4bf.jpg', 'Logo Design', 'Authoritatively formulate equity invested value rather than extensive customer service. Progressively whiteboard real-time ideas for market-driven services. Competently customize leading-edge alignments after state of the art functionalities. Continually fashion go forward action items via collaborative portals. Energistically engineer business users with clicks-and-mortar niche markets.  Synergistically embrace frictionless value for team driven &quot;outside the box&quot; thinking. Efficiently disseminate high standards in alignments before.', 'image');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `about` int(11) NOT NULL,
  `contacts` int(11) NOT NULL,
  `services` int(11) NOT NULL,
  `partners` int(11) NOT NULL,
  `portfolio` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `custom` int(11) NOT NULL,
  `privacy` int(11) NOT NULL,
  `calendar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `settings` (`id`, `about`, `contacts`, `services`, `partners`, `portfolio`, `blog`, `custom`, `privacy`, `calendar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 0, 1, 1);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color` text NOT NULL,
  `skill` text NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

";

$SQL[] = "

INSERT INTO `skills` (`id`, `color`, `skill`, `percent`) VALUES
(1, '#00ebdd', 'PremiÃ©re', 80),
(3, '#42a14c', 'InDesign', 100),
(4, '#8fff00', 'Illustrator', 70),
(5, '#edd13b', 'Photoshop', 95);

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `slide` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

";

$SQL[] = "

INSERT INTO `slide` (`id`, `title`, `description`, `image`, `type`) VALUES
(22, 'Completely harness efficient deliverables.', 'Interactively revolutionize cutting-edge core competencies whereas multimedia based applications.', '16c1cea1e5c76a17c2d42a3c7e828569.jpg', 'slide'),
(23, 'Completely harness efficient deliverables.', 'Interactively revolutionize cutting-edge core competencies whereas multimedia based applications.', '4ad4eb25fb8fbfa871720a1b4c1496a6.jpg', 'slide'),
(24, 'Completely harness efficient deliverables.', 'Interactively revolutionize cutting-edge core competencies whereas multimedia based applications.', 'f2527cb13060ced55c16bb6510f17766.jpg', 'slide'),
(25, 'Completely harness efficient deliverables.', 'Interactively revolutionize cutting-edge core competencies whereas multimedia based applications.', 'b633e3dfb73defe163d63f2657307303.jpg', 'slide');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `slider_type` (
  `id` int(11) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `slider_type` (`id`, `type`) VALUES
(1, 'fullwidth');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `static` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `static` (`id`, `image`, `type`) VALUES
(0, 'eda5597a4b90a1d0cc674957cb75df0e.jpg', 'static');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `stats_day` (
  `date` date NOT NULL DEFAULT '0000-00-00',
  `hits` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `social_fb` text NOT NULL,
  `social_in` text NOT NULL,
  `social_go` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

";

$SQL[] = "

INSERT INTO `team` (`id`, `image`, `name`, `description`, `social_fb`, `social_in`, `social_go`) VALUES
(10, 'e9540f35dd8751aa4a31f9466c123063.jpg', 'Mark Lee', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(11, '0f629b70f8cc80011645d7e47d829a26.jpg', 'Telma Soares', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(12, 'e4d3ab39c6f0d1da846b2d77e8abb9ce.jpg', 'JosÃ© Oliveira', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#'),
(13, '4d0473d58522d736f9119c0b3daf0bcc.jpg', 'Joana Doe', 'Authoritatively repurpose multidisciplinary vortals without market positioning applications. Intrinsicly myocardinate scalable results rather than next-generation platforms. Dynamically.', '#', '#', '#');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `description` text NOT NULL,
  `name` text NOT NULL,
  `website` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

";

$SQL[] = "

INSERT INTO `testimonials` (`id`, `image`, `description`, `name`, `website`) VALUES
(1, '1e2905dd227f3376a5511334706f7c82.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'John Doe', 'https://themeforest.net/user/ezcode'),
(2, '54a893adf973eee208a3578fee8be7c2.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Maria Doe', 'https://themeforest.net/user/ezcode'),
(3, 'ed92804a12f41fae081032167d9ff502.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'JosÃ© Oliveira', 'https://themeforest.net/user/ezcode'),
(4, 'af70e85ac56bdb1791bb6df9b30c6ff3.jpg', 'Uniquely maximize superior niches whereas client-based meta-services. Synergistically synthesize fully researched quality vectors vis-a-vis ethical data. Collaboratively disintermediate tactical.', 'Telma Soares', 'https://themeforest.net/user/ezcode');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `testimonials_img` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `testimonials_img` (`id`, `image`) VALUES
(0, '40b3357f057e2f02642567322668c08e.jpg');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `text` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `text` (`id`, `title`, `description`) VALUES
(1, 'Marketing - Corporate &amp; Enterprise Website CMS', 'The easiest way to create your Personal or Business Multipage Website.');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textabout` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textabout` (`id`, `description`) VALUES
(1, '&lt;p&gt;Enthusiastically expedite cross-unit processes without bleeding-edge growth strategies. Rapidiously create accurate core competencies for future-proof results. Globally repurpose transparent outsourcing and accurate leadership skills. Quickly impact effective portals after robust scenarios. Quickly redefine next-generation data without quality solutions.&lt;/p&gt;&lt;p&gt;Conveniently engineer world-class e-markets through integrated systems. Phosfluorescently syndicate sticky users for alternative web-readiness. Professionally repurpose principle-centered e-tailers before proactive action items. Monotonectally customize diverse functionalities after installed base leadership. Intrinsicly streamline diverse solutions rather than visionary bandwidth. Globally repurpose transparent outsourcing and accurate leadership skills. Quickly impact effective portals after robust scenarios. Quickly redefine next-generation data without quality solutions.&lt;/p&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textblog` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textblog` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;Just some interesting information!&lt;/span&gt;&lt;/p&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textcontacts` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textcontacts` (`id`, `description`) VALUES
(1, '');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textpartners` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textpartners` (`id`, `description`) VALUES
(1, '&lt;span style=&quot;font-size: 24px;&quot;&gt;Who we work with!&lt;/span&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textportfolio` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textportfolio` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;Just check out our work!&lt;/span&gt;&lt;br&gt;&lt;/p&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textprivacy` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textprivacy` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;p&gt;&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;&lt;span style=&quot;font-size: 36px;&quot;&gt;Privacy Notice&lt;/span&gt;&lt;/strong&gt;&lt;/center&gt;&lt;br&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;&lt;br&gt;&lt;/strong&gt;&lt;/center&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;This privacy notice discloses the privacy practices for&amp;nbsp;&lt;span style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; text-decoration-line: underline;&quot;&gt;(website address)&lt;/span&gt;. This privacy notice applies solely to information collected by this website. It will notify you of the following:&lt;/p&gt;&lt;ol type=&quot;1&quot; style=&quot;box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style-position: initial; list-style-image: initial; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;What choices are available to you regarding the use of your data.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;The security procedures in place to protect the misuse of your information.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: decimal;&quot;&gt;How you can correct any inaccuracies in the information.&lt;/li&gt;&lt;/ol&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Information Collection, Use, and Sharing&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Your Access to and Control Over Information&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:&lt;/p&gt;&lt;ul style=&quot;box-sizing: content-box; margin-right: 0px; margin-bottom: 0px; margin-left: 40px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; list-style: none; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;See what data we have about you, if any.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Change/correct any data we have about you.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Have us delete any data we have about you.&lt;/li&gt;&lt;li style=&quot;box-sizing: content-box; margin: 0px 0px 0px 30px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; list-style: disc;&quot;&gt;Express any concern you have about our use of your data.&lt;/li&gt;&lt;/ul&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Security&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for &quot;https&quot; at the beginning of the address of the Web page.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254);&quot;&gt;While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.&lt;/p&gt;&lt;center style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 15px; line-height: inherit; font-family: Arial, Helvetica, sans-serif; color: rgb(51, 51, 51); background-color: rgb(254, 254, 254);&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Optional Clauses&lt;/strong&gt;&lt;/center&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If your site has a registration page that customers must complete to do business with you, insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Registration&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;In order to use this website, a user must first complete the registration form. During registration a user is required to give certain information (such as name and email address). This information is used to contact you about the products/services on our site in which you have expressed interest. At your option, you may also provide demographic information (such as gender or age) about yourself, but it is not required.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you take and fill orders on your site, insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Orders&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We request information from you on our order form. To buy from us, you must provide contact information (like name and shipping address) and financial information (like credit card number, expiration date). This information is used for billing purposes and to fill your orders. If we have trouble processing an order, we&#039;ll use this information to contact you.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you use cookies or other devices that track site visitors, insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Cookies&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We use &quot;cookies&quot; on this site. A cookie is a piece of data stored on a site visitor&#039;s hard drive to help us improve your access to our site and identify repeat visitors to our site. For instance, when we use a cookie to identify you, you would not have to log in a password more than once, thereby saving time while on our site. Cookies can also enable us to track and target the interests of our users to enhance the experience on our site. Usage of a cookie is in no way linked to any personally identifiable information on our site.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If other organizations use cookies or other devices that track site visitors to your site, insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;Some of our business partners may use cookies on our site (for example, advertisers). However, we have no access to or control over these cookies.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you share information collected on your site with other parties, insert one or more of these paragraphs in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Sharing&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We share aggregated demographic information with our partners and advertisers. This is not linked to any personal information that can identify any individual person.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;And/or:&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We use an outside shipping company to ship orders, and a credit card processing company to bill users for goods and services. These companies do not retain, share, store or use personally identifiable information for any secondary purposes beyond filling your order.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;And/or:&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;We partner with another party to provide specific services. When the user signs up for these services, we will share names, or other contact information that is necessary for the third party to provide these services. These parties are not allowed to use personally identifiable information except for the purpose of providing these services.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If your site has links to other sites, you might insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Links&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;This website contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.&lt;/p&gt;&lt;p style=&quot;box-sizing: content-box; color: rgb(51, 51, 51); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(254, 254, 254); text-align: justify;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;If you ever collect data through surveys or contests on your site, you might insert a paragraph like this in your privacy notice:&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;&lt;strong style=&quot;box-sizing: content-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit;&quot;&gt;Surveys &amp;amp; Contests&lt;/strong&gt;&amp;nbsp;&lt;br style=&quot;box-sizing: content-box;&quot;&gt;From time-to-time our site requests information via surveys or contests. Participation in these surveys or contests is completely voluntary and you may choose whether or not to participate and therefore disclose this information. Information requested may include contact information (such as name and shipping address), and demographic information (such as zip code, age level). Contact information will be used to notify the winners and award prizes. Survey information will be used for purposes of monitoring or improving the use and satisfaction of this site.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;/p&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `textservices` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `textservices` (`id`, `description`) VALUES
(1, '&lt;p&gt;&lt;span style=&quot;font-size: 24px;&quot;&gt;These are the awesome services we provide!&lt;/span&gt;&lt;/p&gt;');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

";

$SQL[] = "

INSERT INTO `type` (`id`, `title`) VALUES
(1, 'Marketing');

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `video` (
  `v_id` int(11) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

CREATE TABLE IF NOT EXISTS `youtube` (
  `id` int(11) NOT NULL,
  `video_id` text NOT NULL,
  `type` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

";

$SQL[] = "

INSERT INTO `youtube` (`id`, `video_id`, `type`) VALUES
(1, 'B2vmlBXRuBU', 'youtube');

";


?>