<?php

/**
 * 
 * Please read the LICENSE file
 * @author Vadim V. Gabriel <vadimg88@gmail.com> http://www.vadimg.co.il/
 * @package Class Installer
 * @version 1.0.0a
 * @license GNU Lesser General Public License
 * 
 */

/** Load the template class **/
require_once(INSTALLER_PATH . '/Installer_Template.php');


/**
 * Class installer
 *
 */
class Installer
{
    /**
     * Options property
     *
     * @var array
     */
    protected $_options = array();

    /**
     * View object
     *
     * @var object
     */
    protected $view;

    /**
     * Language array
     *
     * @var array
     */
    protected $lang = array();

    protected $default_lang = 'english';

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        # Do we have a cookie
        if(isset($_COOKIE['lang']) && $_COOKIE['lang'] != '')
        {
            $this->default_lang = $_COOKIE['lang'];
        }
        
        # Change language
        if(isset($_POST['lang']) && $_POST['lang'] != '' && $this->default_lang != $_POST['lang'])
        {
            $path = INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $_POST['lang'] . '.php';
            if(file_exists($path))
            {
                $this->default_lang = $_POST['lang'];
                @setcookie('lang', $this->default_lang, time() + 60 * 60 * 24);
                $_POST['lang'] = 0;
                $this->nextStep('index');
            }
        }

        # Load the language file
        require_once( INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . $this->default_lang . '.php' );
        $this->lang = $lang;

        # Load the template class
        $this->view = new Installer_Template($this->lang);

        # Did we run it again?
        if(file_exists(INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'installer.lock'))
        {
            $this->view->error($this->lang['L-01']);
        }

        $allwed_steps = array('index' => 'indexAction', 'db' => 'dbAction', 'cfg' => 'configAction', 'database' => 'dbTables', 'config' => 'configWrite', 'finish' => 'finishInstaller');
        if(!in_array($_POST['step'], array_keys($allwed_steps)))
        {
            $this->nextStep('index');
        }

        # Display the right step		
		$this->{$allwed_steps[$_POST['step']]}($_POST);
		
		
    }

    /**
     * Show welcome message
     *
     */
    public function indexAction()
    {

        $options = '';
        foreach($this->buildLangSelect() as $lang)
        {
            $options .= "<option value='{$lang}'>".ucfirst($lang)."</option>";
        }

        $this->view->vars = array('options' => $options);
        $this->view->render('index');
    }

    /**
     * Show database setup stuff
     *
     */
    public function dbAction()
    {
        $this->view->render('db');
    }

    public function configAction()
    {
        $this->view->vars = array('path' => ROOT_PATH);
        $this->view->render('config');
    }

    /**
     * Handle DB table quries
     *
     * @param array $options
     */
    public function dbTables(array $options)
    {
        # Make sure we have an array of options
        if(!is_array($options))
        {
            $this->view->error($this->lang['L-02']);
        }

        # Make sure we have everything we need!
        $required_db_options = array('dbname', 'dbuser', 'dbpass', 'dbhost');

        foreach ($required_db_options as $required_db_option)
        {
            if(!isset($options[$required_db_option]))
            {
                $this->view->error($this->lang['L-03']);
            }
        }

        # Pass in to the options property
        $this->_options = $options;

        # First test the connection
        $link = mysqli_connect($this->_options['dbhost'], $this->_options['dbuser'], $this->_options['dbpass'],$this->_options['dbname']);
        if(!$link)
        {
            $this->view->error($this->lang['L-04']);
        }

        # Select the DB
        $db_selected = mysqli_select_db($link,$this->_options['dbname']);
        if(!$db_selected && !$this->_options['create_database'])
        {
            $this->view->error($this->lang['L-05']);
        }
        elseif (!$db_selected && $this->_options['create_database'])
        {
            # Create the DB
            $result = mysql_query($link, "CREATE DATABASE {$this->_options['dbname']}");
            if (!$result)
            {
                $this->view->error(sprintf($this->lang['L-06'], $this->_options['dbname'], htmlspecialchars(mysql_error())));
            }
        }

        # Load the database stuff
        $path = INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'database.php' ;
        if(!file_exists($path))
        {
            $this->view->error(sprintf($this->lang['L-07'], $path));
        }

        $SQL = array();
        require_once($path);

        $count = 0;
        $errors_count = 0;

        # Loop if we have any
        if(count($SQL))
        {
            # Start the count

            $errors = array();
            foreach ($SQL as $query)
            {
                $result = mysqli_query($link, $query);
                if (!$result)
                {
                    $errors[] = sprintf($this->lang['L-08'], htmlspecialchars(mysqli_error($link)));
                    $errors_count++;
                    continue;
                }

                # Increase it
                $count++;
            }
        }

        $error_string = '';

        # Did we had any errors?
        if(count($errors))
        {
            $error_string = "<br /><br />".sprintf($this->lang['I-14'], implode("<br /><br />", $errors));
        }

        # Redirect
        $this->view->vars = array('message' => sprintf($this->lang['L-09'], $count, $errors_count, $error_string), 'button' => $error_string ? $this->lang['I-16'] : $this->lang['I-03']);
        $this->view->render('dbdone');

    }

    /**
     * Display everything
     *
     */
    public function display()
    {
        $this->view->display();
    }

    /**
     * Write the configuration File
     *
     */
    public function configWrite()
    {
        # One level up
        $path = ROOT_PATH . DIRECTORY_SEPARATOR . 'config.php';

        $values = array(

        'base_url' => $_POST['url'],
        'base_dir' => stripslashes($_POST['dir']),
        );


        file_put_contents($path, "<?php\n\n\$CONFIG = " . var_export($values, true) . ';');

        $this->finishInstaller();
    }

    /**
     * Finish the installer proccess
     *
     */
    public function finishInstaller()
    {
		
        # Lock the installer
        $path = INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'installer.lock';
        file_put_contents($path, "installer lock file");

        $this->view->render('finish');
		
    }

    /**
     * Redirect to the next step
     *
     * @param string $step
     */
    public function nextStep($step)
    {
        $url = BASE_URL . '?step='.$step;
        if(!headers_sent())
        {
            header('Location: '. $url);
            exit;
        }

        print "<html><body><meta http-equiv='refresh' content='1;url={$url}'></body></html>";
        exit;
    }

    /**
     * Redirect screen with a message
     *
     * @param string $message
     */
    public function redirectScreen($message, $step)
    {
        $this->view->redirect($message, $step);
    }

    /**
     * Build the language select box
     *
     */
    public function buildLangSelect()
    {
        $path = INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'lang';
        $dirs = scandir($path);
        $files = array();
        foreach($dirs as $file)
        {
            if ($file == '.' || $file == '..')
            {
                continue;
            }
            elseif (is_dir($path.'/'.$file))
            {
                continue;
            }
            else
            {
                $files[] = str_replace('.php', '', $file);
            }
        }

        return $files;
    }

    /**
     * Destructor
     *
     */
    public function __destruct()
    {


        @mysqli_close($this->db);

		
    }

}