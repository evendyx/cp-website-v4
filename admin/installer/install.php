<?php

/**
 * 
 * Please read the LICENSE file
 * @author Vadim V. Gabriel <vadimg88@gmail.com> http://www.vadimg.co.il/
 * @package Class Installer
 * @version 1.0.0a
 * @license GNU Lesser General Public License
 * 
 */

# Define root path
define('ROOT_PATH', dirname(__FILE__));

define('INSTALLER_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'installer');

# Define templates path
if(!defined('TMPL_PATH'))
{
    define('TMPL_PATH', INSTALLER_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR);
}

# Define base url
define('BASE_URL', 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']);

// Error Reporting Active
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

# Default step
$_POST['step'] = isset($_POST['step']) ? $_POST['step'] : 'index';
require_once(INSTALLER_PATH . '/Installer.php');
$installer = new Installer();
$installer->display();

# Done!