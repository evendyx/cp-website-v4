<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_CLIENT');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><a href="clients.php"><?php echo lang('CLIENTS');?></a></li>
				<li class="active"><?php echo lang('EDIT_CLIENT');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						
						<form id="editClient" method="post" enctype="multipart/form-data" class="form-horizontal" name="editClient">
										
							<?php $csrf->echoInputField(); ?>
							
							<fieldset>										
								
							<?php echo editClient($_GET['id']); ?>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-9 control-label" for="singlebutton"></label>
								<div class="col-md-3">
									<input type="submit" name="editClient" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
								</div>
							</div>

							</fieldset>
						</form>
						<?php

						  if(isset($_POST['editClient']))
						  {
							updateClient($_GET['id'],htmlspecialchars($_POST['name'], ENT_QUOTES),htmlspecialchars($_POST['email'], ENT_QUOTES));
						  }

						?>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

		</div>
		<!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>