    
	<!-- Bootstrap Core JavaScript -->
    <script src="assets/js/bootstrap.min.js"></script>
	<!-- DataTables JavaScript -->
    <script src="assets/js/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables.bootstrap.js"></script>
    <script src="assets/js/listings.js"></script>
	<!-- Metis Menu Plugin JavaScript -->
    <script src="assets/js/metisMenu.min.js"></script>
	<script src="assets/js/bootstrap-toggle.js"></script>
	

	<?php
		if(isset($_POST['utilizadoreditado']) && (($_POST['password']) == ($_POST['password1'])))
		{
			updateUser($_SESSION['user']['username'],$_POST['username'],$_POST['password'],'',$_POST['email'],$_POST['lang']);
		}
	?>	
	

	
	<script>
		//Keep Nav state when page reload
		$(document).ready(function () {
			if (location.hash) {
				$("a[href='" + location.hash + "']").tab("show");
			}
			$(document.body).on("click", "a[data-toggle]", function(event) {
				location.hash = this.getAttribute("href");
			});
		});
		$(window).on("popstate", function() {
			var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
			$("a[href='" + anchor + "']").tab("show");
		});
		
		//Keep Accordion state when page reload
		$(document).ready(function () {
		$('a').click(function() {
			//store the id of the collapsible element
			localStorage.setItem('collapseItem', $(this).attr('href'));
		});

		var collapseItem = localStorage.getItem('collapseItem'); 
		if (collapseItem) {
		   $(collapseItem).collapse('show')
		}
	})
	</script>		
	
	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
		$(document).ready(function() {
			$('#dataTables-example').dataTable();
			$('#dataTables-example1').dataTable();
		});
    </script>
	
		<script>
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip(); 
		});
	</script>
	<?php echo getVideo();?>
	
		<script>
			$(document).ready(function() {
				$('#summernote').summernote({
				  height: 300,                 // set editor height
				  minHeight: null,             // set minimum height of editor
				  maxHeight: null,             // set maximum height of editor
				  focus: true,                // set focus to editable area after initializing summernote
				  toolbar: [
					// [groupName, [list of button]]
					['codeview', ['codeview']],
					['style', ['bold', 'italic', 'underline', 'clear']],
					['font', ['strikethrough', 'superscript', 'subscript']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['height', ['height']],
					['link', ['link']],
					['table', ['table']],
					['hr', ['hr']],					
				  ]
				});
			});
		</script>
		<?php if ($active == "settings.php")
		echo "
		<!-- include Menu Orderjs-->
		<script src='//code.jquery.com/ui/1.11.4/jquery-ui.js'></script>
		<script>
			$(function() {
			$( '#sortable' ).sortable();
			$( '#sortable' ).disableSelection();
			});
		</script>
		";
		?>	
			
		<!-- DateTimePicker JavaScript -->
		<script type='text/javascript' src='assets/js/bootstrap-datetimepicker.js' charset='UTF-8'></script>
		<!-- Datetime picker initialization -->
		<script type='text/javascript'>
			$('.form_date').datetimepicker({
				language:  'en',
				weekStart: 1,
				todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0
			});
		</script>
		
</body>
</html>