<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_CLIENT');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><a href="clients.php"><?php echo lang('CLIENTS');?></a></li>
				<li class="active"><?php echo lang('NEW_CLIENT');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newClient">
						<fieldset>
							<?php $csrf->echoInputField(); ?>
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="name"><?php echo lang('NAME');?></label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" class="form-control input-md" required>

								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="email"><?php echo lang('EMAIL');?></label>
								<div class="col-md-6">
									<input id="email" name="email" type="text" class="form-control input-md" required>

								</div>
							</div>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-10 control-label" for="singlebutton"></label>
								<div class="col-md-2">
									<input type="submit" name="newClient" class="btn btn-primary" value="<?php echo lang('NEW_CLIENT');?>" />
								</div>
							</div>

						</fieldset>
					</form>
					<?php		
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['newClient']))
						 {
							global $conection;
							// Recupera os dados dos campos
							$name = htmlspecialchars($_POST['name'], ENT_QUOTES);
							$email = htmlspecialchars($_POST['email'], ENT_QUOTES);
						  
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO clients VALUES (0, '".$name."','".$email."')");
						 
									// Se os dados forem inseridos com sucesso			
									if (!$sql) {
									echo ("Can't insert into database: " . mysqli_error());
									return false;
									} else {
									echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_CLIENT_CREATED')."', 'success');</script>";
											echo '<meta http-equiv="refresh" content="1; clients.php">'; 
											die();
									}		
									return true;

						}
							
						?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>