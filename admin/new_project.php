<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_PROJECT');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="portfolio.php"><?php echo lang('PORTFOLIO');?></a></li>
				<li class="active"><?php echo lang('NEW_PROJECT');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newproject">
										<fieldset>
											<?php $csrf->echoInputField(); ?>

											<!-- Image input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="image"><?php echo lang('UPLOAD_PROJECT_IMAGE');?></label>
												<div class="col-md-6">
													<input type="file" name="image" id="image">
												</div>
											</div>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="category"><?php echo lang('CATEGORY');?></label>
												<div class='col-md-6'>
													<select class="form-control input-md" name="category">
													  <option value="<?php
														global $conection;
														$sql = mysqli_query($conection,"select slug from categories");
														$row = mysqli_fetch_assoc($sql);
														echo $row['slug'];
													?>">------</option>
														<?php echo listCategories(); ?>
													</select>
												</div>
											</div>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="title"><?php echo lang('TITLE');?></label>
												<div class="col-md-6">
													<input id="title" name="title" type="text" class="form-control input-md" required>

												</div>
											</div>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="link"><?php echo lang('LINK');?></label>
												<div class="col-md-6">
													<input id="link" name="link" type="text" class="form-control input-md" required>

												</div>
											</div>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="description"><?php echo lang('PROJECT_DESCRIPTION');?></label>
												<div class="col-md-6">
													<textarea col="3" rows="4" id=description" name="description" type="text" class="form-control input-md"></textarea>

												</div>
											</div>

											<!-- Button -->
											<div class="form-group">
												<label class="col-md-10 control-label" for="singlebutton"></label>
												<div class="col-md-2">
													<input type="submit" name="newproject" class="btn btn-primary" value="<?php echo lang('NEW_PROJECT');?>" />
												</div>
											</div>

										</fieldset>
									</form>
									<?php

										// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
										if (!empty($_POST['newproject']))
										 {
											global $conection;
											// Recupera os dados dos campos
											$image = $_FILES['image'];
											$category = $_POST['category'];
											$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
											$link = htmlspecialchars($_POST['link'], ENT_QUOTES);
											$description = htmlspecialchars($_POST['description'], ENT_QUOTES);

											// Se a foto estiver sido selecionada
											if (!empty($image["name"])) {
										 
												// Largura m�xima em pixels
												$largura = 1000000;
												// Altura m�xima em pixels
												$altura = 1000000;
												// Tamanho m�ximo do arquivo em bytes
												$tamanho = 500000000000;
										 
												// Verifica se o arquivo � uma imagem
												if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
												   $error[1] = "Isso n�o � uma imagem.";
												} 
										 
												// Pega as dimens�es da imagem
												$dimensoes = getimagesize($image["tmp_name"]);
										 
												// Verifica se a largura da imagem � maior que a largura permitida
												if($dimensoes[0] > $largura) {
													$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
												}
										 
												// Verifica se a altura da imagem � maior que a altura permitida
												if($dimensoes[1] > $altura) {
													$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
												}
										 
												// Verifica se o tamanho da imagem � maior que o tamanho permitido
												if($image["size"] > $tamanho) {
													$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
												}
										 
													// Pega extens�o da imagem
													preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
										 
													// Gera um nome �nico para a imagem
													$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
										 
													// Caminho de onde ficar� a imagem
													$caminho_imagem = "../assets/img/uploads/portfolio/" . $nome_imagem;
										 
													// Faz o upload da imagem para seu respectivo caminho
													move_uploaded_file($image["tmp_name"], $caminho_imagem);
										 
													// Insere os dados no banco
													$sql = mysqli_query($conection,"INSERT INTO portfolio VALUES (0, '".$nome_imagem."', '".$category."', '".$title."', '".$link."', '".$description."')");
					
													// Se os dados forem inseridos com sucesso			
													if (!$sql) {
													echo ("Can't insert into database: " . mysqli_error());
													return false;
													} else {
													echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_PROJECT_CREATED')."', 'success');</script>";
															echo '<meta http-equiv="refresh" content="1; portfolio.php">'; 
															die();
													}		
													return true;

												// Se houver mensagens de erro, exibe-as
												if (count($error) != 0) {
													foreach ($error as $erro) {
														echo $erro . "<br />";
													}
												}

										}
										}
	
										?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>