<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('MEDIA');?></span>
				<!-- Trigger the modal with a button -->
				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo lang('NEW_MEDIA');?></button></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li class="active"><?php echo lang('MEDIA');?></li>
			</ul>
		</div>
	</div>
	
	<!-- Modal -->
									<div id="myModal" class="modal fade" role="dialog">
									  <div class="modal-dialog">

										<!-- Modal content-->
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title"><?php echo lang('MEDIA');?></h4>
										  </div>
										  <div class="modal-body">
											
											<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newmedia">
										
											<?php $csrf->echoInputField(); ?>
																						
											<!-- Image input-->
											<div class="form-group">
												<div class="col-md-12">
													<input type="file" name="image" id="image">
												</div>
											</div>

											<!-- Button -->
											<div class="form-group">
												<label class="col-md-8 control-label" for="singlebutton"></label>
												<div class="col-md-2">
													<input type="submit" name="newmedia" class="btn btn-primary" value="<?php echo lang('NEW_MEDIA');?>" />
												</div>
											</div>

									</form>
									<?php

										// Se o usuário clicou no botão cadastrar efetua as ações
										if (!empty($_POST['newmedia']))
										 {
											global $conection;
											// Recupera os dados dos campos
											$image = $_FILES['image'];

											// Se a foto estiver sido selecionada
											if (!empty($image["name"])) {
										 
												// Largura máxima em pixels
												$largura = 1000000;
												// Altura máxima em pixels
												$altura = 1000000;
												// Tamanho máximo do arquivo em bytes
												$tamanho = 500000000000;
										 
												// Verifica se o arquivo é uma imagem
												if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
												   $error[1] = "Isso não é uma imagem.";
												} 
										 
												// Pega as dimensões da imagem
												$dimensoes = getimagesize($image["tmp_name"]);
										 
												// Verifica se a largura da imagem é maior que a largura permitida
												if($dimensoes[0] > $largura) {
													$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
												}
										 
												// Verifica se a altura da imagem é maior que a altura permitida
												if($dimensoes[1] > $altura) {
													$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
												}
										 
												// Verifica se o tamanho da imagem é maior que o tamanho permitido
												if($image["size"] > $tamanho) {
													$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
												}
										 
													// Pega extensão da imagem
													preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
										 
													// Gera um nome único para a imagem
													$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
										 
													// Caminho de onde ficará a imagem
													$caminho_imagem = "../assets/img/uploads/other/" . $nome_imagem;
										 
													// Faz o upload da imagem para seu respectivo caminho
													move_uploaded_file($image["tmp_name"], $caminho_imagem);
										 
													// Insere os dados no banco
													$sql = mysqli_query($conection,"INSERT INTO media VALUES (0, '".$nome_imagem."')");
					
													// Se os dados forem inseridos com sucesso			
													if (!$sql) {
													echo ("Can't insert into database: " . mysqli_error());
													return false;
													} else {
													echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
															echo '<meta http-equiv="refresh" content="1; media.php">'; 
															die();
													}		
													return true;

												// Se houver mensagens de erro, exibe-as
												if (count($error) != 0) {
													foreach ($error as $erro) {
														echo $erro . "<br />";
													}
												}

										}
										}
	
										?>
									
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
										  </div>
										</div>

									  </div>
									</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						<div id="media" class="table-responsive">
							
								<div class="row">

									<?php echo listMedia();?>

								</div>    
							</div>
					</div>
				</div>
				<!-- /basic layout -->

			</div>

		</div>
		<!-- /.row -->
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>