<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

<div id="page-wrapper">

    <div class="container-fluid">

		<br/>

         <div class="row">

             <div class="col-lg-12">
				<div class="panel panel-default dash">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h2 class="sub-header">
								<i class="fa fa-comments" aria-hidden="true"></i> <?php echo lang('MEDIA');?>
							</h2>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">
							
								<div class="col-md-12">

									<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newmedia">
										<fieldset>
											<?php $csrf->echoInputField(); ?>
																						
											<!-- Image input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="image"><?php echo lang('UPLOAD_IMAGE');?></label>
												<div class="col-md-6">
													<input type="file" name="image" id="image">
												</div>
											</div>

											<!-- Button -->
											<div class="form-group">
												<label class="col-md-10 control-label" for="singlebutton"></label>
												<div class="col-md-2">
													<input type="submit" name="newmedia" class="btn btn-primary" value="<?php echo lang('NEW_TESTIMONIAL');?>" />
												</div>
											</div>

										</fieldset>
									</form>
									<?php

										// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
										if (!empty($_POST['newmedia']))
										 {
											global $conection;
											// Recupera os dados dos campos
											$image = $_FILES['image'];

											// Se a foto estiver sido selecionada
											if (!empty($image["name"])) {
										 
												// Largura m�xima em pixels
												$largura = 1000000;
												// Altura m�xima em pixels
												$altura = 1000000;
												// Tamanho m�ximo do arquivo em bytes
												$tamanho = 500000000000;
										 
												// Verifica se o arquivo � uma imagem
												if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
												   $error[1] = "Isso n�o � uma imagem.";
												} 
										 
												// Pega as dimens�es da imagem
												$dimensoes = getimagesize($image["tmp_name"]);
										 
												// Verifica se a largura da imagem � maior que a largura permitida
												if($dimensoes[0] > $largura) {
													$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
												}
										 
												// Verifica se a altura da imagem � maior que a altura permitida
												if($dimensoes[1] > $altura) {
													$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
												}
										 
												// Verifica se o tamanho da imagem � maior que o tamanho permitido
												if($image["size"] > $tamanho) {
													$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
												}
										 
													// Pega extens�o da imagem
													preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
										 
													// Gera um nome �nico para a imagem
													$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
										 
													// Caminho de onde ficar� a imagem
													$caminho_imagem = "../assets/uploads/" . $nome_imagem;
										 
													// Faz o upload da imagem para seu respectivo caminho
													move_uploaded_file($image["tmp_name"], $caminho_imagem);
										 
													// Insere os dados no banco
													$sql = mysqli_query($conection,"INSERT INTO media VALUES (0, '".$nome_imagem."')");
					
													// Se os dados forem inseridos com sucesso			
													if (!$sql) {
													echo ("Can't insert into database: " . mysqli_error());
													return false;
													} else {
													echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_TESTIMONIAL_CREATED')."', 'success');</script>";
															echo '<meta http-equiv="refresh" content="1; media.php">'; 
															die();
													}		
													return true;

												// Se houver mensagens de erro, exibe-as
												if (count($error) != 0) {
													foreach ($error as $erro) {
														echo $erro . "<br />";
													}
												}

										}
										}
	
										?>

								</div>    
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>