<?php
    require_once('functions/functions.php');
	require_once('functions/backup_restore.class.php'); 
	
	// CSRF Protection
	require 'functions/CSRF_Protect.php';
	$csrf = new CSRF_Protect();
	
	// Error Reporting Active
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
    if(empty($_SESSION['user'])) 
    {
        header("Location: index.php");
        die("Redirecting to index.php"); 
    }
	
	echo language();
	
	function curPageURL() {
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
	}
	
	//Fun��o para devolver current page
	function curPageName() {
	 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
	}
	$active = curPageName();
	
	//IMPORT - EXPORT Options
    $newImport = new backup_restore($host,$dbname,$username,$password);

    if(isset($_GET['process'])){
        $process = $_GET['process'];
        if($process == 'backup'){
            $message = $newImport -> backup ();   
        }else if($process == 'restore'){
            $message = $newImport -> restore (); 
            @unlink('assets/backup/'.$dbname.'_'.date('Y-m-d').'_'.time().'.sql');
            
        }
    }
    if(isset($_POST['submit'])){        
        $dbname = ''.$dbname.'-'.date('Y-m-d').'.sql';
        $target_path = 'assets/backup/';
        move_uploaded_file($_FILES["file"]["tmp_name"], $target_path . '/' . $dbname);    
        $message = 'Successfully uploaded. You can now <a href=dashboard.php?process=restore>restore</a> the database!';
    }
	
	
	/*Getting data visitor information for graph counter*/
	if(isset($_GET['last_x_days'])) $last_x_days=$_GET['last_x_days']; # register globals off?
		if (isset($last_x_days))
		{
		  $last_x_days=round($last_x_days);
		  if ($last_x_days>0) $limit=$last_x_days;
		}


if ($no_cache == 1)
{
  # no cache
  header("Expires: Tue, 17 Jun 2030 01:00:00 GMT");			# Date in the past
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");	# always modified
  header("Cache-Control: no-store, no-cache, must-revalidate");	# HTTP/1.1
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");							# HTTP/1.0
}

function diff_of_days($start_date,$end_date)
{
	// Gets the difference in days between two dates.(YYYY-MM-DD)
      // total numbers of days in range = diff_of_days($start_date,$end_date) + 1
	$s_parts = explode("-",$start_date);
	$e_parts = explode("-",$end_date);
	$s_timestamp = mktime(0,0,0,$s_parts[1],$s_parts[2],$s_parts[0]);
	$e_timestamp = mktime(0,0,0,$e_parts[1],$e_parts[2],$e_parts[0]);
	$difference = abs($e_timestamp - $s_timestamp)/86400;
	return($difference);
}

function date_x_days_ago($start_date,$xdays)
{
	// Gets the date x days before $start_date. (YYYY-MM-DD)
	$s_parts = explode("-",$start_date);
	$e_timestamp = mktime(0,0,0,$s_parts[1],$s_parts[2]-$xdays,$s_parts[0]);
	$end_date = date("Y-m-d",$e_timestamp);
	return($end_date);
}

$today = date("Y-m-d");
$time1 = date("H:i:s");
$time2 = date("g:i a");

			
?>
<!DOCTYPE html>
<html lang="<?php echo langName(); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ADMIN - Dashboard</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
	<!-- DateTimePicker CSS -->
	<link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	
	<link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
    <!-- Custom CSS -->
    <link href="assets/css/styles.css" rel="stylesheet">
	
    <!-- Custom Fonts -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap-colorpicker.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap-toggle.css" rel="stylesheet">
	
	<link rel="shortcut icon" href="assets/images/favicon.ico" type="image/x-icon" >
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/colorpicker-color.js"></script>
	<script src="assets/js/bootstrap-colorpicker.js"></script>
	<!-- SweetAlert CSS -->
	<script src="assets/js/sweetalert.min.js"></script> 
	<script src="assets/js/jquery.vide.js"></script>

	<script src="assets/js/dygraph.js"></script>
	
	<link href='assets/css/fullcalendar.css' rel='stylesheet' />
	<script src='../assets/js/moment.min.js'></script>
	<script src='../assets/js/fullcalendar.min.js'></script>

	<!-- include summernote css/js-->
	<link href='https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.css' rel='stylesheet'>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.4/summernote.js'></script>

	<?php if ($active == "settings.php")
		echo "
	<!-- include Menu Order css-->
	<link rel='stylesheet' href='//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css'>
	";
	?>
	
</head>
<body <?php if ($active == "map.php") echo "onLoad='initialize()'" ?>>
	<!-- Modal -->
<div class="modal fade" id="<?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo lang('CLOSE');?></span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" aria-hidden="true"></i> <?php echo lang('UPDATE_INFO');?></h4>
      </div>
      <div class="modal-body">
        <form id="editar_utilizador" method="post" enctype="multipart/form-data" class="form-horizontal" name="utilizadoreditado">
				<?php echo editUser($_SESSION['user']['username']); ?>
				<?php $csrf->echoInputField(); ?>
				<!-- Button -->
					<div class="form-group">
					  <label class="col-md-12 control-label" for="singlebutton"></label><br/>
					  <div class="col-md-6"></div>
					  <div class="col-md-6">
						<input type="submit" name="utilizadoreditado"  class="btn btn-success" value="<?php echo lang('UPDATE_INFO');?>" />
					  </div>
					</div>

				</fieldset>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
      </div>
    </div>
  </div>
</div>
    <div id="wrapper">	
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">		
            <!-- Brand and toggle get grouped for better mobile display -->
			<div class="topbar">			
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?php
					global $conection;
					$sql = mysqli_query($conection,"select * from logo");
					$row = mysqli_fetch_assoc($sql);
					$image = $row['image'];
					$logo_text = $row['logo_text'];
					
					if ($logo_text == 'logo'){

						echo "
							<a class='navbar-brand col-md-6 page-scroll' href='dashboard'>
								<img src ='../assets/img/uploads/other/".$row['image']."' class='img-responsive' />
							</a>";
					}
					if ($logo_text != 'logo'){

						echo "<a class='navbar-brand' href='dashboard'>MARKETING CMS</a>";
					}
				?>     
				</div>				
				<!-- Top Menu Items -->
				 <ul id="info" class="nav navbar-right">				 
					<!-- Button trigger modal -->
					<a href="../" target="_blank" class="btn btn-info"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo lang('VIEW');?></a>
					<button type="button" class="btn btn-success" data-toggle="modal" data-target="#<?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>">
					 <i class="fa fa-user" aria-hidden="true"></i>  <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>
					</button>					
					<a href="logout.php" class="btn btn-warning"><i class="fa fa-close" aria-hidden="true"></i> <?php echo lang('LOG_OUT');?></a>
				</ul>				
			</div>