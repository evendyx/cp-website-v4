<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('DASHBOARD');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li class="active"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
				
		<div class="row">
			<div class="col-md-12">
				<?php include 'statistics.php'; ?>
				 <?php
					$GLOBALS['hits_table_name'] = "hits";
					$GLOBALS['info_table_name'] = "info";
					
					// GET PAGE HIT INFO FROM DB
					$sql = "SELECT * FROM ".$GLOBALS['hits_table_name'];
					$query = $db->prepare($sql);
					$query->execute();
					$page_hits = $query->fetchAll();

					// GET NUMBER OF UNIQUE VISITORS
					$sql = "SELECT COUNT(DISTINCT ip_address) AS alias FROM ".$GLOBALS['info_table_name'];
					$query = $db->prepare($sql);
					$query->execute();
					$unique_visitors = $query->fetch()['alias'];

					// GET VISITOR INFO FROM DB
					$sql = "SELECT * FROM ".$GLOBALS['info_table_name']." ORDER BY time_accessed ASC";
					$query = $db->prepare($sql);
					$query->execute();
					$hits_info = $query->fetchAll();

					// ONLY SHOW 10 LATEST VISITOR INFO
					$visitor_info = array_slice(array_reverse($hits_info), 0, 6);
				  
				  ?>
			</div>
			
		</div>
	
		<div class="row">
			<div class="col-md-12">
			
					<div class="panel-body">
					
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="fa fa-line-chart" aria-hidden="true"></i>
									<?php echo lang('THEME_VIEWS');?>
								</h5>
							</div>
							<div class="panel-body">

								<!-- Chart 1 -->
								<html>
								  <head>
									<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
									<script type="text/javascript">
									  google.charts.load('current', {'packages':['corechart']});
									  google.charts.setOnLoadCallback(drawChart);

									  function drawChart() {
										var data = google.visualization.arrayToDataTable([
										  ['Date', 'Views'],
										  <?php echo theStats(); ?>
										]);

										var options = {
										title: "Visitors Per Day",
										curveType: 'function',
										animation: {
											  duration: 2000,
											  easing: 'out',
											  startup: true
										  },
										colors: ['#13bcb9'],
                                        chartArea: {width: '95%', height: '80%'},
                                        hAxis:  {showTextEvery: 2, textStyle: {fontSize: '9'}},
                                        legend: {position: 'top', textStyle: {color: 'blue', fontSize: 12}},
                                        lineWidth: 4,
                                        pointShape: 'circle',
                                        pointSize: 6,
                                        vAxis: {textPosition: 'out', gridlines: {count: 4}, minorGridlines: {count: 2}, textStyle: {fontSize: 12}},
										};

										var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
										chart.draw(data, options);
									  }
									</script>
								  </head>
								  <body>
									<div id="chart_div" style="width: 100%; height: 500px;"></div>
								  </body>
								</html>

							</div>
						</div>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-area-chart" aria-hidden="true"></i>
								<?php echo lang('PAGE_HITS_VISUALIZATIONS');?>
							</h5>
						</div>
					
						<div class="panel-body">
						
							<table cellspacing="0" cellpadding="0">
								<?php		
									// PRINT PAGE HIT INFO
									$total_hits = 0;
									foreach($page_hits as $ind_page){
										echo '<tr><td><strong>'.$ind_page['page'].'</strong>:</td>
										<td>'.$ind_page['count'].'</td></tr>';
										$total_hits += $ind_page['count'];
									}
									?>
									<tr id="total">
										<td><strong>Total</strong>:</td>
										<td>
											<?php echo $total_hits; ?>
										</td>
									</tr>
							</table>

						</div>
					</div>
						
				</div>
				
				<div class="col-lg-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i>
								<?php echo lang('VISITOR_INFORMATION');?>
							</h5>
						</div>
						<div class="panel-body">
							<strong><?php echo lang('TOTAL_OF_UNIQUE_VISITORS');?></strong>:
							<?php echo $unique_visitors; ?>
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td><strong><?php echo lang('IP_ADDRESS');?></strong></td>
									<td><strong><?php echo lang('USER_AGENT');?></strong></td>
									<td><strong><?php echo lang('TIME_ACCESSED');?></strong></td>
								</tr>
								<?php
									// PRINT VISITOR INFO
									foreach($visitor_info as $ind_visitor){
									  echo '<tr><td>'.$ind_visitor['ip_address'].'</td>
										<td>'.$ind_visitor['user_agent'].'</td>
										  <td>'.$ind_visitor['time_accessed'].'</td></tr>';
									}
								?>
							</table>
						</div>
					</div>
				</div>	
				
			</div>

		<!-- /.row -->

	</div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>