<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_TABLE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="pricing.php"><?php echo lang('PRICING_TABLES');?></a></li>
				<li class="active"><?php echo lang('EDIT_TABLE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					 <form id="editTable" method="post" enctype="multipart/form-data" class="form-horizontal" name="editTable">
										
					<?php $csrf->echoInputField(); ?>
					
					<fieldset>
						
					<?php echo editTable($_GET['id']); ?>
					
					<div class="form-group">
						<label class='col-md-2 control-label' for='color'><?php echo lang('TABLE_COLOR');?></label>
							<div class='col-md-6'>
							  <div id="cp1" class="input-group colorpicker-component">
								<input type="text" name="color" value="<?php echo getTableColor($_GET['id']); ?>" class="form-control" />
								<span class="input-group-addon"><i></i></span>
							</div>
						</div>
					</div>

					<!-- Button -->
					<div class="form-group">
						<label class="col-md-6 control-label" for="singlebutton"></label>
						<div class="col-md-4">
							<input type="submit" name="editTable" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
						</div>
					</div>
					<script>
						$(function() {
							$('#cp1').colorpicker();
						});
					</script>
					</fieldset>
				</form>
				<?php

				  if(isset($_POST['editTable']))
				  {
					updateTable($_GET['id'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['hint'], ENT_QUOTES),htmlspecialchars($_POST['price'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES),htmlspecialchars($_POST['purchase'], ENT_QUOTES),$_POST['color'],$_POST['link']);
				  }

				?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
    </div>
    <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>