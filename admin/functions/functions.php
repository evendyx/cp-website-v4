<?php
require 'config.php';


/*===============================================================================================================================*/
/*==================================================== Statistics Functions =====================================================*/
/*===============================================================================================================================*/

// List number of Database created clients
function listClients(){
	global $conection;
	$result = mysqli_query($conection,"SELECT COUNT(*) FROM clients;");
	$row = mysqli_fetch_array($result);
	$total = $row[0];
	echo $total;
}

// List number of Database created Services
function listServices(){
	global $conection;
	$result = mysqli_query($conection,"SELECT COUNT(*) FROM service;");
	$row = mysqli_fetch_array($result);
	$total = $row[0];
	echo $total;
}

// List number of Database created Testimonials
function listTestimonials(){
	global $conection;
	$result = mysqli_query($conection,"SELECT COUNT(*) FROM testimonials;");
	$row = mysqli_fetch_array($result);
	$total = $row[0];
	echo $total;
}

// List number of Database created Partners
function listPartners(){
	global $conection;
	$result = mysqli_query($conection,"SELECT COUNT(*) FROM partners;");
	$row = mysqli_fetch_array($result);
	$total = $row[0];
	echo $total;
}


/*===============================================================================================================================*/
/*=============================================== ANTI SQL INJECTION Function ===================================================*/
/*===============================================================================================================================*/

function antiSQLInjection($texto){
	// Words for search
	$check[1] = chr(34); // simbol "
	$check[2] = chr(39); // simbol '
	$check[3] = chr(92); // simbol /
	$check[4] = chr(96); // simbol `
	$check[5] = "drop table";
	$check[6] = "update";
	$check[7] = "alter table";
	$check[8] = "drop database";
	$check[9] = "drop";
	$check[10] = "select";
	$check[11] = "delete";
	$check[12] = "insert";
	$check[13] = "alter";
	$check[14] = "destroy";
	$check[15] = "table";
	$check[16] = "database";
	$check[17] = "union";
	$check[18] = "TABLE_NAME";
	$check[19] = "1=1";
	$check[20] = 'or 1';
	$check[21] = 'exec';
	$check[22] = 'INFORMATION_SCHEMA';
	$check[23] = 'like';
	$check[24] = 'COLUMNS';
	$check[25] = 'into';
	$check[26] = 'VALUES';

	// Cria se as vari�veis $y e $x para controle no WHILE que far� a busca e substitui��o
	$y = 1;
	$x = sizeof($check);
	// Faz-se o WHILE, procurando alguma das palavras especificadas acima, caso encontre alguma delas, este script substituir� por um espa�o em branco " ".
	while($y <= $x){
		   $target = strpos($texto,$check[$y]);
			if($target !== false){
				$texto = str_replace($check[$y], "", $texto);
			}
		$y++;
	}
	// Retorna a vari�vel limpa sem perigos de SQL Injection
	return $texto;
}

/*===============================================================================================================================*/
/*====================================================== Clients Functions ======================================================*/
/*===============================================================================================================================*/

// Display All clients over the Clients Dashboard
function listClientsBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM clients ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == ""){
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
                  <th>".lang('NAME')."</th>
                  <th>".lang('EMAIL')."</th>
					<th></th>				  
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['name'];
			echo "</td><td>";			
			echo $row['email'];
			echo "</td>";
			echo "<td class='r'>
			<a href='edit_client.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaCliente(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ){
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0){
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++){
			if($i != $pagina){
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit client function
function editClient($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from clients WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
				

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='name'>".lang('NAME')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='name' name='name' type='text' class='form-control input-md'>".$row['name']."</textarea>
						
					  </div>
					</div>

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='email'>".lang('EMAIL')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='email' name='email' type='text' class='form-control input-md'>".$row['email']."</textarea>
						
					  </div>
					</div>

				";

}

// Update client Information
function updateClient($id,$name,$email){
	global $conection;
	$query = mysqli_query($conection,"UPDATE clients SET name = '$name', email = '$email' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CLIENT_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; clients.php">'; 
					die();
			}				
			return true;
}


/*===============================================================================================================================*/
/*==================================================== Update User Info =========================================================*/
/*===============================================================================================================================*/

// Edit User Information
function editUser($username){
	global $conection;
    $consulta = mysqli_query($conection,"select * from users WHERE username='".$_SESSION['user']['username']."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "	
					
					<div class='col-md-6'>
						".lang('LANGUAGE').": <input disabled='disabled' class='form-control' name='lang' value='".$row['lang']."' />
					</div>
					<div class='col-md-6'>
						".lang('NEW_LANGUAGE')."
						<select class='form-control' name='lang'>
						  <option value='en'>English</option>
						  <option value='pt'>Portugues</option>
						  <option value='fr'>Francais</option>
						</select>
						<br/>
					</div>
					
					<div class='col-md-6'>
						".lang('LOGIN').":
						<textarea rows ='1' cols='40' class='form-control' name='username' >".$row['username']."</textarea><br />
					</div>
					<div class='col-md-6'>
						".lang('EMAIL').":
						<textarea rows ='1' cols='40' class='form-control' name='email' >".$row['email']."</textarea><br />
					</div>
					<div class='col-md-6'>	
						".lang('NEW_PASSWORD').":
						<input type='password' name='password' class='form-control' placeholder='".lang('NEW_PASSWORD')."' required value='' />
					</div>
					<div class='col-md-6'>	
						".lang('NEW_PASSWORD_REPEAT').":
						<input type='password' name='password1' class='form-control' placeholder='".lang('NEW_PASSWORD_REPEAT')."' required value='' />
					</div>
				<br/><br/><br/><br/>

				"

        ;

}

// Update User Information
function updateUser($id,$username,$password,$salt,$email,$lang){
	global $conection;
	 // Security measures
        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
        $password = hash('sha256', $_POST['password'] . $salt);
		for($round = 0; $round < 65536; $round++){ $password = hash('sha256', $password . $salt); }		
	
	$query = mysqli_query($conection,"update users set username='$username', password='$password', salt='$salt', email='$email', lang='$lang' where username='$id';");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('ACCOUNT')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; '.$_SERVER['PHP_SELF'].'">'; 
					die();
			}		
			return true;
}

// Select Language from Database for setting it as the default language relative to language settings
function language(){
	global $conection;
	$consulta = mysqli_query($conection,"select lang from users WHERE username='".$_SESSION['user']['username']."'");
    $row = mysqli_fetch_assoc($consulta);
    
	require("assets/languages/".$row['lang'].".php");
	
}

// Select Language from Database for setting it as the default language relative to language settings
function languageFront(){
	global $conection;
	$consulta = mysqli_query($conection,"select lang from users");
    $row = mysqli_fetch_assoc($consulta);
	
	$resp = $row['lang'];
    
	if($resp == ''){
		require("admin/assets/languages/en.php");
	}
	if($resp != ''){
		require("admin/assets/languages/".$row['lang'].".php");
	}
    
}

// Select Language from Database for setting it as the default language relative to language settings
function langName(){
	global $conection;
	$consulta = mysqli_query($conection,"select lang from users WHERE username='".$_SESSION['user']['username']."'");
    $row = mysqli_fetch_assoc($consulta);
    	
	echo $row['lang'];
}
	
// Display Contacts on the admin
function contacts(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contacts WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);

echo "
<div class='col-md-5'>
  <h3>".lang('ADDRESS')."</h3>
	<br/>
  <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='address'>".lang('ADDRESS')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='1' cols='10' id='address' name='address' type='text' class='form-control input-md'>".$row['address']."</textarea>
	</div>
  </div>
  
  <!-- Text input-->
  <div class='form-group'>
  <label class='col-sm-2 control-label' for='code'>".lang('POSTCODE')."</label>
	<div class='col-sm-10'>
	  <textarea rows ='1' cols='10' id='code' name='code' type='text' class='form-control input-md'>".$row['code']."</textarea>
	</div>
  </div>
  
   <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='city'>".lang('CITY')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='1' cols='10' id='city' name='city' type='text' class='form-control input-md'>".$row['city']."</textarea>
	</div>
  </div>

 <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='latitude'>".lang('LATITUDE')."</label>
	<div class='col-sm-10'>
	  <textarea rows ='1' cols='10' id='latitude' name='latitude' type='text' class='form-control input-md'>".$row['latitude']."</textarea>
	</div>
  </div>

  <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='longitude'>".lang('LONGITUDE')."</label>
	<div class='col-sm-10'>
	  <textarea rows ='1' cols='10' id='longitude' name='longitude' type='text' class='form-control input-md'>".$row['longitude']."</textarea>
	</div>
  </div>
  <br/>

  <h3>".lang('OTHER_INFO')."</h3>
  <br/>
   <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='title'>".lang('WEBSITE_TITLE')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
	</div>
  </div>
  <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='email'>".lang('EMAIL')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='1' cols='10' id='email' name='email' type='text' class='form-control input-md'>".$row['email']."</textarea>
	</div>
  </div>

  <!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='phone'>".lang('PHONE')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='1' cols='10' id='phone' name='phone' type='text' class='form-control input-md'>".$row['phone']."</textarea>
	</div>
	</div>
	
	<!-- Text input-->
  <div class='form-group'>
	<label class='col-sm-2 control-label' for='footer'>".lang('FOOTER_TEXT')."</label>
	<div class='col-sm-10'>
	   <textarea rows ='2' cols='10' id='footer' name='footer' type='text' class='form-control input-md'>".$row['footer']."</textarea>
	</div>
	</div>
</div>
<div class='col-md-7'>
  <h3>".lang('SOCIAL_NETWORKS')."</h3>
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='facebook'><i class='fa fa-facebook-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='facebook' name='facebook' type='text' class='form-control input-md'>".$row['facebook']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='twitter'><i class='fa fa-twitter-square' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='twitter' name='twitter' type='text' class='form-control input-md'>".$row['twitter']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='linkedin'><i class='fa fa-linkedin-square' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='linkedin' name='linkedin' type='text' class='form-control input-md'>".$row['linkedin']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='google'><i class='fa fa-google-plus-square' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='google' name='google' type='text' class='form-control input-md'>".$row['google']."</textarea>
	</div>

	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='youtube'><i class='fa fa-youtube-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='youtube' name='youtube' type='text' class='form-control input-md'>".$row['youtube']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='instagram'><i class='fa fa-instagram' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='instagram' name='instagram' type='text' class='form-control input-md'>".$row['instagram']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='pinterest'><i class='fa fa-pinterest-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='pinterest' name='pinterest' type='text' class='form-control input-md'>".$row['pinterest']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='tumblr'><i class='fa fa-tumblr-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='tumblr' name='tumblr' type='text' class='form-control input-md'>".$row['tumblr']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='vine'><i class='fa fa-vine' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='vine' name='vine' type='text' class='form-control input-md'>".$row['vine']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='snapchat'><i class='fa fa-snapchat-square' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='snapchat' name='snapchat' type='text' class='form-control input-md'>".$row['snapchat']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='reddit'><i class='fa fa-reddit-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='reddit' name='reddit' type='text' class='form-control input-md'>".$row['reddit']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='flickr'><i class='fa fa-flickr' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='flickr' name='flickr' type='text' class='form-control input-md'>".$row['flickr']."</textarea>
	</div>
	
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='soundcloud'><i class='fa fa-soundcloud' aria-hidden='true'></i></span>
	  <textarea rows ='1' cols='10' id='soundcloud' name='soundcloud' type='text' class='form-control input-md'>".$row['soundcloud']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='whatsapp'><i class='fa fa-whatsapp' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='whatsapp' name='whatsapp' type='text' class='form-control input-md'>".$row['whatsapp']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='slack'><i class='fa fa-slack' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='slack' name='slack' type='text' class='form-control input-md'>".$row['slack']."</textarea>
	</div>
	
	<div class='input-group col-sm-6'>
	  <span class='input-group-addon' id='xing'><i class='fa fa-xing-square' aria-hidden='true'></i></span>
	   <textarea rows ='1' cols='10' id='xing' name='xing' type='text' class='form-control input-md'>".$row['xing']."</textarea>
	</div>
	</div>
";
}

// Update Contacts Information
function update_contacts($address,$code,$city,$latitude,$longitude,$title,$email,$phone,$footer,$facebook,$twitter,$linkedin,$google,$youtube,$instagram,$pinterest,$tumblr,$vine,$snapchat,$reddit,$flickr,$soundcloud,$whatsapp,$slack,$xing){			
	global $conection;
	$query = mysqli_query($conection,"update contacts set address='$address', code='$code', city='$city', latitude='$latitude', longitude='$longitude', title='$title', email='$email', phone='$phone', footer='$footer', facebook='$facebook', twitter='$twitter', linkedin='$linkedin', google='$google', youtube='$youtube', instagram='$instagram', pinterest='$pinterest', tumblr='$tumblr', vine='$vine', snapchat='$snapchat', reddit='$reddit', flickr='$flickr', soundcloud='$soundcloud', whatsapp='$whatsapp', slack='$slack', xing='$xing'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CONTACTS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="2; contacts.php">'; 
			die(); 
			}		
			return true;
}  
		
	
// Display all information from user
function getContacts(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contacts");
    $row = mysqli_fetch_assoc($sql);
	
	echo "
		<!-- Footer -->
		<footer class='footer-bs'>
        <div class='row'>
        	<div class='col-md-4 col-sm-6 text-center footer-brand'>
				<h3>About us</h3>
				";
				?>
				<?php
					global $conection;
					$sql = mysqli_query($conection,"select * from logo");
					$row = mysqli_fetch_assoc($sql);
					$image = $row['image'];
					$logo_text = $row['logo_text'];
					
					if ($logo_text == 'logo'){

						echo "
							<div class='col-md-12'>				
									<img src ='assets/img/uploads/other/".$row['image']."' class='img-responsive' />
							</div><br/>";
					}
					if ($logo_text != 'logo'){

						echo "";
					}
				?> 
				<?php
					global $conection;
					$sql = mysqli_query($conection,"select * from contacts");
					$row = mysqli_fetch_assoc($sql);				
				echo "
            	<h4>".$row['title']."</h4>
				<i class='fa fa-home fa-fw'></i> ".$row['address']." | ".$row['code']."
				<ul class='list-unstyled'>
					<li><i class='fa fa-map-marker fa-fw'></i> ".$row['longitude']." | ".$row['latitude']."</li>
					<li><i class='fa fa-envelope-o fa-fw'></i> <a href='mailto:".$row['email']."'>".$row['email']."</a></li>
					<li><i class='fa fa-phone fa-fw'></i> ".$row['phone']."</li>
					
				</ul>
            </div>
        	<div class='col-md-2 col-sm-6 footer-nav'>
            	<h3>Menu</h3>
                    <ul class='pages'>
						";
						?>
						<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from settings");
						$row = mysqli_fetch_assoc($sql);
						$about = $row['about'];
						$services = $row['services'];
						$partners = $row['partners'];
						$portfolio = $row['portfolio'];
						$blog = $row['blog'];
						$contacts = $row['contacts'];
						$custom = $row['custom'];
						$privacy = $row['privacy'];
						
						if ($about == '1'){

							echo " 
							<li>
								<a href='about'>".lang('ABOUT')."</a>
							</li>";

						}
						if ($about == ''){
							echo " ";
						}
						if ($services == '1'){

							echo " 
							<li>
								<a href='services'>".lang('SERVICES')."</a>
							</li>";

						}
						if ($services == ''){
							echo " ";
						}
						
						if ($custom == '1'){

							echo showPages();

						}
						if ($custom == ''){
							echo " ";
						}
						
						
						if ($portfolio == '1'){

							echo " 
							<li>
								<a href='portfolio'>".lang('PORTFOLIO')."</a>
							</li>";

						}
						if ($portfolio == ''){
							echo " ";
						}
						if ($blog == '1'){

							echo " 
							<li>
								<a href='blog'>".lang('BLOG')."</a>
							</li>";

						}
						if ($blog == ''){
							echo " ";
						}
						if ($partners == '1'){

							echo " 
							<li>
								<a href='partners'>".lang('PARTNERS')."</a>
							</li>";

						}
						if ($partners == ''){
							echo " ";
						}
						if ($contacts == '1'){

							echo " 
							<li>
								<a href='contacts'>".lang('CONTACTS')."</a>
							</li>";

						}
						if ($contacts == ''){
							echo " ";
						}
						if ($privacy == '1'){

							echo " 
							<li>
								<a href='privacy'>".lang('PRIVACY')."</a>
							</li>";

						}
						if ($privacy == ''){
							echo " ";
						}
					?>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from contacts");
						$row = mysqli_fetch_assoc($sql);
						echo "
                    </ul>				
            </div>
        	<div class='col-md-2 col-sm-6 footer-social'>
            	<h3>Follow Us</h3>
            	<ul>
				";
	?>
	<?php 
		if (!empty($row['facebook'])){
			echo"<li><a href='".$row['facebook']."' target='_blank'> Facebook</a></li>";
		}
		if (!empty($row['twitter'])){
			echo"<li><a href='".$row['twitter']."' target='_blank'> Twitter</a></li>";
		}
		if (!empty($row['linkedin'])){
			echo"<li><a href='".$row['linkedin']."' target='_blank'> Linkedin</a></li>";
		}
		if (!empty($row['google'])){
			echo"<li><a href='".$row['google']."' target='_blank'> Google+</a></li>";
		}
		if (!empty($row['youtube'])){
			echo"<li><a href='".$row['youtube']."' target='_blank'> Youtube</a></li>";
		}
		if (!empty($row['instagram'])){
			echo"<li><a href='".$row['instagram']."' target='_blank'> Instagram</a></li>";
		}
		if (!empty($row['pinterest'])){
			echo"<li><a href='".$row['pinterest']."' target='_blank'> Pinterest</a></li>";
		}
		if (!empty($row['tumblr'])){
			echo"<li><a href='".$row['tumblr']."' target='_blank'> Tumblr</a></li>";
		}
		if (!empty($row['vine'])){
			echo"<li><a href='".$row['vine']."' target='_blank'> Vine</a></li>";
		}
		if (!empty($row['snapchat'])){
			echo"<li><a href='".$row['snapchat']."' target='_blank'> Snapchat</a></li>";
		}
		if (!empty($row['reddit'])){
			echo"<li><a href='".$row['reddit']."' target='_blank'> Reddit</a></li>";
		}
		if (!empty($row['flickr'])){
			echo"<li><a href='".$row['flickr']."' target='_blank'> Flickr</a></li>";
		}
		if (!empty($row['soundcloud'])){
			echo"<li><a href='".$row['soundcloud']."' target='_blank'> Soundcloud</a></li>";
		}
		if (!empty($row['whatsapp'])){
			echo"<li><a href='".$row['whatsapp']."' target='_blank'> Whatsapp</a></li>";
		}
		if (!empty($row['slack'])){
			echo"<li><a href='".$row['slack']."' target='_blank'> Slack</a><";
		}
		if (!empty($row['xing'])){
			echo"<li><a href='".$row['xing']."' target='_blank'> Xing</a></li>";
		}	
	?>
	<?php 
	echo "
                </ul>
            </div>
        	<div class='col-md-4 col-sm-6 footer-nav'>
            	<h3>Latest Posts</h3>
                    <ul class='list'>
					";
					?>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from post ORDER BY id DESC LIMIT 4");
						$row = mysqli_num_rows($sql);	
						while ($row = mysqli_fetch_array($sql)) {
						
							echo"<li><a href='post?id=". $row['id'] . "'>".$row['title']."</a></li>";	
							
							} 
					?>
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select * from contacts");
						$row = mysqli_fetch_assoc($sql);
						echo "
                    </ul>
            </div>
        </div>
    </footer>
    <section id='the-end' class='text-center'><p>".$row['footer']."</p></section>
	<!-- ./Footer -->
	  ";
}

// Display all information from user
function getSocial(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contacts");
    $row = mysqli_fetch_assoc($sql);
	
		if (!empty($row['facebook'])){
			echo"<li><a href='".$row['facebook']."' target='_blank'> <i class='fa fa-facebook' aria-hidden='true'></i></a></li>";
		}
		if (!empty($row['twitter'])){
			echo"<li><a href='".$row['twitter']."' target='_blank'> <i class='fa fa-twitter' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['linkedin'])){
			echo"<li><a href='".$row['linkedin']."' target='_blank'> <i class='fa fa-linkedin' aria-hidden='true'></i></a></li>";
		}
		if (!empty($row['google'])){
			echo"<li><a href='".$row['google']."' target='_blank'> <i class='fa fa-google-plus' aria-hidden='true'></i></a></li>";
		}
		if (!empty($row['youtube'])){
			echo"<li><a href='".$row['youtube']."' target='_blank'> <i class='fa fa-youtube' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['instagram'])){
			echo"<li><a href='".$row['instagram']."' target='_blank'> <i class='fa fa-instagram' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['pinterest'])){
			echo"<li><a href='".$row['pinterest']."' target='_blank'> <i class='fa fa-pinterest' aria-hidden='true'></i></a></li>";
		}
		if (!empty($row['tumblr'])){
			echo"<li><a href='".$row['tumblr']."' target='_blank'> <i class='fa fa-tumblr' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['vine'])){
			echo"<li><a href='".$row['vine']."' target='_blank'> <i class='fa fa-vine' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['snapchat'])){
			echo"<li><a href='".$row['snapchat']."' target='_blank'> <i class='fa fa-snapchat' aria-hidden='true'></i></a></li>";
		}
		if (!empty($row['reddit'])){
			echo"<li><a href='".$row['reddit']."' target='_blank'> <i class='fa fa-reddit' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['flickr'])){
			echo"<li><a href='".$row['flickr']."' target='_blank'> <i class='fa fa-flickr' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['soundcloud'])){
			echo"<li><a href='".$row['soundcloud']."' target='_blank'> <i class='fa fa-soundcloud' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['whatsapp'])){
			echo"<li><a href='".$row['whatsapp']."' target='_blank'> <i class='fa fa-whatsapp' aria-hidden='true'></i> </a></li>";
		}
		if (!empty($row['slack'])){
			echo"<li><a href='".$row['slack']."' target='_blank'> <i class='fa fa-slack' aria-hidden='true'></i> </a><";
		}
		if (!empty($row['xing'])){
			echo"<li><a href='".$row['xing']."' target='_blank'> <i class='fa fa-xing' aria-hidden='true'></i> </a></li>";
		}	

}


// Function to diplay progress bar under homepage slider area
function progressBar(){
	global $conection;
	$sql = mysqli_query($conection,"select * from progress WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
echo "

		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='percent'>".lang('PERCENT')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='percent' name='percent' type='text' class='form-control input-md'>".$row['percent']."</textarea>
			</div>
		  </div>
		  		  
			<!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='height'>".lang('HEIGHT')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='height' name='height' type='text' class='form-control input-md'>".$row['height']."</textarea>
			</div>
		  </div>
		  ";
}

// Update progress bar function
function updateProgressBar(){
	global $conection;
	$sql = mysqli_query($conection,"select * from progress WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);	
		echo "
			
			<div class='progress'>
			  <div class='progress-bar progress-bar-striped active' role='progressbar'
			  aria-valuenow='".$row['percent']."' aria-valuemin='0' aria-valuemax='100' style='width:".$row['percent']."%'>
				".$row['percent']."%
			  </div>			  
			  <div class='progress'>
				  <div class='progress-bar' role='progressbar' data-transitiongoal='100'></div>
				</div>
				
			</div>	
	";	
}

// Return Color for Progress bar
function getColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from progress WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$color = $row['color'];
	
	echo $color;
	
}

// Update Progress bar area Function
function updateBar($percent,$height,$color){
	global $conection;
	$query = mysqli_query($conection,"UPDATE progress SET percent = '$percent', height = '$height', color = '$color'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PROGRESS_BAR_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; progress.php">'; 
			die();
			}				
			return true;
}

// Display Countdown area under Homepage Slider 
function countDown(){
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
echo "

		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='year'>".lang('YEAR')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='year' name='year' type='text' class='form-control input-md'>".$row['year']."</textarea>
			</div>
		  </div>
		  		  
			<!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='month'>".lang('MONTH')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='month' name='month' type='text' class='form-control input-md'>".$row['month']."</textarea>
			</div>
		  </div>
		  
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='day'>".lang('DAY')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='day' name='day' type='text' class='form-control input-md'>".$row['day']."</textarea>
			</div>
		  </div>
		  
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='bradius'>".lang('BORDER_RADIUS')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='bradius' name='bradius' type='text' class='form-control input-md'>".$row['bradius']."</textarea>
			</div>
		  </div>
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='border'>".lang('BORDER')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='border' name='border' type='text' class='form-control input-md'>".$row['border']."</textarea>
			</div>
		  </div>
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='dsize'>".lang('DATE_SIZE')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='dsize' name='dsize' type='text' class='form-control input-md'>".$row['dsize']."</textarea>
			</div>
		  </div>
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='wsize'>".lang('WORDS_SIZE')."</label>
			<div class='col-sm-4'>
			   <textarea rows ='1' cols='10' id='wsize' name='wsize' type='text' class='form-control input-md'>".$row['wsize']."</textarea>
			</div>
		  </div>
		  ";
}

//Get Border Color for the countdown area 
function getbcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$bcolor = $row['bcolor'];
	
	echo $bcolor;
	
}

//Get Date Color under countdown area 
function getdcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$dcolor = $row['dcolor'];
	
	echo $dcolor;
	
}

//Get Typography Color under countdown area 
function getwcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$wcolor = $row['wcolor'];
	
	echo $wcolor;
	
}

//Get Inicial Loader Color under Settings area 
function getlColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$lcolor = $row['lcolor'];

	echo $lcolor;	
}

//Get Font Color under Settings area for the menu area 
function getMenu_Color(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$menu_color = $row['menu_color'];

	echo $menu_color;	
}

//Get Background Color under Settings area for the menu area 
function getMenu_BColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$menu_bcolor = $row['menu_bcolor'];
	
	echo $menu_bcolor;
	
}

//Get Font Color under Settings area for the menu area when hover
function getMenu_ColorHover(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$menu_colorhover = $row['menu_colorhover'];

	echo $menu_colorhover;	
}

//Get Background Color under Settings area for the menu area when hover
function getMenu_BColorHover(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$menu_bcolorhover = $row['menu_bcolorhover'];
	
	echo $menu_bcolorhover;
	
}

//Get Font Color under Settings area for the primary sections
function getBackgroundTColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$background_tcolor = $row['background_tcolor'];
	
	echo $background_tcolor;
	
}

//Get background Color under Settings area for the primary sections
function getBackgroundColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$background_color = $row['background_color'];
	
	echo $background_color;
	
}

//Get background Color under Settings area for the sidebar menu area
function getBackgroundMenuColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$background_menu_color = $row['background_menu_color'];
	
	echo $background_menu_color;
	
}

//Get Items Color under Settings area for the sidebar menu area
function getItemsColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$items_color = $row['items_color'];
	
	echo $items_color;
	
}

//Get Footer Color under Settings area for the sidebar menu area
function getFooterColor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$footer_color = $row['footer_color'];
	
	echo $footer_color;
	
}

//Get Selected Font from Google Fonts
function getFont(){
	global $conection;
	$sql = mysqli_query($conection,"select * from dashboard WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$font = $row['font'];
	
	echo $font;
	
}

//Get Particles color under Slider Settings
function getppcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from particles WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$ppcolor = $row['ppcolor'];
	
	echo $ppcolor;
	
}

//Get Particles Background color under Slider Settings
function getbpcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from particles WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$bpcolor = $row['bpcolor'];
	
	echo $bpcolor;
	
}

//Get Particles degrad� Background color under Slider Settings
function getdpcolor(){
	global $conection;
	$sql = mysqli_query($conection,"select * from particles WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$dpcolor = $row['dpcolor'];
	
	echo $dpcolor;
	
}

//Get Logo Text
function getLogoText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from logo");
    $row = mysqli_fetch_assoc($sql);
	$logo_text = $row['logo_text'];
	
	echo $logo_text;
	
}

//Get Skill Color 
function getSkillColor($id){
	global $conection;
	$sql = mysqli_query($conection,"select * from skills WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
	$color = $row['color'];

	echo $color;	
}

//Get counter Color 
function getCounterColor($id){
	global $conection;
	$sql = mysqli_query($conection,"select * from counters WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
	$color = $row['color'];

	echo $color;	
}

//Get Table Color 
function getTableColor($id){
	global $conection;
	$sql = mysqli_query($conection,"select * from pricing WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
	$color = $row['color'];

	echo $color;	
}

//Function Update Theme Color Options From Settings page
function updateDashboard($font, $lcolor, $menu_color, $menu_bcolor, $menu_colorhover, $menu_bcolorhover, $background_color, $background_tcolor,$background_menu_color,$items_color,$footer_color){
	global $conection;
	$query = mysqli_query($conection,"UPDATE dashboard SET font = '$font', lcolor = '$lcolor', menu_color = '$menu_color', menu_bcolor = '$menu_bcolor', menu_colorhover = '$menu_colorhover', menu_bcolorhover = '$menu_bcolorhover', background_color = '$background_color', background_tcolor = '$background_tcolor', background_menu_color = '$background_menu_color', items_color = '$items_color', footer_color = '$footer_color' ");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('THEME_COLOR_OPTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;

}

//Function Update Particles Options From Slider page
function updateParticles($bpcolor, $dpcolor, $ppcolor){
	global $conection;
	$query = mysqli_query($conection,"UPDATE particles SET bpcolor = '$bpcolor', dpcolor = '$dpcolor', ppcolor = '$ppcolor'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PARTICLES_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; background.php#particles">'; 
			die();
			}				
			return true;

}

//Get Particles and display them on the index page
function getParticles(){
	global $conection;
	$sql = mysqli_query($conection,"select * from particles WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	<script type='text/javascript' src='assets/js/jquery.particleground.js'></script>
	<script type='text/javascript'>
		document.addEventListener('DOMContentLoaded', function () {
		  particleground(document.getElementById('particles'), {
			dotColor: '".$row['ppcolor']."',
			 density: 5000,
			lineColor: '".$row['ppcolor']."'
		  });
		}, false);
	</script>
	  ";
}

// Update Countdown Area
function updateCountDown($year,$month,$day,$bcolor,$bradius,$border,$dcolor,$dsize,$wcolor,$wsize){
	global $conection;
	$query = mysqli_query($conection,"UPDATE countdown SET year = '$year', month = '$month', day = '$day', bcolor = '$bcolor', bradius = '$bradius', border = '$border', dcolor = '$dcolor', dsize = '$dsize', wcolor = '$wcolor', wsize = '$wsize'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('COUNTDWON_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; countdown.php">'; 
			die();
			}				
			return true;
}

//Get Countdown Area and display them on the index page
function showCountDown(){
	global $conection;
	$sql = mysqli_query($conection,"select * from countdown WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	
		echo "
			
				<div class='section'>
                    <div id='defaultCountdown'></div>
					<script>
						$(function() {
							$('#defaultCountdown').countdown({
								until: new Date(".$row['year'].", ".$row['month']." - 1, ".$row['day'].")
							});
						});
					</script>
                </div>	
	";
	
}

/*================== About Text Description =====================*/

//Get information from about and display it under the About Section
function getAboutText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textabout WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the about Section
function updateAboutText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textabout SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('ABOUT_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; aboutpage.php">'; 
			die();
			}				
			return true;

}

//Function to display the about section in the index page
function showAboutText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textabout WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

//Function to display the about section in the index page
function showCalendarText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textcalendar");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Services Text Description =====================*/

//Get information from services and display it under the Services page
function getServicesText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textservices WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the Services text
function updateServicesText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textservices SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SERVICES_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; servicespage.php">'; 
			die();
			}				
			return true;

}

//Function to display the services text
function showServicesText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textservices WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Blog Text Description =====================*/

//Get information from Blog and display it under the Blog Section
function getBlogText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textblog WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the Blog description
function updateBlogText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textblog SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('BLOG_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; blogpage.php">'; 
			die();
			}				
			return true;

}

//Function to display the Blog page 
function showBlogText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textblog WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Portfolio Text Description =====================*/

//Get information from portfolio and display it under the portfolio Section
function getPortfolioText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textportfolio WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the portfolio description
function updatePortfolioText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textportfolio SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PORTFOLIO_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; portfoliopage.php">'; 
			die();
			}				
			return true;

}

//Function to display the portfolio page 
function showPortfolioText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textportfolio WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Partners Text Description =====================*/

//Get information from partners and display it under the partners page
function getPartnersText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textpartners WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the portfolio description
function updatePartnersText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textpartners SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PARTNERS_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; partnerspage.php">'; 
			die();
			}				
			return true;

}

//Function to display the portfolio page 
function showPartnersText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textpartners WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Privacy Text Description =====================*/

//Get information from privacy and display it under the partners page
function getPrivacyText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textprivacy WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the privacy description
function updatePrivacyText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textprivacy SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PRIVACY_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; privacypage.php">'; 
			die();
			}				
			return true;

}

//Function to display the privacy page 
function showPrivacyText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textprivacy WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*================== Contacts Text Description =====================*/

//Get information from contacts and display it under the contacts page
function getcontactsText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textcontacts WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
		<div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".htmlspecialchars_decode($row['description'])."</textarea>
		</div>
	  </div>
	  ";
	
}

//Function to update the portfolio description
function updateContactsText($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE textcontacts SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CONTACTS_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; contactspage.php">'; 
			die();
			}				
			return true;

}

//Function to display the portfolio page 
function showContactsText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from textcontacts WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = htmlspecialchars_decode($row['description']);
	
	echo $description;
}

/*===============================================================================================================================*/
/*================================================= Background Text Functions ===================================================*/
/*===============================================================================================================================*/

//Get the text information and display it under the slider area
function getTheText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from text WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>
	  <label class='col-sm-2 control-label' for='title'>".lang('TITLE')."</label>
		<div class='col-md-6'>
		   <textarea rows ='2' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
		</div>
	  </div>
	  
	   <!-- Text input-->
	  <div class='form-group'>
	  <label class='col-sm-2 control-label' for='latitude'>".lang('DESCRIPTION')."</label>
		<div class='col-md-6'>
		   <textarea rows ='4' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
		</div>
	  </div>
	  ";
	
}

//Update the slider area text information
function updateText($title,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE text SET title = '$title', description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('TEXT_SECTION_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; background.php#text">'; 
			die();
			}				
			return true;

}

//Display the Text information in the index page
function showText(){
	global $conection;
	$sql = mysqli_query($conection,"select * from text WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
	echo "
		<section id='home' class='home animated'>
			<div class='container'>
				<div class='row text-center'>
					<div class='col-lg-10 col-lg-offset-1'>
						<div class='row'>
							<h1>".$row['title']."</h1>
							<h3>".$row['description']."</h3>
						</div>
						<!-- /.row (nested) -->
					</div>
					<!-- /.col-lg-10 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
	";
}

//Display the Text information in the index page
function showSlider(){
	global $conection;
	$sql = mysqli_query($conection,"select * from slide");
    $row = mysqli_fetch_assoc($sql);
	echo "
	 <!-- Item -->
	  <div class='item'>
		<div class='img-fill'>
		  <img src='assets/img/uploads/other/".$row['image']."' alt=''>
		  <div class='info'>
			<div>
			  <h3>".$row['title']."</h3>
			  <h5>".$row['description']."</h5>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- // Item -->
	";
	while ($row = mysqli_fetch_array($sql)) {	
	echo "
	 <!-- Item -->
	  <div class='item'>
		<div class='img-fill'>
		  <img src='assets/img/uploads/other/".$row['image']."' alt=''>
		  <div class='info'>
			<div>
			  <h3>".$row['title']."</h3>
			  <h5>".$row['description']."</h5>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- // Item -->
	";
	}
}


//Display all the images under admin from the slider 
function getAllImages(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM slide ORDER BY id ASC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	 
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");

	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>			
                  <th>".lang('IMAGE')."</th>
				  <th>".lang('TITLE')."</th>
				  <th>".lang('DESCRIPTION')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo "<img style='width:50px; height:30px;' src='../assets/img/uploads/other/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo $row['description'];
			echo "<td class='r'>
			<a href='javascript:EliminaImageSlide(". $row['id'] . ");'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
	
}

//Get Youtube video_id
function getAllVideo(){
	global $conection;
	$sql = mysqli_query($conection,"select * from video");
	$row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo "
		<video width='250' height='200' controls>
			<source src='../assets/img/uploads/video/".$row['video_name']."' >
		</video> 
		<a href='javascript:EliminaVideo(". $row['v_id'] .")'class='btn btn-danger btn-sm delvideo' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
  ";
	}
}

// Write javascript slider in the index page
function listSlideshow(){
	global $conection;
	$sql = mysqli_query($conection,"select * from slide");
    $row = mysqli_fetch_assoc($sql);
	
		echo " 
		<script>		
		$(document).ready(function ()
		{

		  $('#resizeSlider').resizeSlider(
		  {
		//    'transition' : 750,
		//    'speed'      : 3750,
		//    'hover'      : false,
		//    'aleatorio'  : false,
		//    'float'      : 'right',
		//    'width'      : '500px',
		//    'height'     : '500px',
			'image':
			{
				 'presentation ".$row['id']."':
					  {
						'landscape':
						{
						  'url'      : 'url(assets/img/uploads/other/".$row['image'].")',
						  'size'     : 'cover',
						  'repeat'   : 'no-repeat',
						  'position' : 'center bottom'
						},
						'portrait':
						{
						  'url'      : 'url(assets/img/uploads/other/".$row['image'].")',
						  'size'     : 'cover',
						  'repeat'   : 'no-repeat',
						  'position' : 'left center'
						}
					  },
			  
					";
					while ($row = mysqli_fetch_array($sql)) {
					echo "
				
					 'presentation ".$row['id']."':
					  {
						'landscape':
						{
						  'url'      : 'url(assets/img/uploads/other/".$row['image'].")',
						  'size'     : 'cover',
						  'repeat'   : 'no-repeat',
						  'position' : 'center bottom'
						},
						'portrait':
						{
						  'url'      : 'url(assets/img/uploads/other/".$row['image'].")',
						  'size'     : 'cover',
						  'repeat'   : 'no-repeat',
						  'position' : 'left center'
						}
					  },
					  
					 ";
					}
					echo "
				}
			})
		  });		
	</script>
	";	

}

// Update Background Type
function updateBackground($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE background SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('BACKGROUND_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Update Services Total Number Dashboard
function updateServices($number){
	global $conection;
	$query = mysqli_query($conection,"UPDATE services SET number = '$number'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SERVICES_TOTAL_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Update Services Type Dashboard
function updateServicesType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE services_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SERVICES_TYPE_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Update Slider Type Dashboard
function updateSliderType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE slider_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SLIDER_TYPE_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Display all Pricing Types for selection
function activeContactType(){
	global $conection;
	$sql = mysqli_query($conection,"select type from contact_type");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "
		<div class='col-md-6'>
            <label>						
					<input style='display: none;' type='radio' name='type' value='default' "; if($type == 'default') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/cdefault.jpg' class='img-responsive'/></input>
            </label>
		</div>
		
		<div class='col-md-6'>
            <label>
					<input style='display: none;' type='radio' name='type' value='material' "; if($type == 'material') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/cmaterial.jpg' class='img-responsive'/></input>
            </label>
        </div>

	";
}

// Display all Pricing Types for selection
function activeBlogType(){
	global $conection;
	$sql = mysqli_query($conection,"select type from blog_type");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "		
		<div class='col-md-6'>        
			<label>Display all Posts<br/><br/>						
					<input class='switch' style='display: none;' type='radio' name='type' value='No Pagination' "; if($type == 'No Pagination') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/fblog.jpg' class='img-responsive'/></input>
            </label>
		</div>
		
		<div class='col-md-6'>			
            <label>Display Pagination<br/><br/>
					<input class='switch' style='display: none;' type='radio' name='type' value='Pagination' "; if($type == 'Pagination') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/pblog.jpg' class='img-responsive'/></input>
            </label>
        </div>

	";
}

// Update Contact Type under Settings area 
function updateBlogType($type,$number){
	global $conection;
	$query = mysqli_query($conection,"UPDATE blog_type SET type = '$type', number = '$number'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('BLOG_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Update Contact Type under Settings area 
function updateContactType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE contact_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CONTACT_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Update portfolio Type options under settings area
function updatePortfolioType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE portfolio_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PORTFOLIO_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Display all Pricing Types for selection
function activePricingType(){
	global $conection;
	$sql = mysqli_query($conection,"select type from pricing_type");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "
		<div class='col-md-6'>
            <label>						
					<input style='display: none;' type='radio' name='type' value='simple' "; if($type == 'simple') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/simple.jpg' class='img-responsive'/></input>
            </label>
		</div>
		
		<div class='col-md-6'>
            <label>
					<input style='display: none;' type='radio' name='type' value='material' "; if($type == 'material') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/material.jpg' class='img-responsive'/></input>
            </label>
        </div>

	";
}

// Display all Pricing Types for selection
function activeLoaderType(){
	global $conection;
	$sql = mysqli_query($conection,"select type from loader_type");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "
		<div class='col-md-2 col-sm-2 ld'>
            <label>						
					<input style='display: none;' type='radio' name='type' value='Square' "; if($type == 'Square') echo "checked"; if($type == '') echo ""; echo ">
						<div id='loading-center-absolute'>
							<div class='object' id='object_one'></div>
							<div class='object' id='object_two'></div>
							<div class='object' id='object_three'></div>
							<div class='object' id='object_four'></div>
						</div>
					</input>
            </label>
		</div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Chasing Dots' "; if($type == 'Chasing Dots') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-chasing-dots'>
							<div class='sk-child sk-dot1'></div>
							<div class='sk-child sk-dot2'></div>
						</div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Rotating Plane' "; if($type == 'Rotating Plane') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-rotating-plane'></div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Double Bounce' "; if($type == 'Double Bounce') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-double-bounce'>
							<div class='sk-child sk-double-bounce1'></div>
							<div class='sk-child sk-double-bounce2'></div>
						  </div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Waves' "; if($type == 'Waves') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-wave'>
							<div class='sk-rect sk-rect1'></div>
							<div class='sk-rect sk-rect2'></div>
							<div class='sk-rect sk-rect3'></div>
							<div class='sk-rect sk-rect4'></div>
							<div class='sk-rect sk-rect5'></div>
						  </div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Wandering Cubes' "; if($type == 'Wandering Cubes') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-wandering-cubes'>
							<div class='sk-cube sk-cube1'></div>
							<div class='sk-cube sk-cube2'></div>
						  </div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Pulse' "; if($type == 'Pulse') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-spinner sk-spinner-pulse'></div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Three Bounce' "; if($type == 'Three Bounce') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-three-bounce'>
							<div class='sk-child sk-bounce1'></div>
							<div class='sk-child sk-bounce2'></div>
							<div class='sk-child sk-bounce3'></div>
						  </div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Circle' "; if($type == 'Circle') echo "checked"; if($type == '') echo ""; echo ">
						
						<div class='sk-circle_simple'>
							<div class='sk-circle1 sk-child'></div>
							<div class='sk-circle2 sk-child'></div>
							<div class='sk-circle3 sk-child'></div>
							<div class='sk-circle4 sk-child'></div>
							<div class='sk-circle5 sk-child'></div>
							<div class='sk-circle6 sk-child'></div>
							<div class='sk-circle7 sk-child'></div>
							<div class='sk-circle8 sk-child'></div>
							<div class='sk-circle9 sk-child'></div>
							<div class='sk-circle10 sk-child'></div>
							<div class='sk-circle11 sk-child'></div>
							<div class='sk-circle12 sk-child'></div>
						  </div>						  
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Cube Grid' "; if($type == 'Cube Grid') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-cube-grid'>
							<div class='sk-cube sk-cube1'></div>
							<div class='sk-cube sk-cube2'></div>
							<div class='sk-cube sk-cube3'></div>
							<div class='sk-cube sk-cube4'></div>
							<div class='sk-cube sk-cube5'></div>
							<div class='sk-cube sk-cube6'></div>
							<div class='sk-cube sk-cube7'></div>
							<div class='sk-cube sk-cube8'></div>
							<div class='sk-cube sk-cube9'></div>
						  </div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Fading Circle' "; if($type == 'Fading Circle') echo "checked"; if($type == '') echo ""; echo ">					
						<div class='sk-fading-circle'>
							<div class='sk-circle1 sk-circle'></div>
							<div class='sk-circle2 sk-circle'></div>
							<div class='sk-circle3 sk-circle'></div>
							<div class='sk-circle4 sk-circle'></div>
							<div class='sk-circle5 sk-circle'></div>
							<div class='sk-circle6 sk-circle'></div>
							<div class='sk-circle7 sk-circle'></div>
							<div class='sk-circle8 sk-circle'></div>
							<div class='sk-circle9 sk-circle'></div>
							<div class='sk-circle10 sk-circle'></div>
							<div class='sk-circle11 sk-circle'></div>
							<div class='sk-circle12 sk-circle'></div>
						  </div>
						  <div class='fix'></div>
					</input>
            </label>
        </div>
		
		<div class='col-md-2 col-sm-2 ld'>
            <label>
					<input style='display: none;' type='radio' name='type' value='Folding Cube' "; if($type == 'Folding Cube') echo "checked"; if($type == '') echo ""; echo ">
						<div class='sk-folding-cube'>
							<div class='sk-cube1 sk-cube'></div>
							<div class='sk-cube2 sk-cube'></div>
							<div class='sk-cube4 sk-cube'></div>
							<div class='sk-cube3 sk-cube'></div>
						  </div>
					</input>
            </label>
        </div>

	";
}

// Update Loaders Type Dashboard
function updateLoaderType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE loader_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('LOADER_TYPE_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Update Pricing Tables Type Dashboard
function updatePricingType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE pricing_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PRICING_TYPE_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Update Pricing Tables Total Number Dashboard
function updatePricingNumber($number){
	global $conection;
	$query = mysqli_query($conection,"UPDATE pricing_number SET number = '$number'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('TABLE_TOTAL_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}

// Display all Menu Types for selection
function activeMenuType(){
	global $conection;
	$sql = mysqli_query($conection,"select type from menu_type");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "
		<div class='col-md-12'>
            <label>						
					<input style='display: none;' type='radio' name='type' value='topbar' "; if($type == 'topbar') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/topbar.jpg' class='img-responsive'/></input>
            </label>

            <label>
					<input style='display: none;' type='radio' name='type' value='right' "; if($type == 'right') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/right.jpg' class='img-responsive'/></input>
            </label>
		</div>
		
		<div class='col-md-12'>
            <label>
					<input style='display: none;' type='radio' name='type' value='left' "; if($type == 'left') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/left.jpg' class='img-responsive'/></input>
            </label>

            <label>
					<input style='display: none;' type='radio' name='type' value='fixed' "; if($type == 'fixed') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/fixed.jpg' class='img-responsive'/></input>
            </label>
        </div>

	";
}

// Update menu Type options under settings area
function updateMenuType($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE menu_type SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('MENU_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Display all Menu Types for selection
function activeGDPR(){
	global $conection;
	$sql = mysqli_query($conection,"select type from gdpr");
    $row = mysqli_fetch_assoc($sql);
	$type = $row['type'];
	
echo "
		<div class='col-md-4'>
            <label>						
					<input style='display: none;' type='radio' name='type' value='eupopup eupopup-top' "; if($type == 'eupopup eupopup-top') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/gdpr_top.jpg' class='img-responsive'/></input>
            </label>

            <label>
					<input style='display: none;' type='radio' name='type' value='eupopup eupopup-fixedtop' "; if($type == 'eupopup eupopup-fixedtop') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/gdpr_fixed.jpg' class='img-responsive'/></input>
            </label>           
		</div>
			
		<div class='col-md-4'>
			 <label>
					<input style='display: none;' type='radio' name='type' value='eupopup eupopup-bottomleft' "; if($type == 'eupopup eupopup-bottomleft') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/gdpr_bottoml.jpg' class='img-responsive'/></input>
            </label>
			
			 <label>
					<input style='display: none;' type='radio' name='type' value='eupopup eupopup-bottomright' "; if($type == 'eupopup eupopup-bottomright') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/gdpr_bottomr.jpg' class='img-responsive'/></input>
            </label>
        </div>
		
		<div class='col-md-4'>
			<label>
					<input style='display: none;' type='radio' name='type' value='eupopup eupopup-bottom' "; if($type == 'eupopup eupopup-bottom') echo "checked"; if($type == '') echo ""; echo ">
					<img src='assets/images/gdpr_bottom.jpg' class='img-responsive'/></input>
            </label>
		</div>

	";
}

// Update menu Type options under settings area
function updateGDPR($type){
	global $conection;
	$query = mysqli_query($conection,"UPDATE gdpr SET type = '$type'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('GDPR')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}


// Update youtube video Id
function updateYoutube($video_id){
	global $conection;
	$query = mysqli_query($conection,"UPDATE youtube SET video_id = '$video_id'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('BACKGROUND_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; background.php#youtube">'; 
			die();
			}				
			return true;
}

//get youtube video_id and display it under index page
function getVideo(){
	global $conection;
	$sql = mysqli_query($conection,"select * from youtube");
    $row = mysqli_fetch_assoc($sql);
	
	echo "
<!-- ADD Jquery Video Background -->
  <script src='assets/js/jquery.youtubebackground.js'></script>
  <script>
    jQuery(function($) {
      
      $('#background-video').YTPlayer({
        fitToBackground: true,
        videoId: '".$row['video_id']."',
        callback: function() {
          videoCallbackEvents();
        }
      });
      
      var videoCallbackEvents = function() {
        var player = $('#background-video').data('ytPlayer').player;
      
        player.addEventListener('onStateChange', function(event){
            console.log('Player State Change', event);

            // OnStateChange Data
            if (event.data === 0) {          
                console.log('video ended');
            }
            else if (event.data === 2) {          
              console.log('paused');
            }
        });
      }
    });
  </script>  
  ";
}

// Display Information for the map area under Google Map area
function googleMaps(){
	global $conection;
	$sql = mysqli_query($conection,"select * from map WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
echo "

		 <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='api'>".lang('GOOGLE_MAPS_API')."</label>
			<div class='col-sm-8'>
			   <textarea rows ='1' cols='10' id='api' name='api' type='text' class='form-control input-md'>".$row['api']."</textarea>
			</div>
		  </div>
	
		  
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='title'>".lang('TITLE')."</label>
			<div class='col-sm-8'>
			   <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
			</div>
		  </div>
		  
		  <!-- Text input-->
		  <div class='form-group'>
			<label class='col-sm-2 control-label' for='description'>".lang('DESCRIPTION')."</label>
			<div class='col-sm-8'>
			   <textarea rows ='5' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
			</div>
		  </div>
					
		   <!-- Text input-->
		  <div class='form-group'>
			<label class='col-md-2 control-label' for='zoom'>".lang('ACTIVE_ZOOM')." <input disabled='disabled' class='form-control' name='zoom' value='".$row['zoom']."' /></label>
			  <div class='col-md-2'>
					<label class='control-label' for='zoom'>".lang('NEW_ZOOM')."</label>
					<select class='form-control' name='zoom'>
					  <option value='".$row['zoom']."'>----</option>
					  <option value='1'>1</option>
					  <option value='2'>2</option>
					  <option value='3'>3</option>
					  <option value='4'>4</option>
					  <option value='5'>5</option>
					  <option value='6'>6</option>
					  <option value='7'>7</option>
					  <option value='8'>8</option>
					  <option value='9'>9</option>
					  <option value='10'>10</option>
					  <option value='11'>11</option>
					  <option value='12'>12</option>
					  <option value='13'>13</option>
					  <option value='14'>14</option>
					  <option value='15'>15</option>
					  <option value='16'>16</option>
					  <option value='17'>17</option>
					  <option value='18'>18</option>
					  <option value='19'>19</option>
					  <option value='20'>20</option>
					</select>
					<br/>
			</div>
		</div>

		  ";
}

//Get Google Map information and display it under index page
function getGoogleMap(){
		global $conection;
		$sql = mysqli_query($conection,"select * from map where id='1'");
		$row = mysqli_fetch_assoc($sql);
		$id = $row['id'];
		
		if ($id == '1'){
		echo "
			
				<script>
		var map;
		var infowindow;
		 var imagePath = 'assets/img/map-marker.png';

		function initMap() {
		  var myLatLng = {lng: ".$row['longitude'].", lat: ".$row['latitude']."};
		  

		  
		   var map = new google.maps.Map(document.getElementById('map'), {
			zoom: ".$row['zoom'].",
			center: myLatLng
		  });

		  var marker = new google.maps.Marker({
			position: myLatLng,			
			map: map,
			icon: imagePath,
			title: '".$row['title']."'
		  });
		  
		  var contentString = '".$row['description']."';

		  var infowindow = new google.maps.InfoWindow({
			content: contentString
		  });
		   marker.addListener('click', function() {
			infowindow.open(map, marker);
		  });


		  var service = new google.maps.places.PlacesService(map);
		  service.nearbySearch({
			location: myLatLng,
			radius: 500,
			types: ['store']
		  }, callback);
		}

		function callback(results, status) {
		  if (status === google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
			  createMarker(results[i]);
			}
		  }
		}

		function createMarker(place) {
		  var placeLoc = place.geometry.location;
		  var marker = new google.maps.Marker({
			map: map,			
			position: place.geometry.location
		  });

		  google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(place.name);
			infowindow.open(map, this);
		  });
		}
		
		

    </script>
	 <script src='https://maps.googleapis.com/maps/api/js?key=".$row['api']."&libraries=places&callback=initMap'
        async defer></script>
	
	";
	}else{
		echo " ";
	}
	
}

// Update google map information
function updateMap($latitude,$longitude,$title,$description,$zoom,$api){
	global $conection;
	$query = mysqli_query($conection,"UPDATE map SET latitude = '$latitude', longitude = '$longitude', title = '$title', description = '$description', zoom = '$zoom', api = '$api'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('GOOGLE_MAP_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; map.php">'; 
			die();
			}				
			return true;
}


/*===============================================================================================================================*/
/*====================================================== Services Functions ======================================================*/
/*===============================================================================================================================*/

// Display All services in the admin
function listServicesBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM service ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th>".lang('LOGO')."</th>
				  <th>".lang('COLOR')."</th>
				  <th>".lang('TITLE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo "<i class='fa ".$row['logo']."' aria-hidden='true'></i>";
			echo "</td><td>";
			echo "<div id='color' style='background-color:".$row['color']."'></div>";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo"
			<a href='edit_service.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaService(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

function listServicesImagesBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM service_images ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example1'>";
		echo "  <thead>
                <tr>	
                  <th>".lang('IMAGE')."</th>
				  <th>".lang('TITLE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='service_img'>"; 
			echo "<img src='../assets/img/uploads/services/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo"
			<a href='edit_service_img.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaServiceImage(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit service function
function editService($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from service WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
				

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='title'>".lang('TITLE')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
						
					  </div>
					</div>

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='5' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
						
					  </div>
					</div>

				";

}

// Edit service function
function editServiceImage($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from service_images WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
				

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='title'>".lang('TITLE')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
						
					  </div>
					</div>

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='5' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
						
					  </div>
					</div>

				";

}

// Update service Information
function updateServiceImage($id,$title,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE service_images SET title = '$title', description = '$description' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SERVICE_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; services.php">'; 
					die();
			}				
			return true;
}

// Update service Information
function updateService($id,$logo,$color,$title,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE service SET logo = '$logo', color = '$color', title = '$title', description = '$description' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SERVICE_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; services.php">'; 
					die();
			}				
			return true;
}



//Display all services and display them in the index page
function showServices(){
	global $conection;
	$sql = mysqli_query($conection,"select * from services");
    $row = mysqli_fetch_assoc($sql);
	
	$number = $row['number'];
	
	
	if ($number == 0)
	{
		$sql = mysqli_query($conection,"select * from service limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-3 col-sm-3'>
								<div class='service-item'>
									<span class='fa-stack fa-4x'>
									<i class='fa fa-circle fa-stack-2x'></i>
									<i style='color:".$row['color'].";' class='fa ".$row['logo']." fa-stack-1x text-primary'></i>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 2)
	{
		$sql = mysqli_query($conection,"select * from service limit 2");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-6 col-sm-6'>
								<div class='service-item'>
									<span class='fa-stack fa-4x'>
									<i class='fa fa-circle fa-stack-2x'></i>
									<i style='color:".$row['color'].";' class='fa ".$row['logo']." fa-stack-1x text-primary'></i>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 3)
	{
		$sql = mysqli_query($conection,"select * from service limit 3");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-4 col-sm-4'>
								<div class='service-item'>
									<span class='fa-stack fa-4x'>
									<i class='fa fa-circle fa-stack-2x'></i>
									<i style='color:".$row['color'].";' class='fa ".$row['logo']." fa-stack-1x text-primary'></i>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 4)
	{
		$sql = mysqli_query($conection,"select * from service limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-3 col-sm-3'>
								<div class='service-item'>
									<span class='fa-stack fa-4x'>
									<i class='fa fa-circle fa-stack-2x'></i>
									<i style='color:".$row['color'].";' class='fa ".$row['logo']." fa-stack-1x text-primary'></i>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 6)
	{
		$sql = mysqli_query($conection,"select * from service limit 6");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-2 col-sm-2'>
								<div class='service-item'>
									<span class='fa-stack fa-4x'>
									<i class='fa fa-circle fa-stack-2x'></i>
									<i style='color:".$row['color'].";' class='fa ".$row['logo']." fa-stack-1x text-primary'></i>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}

}

//Display all services and display them in the index page
function showServicesImage(){
	global $conection;
	$sql = mysqli_query($conection,"select * from services");
    $row = mysqli_fetch_assoc($sql);
	
	$number = $row['number'];
	
	
	if ($number == 0)
	{
		$sql = mysqli_query($conection,"select * from service_images limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-3 col-sm-3'>
								<div class='service-item'>
									
									<img class='img-thumbnail image' src='assets/img/uploads/services/".$row['image']."'>
									<div class='middle'>
										<div class='text_s btn'>
											<a href='services'><strong> ".lang('SEE_MORE')." </strong></a>
										</div>
									</div>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
								</span>
									<p>".getExcerpt($row['description'])."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 2)
	{
		$sql = mysqli_query($conection,"select * from service_images limit 2");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-6 col-sm-6'>
								<div class='service-item'>
									
									<img class='img-thumbnail image' src='assets/img/uploads/services/".$row['image']."'>
									<div class='middle'>
										<div class='text_s btn'>
											<a href='services'><strong> ".lang('SEE_MORE')." </strong></a>
										</div>
									</div>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
								</span>
									<p>".getExcerpt($row['description'])."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 3)
	{
		$sql = mysqli_query($conection,"select * from service_images limit 3");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-4 col-sm-4'>
								<div class='service-item'>
									
									<img class='img-thumbnail image' src='assets/img/uploads/services/".$row['image']."'>
									<div class='middle'>
										<div class='text_s btn'>
											<a href='services'><strong> ".lang('SEE_MORE')." </strong></a>
										</div>
									</div>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
								</span>
									<p>".getExcerpt($row['description'])."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 4)
	{
		$sql = mysqli_query($conection,"select * from service_images limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-3 col-sm-3'>
								<div class='service-item'>
									
									<img class='img-thumbnail image' src='assets/img/uploads/services/".$row['image']."'>
									<div class='middle'>
										<div class='text_s btn'>
											<a href='services'><strong> ".lang('SEE_MORE')." </strong></a>
										</div>
									</div>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
								</span>
									<p>".getExcerpt($row['description'])."</p> 
								</div>
							</div>
			";
		}
		
	}
	if ($number == 6)
	{
		$sql = mysqli_query($conection,"select * from service_images limit 6");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-md-2 col-sm-2'>
								<div class='service-item'>
									
									<img class='img-thumbnail image' src='assets/img/uploads/services/".$row['image']."'>
									<div class='middle'>
										<div class='text_s btn'>
											<a href='services'><strong> ".lang('SEE_MORE')." </strong></a>
										</div>
									</div>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
								</span>
									<p>".getExcerpt($row['description'])."</p> 
								</div>
							</div>
			";
		}
		
	}

}

//Display all services and display them in the index page
function showAllServices(){
	global $conection;
	$sql = mysqli_query($conection,"select * from service_images");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
			echo"
							<div class='col-lg-4 col-md-4 col-xs-12'>
								<div class='service-item'>
									
									<img class='img-responsive' src='assets/img/uploads/services/".$row['image']."'>
								</span>
									<h4>
										<strong>".$row['title']."</strong>
									</h4>
									<p>".$row['description']."</p> 
								</div>
							</div>
			";
		}
		
	}


/*===============================================================================================================================*/
/*================================================== Categories Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Categories in the admin for the portfolio
function listCategoriesBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM categories ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th>".lang('NAME')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['name'];
			echo "</td><td>";
			echo"
			<a href='javascript:EliminaCategory(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// display the categories for select them under the new_project and edit_project pages
function listCategories(){
	global $conection;
	$sql = mysqli_query($conection,"select * from categories");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
		
	echo "<option value='".$row['slug']."'>".$row['name']."</option>";
	}
}

// Display categories so they can be used as filters for the portfolio section in the index page
function listCategoriesFront(){
	global $conection;
	$sql = mysqli_query($conection,"select * from categories");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
		
	echo "
	 <li><a href='#' data-filter='.".$row['slug']."'>".$row['name']."</a><span>/</span></li>";
	}
}

//Functions to replace accents and create custom slugs for stop sql injection
function replace_accents($string){ 
  return str_replace( array('�','�','�','�','�', '�', '�','�','�','�', '�','�','�','�', '�', '�','�','�','�','�', '�','�','�','�', '�','�', '�','�','�','�','�', '�', '�','�','�','�', '�','�','�','�', '�', '�','�','�','�','�', '�','�','�','�', '�'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $string); 
} 

/*===============================================================================================================================*/
/*================================================== Portfolio Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Projects on the admin
function listPortfolioBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM portfolio ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
				  <th>".lang('IMAGE')."</th>
                  <th>".lang('TITLE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='testi_img'>"; 
			echo "<img style='width:20%;' src='../assets/img/uploads/portfolio/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo"
			<a href='edit_project.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaProject(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit project function
function editProject($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from portfolio WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
				

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='title'>".lang('TITLE')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
						
					  </div>
					</div>	

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='link'>".lang('LINK')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='link' name='link' type='text' class='form-control input-md'>".$row['link']."</textarea>
						
					  </div>
					</div>	

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='5' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
						
					  </div>
					</div>

				";

}

// Update project Information
function updateProject($id,$category,$title,$link,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE portfolio SET category = '$category', title = '$title', link = '$link', description = '$description' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PORTFOLIO_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; portfolio.php">'; 
					die();
			}				
			return true;
}

//Display portfolio as Gallery in the index page
function showPortfolioGallery(){	
	global $conection;
	$sql = mysqli_query($conection,"select * from portfolio");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
		<!--PROJECT ".$row['id']." -->
		<div class='col-sm-3 isotopeSelector ".$row['category']."'>
                <article class=''>
                    <figure>
                        <img src='assets/img/uploads/portfolio/".$row['image']."' alt=''>
                        <div class='overlay-background'>
                            <div class='inner'></div>
                        </div>
                        <div class='overlay'>
                            <div class='inner-overlay'>
                                <div class='inner-overlay-content with-icons'>
                                    <a title='".$row['description']."' class='fancybox-pop' rel='".$row['category']."' href='assets/img/uploads/portfolio/".$row['image']."'><i class='fa fa-search-plus'></i></a>
                                </div>
                            </div>
                        </div>
                    </figure>
                    <div class='article-title'><a href='#'>".$row['title']."</a></div>
                </article>
            </div>
			";	
		} 

}

//Display Portfolio as Porjects section with all the information available
function showPortfolio(){	
	global $conection;
	$sql = mysqli_query($conection,"select * from portfolio");
    $row = mysqli_num_rows($sql);
	
	while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			
				<div class='grid__item  col-sm-3  isotopeSelector ".$row['category']."' data-size='1280x857'>
					<article>
						<figure>						
						 <div class='overlay-background'>
                            <div class='inner'></div>
                        </div>						
                        <div class='overlay'>
                            <div class='inner-overlay'>
                                <div class='inner-overlay-content with-icons'>
                                    <a title='".$row['description']."' rel='".$row['category']."' href='assets/img/uploads/portfolio/".$row['image']."'><i class='fa fa-link'></i></a>								
                                </div>								
                            </div>							
                        </div>
						<a href='assets/img/uploads/portfolio/".$row['image']."'><img src='assets/img/uploads/portfolio/".$row['image']."' alt='img06' />
							<div class='description description--grid'>
								<h3>".$row['title']."</h3>
								<p>".$row['description']."</p>
								";
								
								$link = $row['link'];
								if ($link == ''){
									echo " ";
								}
								if ($link != ''){
									echo "
										<a class='btn btn-lg btn-light' href='".$row['link']."' target='_blank'>".lang('WATCH_PROJECT')."</a>
									";
								}
		echo "								
							</div>
						</a>
						 </figure>
						<div class='article-title'><a href='#'>".$row['title']."</a></div>
						
					</article>
				</div>
			
			";

		} 

}


/*===============================================================================================================================*/
/*================================================== Testimonials Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Testimonials created in the admin
function listTestimonialsBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM testimonials ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
				  <th>".lang('IMAGE')."</th>
                  <th>".lang('NAME')."</th>
				  <th>".lang('WEBSITE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='testi_img'>"; 
			echo "<img src='../assets/img/uploads/testimonials/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['name'];
			echo "</td><td>";
			echo $row['website'];
			echo "</td><td>";
			echo"
			<a href='edit_testimonial.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaTestimonial(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit Testimonial function
function editTestimonial($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from testimonials WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
				

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='name'>".lang('NAME')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='name' name='name' type='text' class='form-control input-md'>".$row['name']."</textarea>
						
					  </div>
					</div>
					
					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='website'>".lang('WEBSITE')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='website' name='website' type='text' class='form-control input-md'>".$row['website']."</textarea>
						
					  </div>
					</div>

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='5' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
						
					  </div>
					</div>

				";

}

// Update Testimonial Information
function updateTestimonial($id,$name,$website,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE testimonials SET name = '$name', website = '$website', description = '$description' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('TESTIMONIAL_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; testimonials.php">'; 
					die();
			}				
			return true;
}

//Display all the testimonials in the index page
function showTestimonials(){
	global $conection;
	$sql = mysqli_query($conection,"select * from testimonials");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
							<!--TESTIMONIAL ".$row['id']." -->
							<div class='item'>
							  <div class='shadow-effect'>
								<a href='".$row['website']."' target='_blank'><img class='img-circle' src='assets/img/uploads/testimonials/".$row['image']."' alt='' /></a>
								<p>".$row['description']."</p>
							  </div>
							  <div class='testimonial-name'>".$row['name']."</div>
							</div>
							<!--END OF TESTIMONIAL ".$row['id']." -->
			";	
		} 

}


/*===============================================================================================================================*/
/*================================================== Partners Functions =====================================================*/
/*===============================================================================================================================*/

// Display All partners created in the admin
function listPartnersBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM partners ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
				  <th>".lang('IMAGE')."</th>
				  <th>".lang('NAME')."</th>
				  <th>".lang('WEBSITE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='partner_img'>"; 
			echo "<img src='../assets/img/uploads/partners/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['name'];
			echo "</td><td>";
			echo $row['website'];
			echo "</td><td>";
			echo"
			<a href='edit_partner.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaPartner(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit partner function
function editPartner($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from partners WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "

					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='name'>".lang('NAME')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='name' name='name' type='text' class='form-control input-md'>".$row['name']."</textarea>
						
					  </div>
					</div>
					
					<!-- Text input-->
					<div class='form-group'>
					  <label class='col-md-2 control-label' for='website'>".lang('WEBSITE')."</label>  
					  <div class='col-md-6'>
					  <textarea rows ='1' cols='10' id='website' name='website' type='text' class='form-control input-md'>".$row['website']."</textarea>
						
					  </div>
					</div>

				";

}

// Update partner Information
function updatePartner($id,$name, $website){
	global $conection;
	$query = mysqli_query($conection,"UPDATE partners SET name = '$name', website = '$website' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PARTNER_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; partners.php">'; 
					die();
			}				
			return true;
}

//Display partners in the index page
function showPartners(){
	global $conection;
	$sql = mysqli_query($conection,"select * from partners");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
								<div class='item'>
								<a href='".$row['website']."' target='_blank'><img src='assets/img/uploads/partners/".$row['image']."' class='center-block' alt='".$row['name']."' /></a>
								</div>
			";	
		} 

}

//Display partners in the index page
function showAllPartners(){
	global $conection;
	$sql = mysqli_query($conection,"select * from partners");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
		<div class='col-lg-4 col-md-4 col-sm-6'>
			<figure class='effect-steve'>
				<img src='assets/img/uploads/partners/".$row['image']."' class='center-block' alt='".$row['name']."' />
				<figcaption>
					<h2>".$row['name']."</h2>
					<p><a href='".$row['website']."' target='_blank'>".$row['website']."</a></p>
				</figcaption>			
			</figure>
		</div>
			";	
		} 

}

/*===============================================================================================================================*/
/*==================================================== Custom CSS Functions =====================================================*/
/*===============================================================================================================================*/

//Get the CSS in the admin
function getCss(){
	global $conection;
	$sql = mysqli_query($conection,"select * from css WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	echo
	"
	 <!-- Text input-->
	  <div class='form-group'>		
		<div class='col-md-12'>
		   <textarea rows ='18' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
		</div>
	  </div>
	  ";
	
}

//Update CSS options 
function updateCss($description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE css SET description = '$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CUSTOM_CSS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;

}

//Display css in the index page
function showCss(){
	global $conection;
	$sql = mysqli_query($conection,"select * from css WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$description = $row['description'];
	
	echo $description;
}

/*===============================================================================================================================*/
/*================================================ Active Sections Functions ====================================================*/
/*===============================================================================================================================*/

// Display all active Pages for displaying on the website in Settings page
function activeSettings(){
	global $conection;
	$sql = mysqli_query($conection,"select * from settings WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$about = $row['about'];
	$contacts = $row['contacts'];
	$services = $row['services'];
	$partners = $row['partners'];	
	$portfolio = $row['portfolio'];
	$blog = $row['blog'];
	$custom = $row['custom'];
	$privacy = $row['privacy'];
	$calendar = $row['calendar'];
	
echo "

		<div class='checkbox'>
            <label>
                ".lang('ABOUT')." 
				<input type='hidden' name='about' value='0'></input>
				<input type='checkbox' name='about' value='1' "; if($about == 1) echo "checked"; if($about == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SERVICES')."
				<input type='hidden' name='services' value='0'></input>
				<input type='checkbox' name='services' value='1' "; if($services == 1) echo "checked"; if($services == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PORTFOLIO')." 
				<input type='hidden' name='portfolio' value='0'></input>
				<input type='checkbox' name='portfolio' value='1' "; if($portfolio == 1) echo "checked"; if($portfolio == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('BLOG')." 
				<input type='hidden' name='blog' value='0'></input>
				<input type='checkbox' name='blog' value='1' "; if($blog == 1) echo "checked"; if($blog == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CONTACTS')." 
				<input type='hidden' name='contacts' value='0'></input>
				<input type='checkbox' name='contacts' value='1' "; if($contacts == 1) echo "checked"; if($contacts == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CUSTOM_PAGES')." 
				<input type='hidden' name='custom' value='0'></input>
				<input type='checkbox' name='custom' value='1' "; if($custom == 1) echo "checked"; if($custom == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRIVACY')." 
				<input type='hidden' name='privacy' value='0'></input>
				<input type='checkbox' name='privacy' value='1' "; if($privacy == 1) echo "checked"; if($privacy == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CALENDAR')." 
				<input type='hidden' name='calendar' value='0'></input>
				<input type='checkbox' name='calendar' value='1' "; if($calendar == 1) echo "checked"; if($calendar == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		  ";
}

// Update Active Pages under Settings
function updateSettings($about,$contacts,$services,$partners,$portfolio,$blog,$custom,$privacy,$calendar){
	global $conection;
	$query = mysqli_query($conection,"UPDATE settings SET about = '$about', contacts = '$contacts', services = '$services', partners = '$partners', portfolio = '$portfolio', blog = '$blog', custom = '$custom', privacy = '$privacy', calendar = '$calendar' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

// Display all active sections in Settings page
function activeHomepage(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activehomepage WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$about = $row['about'];
	$services = $row['services'];
	$testimonials = $row['testimonials'];
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	$portfolio = $row['portfolio'];
	$team = $row['team'];
	$pricing = $row['pricing'];
	$blog = $row['blog'];
	$skills2 = $row['skills2'];
	$calendar = $row['calendar'];
	
echo "
		<div class='checkbox'>
            <label>
                ".lang('ABOUT')." 
				<input type='hidden' name='about' value='0'></input>
				<input type='checkbox' name='about' value='1' "; if($about == 1) echo "checked"; if($about == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SERVICES')."
				<input type='hidden' name='services' value='0'></input>
				<input type='checkbox' name='services' value='1' "; if($services == 1) echo "checked"; if($services == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TESTIMONIALS')."
				<input type='hidden' name='testimonials' value='0'></input>
				<input type='checkbox' name='testimonials' value='1' "; if($testimonials == 1) echo "checked"; if($testimonials == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PORTFOLIO')." 
				<input type='hidden' name='portfolio' value='0'></input>
				<input type='checkbox' name='portfolio' value='1' "; if($portfolio == 1) echo "checked"; if($portfolio == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TEAM')." 
				<input type='hidden' name='team' value='0'></input>
				<input type='checkbox' name='team' value='1' "; if($team == 1) echo "checked"; if($team == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRICING_TABLES')." 
				<input type='hidden' name='pricing' value='0'></input>
				<input type='checkbox' name='pricing' value='1' "; if($pricing == 1) echo "checked"; if($pricing == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('BLOG')." 
				<input type='hidden' name='blog' value='0'></input>
				<input type='checkbox' name='blog' value='1' "; if($blog == 1) echo "checked"; if($blog == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SKILLS')." 
				<input type='hidden' name='skills2' value='0'></input>
				<input type='checkbox' name='skills2' value='1' "; if($skills2 == 1) echo "checked"; if($skills2 == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CALENDAR')." 
				<input type='hidden' name='calendar' value='0'></input>
				<input type='checkbox' name='calendar' value='1' "; if($calendar == 1) echo "checked"; if($calendar == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>		
		<br/>
		  ";
}

// Update Active Homepage sections
function updateActiveHomepage($about,$services,$testimonials,$partners,$subscribe,$portfolio,$team,$pricing,$blog,$skills2,$calendar){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activehomepage SET about = '$about', services = '$services', testimonials = '$testimonials', partners = '$partners', subscribe = '$subscribe', portfolio = '$portfolio' , team = '$team' , pricing = '$pricing' , blog = '$blog' , skills2 = '$skills2' , calendar = '$calendar' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}


// Display all active sections in Settings page
function activeAbout(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activeabout WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$services = $row['services'];
	$testimonials = $row['testimonials'];
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	$team = $row['team'];
	$pricing = $row['pricing'];
	$skills2 = $row['skills2'];
	
echo "
		
		<div class='checkbox'>
            <label>
                ".lang('SERVICES')."
				<input type='hidden' name='services' value='0'></input>
				<input type='checkbox' name='services' value='1' "; if($services == 1) echo "checked"; if($services == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TESTIMONIALS')."
				<input type='hidden' name='testimonials' value='0'></input>
				<input type='checkbox' name='testimonials' value='1' "; if($testimonials == 1) echo "checked"; if($testimonials == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TEAM')." 
				<input type='hidden' name='team' value='0'></input>
				<input type='checkbox' name='team' value='1' "; if($team == 1) echo "checked"; if($team == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRICING_TABLES')." 
				<input type='hidden' name='pricing' value='0'></input>
				<input type='checkbox' name='pricing' value='1' "; if($pricing == 1) echo "checked"; if($pricing == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SKILLS')." 
				<input type='hidden' name='skills2' value='0'></input>
				<input type='checkbox' name='skills2' value='1' "; if($skills2 == 1) echo "checked"; if($skills2 == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		  ";
}

// Update Active About Page sections
function updateActiveAbout($services,$testimonials,$partners,$subscribe,$team,$pricing,$skills2){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activeabout SET services = '$services', testimonials = '$testimonials', partners = '$partners', subscribe = '$subscribe', team = '$team' , pricing = '$pricing' , skills2 = '$skills2' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; aboutpage.php">'; 
			die();
			}				
			return true;
}

// Display all active sections in Settings page
function activeCalendar(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activecalendar WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$services = $row['services'];
	$testimonials = $row['testimonials'];
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	$team = $row['team'];
	$pricing = $row['pricing'];
	$skills2 = $row['skills2'];
	
echo "
		
		<div class='checkbox'>
            <label>
                ".lang('SERVICES')."
				<input type='hidden' name='services' value='0'></input>
				<input type='checkbox' name='services' value='1' "; if($services == 1) echo "checked"; if($services == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TESTIMONIALS')."
				<input type='hidden' name='testimonials' value='0'></input>
				<input type='checkbox' name='testimonials' value='1' "; if($testimonials == 1) echo "checked"; if($testimonials == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TEAM')." 
				<input type='hidden' name='team' value='0'></input>
				<input type='checkbox' name='team' value='1' "; if($team == 1) echo "checked"; if($team == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRICING_TABLES')." 
				<input type='hidden' name='pricing' value='0'></input>
				<input type='checkbox' name='pricing' value='1' "; if($pricing == 1) echo "checked"; if($pricing == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SKILLS')." 
				<input type='hidden' name='skills2' value='0'></input>
				<input type='checkbox' name='skills2' value='1' "; if($skills2 == 1) echo "checked"; if($skills2 == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		  ";
}

// Update Active About Page sections
function updateActiveCalendar($services,$testimonials,$partners,$subscribe,$team,$pricing,$skills2){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activecalendar SET services = '$services', testimonials = '$testimonials', partners = '$partners', subscribe = '$subscribe', team = '$team' , pricing = '$pricing' , skills2 = '$skills2' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; eventspage.php">'; 
			die();
			}				
			return true;
}

// Display Partners sections in Partners page
function activePartners(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activepartners WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$subscribe = $row['subscribe'];
	
echo "
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>

		  ";
}

// Update Active Partners Page sections
function updateActivePartners($subscribe){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activepartners SET subscribe = '$subscribe' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; partnerspage.php">'; 
			die();
			}				
			return true;
}

// Update Active Partners Page sections
function updateActivePrivacy($subscribe){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activeprivacy SET subscribe = '$subscribe' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; privacypage.php">'; 
			die();
			}				
			return true;
}

// Display Partners sections in Partners page
function activePrivacy(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activeprivacy WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$subscribe = $row['subscribe'];
	
echo "
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>

		  ";
}

// Display Portfolio sections in Portfolio page
function activePortfolio(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activeportfolio WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	
echo "
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>

		  ";
}

// Update Active sections
function updateActivePortfolio($partners,$subscribe){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activeportfolio SET partners = '$partners', subscribe = '$subscribe' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; portfoliopage.php">'; 
			die();
			}				
			return true;
}

// Display Portfolio sections in Portfolio page
function activeBlog(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activeblog WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	
echo "		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		  ";
}

// Update Active sections
function updateActiveBlog($partners,$subscribe){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activeblog SET partners = '$partners', subscribe = '$subscribe' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; blogpage.php">'; 
			die();
			}				
			return true;
}


// Display Services active sections
function activeServices(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activeservices WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$testimonials = $row['testimonials'];
	$partners = $row['partners'];
	$subscribe = $row['subscribe'];
	$portfolio = $row['portfolio'];
	$team = $row['team'];
	$pricing = $row['pricing'];
	$skills2 = $row['skills2'];
	
echo "
		
		<div class='checkbox'>
            <label>
                ".lang('TESTIMONIALS')."
				<input type='hidden' name='testimonials' value='0'></input>
				<input type='checkbox' name='testimonials' value='1' "; if($testimonials == 1) echo "checked"; if($testimonials == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PORTFOLIO')." 
				<input type='hidden' name='portfolio' value='0'></input>
				<input type='checkbox' name='portfolio' value='1' "; if($portfolio == 1) echo "checked"; if($portfolio == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TEAM')." 
				<input type='hidden' name='team' value='0'></input>
				<input type='checkbox' name='team' value='1' "; if($team == 1) echo "checked"; if($team == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRICING_TABLES')." 
				<input type='hidden' name='pricing' value='0'></input>
				<input type='checkbox' name='pricing' value='1' "; if($pricing == 1) echo "checked"; if($pricing == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SKILLS')." 
				<input type='hidden' name='skills2' value='0'></input>
				<input type='checkbox' name='skills2' value='1' "; if($skills2 == 1) echo "checked"; if($skills2 == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		  ";
}

// Update Active Services sections
function updateActiveServices($testimonials,$partners,$subscribe,$portfolio,$team,$pricing,$skills2){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activeservices SET testimonials = '$testimonials', partners = '$partners', subscribe = '$subscribe', portfolio = '$portfolio' , team = '$team' , pricing = '$pricing' , skills2 = '$skills2' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; servicespage.php">'; 
			die();
			}				
			return true;
}

// Display Contact sections
function activeContacts(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activecontacts WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$partners = $row['partners'];
	$map = $row['map'];
	$form = $row['form'];
	$subscribe = $row['subscribe'];
	
echo "
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('MAP')."
				<input type='hidden' name='map' value='0'></input>
				<input type='checkbox' name='map' value='1' "; if($map == 1) echo "checked"; if($map == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CONTACT_FORM')." 
				<input type='hidden' name='form' value='0'></input>
				<input type='checkbox' name='form' value='1' "; if($form == 1) echo "checked"; if($form == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>	
	";
}

// Update Active sections
function updateActiveContacts($partners,$map,$form,$subscribe){
	global $conection;
	$query = mysqli_query($conection,"UPDATE activecontacts SET partners = '$partners', map = '$map', form = '$form', subscribe = '$subscribe' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; contactspage.php">'; 
			die();
			}				
			return true;
}


// Display all active sections in Settings page
function activeSections(){
	global $conection;
	$sql = mysqli_query($conection,"select * from activehomepage WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$about = $row['about'];
	$contacts = $row['contacts'];
	$services = $row['services'];
	$testimonials = $row['testimonials'];
	$partners = $row['partners'];
	$map = $row['map'];
	$form = $row['form'];
	$subscribe = $row['subscribe'];
	$portfolio = $row['portfolio'];
	$team = $row['team'];
	$pricing = $row['pricing'];
	
echo "
		<div class='checkbox'>
            <label>
                ".lang('ABOUT')." 
				<input type='hidden' name='about' value='0'></input>
				<input type='checkbox' name='about' value='1' "; if($about == 1) echo "checked"; if($about == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CONTACTS')."
				<input type='hidden' name='contacts' value='0'></input>
				<input type='checkbox' name='contacts' value='1' "; if($contacts == 1) echo "checked"; if($contacts == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('SERVICES')."
				<input type='hidden' name='services' value='0'></input>
				<input type='checkbox' name='services' value='1' "; if($services == 1) echo "checked"; if($services == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TESTIMONIALS')."
				<input type='hidden' name='testimonials' value='0'></input>
				<input type='checkbox' name='testimonials' value='1' "; if($testimonials == 1) echo "checked"; if($testimonials == 0) echo ""; echo ">"; 
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PARTNERS')." 
				<input type='hidden' name='partners' value='0'></input>
				<input type='checkbox' name='partners' value='1' "; if($partners == 1) echo "checked"; if($partners == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('MAP')."
				<input type='hidden' name='map' value='0'></input>
				<input type='checkbox' name='map' value='1' "; if($map == 1) echo "checked"; if($map == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('CONTACT_FORM')." 
				<input type='hidden' name='form' value='0'></input>
				<input type='checkbox' name='form' value='1' "; if($form == 1) echo "checked"; if($form == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                 ".lang('SUBSCRIBE_FORM')." 
				<input type='hidden' name='subscribe' value='0'></input>
				<input type='checkbox' name='subscribe' value='1' "; if($subscribe == 1) echo "checked"; if($subscribe == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PORTFOLIO')." 
				<input type='hidden' name='portfolio' value='0'></input>
				<input type='checkbox' name='portfolio' value='1' "; if($portfolio == 1) echo "checked"; if($portfolio == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('TEAM')." 
				<input type='hidden' name='team' value='0'></input>
				<input type='checkbox' name='team' value='1' "; if($team == 1) echo "checked"; if($team == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
		
		<div class='checkbox'>
            <label>
                ".lang('PRICING_TABLES')." 
				<input type='hidden' name='pricing' value='0'></input>
				<input type='checkbox' name='pricing' value='1' "; if($pricing == 1) echo "checked"; if($pricing == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		<br/>
	";
}

// Update Active sections
function updateActiveSections($about,$contacts,$services,$testimonials,$partners,$map,$form,$subscribe,$portfolio,$team,$pricing){
	global $conection;
	$query = mysqli_query($conection,"UPDATE active SET about = '$about', contacts = '$contacts', services = '$services', testimonials = '$testimonials', partners = '$partners', map = '$map', form = '$form', subscribe = '$subscribe', portfolio = '$portfolio' , team = '$team' , pricing = '$pricing' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('WEBSITE_SECTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}


// Display Progress bar and Countdown in the index page
function activeBackgroundOptions(){
	global $conection;
	$sql = mysqli_query($conection,"select * from comingsoon WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	$progress = $row['progress'];
	$countdown = $row['countdown'];
	
echo "
		<div class='checkbox'>
            <label>
                Progress 
				<input type='hidden' name='progress' value='0'></input>
				<input type='checkbox' name='progress' value='1' "; if($progress == 1) echo "checked"; if($progress == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
		
		<br/>
		
		<div class='checkbox'>
            <label>
                Countdown 
				<input type='hidden' name='countdown' value='0'></input>
				<input type='checkbox' name='countdown' value='1' "; if($countdown == 1) echo "checked"; if($countdown == 0) echo ""; echo ">";
				echo "</input>
                <span class='cr'><i class='cr-icon fa fa-check'></i></span>
            </label>
        </div>
	";
}

// Update Progress bar and Countdown in settings page
function updateActiveBackgroundOptions($progress,$countdown){
	global $conection;
	$query = mysqli_query($conection,"UPDATE comingsoon SET progress = '$progress', countdown = '$countdown' WHERE id = '1'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('BACKGROUND_OPTIONS_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; homepage.php">'; 
			die();
			}				
			return true;
}


/*===============================================================================================================================*/
/*==================================================== Contact Form Area =====================================================*/
/*===============================================================================================================================*/

//Display Material Contact form Style
function contactFormMaterial(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contact_form WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
	echo "		
			<div class='messages'></div>

			<div class='controls'>

				<div class='row'>
					<div class='col-md-6'>
						<div class='form-group'>
							<span class='input input--hoshi'>
								<input class='input__field input__field--hoshi' type='text' name='name' id='form_name' required='required' data-error='".$row['fn_error']."'/>
								<label class='input__label input__label--hoshi input__label--hoshi-color-1' for='form_name'>
									<span class='input__label-content input__label-content--hoshi'>".$row['firstname']."</span>
								</label>
							</span>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class='form-group'>
							<span class='input input--hoshi'>
								<input class='input__field input__field--hoshi' type='text' name='surname' id='form_lastname' required='required' data-error='".$row['ln_error']."'/>
								<label class='input__label input__label--hoshi input__label--hoshi-color-1' for='form_lastname'>
									<span class='input__label-content input__label-content--hoshi'>".$row['lastname']."</span>
								</label>
							</span>
							<div class='help-block with-errors'></div>
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-6'>
						<div class='form-group'>
							<span class='input input--hoshi'>
								<input class='input__field input__field--hoshi' type='email' name='email' id='form_email' required='required' data-error='".$row['em_error']."'/>
								<label class='input__label input__label--hoshi input__label--hoshi-color-1' for='form_email'>
									<span class='input__label-content input__label-content--hoshi'>".$row['email']."</span>
								</label>
							</span>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class='form-group'>
							<span class='input input--hoshi'>
								<input class='input__field input__field--hoshi' type='tel' name='phone' id='form_phone'/>
								<label class='input__label input__label--hoshi input__label--hoshi-color-1' for='form_phone'>
									<span class='input__label-content input__label-content--hoshi'>".$row['phone']."</span>
								</label>
							</span>
							<div class='help-block with-errors'></div>
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>
						<div class='form-group'>
							<span class='input input--hoshi'>
								<textarea class='input__field input__field--hoshi' type='text' name='message' id='form_message' rows='3' required='required' data-error='".$row['mg_error']."'></textarea>
								<label class='input__label input__label--hoshi input__label--hoshi-color-1' for='form_message'>
									<span class='input__label-content input__label-content--hoshi'>".$row['message']."</span>
								</label>
							</span>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-12'>
						<input type='submit' class='btn btn-primary btn-send' value='".$row['button']."'>
					</div>
				</div>
			</div>
	";
	
}

//Display default Contact form Style
function contactForm(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contact_form WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);
	
	echo "
			<div class='messages'></div>

			<div class='controls'>

				<div class='row'>
					<div class='col-md-6'>
						<div class='form-group'>
							<label for='form_name'>".$row['firstname']."</label>
							<input id='form_name' type='text' name='name' class='form-control' placeholder='".$row['fn_holder']."' required='required' data-error='".$row['fn_error']."'>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class='form-group'>
							<label for='form_lastname'>".$row['lastname']."</label>
							<input id='form_lastname' type='text' name='surname' class='form-control' placeholder='".$row['ln_holder']."' required='required' data-error='".$row['ln_error']."'>
							<div class='help-block with-errors'></div>
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-6'>
						<div class='form-group'>
							<label for='form_email'>".$row['email']."</label>
							<input id='form_email' type='email' name='email' class='form-control' placeholder='".$row['em_holder']."' required='required' data-error='".$row['em_error']."'>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-6'>
						<div class='form-group'>
							<label for='form_phone'>".$row['phone']."</label>
							<input id='form_phone' type='tel' name='phone' class='form-control' placeholder='".$row['ph_holder']."'>
							<div class='help-block with-errors'></div>
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12'>
						<div class='form-group'>
							<label for='form_message'>".$row['message']."</label>
							<textarea id='form_message' name='message' class='form-control' placeholder='".$row['mg_holder']."' rows='4' required='required' data-error='".$row['mg_error']."'></textarea>
							<div class='help-block with-errors'></div>
						</div>
					</div>
					<div class='col-md-12'>
						<input type='submit' class='btn btn-primary btn-send' value='".$row['button']."'>
					</div>
				</div>
			</div>

	";
	
}

// Display Information from the Contact form area in the admin
function contactFormBack(){
	global $conection;
	$sql = mysqli_query($conection,"select * from contact_form WHERE id='1'");
	$row = mysqli_fetch_assoc($sql);
	
    echo "
	  <h3>".lang('EMAIL_BODY')."</h3>
	  <br>
	  
	   <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='sendyouremail'>".lang('PLACE_YOUR_EMAIL')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='sendyouremail' name='sendyouremail' type='text' class='form-control input-md'>".$row['sendyouremail']."</textarea>
		</div>
	  </div>

	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='mailfrom'>".lang('FROM_TEXT')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='mailfrom' name='mailfrom' type='text' class='form-control input-md'>".$row['mailfrom']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
	  <label class='col-sm-3 control-label' for='sendto'>".lang('SEND_TO_TEXT')."</label>
		<div class='col-sm-9'>
		  <textarea rows ='1' cols='10' id='sendto' name='sendto' type='text' class='form-control input-md'>".$row['sendto']."</textarea>
		</div>
	  </div>
	  
	   <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='subject'>".lang('SUBJECT')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='subject' name='subject' type='text' class='form-control input-md'>".$row['subject']."</textarea>
		</div>
	  </div>

	 <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='okmessage'>".lang('OK_MESSAGE')."</label>
		<div class='col-sm-9'>
		  <textarea rows ='2' cols='10' id='okmessage' name='okmessage' type='text' class='form-control input-md'>".$row['okmessage']."</textarea>
		</div>
	  </div>

	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='errormessage'>".lang('ERROR_MESSAGE')."</label>
		<div class='col-sm-9'>
		  <textarea rows ='2' cols='10' id='errormessage' name='errormessage' type='text' class='form-control input-md'>".$row['errormessage']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='emailtext'>".lang('EMAIL_HEADER_TEXT')."</label>
		<div class='col-sm-9'>
		  <textarea rows ='1' cols='10' id='emailtext' name='emailtext' type='text' class='form-control input-md'>".$row['emailtext']."</textarea>
		</div>
	  </div>

		<hr>
		
	  <h3>".lang('FORM_LABELS')."</h3>
	  <br>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='firstname'>".lang('FIRSTNAME')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='firstname' name='firstname' type='text' class='form-control input-md'>".$row['firstname']."</textarea>
		</div>
	  </div>

	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='lastname'>".lang('LASTNAME')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='lastname' name='lastname' type='text' class='form-control input-md'>".$row['lastname']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='email'>".lang('EMAIL')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='email' name='email' type='text' class='form-control input-md'>".$row['email']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='phone'>".lang('PHONE')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='phone' name='phone' type='text' class='form-control input-md'>".$row['phone']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='message'>".lang('MESSAGE')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='message' name='message' type='text' class='form-control input-md'>".$row['message']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='button'>".lang('BUTTON_TEXT')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='button' name='button' type='text' class='form-control input-md'>".$row['button']."</textarea>
		</div>
	  </div>

		<hr>

	  <h3>".lang('INPUT_PLACEHOLDERS')."</h3>
	  <br>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='fn_holder'>".lang('FIRST_NAME_HOLDER')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='fn_holder' name='fn_holder' type='text' class='form-control input-md'>".$row['fn_holder']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='ln_holder'>".lang('LAST_NAME_HOLDER')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='ln_holder' name='ln_holder' type='text' class='form-control input-md'>".$row['ln_holder']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='em_holder'>".lang('EMAIL_HOLDER')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='em_holder' name='em_holder' type='text' class='form-control input-md'>".$row['em_holder']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='ph_holder'>".lang('PHONE_HOLDER')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='ph_holder' name='ph_holder' type='text' class='form-control input-md'>".$row['ph_holder']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='mg_holder'>".lang('MESSAGE_HOLDER')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='mg_holder' name='mg_holder' type='text' class='form-control input-md'>".$row['mg_holder']."</textarea>
		</div>
	  </div>
	  
		<hr>
	  
	   <h3>".lang('INPUT_ERROR_MESSAGES')."</h3>
	   <br>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='fn_error'>".lang('FN_ERROR_MESSAGES')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='fn_error' name='fn_error' type='text' class='form-control input-md'>".$row['fn_error']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='ln_error'>".lang('LN_ERROR_MESSAGES')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='ln_error' name='ln_error' type='text' class='form-control input-md'>".$row['ln_error']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='em_error'>".lang('EM_ERROR_MESSAGES')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='em_error' name='em_error' type='text' class='form-control input-md'>".$row['em_error']."</textarea>
		</div>
	  </div>
	  
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-3 control-label' for='mg_error'>".lang('MG_ERROR_MESSAGES')."</label>
		<div class='col-sm-9'>
		   <textarea rows ='1' cols='10' id='mg_error' name='mg_error' type='text' class='form-control input-md'>".$row['mg_error']."</textarea>
		</div>
	  </div>
	";
}

	

// Update Contact Form Information
function update_contactForm($sendyouremail,$mailfrom,$sendto,$subject,$okmessage,$errormessage,$emailtext,$firstname,$lastname,$email,$phone,$message,$button,$fn_holder,$ln_holder,$em_holder,$ph_holder,$mg_holder,$fn_error,$ln_error,$em_error,$mg_error){			
	global $conection;
	$query = mysqli_query($conection,"update contact_form set  sendyouremail='$sendyouremail', mailfrom='$mailfrom', sendto='$sendto', subject='$subject', okmessage='$okmessage', errormessage='$errormessage', emailtext='$emailtext', firstname='$firstname', lastname='$lastname', email='$email', email='$email', phone='$phone', message='$message', button='$button', fn_holder='$fn_holder', ln_holder='$ln_holder', em_holder='$em_holder', ph_holder='$ph_holder', mg_holder='$mg_holder', fn_error='$fn_error', ln_error='$ln_error', em_error='$em_error', mg_error='$mg_error'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('CONTACT_FORM_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="2; contactform.php">'; 
		die(); 
	}		
	return true;
} 

	
/*===============================================================================================================================*/
/*========================================================= SEO Functions =======================================================*/
/*===============================================================================================================================*/	
	
// Display Seo Information in the backend and in index page
function seo(){
	global $conection;
	$sql = mysqli_query($conection,"select * from seo WHERE id='1'");
    $row = mysqli_fetch_assoc($sql);

echo "
	<!-- Text input-->
	  <div class='form-group'>
	  <label class='col-sm-2 control-label' for='title'>".lang('TITLE')."</label>
		<div class='col-sm-10'>
		  <textarea rows ='1' cols='20' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
		</div>
	  </div>
		
	  <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-2 control-label' for='keywords'>".lang('KEYWORDS')."</label>
		<div class='col-sm-10'>
		   <textarea rows ='2' cols='20' id='keywords' name='keywords' type='text' class='form-control input-md'>".$row['keywords']."</textarea>
		</div>
	  </div>
	  
	   <!-- Text input-->
	  <div class='form-group'>
		<label class='col-sm-2 control-label' for='description'>".lang('DESCRIPTION')."</label>
		<div class='col-sm-10'>
		   <textarea rows ='3' cols='20' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
		</div>
	  </div>
";
	}
	

// Update SEO Information
function updateSeo($keywords,$title,$description){			
	global $conection;
	$query = mysqli_query($conection,"update seo set keywords='$keywords', title='$title', description='$description'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SEO_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="2; settings.php">'; 
		die(); 
	}		
	return true;
} 

/*===============================================================================================================================*/
/*================================================== SKILLS Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Skills On admin
function listSkillsBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM skills ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th>".lang('COLOR')."</th>
				  <th>".lang('SKILL')."</th>
				  <th>".lang('PERCENT')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo "<div id='color' style='background-color:".$row['color']."'></div>";
			echo "</td><td>";
			echo $row['skill'];
			echo "</td><td>";
			echo $row['percent']."&#37;";
			echo "</td><td>";
			echo"
			<a href='edit_skill.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaSkill(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit Skill function
function editSkill($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from skills WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    
	echo "
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='skill'>".lang('SKILL')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='skill' name='skill' type='text' class='form-control input-md'>".$row['skill']."</textarea>
			
		  </div>
		</div>

		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='percent'>".lang('PERCENT')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='percent' name='percent' type='text' class='form-control input-md'>".$row['percent']."</textarea>
			
		  </div>
		</div>
	";

}

// Update Skill Information
function updateSkill($id,$color,$skill,$percent){
	global $conection;
	$query = mysqli_query($conection,"UPDATE skills SET color = '$color', skill = '$skill', percent = '$percent' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('SKILL_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="1; skills.php">'; 
		die();
	}				
	return true;
}

// Display categories so they can be used as filters for the portfolio section in the index page
function listSkillsFront(){
	global $conection;
	$sql = mysqli_query($conection,"select * from skills");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
		
	echo "	
		<div class='meter'>
			<span style='background-color: ".$row['color']."; width: ".$row['percent']."%;'></span>
			<div>
			  <span class='pull-left'>".$row['skill']."</span>
			  <span class='pull-right'>".$row['percent']."%</span>
			</div>
		</div>	
	";
	}
}

/*===============================================================================================================================*/
/*================================================== Team Functions =====================================================*/
/*===============================================================================================================================*/

// Display All partners created in the admin
function listTeamBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM team ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
		
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
				  <th>".lang('IMAGE')."</th>
				  <th>".lang('NAME')."</th>
				  <th>".lang('DESCRIPTION')."</th>				  
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='team_img'>"; 
			echo "<img src='../assets/img/uploads/team/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['name'];
			echo "</td><td>";
			echo $row['description'];
			echo "</td><td>";
			echo"
			<a href='edit_team.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaTeam(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit partner function
function editTeam($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from team WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    
	echo "
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='name'>".lang('NAME')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='name' name='name' type='text' class='form-control input-md'>".$row['name']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='4' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
			
		  </div>
		</div>
		
		<h3>".lang('SOCIAL_NETWORKS')."</h3>
		
		<div class='input-group col-sm-8'>
		  <span class='input-group-addon' id='social_fb'><i class='fa fa-facebook-square' aria-hidden='true'></i></span>
		   <textarea rows ='1' cols='10' id='social_fb' name='social_fb' type='text' class='form-control input-md'>".$row['social_fb']."</textarea>
		</div>
		
		<div class='input-group col-sm-8'>
		  <span class='input-group-addon' id='social_in'><i class='fa fa-linkedin-square' aria-hidden='true'></i></span>
		  <textarea rows ='1' cols='10' id='social_in' name='social_in' type='text' class='form-control input-md'>".$row['social_in']."</textarea>
		</div>
		
		<div class='input-group col-sm-8'>
		  <span class='input-group-addon' id='social_go'><i class='fa fa-google-plus-square' aria-hidden='true'></i></span>
		  <textarea rows ='1' cols='10' id='social_go' name='social_go' type='text' class='form-control input-md'>".$row['social_go']."</textarea>
		</div>
	";

}

// Update partner Information
function updateTeam($id,$name,$description,$social_fb,$social_in,$social_go){
	global $conection;
	$query = mysqli_query($conection,"UPDATE team SET name = '$name', description = '$description', social_fb = '$social_fb', social_in = '$social_in', social_go = '$social_go' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('MEMBER_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="1; team.php">'; 
		die();
	}				
	return true;
}

//Display partners in the index page
function showTeam(){
	global $conection;
	$sql = mysqli_query($conection,"select * from team");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
		<div class='col-md-3 col-sm-6 col-xs-12'>
			<figure class='effect-zoe'>
				<img class='img-responsive' src='assets/img/uploads/team/".$row['image']."' alt='".$row['name']."'/>
				<figcaption>
					<h2>".$row['name']."</h2>
					<p class='icon-links'>";
						?>
						<?php					
						$in = $row['social_in'];
						$go = $row['social_go'];
						$fb = $row['social_fb'];
						
						if ($in == '')
						{
							echo "";
						}
						if ($in != '')
						{
							echo "<a href='".$row['social_in']."' target='_blank'><i class='fa fa-linkedin-square fa-2x' aria-hidden='true'></i></a>";
						}
						if ($go == '')
						{
							echo "";
						}
						if ($go != '')
						{
							echo "<a href='".$row['social_go']."' target='_blank'><i class='fa fa-google-plus-square fa-2x' aria-hidden='true'></i></a>";
						}
						if ($fb == '')
						{
							echo "";
						}
						if ($fb != '')
						{
							echo "<a href='".$row['social_fb']."' target='_blank'><i class='fa fa-facebook-square fa-2x' aria-hidden='true'></i></a>";
						}
						?>
						<?php
		echo"		</p>
					<p class='description'>".$row['description']."</p>
				</figcaption>			
			</figure>
		</div>
			";	
		} 

}

//Display partners in the index page
function showAllTeam(){
	global $conection;
	$sql = mysqli_query($conection,"select * from team");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	echo"
		<div class='col-md-4 col-sm-6'>
			<div class='service-item'>
				
				<a href='".$row['website']."' target='_blank'><img src='assets/img/uploads/partners/".$row['image']."' class='center-block' alt='".$row['name']."' /></a>
			</span>
				<h4>
					<strong>".$row['name']."</strong>
				</h4>
			</div>
		</div>

			";	
		} 

}

/*===============================================================================================================================*/
/*================================================== Pricing Tables Functions =====================================================*/
/*===============================================================================================================================*/

// Display All pricing tables created in the admin
function listTablesBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM pricing ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
		
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
				  <th>".lang('COLOR')."</th>
				  <th>".lang('TITLE')."</th>
				  <th>".lang('HINT')."</th>
				  <th>".lang('PRICE')."</th>			  
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo "<div id='color' style='background-color:".$row['color']."'></div>";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo $row['hint'];
			echo "</td><td>";
			echo $row['price']." &euro;";
			echo "</td><td>";
			echo"
			<a href='edit_table.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaTable(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit Pricing Table function
function editTable($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from pricing WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    
	echo "
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='title'>".lang('TITLE')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='hint'>".lang('HINT')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='hint' name='hint' type='text' class='form-control input-md'>".$row['hint']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='price'>".lang('PRICE')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='price' name='price' type='text' class='form-control input-md'>".$row['price']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='description'>".lang('DESCRIPTION')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='3' cols='10' id='description' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='purchase'>".lang('PURCHASE')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='purchase' name='purchase' type='text' class='form-control input-md'>".$row['purchase']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='link'>".lang('LINK')."</label>  
		  <div class='col-md-6'>
		  <textarea rows ='1' cols='10' id='link' name='link' type='text' class='form-control input-md'>".$row['link']."</textarea>
			
		  </div>
		</div>
	";
}

// Update Pricing Table Information
function updateTable($id,$title,$hint,$price,$description,$purchase,$color,$link){
	global $conection;
	$query = mysqli_query($conection,"UPDATE pricing SET title = '$title', hint = '$hint', price = '$price', description = '$description', purchase = '$purchase', color = '$color', link = '$link' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PRICING_TABLE_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="1; pricing.php">'; 
		die();
	}				
	return true;
}

//Display Pricing Tables in the index page
function showTables(){
	global $conection;
	$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 3");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	
	echo "
		<div class='block hover01 personal fl col-md-4 col-sm-6 col-xs-12'>
			<h2 class='title' style='background-color: ".$row['color'].";opacity: 0.7;'>".$row['title']."</h2>
			<div class='content' style='background-color: ".$row['color'].";'>
				<p class='price'>
					<sup>&euro;</sup>
					<span>".$row['price']."</span>
				</p>
				<p class='hint'>".$row['hint']."</p>
			</div>
			<ul class='features'>
				".htmlspecialchars_decode($row['description'])."
			</ul>
			<div class='pt-footer' style='background-color: ".$row['color'].";'>
				<p><a href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
			</div>
		</div>
			";	
		} 
}

//Display Pricing Tables in the index page
function showTablesMaterial(){
	global $conection;
	$sql = mysqli_query($conection,"select * from pricing_number");
    $row = mysqli_fetch_assoc($sql);
	
	$number = $row['number'];
	
	
	if ($number == 0)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-12'>
				<div class='pricing-table'>
					<div class='pricing-panel pricing-panel-4'>
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-4'>
								<div style='background-color: ".$row['color'].";' class='pricing--heading'>
									<h4>".$row['hint']."</h4>
								</div>
							</div>
							<!-- .col-md-4 end-->
							<div class='col-xs-12 col-sm-12 col-md-7'>
								<!-- .pricing-body end -->
								<div class='pricing--body'>
									<div class='price'>
										<p><span class='currency'>$</span>".$row['price']."</p>
									</div>
									<ul class='pricing--list list-unstyled'>
										".htmlspecialchars_decode($row['description'])."
									</ul>
								</div>
							</div>
							<!-- .col-md-7 end-->
							<div class='col-xs-12 col-sm-12 col-md-1'>
								<a class='btn btn--secondary' href='".$row['link']."' target='_blank'> <i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
							</div>
							<!-- .col-md-1 end-->
							<!-- .pricing-table end -->
						</div>
					</div>
					<!-- .pricing-panel end-->
				</div>
				<!-- .pricing-table end -->
			</div>
			<!-- .col-md-12 end -->
			";	
		} 
		
	}
	if ($number == 2)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 2");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-12'>
				<div class='pricing-table'>
					<div class='pricing-panel pricing-panel-4'>
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-4'>
								<div style='background-color: ".$row['color'].";' class='pricing--heading'>
									<h4>".$row['hint']."</h4>
								</div>
							</div>
							<!-- .col-md-4 end-->
							<div class='col-xs-12 col-sm-12 col-md-7'>
								<!-- .pricing-body end -->
								<div class='pricing--body'>
									<div class='price'>
										<p><span class='currency'>$</span>".$row['price']."</p>
									</div>
									<ul class='pricing--list list-unstyled'>
										".htmlspecialchars_decode($row['description'])."
									</ul>
								</div>
							</div>
							<!-- .col-md-7 end-->
							<div class='col-xs-12 col-sm-12 col-md-1'>
								<a class='btn btn--secondary' href='".$row['link']."' target='_blank'> <i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
							</div>
							<!-- .col-md-1 end-->
							<!-- .pricing-table end -->
						</div>
					</div>
					<!-- .pricing-panel end-->
				</div>
				<!-- .pricing-table end -->
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 3)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 3");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-12'>
				<div class='pricing-table'>
					<div class='pricing-panel pricing-panel-4'>
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-4'>
								<div style='background-color: ".$row['color'].";' class='pricing--heading'>
									<h4>".$row['hint']."</h4>
								</div>
							</div>
							<!-- .col-md-4 end-->
							<div class='col-xs-12 col-sm-12 col-md-7'>
								<!-- .pricing-body end -->
								<div class='pricing--body'>
									<div class='price'>
										<p><span class='currency'>$</span>".$row['price']."</p>
									</div>
									<ul class='pricing--list list-unstyled'>
										".htmlspecialchars_decode($row['description'])."
									</ul>
								</div>
							</div>
							<!-- .col-md-7 end-->
							<div class='col-xs-12 col-sm-12 col-md-1'>
								<a class='btn btn--secondary' href='".$row['link']."' target='_blank'> <i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
							</div>
							<!-- .col-md-1 end-->
							<!-- .pricing-table end -->
						</div>
					</div>
					<!-- .pricing-panel end-->
				</div>
				<!-- .pricing-table end -->
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 4)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-12'>
				<div class='pricing-table'>
					<div class='pricing-panel pricing-panel-4'>
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-4'>
								<div style='background-color: ".$row['color'].";' class='pricing--heading'>
									<h4>".$row['hint']."</h4>
								</div>
							</div>
							<!-- .col-md-4 end-->
							<div class='col-xs-12 col-sm-12 col-md-7'>
								<!-- .pricing-body end -->
								<div class='pricing--body'>
									<div class='price'>
										<p><span class='currency'>$</span>".$row['price']."</p>
									</div>
									<ul class='pricing--list list-unstyled'>
										".htmlspecialchars_decode($row['description'])."
									</ul>
								</div>
							</div>
							<!-- .col-md-7 end-->
							<div class='col-xs-12 col-sm-12 col-md-1'>
								<a class='btn btn--secondary' href='".$row['link']."' target='_blank'> <i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
							</div>
							<!-- .col-md-1 end-->
							<!-- .pricing-table end -->
						</div>
					</div>
					<!-- .pricing-panel end-->
				</div>
				<!-- .pricing-table end -->
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 5)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 5");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-12'>
				<div class='pricing-table'>
					<div class='pricing-panel pricing-panel-4'>
						<div class='row'>
							<div class='col-xs-12 col-sm-12 col-md-4'>
								<div style='background-color: ".$row['color'].";' class='pricing--heading'>
									<h4>".$row['hint']."</h4>
								</div>
							</div>
							<!-- .col-md-4 end-->
							<div class='col-xs-12 col-sm-12 col-md-7'>
								<!-- .pricing-body end -->
								<div class='pricing--body'>
									<div class='price'>
										<p><span class='currency'>$</span>".$row['price']."</p>
									</div>
									<ul class='pricing--list list-unstyled'>
										".htmlspecialchars_decode($row['description'])."
									</ul>
								</div>
							</div>
							<!-- .col-md-7 end-->
							<div class='col-xs-12 col-sm-12 col-md-1'>
								<a class='btn btn--secondary' href='".$row['link']."' target='_blank'> <i class='fa fa-shopping-cart' aria-hidden='true'></i></a>
							</div>
							<!-- .col-md-1 end-->
							<!-- .pricing-table end -->
						</div>
					</div>
					<!-- .pricing-panel end-->
				</div>
				<!-- .pricing-table end -->
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	
}


//Display Pricing Tables in the index page
function showTablesSimple(){
	global $conection;
	$sql = mysqli_query($conection,"select * from pricing_number");
    $row = mysqli_fetch_assoc($sql);
	
	$number = $row['number'];
	
	
	if ($number == 0)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-3'>
				<div style='background-color: ".$row['color'].";' class='pricing__item'>
					<div class='title'>".$row['title']."</div>
					<h3 class='pricing_title'>".$row['hint']."</h3>
					<div class='pricing_price'><span class='pricing_currency'>$</span>".$row['price']."<span class='pricing_period'>/ month</span></div>
					<ul class='pricing_feature-list'>
						".htmlspecialchars_decode($row['description'])."
					</ul>
					<p><a class='button pricing_action' href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
				</div>
			</div>
			<!-- .col-md-12 end -->
			";	
		} 
		
	}
	if ($number == 2)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 2");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-6'>
				<div style='background-color: ".$row['color'].";' class='pricing_item'>
					<div class='title'>".$row['title']."</div>
					<h3 class='pricing_title'>".$row['hint']."</h3>
					<div class='pricing_price'><span class='pricing_currency'>$</span>".$row['price']."<span class='pricing_period'>/ month</span></div>
					<ul class='pricing_feature-list'>
						".htmlspecialchars_decode($row['description'])."
					</ul>
					<p><a class='button pricing_action' href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
				</div>
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 3)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 3");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-4'>
				<div style='background-color: ".$row['color'].";' class='pricing_item'>
					<div class='title'>".$row['title']."</div>
					<h3 class='pricing_title'>".$row['hint']."</h3>
					<div class='pricing_price'><span class='pricing_currency'>$</span>".$row['price']."<span class='pricing_period'>/ month</span></div>
					<ul class='pricing_feature-list'>
						".htmlspecialchars_decode($row['description'])."
					</ul>
					<p><a class='button pricing_action' href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
				</div>
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 4)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-3'>
				<div style='background-color: ".$row['color'].";' class='pricing_item'>
					<div class='title'>".$row['title']."</div>
					<h3 class='pricing_title'>".$row['hint']."</h3>
					<div class='pricing_price'><span class='pricing_currency'>$</span>".$row['price']."<span class='pricing_period'>/ month</span></div>
					<ul class='pricing_feature-list'>
						".htmlspecialchars_decode($row['description'])."
					</ul>
					<p><a class='button pricing_action' href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
				</div>
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	if ($number == 6)
	{
		$sql = mysqli_query($conection,"select * from pricing ORDER BY id DESC LIMIT 6");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
		
		echo "
			<div class='col-xs-12 col-sm-12 col-md-2'>
				<div style='background-color: ".$row['color'].";' class='pricing_item'>
					<div class='title'>".$row['title']."</div>
					<h3 class='pricing_title'>".$row['hint']."</h3>
					<div class='pricing_price'><span class='pricing_currency'>$</span>".$row['price']."<span class='pricing_period'>/ month</span></div>
					<ul class='pricing_feature-list'>
						".htmlspecialchars_decode($row['description'])."
					</ul>
					<p><a class='button pricing_action' href='".$row['link']."' target='_blank'>".$row['purchase']."</a></p>
				</div>
			</div>
			<!-- .col-md-12 end -->
			";	
		} 	
	}
	
}


/*===============================================================================================================================*/
/*================================================== BLOG Functions =====================================================*/
/*===============================================================================================================================*/

//Conver to slug from title function
function Slug($string){
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}

function getExcerpt($str, $startPos=0, $maxLength=100){
	if(strlen($str) > $maxLength) {
		$excerpt   = substr($str, $startPos, $maxLength-3);
		$lastSpace = strrpos($excerpt, ' ');
		$excerpt   = substr($excerpt, 0, $lastSpace);
		$excerpt  .= '<span>...</span>';
	} else {
		$excerpt = $str;
	}
	
	return $excerpt;
}

// Display All pricing tables created in the admin
function listBlogBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM post ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
		
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
				  <th>".lang('IMAGE')."</th>
				  <th>".lang('DATE')."</th>	
				  <th>".lang('TITLE')."</th>
				  <th>".lang('SLUG')."</th>	
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td class='team_img'>"; 
			echo "<img src='../assets/img/uploads/other/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['date'];
			echo "</td><td>";
			echo $row['title'];	
			echo "</td><td>";
			echo $row['slug'];				
			echo "</td><td>";
			echo"
			<a href='edit_post.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaPost(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit Pricing Table function
function editPost($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from post WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    echo "
		<!-- Text input-->
		<div class='form-group'>
			<div class='col-md-12'>
				<textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>

			</div>
		</div>
		
		<div class='form-group'>
			<div class='input-group date form_date col-md-4' data-date='' data-date-format='yyyy-mm-dd hh:ii' data-link-field='date' data-link-format='yyyy-mm-dd hh:ii'>
				<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span><input class='form-control' size='16' type='text' value='".$row['date']."' readonly>
			</div>
			<input id='date' name='date' type='hidden' value='".$row['date']."' required>
		</div>
		
		<!-- Text input-->
		<div class='form-group'>
			<div class='col-md-12'>
				 <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'>".$row['description']."</textarea>

			</div>
		</div>
		
	";

}

// Update Pricing Table Information
function updatePost($id,$date,$title,$description){
	global $conection;
	$query = mysqli_query($conection,"UPDATE post SET date = '$date', title = '$title', description = '$description' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('POST_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="1; blog.php">'; 
		die();
	}				
	return true;
}

//Display partners in the index page
function showBlog(){
	global $conection;
	$sql = mysqli_query($conection,"select * from post ORDER BY id DESC");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
	$excerpt = getExcerpt(htmlspecialchars_decode($row['description']), 0, 100);
	echo"
		<div class='col-xs-12 col-sm-6 col-md-3'>
              <a href='post?id=". $row['id'] . "' class='post-bg'>
                <div class='post-img'>
                  <img class='img-responsive' src='assets/img/uploads/other/".$row['image']."' alt='".$row['title']."' />
                </div>
                <div class='excerpt'>
                  <h3>".$row['title']."</h3>
                  <p>".$excerpt."</p>
                </div>
                <div class='hover-post'>
                 <span>".lang('SEE_MORE')."</span>
                </div>
              </a>
            </div>
			";	
		} 

}

//Display partners in the index page
function showLatestPosts(){
	global $conection;
	$sql = mysqli_query($conection,"select * from post ORDER BY id DESC LIMIT 3");
    $row = mysqli_num_rows($sql);	
	while ($row = mysqli_fetch_array($sql)) {
	$excerpt = getExcerpt(htmlspecialchars_decode($row['description']), 0, 100);
	
	echo"
		<div class='col-page col-sm-4 col-md-4'>
		  <a href='post?id=". $row['id'] . "' class='post-bg'>
			<div class='post-img'>
			  <img class='img-responsive' src='assets/img/uploads/other/".$row['image']."' alt='".$row['title']."' />
			</div>
			<div class='excerpt'>
			  <h3>".$row['title']."</h3>
			  <p>".$excerpt."</p>
			</div>
			<div class='hover-post'>
			 <span>".lang('SEE_MORE')."</span>
			</div>
		  </a>
		</div>
		";	
	} 
}

function post($id){
	global $conection;
    $sql = mysqli_query($conection, "select * from post WHERE id='".mysqli_escape_string($conection,$id)."'");
    $row = mysqli_fetch_assoc($sql);
	
		if (mysqli_num_rows($sql) > 0) {
			
				// rest of your code
				$image = $row['image'];
	
				if ($image == ''){
				
				echo "
						<div id='page_title' class='text-center'>
							<div class='parallax-window'>
								<div class='container inner parallax'>
									<h1>". $row['title'] ."</h1>
								</div>	
							</div>
						</div>

						<div id='breadcrumbs'>
							<div class='container'>
								<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; <a href='blog'>".lang('BLOG')."</a> &gt; ". $row['title'] ."</span></p>
							</div>	
						</div>
						
						<section id='blog' class='post container'>
							<div class='row blog-row bg-color-white block-padding'>
									<div class='col-md-12'>

										<!-- Single Blog Post -->
											<div class='post'>
												<div class='col-md-4'>
													<img src='assets/img/uploads/other/". $row['image'] ."' class='img-responsive' />
												</div>
												<br/>
												".htmlspecialchars_decode($row['description'])."												
										</div>
									</div>
								</div>
								<br/><hr>
												<div class='info'>
												<span class='glyphicon glyphicon-time'></span> ".lang('RELEASED')." &bull; ".$row['date'] ."
												</div>

				";
				}
				if ($image != ''){
					echo "
							<div id='page_title' class='text-center'>
								<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/other/". $row['image'] ."'>
									<div class='container inner parallax'>
										<h1>". $row['title'] ."</h1>
									</div>	
								</div>
							</div>

							<div id='breadcrumbs'>
								<div class='container'>
									<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; <a href='blog'>".lang('BLOG')."</a> &gt; ". $row['title'] ."</span></p>
								</div>	
							</div>
							
							<section id='blog' class='post container'>
								<div class='row blog-row bg-color-white block-padding'>
										<div class='col-md-12'>
											<!-- Single Blog Post -->
												<div class='post'>
													<div class='col-md-5'>
														<img src='assets/img/uploads/other/". $row['image'] ."' class='img-responsive' />
													</div>
													<br/>
													".htmlspecialchars_decode($row['description'])."													
											</div>
										</div>
									</div>
									<br/><hr>
									<div class='info'>
									<span class='glyphicon glyphicon-time'></span> ".lang('RELEASED')." &bull; ".$row['date'] ."
									</div>

				";
					
				}
			}

		else {
			// redirect to another page
			echo("<script>location.href = '404';</script>");
		
		}

}

/*===============================================================================================================================*/
/*================================================== Custom Pages Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Categories in the admin for the portfolio
function listPagesBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM custom ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
		
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
				  <th>".lang('IMAGE')."</th>
                  <th>".lang('TITLE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr>";		
			echo "<td id='custom_img'>"; 
			echo "<img src='../assets/img/uploads/other/".$row['image']."' alt='' />";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo"
			<a href='edit_page.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaPage(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// display the categories for select them under the new_project and edit_project pages
function showPages(){
	global $conection;
	$sql = mysqli_query($conection,"select * from custom order by display_order ASC");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
		echo "
			<li>
				<a href='page?slug=". $row['slug'] . "'class=' active1 page-scroll'>".$row['title']."</a>
			</li>
		";
	}
}

// display the categories for select them under the new_project and edit_project pages
function showPages1(){
	global $conection;
	$sql = mysqli_query($conection,"select * from custom order by display_order ASC");
    $row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)) {
		echo "<li>
				<a href='page?slug=". $row['slug'] . "' onclick=$('#menu-close').click();>".$row['title']."</a>
			</li>";
	}
}

function page($slug){
	global $conection;
    $sql = mysqli_query($conection, "select * from custom WHERE slug='".mysqli_escape_string($conection, $slug)."'");
    $row = mysqli_fetch_assoc($sql);
	
	if (mysqli_num_rows($sql) > 0) {
	
		$image = $row['image'];
			
		if ($image == ''){
			echo "
				<div id='page_title' class='text-center'>
						<div class='parallax-window'>
							<div class='container inner parallax'>
								<h1>". $row['title'] ."</h1>
							</div>	
						</div>
					</div>

					<div id='breadcrumbs'>
						<div class='container'>
							<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ". $row['title'] ."</span></p>
						</div>	
					</div>
					
					<section id='". $row['slug'] ."' class='custom'>
						<div class='container'>
								<br/>
								<p>".htmlspecialchars_decode($row['content'])."</p>
								<br/>
						</div>
					</section>
			";
		}
		if ($image != ''){	
		echo "
					<div id='page_title' class='text-center'>
						<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/other/". $row['image'] ."'>
							<div class='container inner parallax'>
								<h1>". $row['title'] ."</h1>
							</div>	
						</div>
					</div>

					<div id='breadcrumbs'>
						<div class='container'>
							<p>You are here: <span><a href='./'>".lang('HOMEPAGE')."</a> &gt; ". $row['title'] ."</span></p>
						</div>	
					</div>
					
					<section id='". $row['slug'] ."' class='custom'>
						<div class='container'>
								<br/>
								<p>".htmlspecialchars_decode($row['content'])."</p>
								<br/>
						</div>
					</section>

		";
		}
	}
	else {
			// redirect to another page
			echo("<script>location.href = '404';</script>");
		}

}

// Edit Custom Page function
function editPage($id){
	global $conection;
    $sql = mysqli_query($conection,"select * from custom WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
    echo "
	
		<!-- Text input-->
		<div class='form-group'> 
		  <div class='col-md-12'>
		  <textarea rows ='1' cols='10' id='title' name='title' type='text' placeholder='Title' class='form-control input-md'>".$row['title']."</textarea>
			
		  </div>
		</div>
		
		<!-- Text input-->
		<div class='form-group'> 
		  <div class='col-md-12'>
		  <textarea rows ='1' cols='10' id='slug' name='slug' type='text' placeholder='Slug' class='form-control input-md'>".$row['slug']."</textarea>
			
		  </div>
		</div>
		
		<input type='hidden' name='display_order' value='".$row['display_order']."'></input>
		
		<!-- Text input-->
		<div class='form-group'>
		  <div class='col-md-12'>
		   <textarea rows ='14' cols='10' id='summernote' name='content' type='text' class='form-control input-md'>".$row['content']."</textarea>					
			
		  </div>
		</div>

	";

}

function getPageImage($id){
	global $conection;
    $sql = mysqli_query($conection,"select * from custom WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($sql);
    echo "
		<label class='col-md-12 control-label' for='image'>".lang('CURRENT_IMAGE')."</label>  
		<!-- Text input-->
		<div class='form-group'> 
		  <div class='col-md-12'>
			<img src='../assets/img/uploads/other/".$row['image']."' class='img-responsive' alt='' />
			<input type='hidden' name='image' value='".$row['image']."'></input>
		  </div>
		</div>

	";
}

// Update Page content
function updatePage($id,$title,$slug,$content,$image,$display_order){
	global $conection;
	if ($slug != ''){
		$slug = str_replace(' ', '-', str_without_accents($_POST['slug']));	
	}
	if ($slug == ''){
		$slug = str_replace(' ', '-', str_without_accents($_POST['title']));	
	}
	$query = mysqli_query($conection,"UPDATE custom SET title = '$title', slug = '$slug', content = '$content', image = '$image', display_order = '$display_order' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('PAGE_UPDATED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; custom.php">'; 
					die();
			}				
			return true;
}

// display Custom Sections available with the image 
function showCustomSections(){
	global $conection;
	$sql = mysqli_query($conection,"select * from custom order by display_order ASC");
    $row = mysqli_num_rows($sql);


	while ($row = mysqli_fetch_array($sql)) {
			$image = $row['image'];
	
		 if ($image != ''){echo "<div class='parallax-window' data-parallax='scroll' data-image-src='assets/img/uploads/other/". $row['image'] ."'>";} if ($image == ''){echo "";} echo"
			
				<!-- ".str_without_accents($row['slug'])." -->
					<section id='".str_without_accents($row['slug'])."' class='custom "; if ($image == ''){echo "bg-primary";} if ($image != ''){echo "";} echo"'>
						<div class='container'>
							<div class='row text-center'>
								<div class='col-sm-12'>
									<h2>".$row['title']."</h2>
									<hr class='small'>							
								</div>					
							</div>
							<div id='content'>
								".htmlspecialchars_decode($row['content'])."
							</div>
					</div>
				</section>";
	
			 if ($image != ''){echo "</div>";} if ($image == ''){echo "";} 			
		}
		
}


function str_without_accents($str, $charset='utf-8'){
    $str = htmlentities($str, ENT_NOQUOTES, $charset);

    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caract�res

    return mb_strtolower($str);   // or add this : mb_strtoupper($str); for uppercase :)
}

/*===============================================================================================================================*/
/*================================================== Media Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Testimonials created in the admin
function listMedia(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM media ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");

		while ($row = mysqli_fetch_array($sql)) {
			
			echo "
				<div class='col-page col-sm-4 col-md-3'>
					<div class='media_img'>
					  <img class='img-responsive' src='../assets/img/uploads/other/".$row['image']."' alt=''>
					  <div class='middle'>
						<div class='text'>
							<a href='javascript:EliminaMediaImage(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
						  </div>
					  </div>
					</div>
				</div>
			

			";
		} 

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

/*===============================================================================================================================*/
/*================================================= Custom Pages Order Functions ================================================*/
/*===============================================================================================================================*/

function get_records(){
    global $conection;
    $records=mysqli_query($conection,"SELECT * FROM custom order by display_order ASC");
    $all=array();
    while($data=$records->fetch_assoc())
    {
        $all[]=$data;
    }
    return $all;
}
function save_record($id,$order){
    global $conection;
    $query="UPDATE custom SET display_order=".$order." WHERE id=".$id;
    if ($conection->query($query) === TRUE) {
    //echo "Record updated successfully";
    } else {
    //echo "Error updating record: " . $conn->error;
    }
}

/*===============================================================================================================================*/
/*================================================== COUNTERS Functions =====================================================*/
/*===============================================================================================================================*/

// Display All Counters On admin
function listCountersBack(){
	global $conection;
	// Informa��es da query
	$campos_query = "*";
	$final_query  = "FROM counters ORDER BY id DESC";
	 
	// Maximo de registros por pagina
	$maximo = 10000000;
	
	$_GET["pagina"] = 0;
	// Declara��o da pagina inicial
	$pagina = antiSQLInjection($_GET["pagina"]);
	if($pagina == "") {
		$pagina = "1";
	}
	 
	// Calculando o registro inicial
	$inicio = $pagina - 1;
	$inicio = $maximo * $inicio;
	 
	// Conta os resultados no total da query
	$strCount = "SELECT COUNT(*) AS 'num_registros' $final_query";
	$query = mysqli_query($conection,$strCount);
	$row = mysqli_fetch_array($query);
	$total = $row["num_registros"];
	 
	###################################################################################
	// INICIO DO CONTE�DO
	 
	// Realizamos a query
	$sql = mysqli_query($conection,"SELECT $campos_query $final_query LIMIT $inicio,$maximo");
	
	
	echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
			      <th>".lang('ICON')."</th>				
                  <th>".lang('COLOR')."</th>
				  <th>".lang('TITLE')."</th>
				  <th>".lang('VALUE')."</th>
				  <th></th>
                </tr>
              </thead>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";
			echo"<i class='fa ".$row['icon']." fa-2x' aria-hidden='true'></i>";
			echo "</td><td>";
			echo "<div id='color' style='background-color:".$row['color']."'></div>";
			echo "</td><td>";
			echo $row['title'];
			echo "</td><td>";
			echo $row['value'];
			echo "</td><td>";
			echo"
			<a href='edit_counter.php?id=". $row['id'] . "' class='btn btn-primary btn-sm' role='button'><i class='fa fa-fw fa-edit'></i> ".lang('EDIT')."</a>
			<a href='javascript:EliminaCounter(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a>
			</td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	

	// FIM DO CONTEUDO
	###################################################################################

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	 
	$pgs = ceil($total / $maximo);
	 
	if($pgs > 1 ) {
	 
		echo "<br />";
	 
		// Mostragem de pagina
		if($menos > 0) {
			//echo "<a href=".$_SERVER['PHP_SELF']."?pagina=$menos>anterior</a>&nbsp; ";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$menos'>< anterior</a>";
		}
	 
		// Listando as paginas
		for($i=1;$i <= $pgs;$i++) {
			if($i != $pagina) {
				echo " <".$_SERVER['PHP_SELF']."?pagina=".($i)."> [$i</a>] ";
			} else {
				echo "[<strong>".$i."</strong>] ";
			}
		}
	 
		if($mais <= $pgs) {
			//echo " <a href=".$_SERVER['PHP_SELF']."?pagina=$mais#tabela>pr�xima</a>";
			echo "<a class='paginas' href='{$_SERVER['PHP_SELF']}?pagina=$mais'>proxima ></a>";
		}
	}
}

// Edit Skill function
function editCounter($id){
	global $conection;
    $consulta = mysqli_query($conection,"select * from counters WHERE id='".$id."'");
    $row = mysqli_fetch_assoc($consulta);
    
	echo "
		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='title'>".lang('TITLE')."</label>  
		  <div class='col-md-3'>
		  <textarea rows ='1' cols='10' id='title' name='title' type='text' class='form-control input-md'>".$row['title']."</textarea>
			
		  </div>
		</div>

		<!-- Text input-->
		<div class='form-group'>
		  <label class='col-md-2 control-label' for='value'>".lang('VALUE')."</label>  
		  <div class='col-md-3'>
		  <textarea rows ='1' cols='10' id='value' name='value' type='text' class='form-control input-md'>".$row['value']."</textarea>
			
		  </div>
		</div>
	";

}

// Update Skill Information
function updateCounters($id,$color,$title,$value,$icon){
	global $conection;
	$query = mysqli_query($conection,"UPDATE counters SET color = '$color', title = '$title', value = '$value', icon = '$icon' WHERE id = '$id'");
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
		echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('COUNTER_UPDATED')."', 'success');</script>";
		echo '<meta http-equiv="refresh" content="1; counters.php">'; 
		die();
	}				
	return true;
}

// Update Counter Total Number Dashboard
function updateCounterSetting($number){
	global $conection;
	$query = mysqli_query($conection,"UPDATE counter SET number = '$number'");
	
	if (!$query) {
		echo ("No data was inserted!: " . mysqli_error());
		return false;
	} else {
			echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('COUNTER_TOTAL_UPDATED')."', 'success');</script>";
			echo '<meta http-equiv="refresh" content="1; settings.php">'; 
			die();
			}				
			return true;
}

//Display all services and display them in the index page
function showCounters(){
	global $conection;
	$sql = mysqli_query($conection,"select * from counter");
    $row = mysqli_fetch_assoc($sql);
	
	$number = $row['number'];
	
	
	if ($number == 0)
	{
		$sql = mysqli_query($conection,"select * from counters limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"			
				<div class='col-md-3 col-sm-3 col-xs-6 stats'>
					<i style='color:".$row['color']." !important;' class='fa ".$row['icon']."' aria-hidden='true'></i>
					<div class='counter-value' data-count='".$row['value']."'>0</div>
					<h5>".$row['title']."</h5>
				</div>
			";
		}
		
	}
	if ($number == 2)
	{
		$sql = mysqli_query($conection,"select * from counters limit 2");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
				<div class='col-md-6 col-sm-6 col-xs-6 stats'>
					<i style='color:".$row['color']." !important;' class='fa ".$row['icon']."' aria-hidden='true'></i>
					<div class='counter-value' data-count='".$row['value']."'>0</div>
					<h5>".$row['title']."</h5>
				</div>	
			";
		}
		
	}
	if ($number == 3)
	{
		$sql = mysqli_query($conection,"select * from counters limit 3");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
				<div class='col-md-4 col-sm-4 col-xs-4 stats'>
					<i style='color:".$row['color']." !important;' class='fa ".$row['icon']."' aria-hidden='true'></i>
					<div class='counter-value' data-count='".$row['value']."'>0</div>
					<h5>".$row['title']."</h5>
				</div>	
			";
		}
		
	}
	if ($number == 4)
	{
		$sql = mysqli_query($conection,"select * from counters limit 4");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
				<div class='col-md-3 col-sm-3 col-xs-6 stats'>
					<i style='color:".$row['color']." !important;' class='fa ".$row['icon']."' aria-hidden='true'></i>
					<div class='counter-value' data-count='".$row['value']."'>0</div>
					<h5>".$row['title']."</h5>
				</div>	
			";
		}
		
	}
	if ($number == 6)
	{
		$sql = mysqli_query($conection,"select * from counters limit 6");
		$row = mysqli_num_rows($sql);
		while ($row = mysqli_fetch_array($sql)) {
			echo"
				<div class='col-md-2 col-sm-2 col-xs-6 stats'>
					<i style='color:".$row['color']." !important;' class='fa ".$row['icon']."' aria-hidden='true'></i>
					<div class='counter-value' data-count='".$row['value']."'>0</div>
					<h5>".$row['title']."</h5>
				</div>	
			";
		}
		
	}

}

function theStats(){
	global $conection;
	
	$sql = mysqli_query($conection,"select * from stats_day ORDER BY date DESC limit 31");
		$row = mysqli_num_rows($sql);		
		while ($row = mysqli_fetch_array($sql)) {
			echo"['".$row['date']."',  ".$row['hits']."],";
		}		
		
}

/*===============================================================================================================================*/
/*======================================================= Event Functions =======================================================*/
/*===============================================================================================================================*/

// Write javascript with events listing without the need of getting it from external file
function listEvents()
{
	global $conection;
	$sql = mysqli_query($conection,"select * from events");
    $row = mysqli_fetch_assoc($sql);
		echo " 
		<script>		
		$(document).ready(function() {
			$('#events').fullCalendar({
				defaultDate: '".date("Y-m-d")."',
				editable: true,
				eventLimit: true,
				header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,basicWeek,basicDay'
						},
						
				eventClick:  function(event, jsEvent, view) {				
					$('#modalTitle').html(event.title);
					$('#modalBody').html(event.description);
					$('#startTime').html(moment(event.start).format('YYYY-MM-DD HH:mm'));
					$('#endTime').html(moment(event.end).format('YYYY-MM-DD HH:mm'));
					$('#eventUrl').attr('href',event.url);
					$('#fullCalModal').modal();
					 return false;
				},
							
				events: [
					{
						id: '".$row['id']."',
						title: '".$row['title']."',
						description: '<p>".$row['description']."</p>',					
						start: '".$row['start']."',
						end: '".$row['end']."',
						url: '".$row['url']."',
					},";
					while ($row = mysqli_fetch_array($sql)) {
				echo "
					{
						id: '".$row['id']."',
						title: '".$row['title']."',
						description: '<p>".$row['description']."</p>',					
						start: '".$row['start']."',
						end: '".$row['end']."',
						url: '".$row['url']."',
					},"; 	
			} ;
			echo "
				],			
			});	
		});			
	</script>
	";		
}

// Display events information inside a modal box
function modalEvents()
{

	echo "	
	
	<div id='fullCalModal' class='modal fade'>
		<div class='modal-dialog'>
			<div class='modal-content'>
				<div class='modal-header'>
					<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span> <span class='sr-only'>close</span></button>
					<h4><i class='fa fa-calendar' aria-hidden='true'></i> ".lang('EVENT_DETAILS')."</h4>
				</div>
				
				<div class='modal-body'>
					<div class='table-responsive'>
						<div class='col-md-12'>
							<h4><i class='fa fa-calendar' aria-hidden='true'></i> <span id='modalTitle'></span></h4>
							 <i class='fa fa-clock-o' aria-hidden='true'></i> <span id='startTime'></span>
							
						</div>
						<div class='col-md-12'>
							<h4><i class='fa fa-globe'></i> ".lang('DESCRIPTION').":</h4>
							 <blockquote><p id='modalBody'></p></blockquote>
						</div>
					</div>
				</div>
				<div class='modal-footer'>
					<button type='button' class='btn btn-primary' data-dismiss='modal'>".lang('CLOSE')."</button>
					<a class='btn btn-success' id='eventUrl' target='_blank' role='button'>".lang('VIEW_LINK')."</a>					
				</div>
			</div>
		</div>
	</div> 
";
}

// Display all events
function listAllEvents()
{
	global $conection;
	$sql = mysqli_query($conection,"select * from events ORDER BY start ASC");
    $row = mysqli_fetch_assoc($sql);
		
		echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>	
                  <th>".lang('TITLE')."</th>
				  <th>".lang('LINK')."</th>
				  <th>".lang('START_DATE')."</th>
				  <th></th>
                </tr>
              </thead>";
			  echo "<tr><td>";		
			echo $row['title'];
			echo "</td><td>"; 
			echo $row['url'];
			echo "</td><td>"; 
			echo $row['start'];
			echo "</td>";
			echo "<td class='r'>		
			<a href='javascript:EliminaEvento(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a></td>";
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['title'];
			echo "</td><td>"; 
			echo $row['url'];
			echo "</td><td>"; 
			echo $row['start'];
			echo "</td>";
			echo "<td class='r'>		
			<a href='javascript:EliminaEvento(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
}

// Display all Types (sort of category for Events)
function listAllTypes()
{
	global $conection;
	$sql = mysqli_query($conection,"select * from type ORDER BY id ASC");
    $row = mysqli_fetch_assoc($sql);
		
		echo "<table class='table table-striped table-bordered table-hover' id='dataTables-example'>";
		echo "  <thead>
                <tr>
					<th>ID</th>				
                  <th>".lang('TITLE')."</th>
				   <th></th>
                </tr>
              </thead>";
			echo "<tr><td>";		
			echo $row['id'];
			echo "</td>";
			echo "<td>";		
			echo $row['title'];
			echo "</td>"; 
			echo "<td class='r'>		
			<a href='javascript:EliminaTipo(". $row['id'] .")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a></td>";
			echo "</tr>"; 
		while ($row = mysqli_fetch_array($sql)) {
			// Print out the contents of each row into a table
			echo "<tr><td>";		
			echo $row['id'];
			echo "</td>";
			echo "<td>";		
			echo $row['title'];
			echo "</td>"; 
			echo "<td class='r'>		
			<a href='javascript:EliminaTipo(". $row['id'] . ")'class='btn btn-danger btn-sm' role='button'><i class='fa fa-fw fa-trash'></i> ".lang('DELETE')."</a></td>";
			echo "</tr>"; 	
		} 

		echo "</table>";
	
}


