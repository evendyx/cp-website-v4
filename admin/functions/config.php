<?php 

    // These variables define the connection information for your MySQL database 
    $username = "root"; 
    $password = ""; 
    $host = "localhost"; 
    $dbname = "cp1"; 
	
    $url = "admin/installer/install.php"; //Link for the installer page folder
	
	$limit = 30;			# default show hits for last (this many) days
	$bar_image  = "assets/images/bar.gif";	# main bar image
	$bar2_image = "assets/images/bar2.gif";	# bar image for extra projected hits
	$bar_width = 350;			# maximum width of the bars
	$bar_height = 16;			# height of the bars
	$no_cache = 1;			# 1=no cache page;  0=allow cache page
	
    $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'); 
    try { $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options); } 
    catch(PDOException $ex){ echo header('Location: '.$url); die;} //redirect to install page if no database connection created
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
    header('Content-Type: text/html; charset=utf-8'); 
	
	// 1. Create a database connection
	$conection=mysqli_connect($host,$username,$password,$dbname);
	if (!$conection) {
		die("Database connection failed: " . mysqli_error());
	}

	// 2. Select a database to use 
	$db_selected = mysqli_select_db($conection, $dbname);
	if (!$db_selected) {
		die("Database selection failed: " . mysqli_error());
	}
	
    session_start(); 
	
	//Expire the session if user is inactive for 30
	//minutes or more.
	$expireAfter = 15;

	//Check to see if our "last action" session
	//variable has been set.
	if(isset($_SESSION['last_action'])){
		
		//Figure out how many seconds have passed
		//since the user was last active.
		$secondsInactive = time() - $_SESSION['last_action'];
		
		//Convert our minutes into seconds.
		$expireAfterSeconds = $expireAfter * 60;
		
		//Check to see if they have been inactive for too long.
		if($secondsInactive >= $expireAfterSeconds){
			//User has been inactive for too long.
			//Kill their session.
			session_unset();
			session_destroy();
		}
		
	}

	//Assign the current timestamp as the user's
	//latest activity
	$_SESSION['last_action'] = time();
?>