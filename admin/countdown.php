<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('COUNTDOWN');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('BACKGROUND_SETTINGS');?></li>
				<li class="active"><?php echo lang('COUNTDOWN');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
					
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatecountdown" >
							<fieldset>
							<?php $csrf->echoInputField(); ?>
							<?php echo countDown();?>
																
							  <!-- Text input-->
							  <div class='form-group'>
								<label class='col-sm-2 control-label' for='bcolor'><?php echo lang('BORDER_COLOR');?></label>
									<div class='col-sm-4'>
									  <div id="cp1" class="input-group colorpicker-component">
										<input type="text" name="bcolor" value="<?php echo getbcolor(); ?>" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							  </div>
							  <!-- Text input-->
							  <div class='form-group'>
								<label class='col-sm-2 control-label' for='dcolor'><?php echo lang('DATE_COLOR');?></label>
									<div class='col-sm-4'>
									  <div id="cp2" class="input-group colorpicker-component">
										<input type="text" name="dcolor" value="<?php echo getdcolor(); ?>" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							  </div><!-- Text input-->
							  <div class='form-group'>
								<label class='col-sm-2 control-label' for='wcolor'><?php echo lang('TYPOGRAPHY_COLOR');?></label>
									<div class='col-sm-4'>
									  <div id="cp3" class="input-group colorpicker-component">
										<input type="text" name="wcolor" value="<?php echo getwcolor(); ?>" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							  </div><!-- Text input-->

							<script>
								$(function() {
									$('#cp1').colorpicker();
									$('#cp2').colorpicker();
									$('#cp3').colorpicker();
								});
							</script>

							</fieldset>
							
							<br/>
						  
						 <!-- Button -->
						<div class="form-group">
						  <label class="col-md-10 control-label" for="singlebutton"></label>
						  <div class="col-md-2">
							<input type="submit" name="updatecountdown" class="btn btn-primary" value="Update" />
						  </div>
						</div>
						
						 <?php
					  
						  if(isset($_POST['updatecountdown']))
							{
								updateCountDown($_POST['year'], $_POST['month'], $_POST['day'], $_POST['bcolor'], $_POST['bradius'], $_POST['border'], $_POST['dcolor'], $_POST['dsize'], $_POST['wcolor'], $_POST['wsize']);
							}
						?>

					  </form>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

		</div>
		<!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>