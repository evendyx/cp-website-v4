<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('HOMEPAGE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('PAGES');?></li>
				<li class="active"><?php echo lang('HOMEPAGE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
		
		<div class="row">
			<div class="col-md-4">
					
				<!--Background Type-->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h5 class="panel-title"><i class="fa fa-desktop" aria-hidden="true"></i>
									 <?php echo lang('BACKGROUND_SETTINGS');?>
								</h5>
							</div>
							<div class="panel-body">
							
							
							<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateBackground" >
							<?php $csrf->echoInputField(); ?>							
							<fieldset>

							<div class="col-md-12">
							<?php
								global $conection;
								$sql = mysqli_query($conection,"select type from background");
								$row = mysqli_fetch_assoc($sql);
								echo
								"
								<div class='col-md-12'>
									".lang('CURRENT_BACKGROUND')."<br/> <input type='text' class='col-md-8' name='type' value='".$row['type']."' disabled />
								</div>
								";
							?>
								
								<div class='col-md-12'>
									<?php echo lang('SELECT_BACKGROUND');?><br/> 
									<select class='col-md-8' name="type">
									  <option value="<?php
										global $conection;
										$sql = mysqli_query($conection,"select type from background");
										$row = mysqli_fetch_assoc($sql);
										echo $row['type'];
									?>">------</option>
									  <option value="static"><?php echo lang('STATIC_BACKGROUND');?></option>
									  <option value="slide"><?php echo lang('SLIDE_BACKGROUND');?></option>
									  <option value="particles"><?php echo lang('PARTICLES_BACKGROUND');?></option>
									  <option value="youtube"><?php echo lang('YOUTUBE_BACKGROUND');?></option>
									  <option value="own"><?php echo lang('OWN_BACKGROUND');?></option>
									</select>
								</div>
								
								<!-- Button -->
								<div class="form-group">
								  <label class="col-md-8 control-label" for="singlebutton"></label>
								  <div class="col-md-2">
									<input type="submit" name="updateBackground" class="btn btn-primary submit" value="update" />
								  </div>
								</div>
							</div>

							</fieldset>
							
						</form>
						 <?php
									  
						  if(isset($_POST['updateBackground']))
							{
								updateBackground($_POST['type']);							
							}
						?>
							 
							</div>
					
				</div> 
				
				<?php
					global $conection;
					$sql = mysqli_query($conection,"select type from background");
					$row = mysqli_fetch_assoc($sql);
					$type = $row['type'];
					
					if ($type == 'slide')
					{
					
								
				?>
				
				<!--Slider Type-->
					<div id="on" class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-desktop" aria-hidden="true"></i>
								<?php echo lang('SLIDER_TYPE');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateSliderType" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select type from slider_type");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_SLIDER_TYPE')." <br/> <input type='text' class='col-md-8' name='type' value='".$row['type']."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_SLIDER_TYPE');?><br/>
								<select class='col-md-8' name="type">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select type from slider_type");
									$row = mysqli_fetch_assoc($sql);
									echo $row['type'];
								?>">------</option>
								  <option value="fullscreen">Fullscreen (1600x900)px</option>
								  <option value="boxed">Boxed (1100x400)px</option>
								  <option value="fullwidth">Fullwidth (1600x500)px</option>
								</select>
							</div>
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateSliderType" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateSliderType']))
						{
							updateSliderType($_POST['type']);
						}
					?>
						 
						</div>
					</div>
					
					<?php 
					}
					?>

					<!--Services Type-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-columns" aria-hidden="true"></i>
								<?php echo lang('PRICING_TABLES_TYPE');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatePricingType" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select type from pricing_type");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_PRICING_TYPE')." <br/> <input type='text' class='col-md-8' name='type' value='".$row['type']."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_PRICING_TYPE');?><br/>
								<select class='col-md-8' name="type">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select type from pricing_type");
									$row = mysqli_fetch_assoc($sql);
									echo $row['type'];
								?>">------</option>
								  <option value="default">Default</option>
								  <option value="material">Material</option>
								</select>
							</div>
							
							
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updatePricingType" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updatePricingType']))
						{
							updatePricingType($_POST['type']);
						}
					?>
						 
						</div>
					</div>
								
				<!--Services Total-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-columns" aria-hidden="true"></i>
								<?php echo lang('PRICING_TABLES_SETTINGS');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatePricingNumber" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select number from pricing_number");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_TABLES')." <br/> <input type='text' class='col-md-8' name='number' value='".$row['number']." ".lang('TABLES')."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_TABLES_TOTAL');?><br/>
								<select class='col-md-8' name="number">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select number from pricing_number");
									$row = mysqli_fetch_assoc($sql);
									echo $row['number'];
								?>">------</option>
								  <option value="2">2 <?php echo lang('TABLES');?></option>
								  <option value="3">3 <?php echo lang('TABLES');?></option>
								  <option value="4">4 <?php echo lang('TABLES');?></option>
								  <option value="5">5 <?php echo lang('TABLES');?></option>
								</select>
							</div>
							
							
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updatePricingNumber" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updatePricingNumber']))
						{
							updatePricingNumber($_POST['number']);
						}
					?>
						 
						</div>
					</div>

			</div>
			<div class="col-md-4">
			
					<?php
						global $conection;
						$sql = mysqli_query($conection,"select type from background");
						$row = mysqli_fetch_assoc($sql);
						$type = $row['type'];
						
						if ($type != 'slide')
						{
						
									
					?>
			
					<!--Background Type-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-spinner" aria-hidden="true"></i>
								 <?php echo lang('COMING_SETTINGS');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateActiveBackgroundOptions" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
							
							<?php activeBackgroundOptions(); ?>
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateActiveBackgroundOptions" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateActiveBackgroundOptions']))
						{
							updateActiveBackgroundOptions($_POST['progress'],$_POST['countdown']);
						}
					?>
						 
						</div>
					</div>
					
					<?php 
					}
					?>
					
					<!--Services Type-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-info-circle" aria-hidden="true"></i>
								<?php echo lang('SERVICES_TYPE');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateServicesType" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select type from services_type");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_SERVICES_TYPE')." <br/> <input type='text' class='col-md-8' name='type' value='".$row['type']."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_SERVICES_TYPE');?><br/>
								<select class='col-md-8' name="type">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select type from services_type");
									$row = mysqli_fetch_assoc($sql);
									echo $row['type'];
								?>">------</option>
								  <option value="image">Image</option>
								  <option value="icon">Icon</option>
								</select>
							</div>
							
							
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateServicesType" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateServicesType']))
						{
							updateServicesType($_POST['type']);
						}
					?>
						 
						</div>
					</div>
								
				<!--Services Total-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-info-circle" aria-hidden="true"></i>
								<?php echo lang('SERVICE_SETTINGS');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateServices" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select number from services");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_SERVICES')." <br/> <input type='text' class='col-md-8' name='number' value='".$row['number']." ".lang('SERVICES')."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_SERVICES_TOTAL');?><br/>
								<select class='col-md-8' name="number">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select number from services");
									$row = mysqli_fetch_assoc($sql);
									echo $row['number'];
								?>">------</option>
								  <option value="2">2 <?php echo lang('SERVICES');?></option>
								  <option value="3">3 <?php echo lang('SERVICES');?></option>
								  <option value="4">4 <?php echo lang('SERVICES');?></option>
								  <option value="6">6 <?php echo lang('SERVICES');?></option>
								</select>
							</div>
							
							
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateServices" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateServices']))
						{
							updateServices($_POST['number']);
						}
					?>
						 
						</div>
					</div>

					<!--Portfolio Section-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-laptop" aria-hidden="true"></i>
								<?php echo lang('PORTFOLIO_SETTINGS');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatePortfolioType" >
						<?php $csrf->echoInputField(); ?>							
						<fieldset>

						<div class="col-md-12">
						<?php
							global $conection;
							$sql = mysqli_query($conection,"select type from portfolio_type");
							$row = mysqli_fetch_assoc($sql);
							echo
							"
							<div class='col-md-12'>
								".lang('CURRENT_PORTFOLIO_TYPE')." <input type='text' class='col-md-8' name='type' value='".$row['type']."' disabled />
							</div>
							";
						?>

							<div class='col-md-12'>
								<?php echo lang('SELECT_PORTFOLIO_TYPE');?><br/>
								<select class='col-md-8' name="type">
								  <option value="<?php
									global $conection;
									$sql = mysqli_query($conection,"select type from portfolio_type");
									$row = mysqli_fetch_assoc($sql);
									echo $row['type'];
								?>">------</option>
								  <option value="gallery">Gallery</option>
								  <option value="portfolio">Portfolio</option>
								</select>
							</div>
							
							
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updatePortfolioType" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
					</form>
					 <?php
								  
					  if(isset($_POST['updatePortfolioType']))
						{
							updatePortfolioType($_POST['type']);
						}
					?>
						 
						</div>
					</div>

			</div>
			
			<div class="col-md-4">
			
				<!-- Active sections Total-->
					<div class="panel panel-default">
						<div class="panel-heading">
							<h5 class="panel-title"><i class="fa fa-check-square" aria-hidden="true"></i>
								 <?php echo lang('WEBSITE_SECTIONS');?>
							</h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateActive" >
						<?php $csrf->echoInputField(); ?>					
						<fieldset>

						<div class="col-md-12">
						
						<?php echo activeHomepage(); ?>
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateActive" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateActive']))
						{
							updateActiveHomepage($_POST['about'],$_POST['services'],$_POST['testimonials'],$_POST['partners'],$_POST['subscribe'],$_POST['portfolio'],$_POST['team'],$_POST['pricing'],$_POST['blog'],$_POST['skills2'],$_POST['calendar']);
						}
					?>
						 
						</div>
					</div>	
			
				

			</div>

		</div>
		<!-- /.row -->
  
        </div>
        <!-- /.row -->



    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>