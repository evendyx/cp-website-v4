<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_SERVICE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="services.php"><?php echo lang('SERVICES');?></a></li>
				<li class="active"><?php echo lang('NEW_SERVICE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
				
					<div class="form-group">
						<label class="col-md-2 control-label" ><?php echo lang('ENABLE_IMAGE');?></label>
						<div class="col-md-6">
							<!-- Rounded switch -->
							<label class="switch" data-toggle="tooltip" data-placement="bottom" title="Enable Services Image">
							  <input type="checkbox">
							  <span class="slider round"></span>
							</label>
						</div>
					</div>
				
					<script>
					
						$('.switch').change(function(){
							var x = document.getElementById('service_image');
							var y = document.getElementById('service_logo');
							if(this.checked) {
								 
								if (x.style.display === 'none') {
									x.style.display = 'block';
									y.style.display = 'none';
								} else {
									x.style.display = 'none';
									y.style.display = 'block';
								}
							}
							else {
								if (x.style.display === 'none') {
									x.style.display = 'block';
									y.style.display = 'none';
								} else {
									x.style.display = 'none';
									y.style.display = 'block';
								}
							}
						});
					</script>
					<div class="col-md-12">
					<div id="service_logo">											
						<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newservice">
							<fieldset>
								<?php $csrf->echoInputField(); ?>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-2 control-label" for="logo">Logo</label>
									<div class="col-md-6">
										
										<?php
											$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"\\\\(.+)";\s+}/';
											$subject =  file_get_contents('assets/css/font-awesome.css');
											preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

											echo'<select name="logo" id="logo" class="fa-select col-md-6 control-label">
													<option selected="selected">'.lang('CHOOSE_ONE').'</option>';
											foreach ($matches as $match) {
											  echo '<option value="'.$match[1].'">'."&#x".$match[2].' '.$match[1].'</option>';
												}
											echo'</select> ';

											?>

									</div>
								</div>
								<div class="form-group">
									<label class='col-md-2 control-label' for='color'><?php echo lang('COLOR');?></label>
										<div class='col-md-2'>
										  <div id="cp1" class="input-group colorpicker-component">
											<input type="text" name="color" value="#fff" class="form-control" />
											<span class="input-group-addon"><i></i></span>
										</div>
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-2 control-label" for="title"><?php echo lang('TITLE');?></label>
									<div class="col-md-6">
										<input id="title" name="title" type="text" class="form-control input-md" required>

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-2 control-label" for="description"><?php echo lang('DESCRIPTION');?></label>
									<div class="col-md-6">
										<input id="description" name="description" type="text" class="form-control input-md" required>

									</div>
								</div>

								<!-- Button -->
								<div class="form-group">
									<label class="col-md-10 control-label" for="singlebutton"></label>
									<div class="col-md-2">
										<input type="submit" name="newservice" class="btn btn-primary" value="<?php echo lang('NEW_SERVICE');?>" />
									</div>
								</div>
								
								<script>
									$(function() {
										$('#cp1').colorpicker();
									});
								</script>

							</fieldset>
						</form>
						<?php		
							// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
							if (!empty($_POST['newservice']))
							 {
								global $conection;
								// Recupera os dados dos campos
								$logo = $_POST['logo'];
								$color = $_POST['color'];
								$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
								$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
								$type = 'icon';
							  
										// Insere os dados no banco
										$sql = mysqli_query($conection,"INSERT INTO service VALUES (0, '".$logo."','".$color."','".$title."','".$description."','".$type."')");
							 
										// Se os dados forem inseridos com sucesso			
										if (!$sql) {
										echo ("Can't insert into database: " . mysqli_error());
										return false;
										} else {
										echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_SERVICE_CREATED')."', 'success');</script>";
												echo '<meta http-equiv="refresh" content="1; services.php">'; 
												die();
										}		
										return true;

							}
								
						?>
					</div>
						
					<div id="service_image" style="display:none">	

						<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newimageservice">
							<fieldset>
								<?php $csrf->echoInputField(); ?>
								
								
									<!-- Image input-->
									<div class="form-group">
										<label class="col-md-2 control-label" id="image" for="image"><?php echo lang('UPLOAD_LOGO_IMAGE');?></label>
										<div class="col-md-6">
											<input type="file" name="image" id="image">
										</div>
									</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-2 control-label" for="title"><?php echo lang('TITLE');?></label>
									<div class="col-md-6">
										<input id="title" name="title" type="text" class="form-control input-md" required>

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-2 control-label" for="description"><?php echo lang('DESCRIPTION');?></label>
									<div class="col-md-6">
										<input id="description" name="description" type="text" class="form-control input-md" required>

									</div>
								</div>

								<!-- Button -->
								<div class="form-group">
									<label class="col-md-10 control-label" for="singlebutton"></label>
									<div class="col-md-2">
										<input type="submit" name="newimageservice" class="btn btn-primary" value="<?php echo lang('NEW_SERVICE');?>" />
									</div>
								</div>

							</fieldset>
						</form>
					
					
					<?php		
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['newimageservice']))
						 {
							global $conection;
							// Recupera os dados dos campos
							$image = $_FILES['image'];
							$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
							$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
							$type = 'image';
						  
								// Se a foto estiver sido selecionada
							if (!empty($image["name"])) {
						 
								// Largura m�xima em pixels
								$largura = 1000000;
								// Altura m�xima em pixels
								$altura = 1000000;
								// Tamanho m�ximo do arquivo em bytes
								$tamanho = 500000000000;
						 
								// Verifica se o arquivo � uma imagem
								if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
								   $error[1] = "Isso n�o � uma imagem.";
								} 
						 
								// Pega as dimens�es da imagem
								$dimensoes = getimagesize($image["tmp_name"]);
						 
								// Verifica se a largura da imagem � maior que a largura permitida
								if($dimensoes[0] > $largura) {
									$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
								}
						 
								// Verifica se a altura da imagem � maior que a altura permitida
								if($dimensoes[1] > $altura) {
									$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
								}
						 
								// Verifica se o tamanho da imagem � maior que o tamanho permitido
								if($image["size"] > $tamanho) {
									$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
								}
						 
									// Pega extens�o da imagem
									preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
						 
									// Gera um nome �nico para a imagem
									$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
						 
									// Caminho de onde ficar� a imagem
									$caminho_imagem = "../assets/img/uploads/services/" . $nome_imagem;
						 
									// Faz o upload da imagem para seu respectivo caminho
									move_uploaded_file($image["tmp_name"], $caminho_imagem);
						 
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO service_images VALUES (0, '".$nome_imagem."', '".$title."', '".$description."','".$type."')");
	
									// Se os dados forem inseridos com sucesso			
									if (!$sql) {
									echo ("Can't insert into database: " . mysqli_error());
									return false;
									} else {
									echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_SERVICE_CREATED')."', 'success');</script>";
											echo '<meta http-equiv="refresh" content="1; services.php">'; 
											die();
									}		
									return true;

								// Se houver mensagens de erro, exibe-as
								if (count($error) != 0) {
									foreach ($error as $erro) {
										echo $erro . "<br />";
									}
								}

						}

						}
							
						?>
					</div>
					
				</div>
			</div>
			<!-- /basic layout -->

		</div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->



	
<?php include 'footer.php'; ?>