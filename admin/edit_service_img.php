<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_SERVICE_IMG');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="services.php"><?php echo lang('SERVICES');?></a></li>
				<li class="active"><?php echo lang('EDIT_SERVICE_IMG');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
				
					<form id="editServiceImage" method="post" enctype="multipart/form-data" class="form-horizontal" name="editServiceImage">
										
							<?php $csrf->echoInputField(); ?>
							
							<fieldset>
							<!-- Text input-->
								
							<?php echo editServiceImage($_GET['id']); ?>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-6 control-label" for="singlebutton"></label>
								<div class="col-md-4">
									<input type="submit" name="editServiceImage" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
								</div>
							</div>

							</fieldset>
						</form>
						<?php

						  if(isset($_POST['editServiceImage']))
						  {
							updateServiceImage($_GET['id'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
						  }

						?>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>