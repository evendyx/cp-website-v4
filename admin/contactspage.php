<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('CONTACTS');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('PAGES');?></li>
				<li class="active"><?php echo lang('CONTACTS');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('DESCRIPTION');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					</div>
					<div class="panel-body">
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateabout" >
								<fieldset>
								<?php $csrf->echoInputField(); ?>
								<?php echo getContactsText();?>

								</fieldset>

							 <!-- Button -->
							<div class="form-group">
							  <label class="col-md-10 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateabout" class="btn btn-primary" value="update" />
							  </div>
							</div>
							<?php

							if(isset($_POST['updateabout']))
							{
								updateContactsText(htmlspecialchars($_POST['description'], ENT_QUOTES));
							}

							?>
						  </form>
					</div>
				</div>
				<!-- /basic layout -->

			</div>

			<div class="col-md-4">

				<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('TOP_IMAGE');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					</div>
					
					<div class="panel-body">

						 <form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="contactformImg" >
							<fieldset>
							<?php $csrf->echoInputField(); ?>
									<?php 
										global $conection;
										$sql = mysqli_query($conection,"select * from contactform_img");
										$row = mysqli_fetch_assoc($sql);
										
										$image = $row['image'];
										
										if ($image == ''){
											echo"
												<div class='form-group'>
													<div class='col-md-9'>
													<label class='col-md-12 control-label' for='image'>".lang('UPLOAD_IMAGE')."</label>
														<input type='file' name='image' id='image'>
													</div>
													
												</div>

												</fieldset>
												<div class='col-md-12'>
													<input type='submit' name='contactformImg' class='btn btn-primary submit' value='".lang('ADD_NEW')."' />
												</div>
											";
										}
										else{
											echo"
												<div class='col-md-12'>
													<img class='img-responsive' src='../assets/img/uploads/parallax/".$row['image']."' alt='' />
												</div>
												
												<a id='del' class='btn btn-danger' href='javascript:EliminaContactformImageParallax(".$row['id'].")' title='click for delete' ><i class='fa fa-fw fa-trash'></i> ".lang('DELETE_IMAGE')."</a>
											";
										}
									?>
								
								<?php
									global $conection;
									// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
									if (!empty($_POST['contactformImg']))
									 {
									  
										// Recupera os dados dos campos
										$image = $_FILES['image'];

										// Se a foto estiver sido selecionada
										if (!empty($image["name"])) {
									 
											// Largura m�xima em pixels
											$largura = 1000000;
											// Altura m�xima em pixels
											$altura = 1000000;
											// Tamanho m�ximo do arquivo em bytes
											$tamanho = 500000000000;
									 
											// Verifica se o arquivo � uma imagem
											if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
											   $error[1] = "Isso n�o � uma imagem.";
											} 
									 
											// Pega as dimens�es da imagem
											$dimensoes = getimagesize($image["tmp_name"]);
									 
											// Verifica se a largura da imagem � maior que a largura permitida
											if($dimensoes[0] > $largura) {
												$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
											}
									 
											// Verifica se a altura da imagem � maior que a altura permitida
											if($dimensoes[1] > $altura) {
												$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
											}
									 
											// Verifica se o tamanho da imagem � maior que o tamanho permitido
											if($image["size"] > $tamanho) {
												$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
											}
									 
												// Pega extens�o da imagem
												preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
									 
												// Gera um nome �nico para a imagem
												$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
									 
												// Caminho de onde ficar� a imagem
												$caminho_imagem = "../assets/img/uploads/parallax/" . $nome_imagem;
									 
												// Faz o upload da imagem para seu respectivo caminho
												move_uploaded_file($image["tmp_name"], $caminho_imagem);
									 
												// Insere os dados no banco
												$sql = mysqli_query($conection,"INSERT INTO contactform_img VALUES (0, '".$nome_imagem."')");
									 
												// Se os dados forem inseridos com sucesso			
												if (!$sql) {
												echo ("Can't insert into database: " . mysqli_error());
												return false;
												} else {
														echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
														echo '<meta http-equiv="refresh" content="1; contactspage.php">'; 
														die();
												}

											// Se houver mensagens de erro, exibe-as
											if (count($error) != 0) {
												foreach ($error as $erro) {
													echo $erro . "<br />";
												}
											}

									}
									}
								?>
							  </form>
				</div>
			</div>
			
			<!-- Active sections Total-->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php echo lang('WEBSITE_SECTIONS');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateActive" >
						<?php $csrf->echoInputField(); ?>					
						<fieldset>

						<div class="col-md-12">
						
						<?php echo activeContacts(); ?>
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateActive" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateActive']))
						{
							updateActiveContacts($_POST['partners'],$_POST['map'],$_POST['form'],$_POST['subscribe']);
						}
					?>
						 
						</div>
					</div>
			
			
		</div>
	

  </div>
  <!-- /.row -->

</div>
 <!-- /.container-fluid -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>