<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_COUNTER');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="counters.php"><?php echo lang('COUNTERS');?></a></li>
				<li class="active"><?php echo lang('EDIT_COUNTER');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					 <form id="editSkill" method="post" enctype="multipart/form-data" class="form-horizontal" name="editCounter">
										
					<?php $csrf->echoInputField(); ?>
					
					<fieldset>
					
					<div class="form-group">
						<label class='col-md-2 control-label' for='color'><?php echo lang('COUNTER_COLOR');?></label>
							<div class='col-md-2'>
							  <div id="cp1" class="input-group colorpicker-component">
								<input type="text" name="color" value="<?php echo getCounterColor($_GET['id']); ?>" class="form-control" />
								<span class="input-group-addon"><i></i></span>
							</div>
						</div>
					</div>
					
					<div class="form-group">
					<label class='col-md-2 control-label' for='color'><?php echo lang('ICON');?></label>
							<div class='col-md-6'>
					<?php
						$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"\\\\(.+)";\s+}/';
						$subject =  file_get_contents('assets/css/font-awesome.css');
						preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

						echo'<select name="icon" id="icon" class="fa-select col-md-6 control-label">
								<option selected="';
									global $conection;
									$sql = mysqli_query($conection,"select icon from counters");
									$row = mysqli_fetch_assoc($sql);
									echo $row['icon'];
									echo'">';global $conection;
									$sql = mysqli_query($conection,"select icon from counters");
									$row = mysqli_fetch_assoc($sql);
									echo $row['icon'];
									echo'</option>';
						foreach ($matches as $match) {
						  echo '<option value="'.$match[1].'">'."&#x".$match[2].' '.$match[1].'</option>';
							}
						echo'</select> ';

						?>
						</div>
					</div>
						
					<?php echo editCounter($_GET['id']); ?>

					<!-- Button -->
					<div class="form-group">
						<label class="col-md-6 control-label" for="singlebutton"></label>
						<div class="col-md-4">
							<input type="submit" name="editCounter" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
						</div>
					</div>
					
					<script>
						$(function() {
							$('#cp1').colorpicker();
						});
					</script>

					</fieldset>
				</form>
				<?php

				  if(isset($_POST['editCounter']))
				  {
					updateCounters($_GET['id'],$_POST['color'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['value'], ENT_QUOTES),$_POST['icon']);
				  }

				?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
    </div>
    <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>