			<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li <?php if ($active == "dashboard.php") echo "class=active" ?>>
                        <a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> <?php echo lang('DASHBOARD');?></a>
                    </li>
					
					<li <?php if ($active == "clients.php" || $active == "edit_client.php" || $active == "new_client.php") echo "class=active" ?>>
						<a href="clients.php"><i class="fa fa-fw fa-newspaper-o" aria-hidden="true"></i> <?php echo lang('CLIENTS');?></a>
					</li>
					<li <?php if ($active == "media.php") echo "class=active" ?>>
						<a href="media.php"><i class="fa fa-fw fa-folder-open" aria-hidden="true"></i> <?php echo lang('MEDIA');?></a>
					</li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" <?php if ($active == "text.php" || $active == "background.php" || $active == "progress.php" || $active == "countdown.php") echo "class=active" ?> data-target="#background"><i class="fa fa-fw fa-desktop"></i> <?php echo lang('BACKGROUND_SETTINGS');?> <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="background" class="collapse">
							<li <?php if ($active == "background.php") echo "class=active" ?>>
								<a href="background.php"><i class="fa fa-fw fa-picture-o" aria-hidden="true"></i> <?php echo lang('BACKGROUND');?></a>
							</li>
							<li <?php if ($active == "progress.php") echo "class=active" ?>>
								<a href="progress.php"><i class="fa fa-fw fa-spinner" aria-hidden="true"></i> <?php echo lang('PROGRESS_BAR');?></a>
							</li>
							<li <?php if ($active == "countdown.php") echo "class=active" ?>>
								<a href="countdown.php"><i class="fa fa-fw fa-hourglass-start" aria-hidden="true"></i> <?php echo lang('COUNTDOWN');?></a>
							</li>
						</ul>
					</li>
					<li>
                        <a href="javascript:;" data-toggle="collapse" <?php if ($active == "homepage.php" || $active == "aboutpage.php" || $active == "servicespage.php" || $active == "portfoliopage.php" || $active == "partnerspage.php" || $active == "contactspage.php" || $active == "custom.php" || $active == "new_page.php" || $active == "edit_page.php" || $active == "blogpage.php" || $active == "privacypage.php" || $active == "eventspage.php") echo "class=active" ?> data-target="#pages"><i class="fa fa-fw fa-clone"></i> <?php echo lang('PAGES');?> <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="pages" class="collapse">
							<li <?php if ($active == "custom.php" || $active == "new_page.php" || $active == "edit_page.php") echo "class=active" ?>>
								<a href="custom.php"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i> <?php echo lang('CUSTOM_PAGES');?></a>
							</li>
							<li <?php if ($active == "homepage.php") echo "class=active" ?>>
								<a href="homepage.php"><i class="fa fa-fw fa-file-text-o" aria-hidden="true"></i> <?php echo lang('HOMEPAGE');?></a>
							</li>
							<li <?php if ($active == "aboutpage.php") echo "class=active" ?>>
								<a href="aboutpage.php"><i class="fa fa-fw fa-star" aria-hidden="true"></i> <?php echo lang('ABOUT');?></a>
							</li>
							<li <?php if ($active == "servicespage.php") echo "class=active" ?>>
								<a href="servicespage.php"><i class="fa fa-fw fa-info-circle" aria-hidden="true"></i> <?php echo lang('SERVICES');?></a>
							</li>
							<li <?php if ($active == "eventspage.php") echo "class=active" ?>>
								<a href="eventspage.php"><i class="fa fa-fw fa-calendar" aria-hidden="true"></i> <?php echo lang('EVENTS');?></a>
							</li>
							<li <?php if ($active == "portfoliopage.php") echo "class=active" ?>>
								<a href="portfoliopage.php"><i class="fa fa-fw fa-laptop" aria-hidden="true"></i> <?php echo lang('PORTFOLIO');?></a>
							</li>
							<li <?php if ($active == "blogpage.php") echo "class=active" ?>>
								<a href="blogpage.php"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> <?php echo lang('BLOG');?></a>
							</li>							
							<li <?php if ($active == "partnerspage.php") echo "class=active" ?>>
								<a href="partnerspage.php"><i class="fa fa-fw fa-thumbs-up" aria-hidden="true"></i> <?php echo lang('PARTNERS');?></a>
							</li>
							<li <?php if ($active == "contactspage.php") echo "class=active" ?>>
								<a href="contactspage.php"><i class="fa fa-fw fa-envelope" aria-hidden="true"></i> <?php echo lang('CONTACTS');?></a>
							</li>
							<li <?php if ($active == "privacypage.php") echo "class=active" ?>>
								<a href="privacypage.php"><i class="fa fa-fw fa-lock" aria-hidden="true"></i> <?php echo lang('PRIVACY');?></a>
							</li>
						</ul>
					</li>					
					<li>
                        <a href="javascript:;" data-toggle="collapse" <?php if ($active == "services.php" || $active == "contacts.php" || $active == "contactform.php" || $active == "categories.php" || $active == "portfolio.php" || $active == "testimonials.php" || $active == "partners.php" || $active == "map.php" || $active == "skills.php" || $active == "team.php" || $active == "pricing.php" || $active == "new_table.php" || $active == "edit_table.php" || $active == "blog.php" || $active == "new_post.php" || $active == "edit_post.php" || $active == "new_team.php" ||  $active == "edit_team.php" || $active == "edit_skill.php" || $active == "new_skill.php" || $active == "new_service.php" || $active == "edit_service.php" || $active == "edit_service_img.php" || $active == "new_testimonial.php" || $active == "edit_testimonial.php" || $active == "new_partner.php" || $active == "new_category.php" || $active == "new_project.php" || $active == "edit_project.php" || $active == "edit_partner.php" || $active == "counters.php" || $active == "new_counter.php" || $active == "edit_counter.php") echo "class=active" ?> data-target="#sections"><i class="fa fa-fw fa-bars"></i> <?php echo lang('WEBSITE_SECTIONS');?> <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="sections" class="collapse">	
							<li <?php if ($active == "blog.php" || $active == "new_post.php" || $active == "edit_post.php") echo "class=active" ?>>
								<a href="blog.php"><i class="fa fa-fw fa-pencil" aria-hidden="true"></i> <?php echo lang('BLOG_POSTS');?></a>
							</li>						
							<li <?php if ($active == "services.php" || $active == "new_service.php" || $active == "edit_service.php" || $active == "edit_service_img.php") echo "class=active" ?>>
								<a href="services.php"><i class="fa fa-fw fa-info-circle" aria-hidden="true"></i> <?php echo lang('SERVICES');?></a>
							</li>
							<li <?php if ($active == "portfolio.php" || $active == "new_project.php" || $active == "new_category.php" || $active == "edit_project.php") echo "class=active" ?>>
								<a href="portfolio.php"><i class="fa fa-fw fa-laptop" aria-hidden="true"></i> <?php echo lang('PORTFOLIO');?></a>
							</li>								
							<li <?php if ($active == "testimonials.php" || $active == "new_testimonial.php" || $active == "edit_testimonial.php") echo "class=active" ?>>
								<a href="testimonials.php"><i class="fa fa-fw fa-comments" aria-hidden="true"></i> <?php echo lang('TESTIMONIALS');?></a>
							</li>
							<li <?php if ($active == "partners.php" || $active == "new_partner.php" || $active == "edit_partner.php") echo "class=active" ?>>
								<a href="partners.php"><i class="fa fa-fw fa-thumbs-up" aria-hidden="true"></i> <?php echo lang('PARTNERS');?></a>
							</li>
							<li <?php if ($active == "contactform.php") echo "class=active" ?>>
								<a href="contactform.php"><i class="fa fa-fw fa-envelope" aria-hidden="true"></i> <?php echo lang('CONTACT_FORM');?></a>
							</li>
							<li <?php if ($active == "map.php") echo "class=active" ?>>
								<a href="map.php"><i class="fa fa-fw fa-map-marker" aria-hidden="true"></i> <?php echo lang('MAP');?></a>
							</li>
							<li <?php if ($active == "contacts.php") echo "class=active" ?>>
								<a href="contacts.php"><i class="fa fa-fw fa-briefcase" aria-hidden="true"></i> <?php echo lang('FOOTER_INFORMATION');?></a>
							</li>
							<li <?php if ($active == "skills.php" || $active == "new_skill.php" || $active == "edit_skill.php") echo "class=active" ?>>
								<a href="skills.php"><i class="fa fa-line-chart" aria-hidden="true"></i> <?php echo lang('SKILLS');?></a>
							</li>
							<li <?php if ($active == "team.php" || $active == "new_team.php" || $active == "edit_team.php") echo "class=active" ?>>
								<a href="team.php"><i class="fa fa-users" aria-hidden="true"></i> <?php echo lang('TEAM');?></a>
							</li>
							<li <?php if ($active == "pricing.php" || $active == "new_table.php" || $active == "edit_table.php") echo "class=active" ?>>
								<a href="pricing.php"><i class="fa fa-columns" aria-hidden="true"></i> <?php echo lang('PRICING_TABLES');?></a>
							</li>
							<li <?php if ($active == "counters.php" || $active == "new_counter.php" || $active == "edit_counter.php") echo "class=active" ?>>
								<a href="counters.php"><i class="fa fa-percent" aria-hidden="true"></i> <?php echo lang('COUNTERS');?></a>
							</li>
                        </ul>
                    </li>
					<li <?php if ($active == "settings.php") echo "class=active" ?>>
                        <a href="settings.php"><i class="fa fa-fw fa-cogs"></i> <?php echo lang('SETTINGS');?></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->			
		</nav>