<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_TABLE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="pricing.php"><?php echo lang('PRICING_TABLES');?></a></li>
				<li class="active"><?php echo lang('NEW_TABLE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newTable">
						<fieldset>
							<?php $csrf->echoInputField(); ?>

							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="title"><?php echo lang('TITLE');?></label>
								<div class="col-md-6">
									<input id="title" name="title" type="text" class="form-control input-md" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="hint"><?php echo lang('HINT');?></label>
								<div class="col-md-6">
									<input id="hint" name="hint" type="text" class="form-control input-md" required>
								</div>
							</div>
							<div class="form-group">	
								<label class="col-md-2 control-label" for="price"><?php echo lang('PRICE');?></label>
								<div class="col-md-6">
									<input id="price" name="price" type="text" class="form-control input-md" required>
								</div>
							</div>
							<div class="form-group">	
								<label class="col-md-2 control-label" for="description"><?php echo lang('DESCRIPTION');?></label>
								<div class="col-md-6">
									<textarea id="description" name="description" type="text" class="form-control input-md" required></textarea>
								</div>
							</div>
							<div class="form-group">	
								<label class="col-md-2 control-label" for="purchase"><?php echo lang('PURCHASE');?></label>
								<div class="col-md-6">
									<input id="purchase" name="purchase" type="text" class="form-control input-md" required>
								</div>
							</div>
							<div class="form-group">	
								<label class="col-md-2 control-label" for="link"><?php echo lang('LINK');?></label>
								<div class="col-md-6">
									<input id="link" name="link" type="text" class="form-control input-md" required>
								</div>
							</div>
							<div class="form-group">	
								<label class='col-md-2 control-label' for='wcolor'><?php echo lang('TABLE_COLOR');?></label>
									<div class='col-md-4'>
									  <div id="cp1" class="input-group colorpicker-component">
										<input type="text" name="color" value="#000" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							</div>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-10 control-label" for="singlebutton"></label>
								<div class="col-md-2">
									<input type="submit" name="newTable" class="btn btn-primary" value="<?php echo lang('NEW_TABLE');?>" />
								</div>
							</div>
							
							<script>
								$(function() {
									$('#cp1').colorpicker();
								});
							</script>

						</fieldset>
					</form>
					<?php		
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['newTable']))
						 {
							global $conection;
							// Recupera os dados dos campos

							$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
							$hint = htmlspecialchars($_POST['hint'], ENT_QUOTES);
							$price = htmlspecialchars($_POST['price'], ENT_QUOTES);
							$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
							$purchase = htmlspecialchars($_POST['purchase'], ENT_QUOTES);
							$color = $_POST['color'];
							$link = $_POST['link'];
						  
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO pricing VALUES (0, '".$title."', '".$hint."', '".$price."', '".$description."', '".$purchase."', '".$color."', '".$link."')");
						 
									// Se os dados forem inseridos com sucesso			
									if (!$sql) {
									echo ("Can't insert into database: " . mysqli_error());
									return false;
									} else {
									echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_TABLE_CREATED')."', 'success');</script>";
											echo '<meta http-equiv="refresh" content="1; pricing.php">'; 
											die();
									}		
									return true;

						}
							
						?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>