<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_CUSTOM_PAGE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="custom.php"><?php echo lang('CUSTOM_PAGES');?></a></li>
				<li class="active"><?php echo lang('EDIT_CUSTOM_PAGE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-8">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					 <form id="editPage" method="post" enctype="multipart/form-data" class="form-horizontal" name="editPage">
										
										<?php $csrf->echoInputField(); ?>
										
										<fieldset>
											
										<?php echo editPage($_GET['id']); ?>

								</div> 
								
								
								<!-- Button -->
								<div class="form-group">
									<label class="col-md-10 control-label" for="singlebutton"></label>
									<div class="col-md-2">
										<input type="submit" name="editPage" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
									</div>
								</div>
								<br/><br/>	
							 
			</div>
			<!-- /basic layout -->

		</div>
		<div class="col-md-4">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title"><?php echo lang('PARALLAX');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				</div>
				
				<!-- /.panel-heading -->
				<div class="panel-body">
				
					
						<div class="col-md-12">
						
						<?php echo getPageImage($_GET['id']); ?>
						
							<!-- Trigger the modal with a button -->
							<button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal'><?php echo lang('SELECT_IMAGE');?></button>

							<?php 
								global $conection;
								$sql = mysqli_query($conection,"select * from media");
								$row = mysqli_num_rows($sql);
								
								$image = $row['image'];
								
								echo "
									<!-- Modal -->
									<div id='myModal' class='modal fade' role='dialog'>
									  <div class='modal-dialog'>

										<!-- Modal content-->
										<div class='modal-content'>
										  <div class='modal-header'>
											<button type='button' class='close' data-dismiss='modal'>&times;</button>
											<h4 class='modal-title'>".lang('MEDIA')."</h4>
										  </div>
										  <div class='modal-body'>
											<div class='options'>";
								
								  while ($row = mysqli_fetch_array($sql)) {
									  echo "
											<label title='Media Image'>
												<input type='radio' name='image' value='".$row['image']."'/>
												<img src='../assets/img/uploads/other/".$row['image']."'/>
											</label>
										";
								  }
								echo " 
											</div>
											</div>
										  <div class='modal-footer'>
											<button type='button' class='btn btn-primary' data-dismiss='modal'>".lang('SAVE_SELECTION')."</button>
										  </div>
										</div>

									  </div>
									</div>";
											 

							?>
							 
							<?php

							  if(isset($_POST['editPage']))
							  {
								updatePage($_GET['id'],htmlspecialchars($_POST['title'], ENT_QUOTES),$_POST['slug'],htmlspecialchars($_POST['content'], ENT_QUOTES),$_POST['image'],$_POST['display_order']);
							  }

							?>

					</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script>
	$(document).ready(function() {
		$('#summernote').summernote({
		  height: 300,                 // set editor height
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		  focus: true,                // set focus to editable area after initializing summernote
		  toolbar: [
			// [groupName, [list of button]]
			['codeview', ['codeview']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['link', ['link']],
			['table', ['table']],
			['hr', ['hr']],					
		  ]
		});
	});
</script>

<?php include 'footer.php'; ?>