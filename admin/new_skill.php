<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_SKILL');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="skills.php"><?php echo lang('SKILLS');?></a></li>
				<li class="active"><?php echo lang('NEW_SKILL');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newskill">
						<fieldset>
							<?php $csrf->echoInputField(); ?>
							
							<div class="form-group">
								<label class='col-md-2 control-label' for='color'><?php echo lang('SKILL_COLOR');?></label>
									<div class='col-md-6'>
									  <div id="cp1" class="input-group colorpicker-component">
										<input type="text" name="color" value="#000" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="skill"><?php echo lang('SKILL');?></label>
								<div class="col-md-6">
									<input id="skill" name="skill" type="text" class="form-control input-md" required>

								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="percent"><?php echo lang('PERCENT');?></label>
								<div class="col-md-6">
									<input id="percent" name="percent" type="text" class="form-control input-md" required>

								</div>
							</div>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-10 control-label" for="singlebutton"></label>
								<div class="col-md-2">
									<input type="submit" name="newskill" class="btn btn-primary" value="<?php echo lang('NEW_SKILL');?>" />
								</div>
							</div>
							
							<script>
								$(function() {
									$('#cp1').colorpicker();
								});
							</script>

						</fieldset>
					</form>
					<?php		
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['newskill']))
						 {
							global $conection;
							// Recupera os dados dos campos

							$color = $_POST['color'];
							$skill = htmlspecialchars($_POST['skill'], ENT_QUOTES);
							$percent = htmlspecialchars($_POST['percent'], ENT_QUOTES);
						  
							// Insere os dados no banco
							$sql = mysqli_query($conection,"INSERT INTO skills VALUES (0, '".$color."','".$skill."','".$percent."')");
				 
							// Se os dados forem inseridos com sucesso			
							if (!$sql) {
							echo ("Can't insert into database: " . mysqli_error());
							return false;
							} else {
							echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_SKILL_CREATED')."', 'success');</script>";
									echo '<meta http-equiv="refresh" content="1; skills.php">'; 
									die();
							}		
							return true;

						}
							
						?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>