<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('BACKGROUND');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('BACKGROUND_SETTINGS');?></li>
				<li class="active"><?php echo lang('BACKGROUND');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
	<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
					
						<div class="table-responsive">
						
							<div class="row">

								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">									
									<li role="presentation" class="active"><a href="#slide" aria-controls="slide" role="tab" data-toggle="tab"> <?php echo lang('IMAGES_SLIDESHOW');?></a></li>
									<li role="presentation"><a href="#text" aria-controls="static" role="tab" data-toggle="tab"><?php echo lang('TEXT');?></a></li>
									<li role="presentation"><a href="#static" aria-controls="static" role="tab" data-toggle="tab"><?php echo lang('STATIC_IMAGE');?></a></li>
									<li role="presentation"><a href="#particles" aria-controls="particles" role="tab" data-toggle="tab"> <?php echo lang('PARTICLES');?></a></li>
									<li role="presentation"><a href="#youtube" aria-controls="youtube" role="tab" data-toggle="tab"><?php echo lang('YOUTUBE_VIDEO');?></a></li>
									<li role="presentation"><a href="#own" aria-controls="own" role="tab" data-toggle="tab"><?php echo lang('OWN_VIDEO');?></a></li>
								  </ul>

								  <!-- Tab panes -->
								  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane" id="text">
									  <div class="row">
											<div class="alert alert-danger">
											   <strong><?php echo lang('HELP2');?></strong>
												 <p><?php echo lang('HELP1');?> <?php echo lang('PAGES');?>/<a href="homepage" target="_self"><?php echo lang('HOMEPAGE');?></a>/<?php echo lang('BACKGROUND_SETTINGS');?>
											</div>
									  </div>
											<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatetext" >
												<br/>
												
												<?php $csrf->echoInputField(); ?>
												<div class="row">

														<fieldset>
											
												<?php echo getTheText();?>

												</fieldset>
												
												<br/>
											  
											<!-- Button -->
											<div class="form-group">
											  <label class="col-md-10 control-label" for="singlebutton"></label>
											  <div class="col-md-2">
												<input type="submit" name="updatetext" class="btn btn-primary" value="update" />
											  </div>
											</div>
											<?php

											if(isset($_POST['updatetext']))
											{
												updateText(htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
											}

											?>
										  </form>
									  </div>
								  </div>
								  <!-- Tab Static Image Form -->
									<div role="tabpanel" class="tab-pane" id="static">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="static" >
										<?php $csrf->echoInputField(); ?>
										<br/>
											<div class="row">
										
												<fieldset>
												
												<?php 
													global $conection;
													$sql = mysqli_query($conection,"select * from static");
													$row = mysqli_fetch_assoc($sql);
													
													$image = $row['image'];
													
													if ($image == ''){
														echo"
															<div class='form-group'>
																<div class='col-md-9'>
																<label class='col-md-3 control-label' for='image'>".lang('UPLOAD_IMAGE')."</label>
																	<input type='file' name='image' id='image' data-toggle='tooltip' data-placement='right' title='Only one image will be uploaded. In order to add a new one, you`ll need to delete the existing one.'>
																</div>
																
															</div>

															</fieldset>
															<div class='col-md-3'>
																<input type='submit' name='static' class='btn btn-primary submit' value='".lang('ADD_NEW')."' />
															</div>
														";
													}
													else{
														echo"
															<div class='col-md-5'>
																<img class='img-responsive' src='../assets/img/uploads/other/".$row['image']."' alt='' />
															</div>
															
															<a id='del' class='btn btn-danger' href='javascript:EliminaImage(".$row['id'].")' title='click for delete' ><i class='fa fa-fw fa-trash'></i> ".lang('DELETE_IMAGE')."</a>
														";
													}
												?>

											</div>
	
										</form>
									</div>
									<!-- Tab Slideshow Form -->
									<div role="tabpanel" class="tab-pane active" id="slide">
									<div class="row">
										<div class="col-md-5">
																						
											<div class="alert alert-danger">
											   <strong><?php echo lang('HELP');?></strong>
											   <br/><br/>										   
												  <ul>
													<li><b>Boxed Slider -></b> 1100x400 px</li>
													<li><b>Fullwidth Slider -></b> 1600x500 px</li>
													<li><b>Fullscreen Slider -></b> 1600x900 px</li>
												  </ul>
												  <br/>
												 <p><?php echo lang('HELP1');?> <br/><?php echo lang('PAGES');?>/<a href="homepage" target="_self"><?php echo lang('HOMEPAGE');?></a>/<?php echo lang('SLIDER_TYPE');?>
											</div>
												
											<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="slide" >
												<?php $csrf->echoInputField(); ?>
												<br/>
												
												<!-- Text input-->
												<div class="form-group">
													<div class="col-md-12">
														<input id="title" name="title" type="text" class="form-control input-md" placeholder="<?php echo lang('TITLE');?>" required>
													</div>
												</div>
												
												<!-- Text input-->
												<div class="form-group">
													<div class="col-md-12">
														 <textarea rows ='4' cols='2' id='description' name='description' type='text' placeholder="<?php echo lang('DESCRIPTION');?>" class='form-control input-md'></textarea>
													</div>
												</div>
	
												<!-- Image input-->
												<div class="form-group">
													<div class="col-md-9">
													<label class="col-md-12 control-label" for="url"><?php echo lang('UPLOAD_IMAGE');?></label>
														<input type="file" data-toggle="tooltip" data-placement="right" title="You can upload several images to the slide." name="image" id="image">
													</div>
												</div>
		
											<div class="col-md-12">
												<input type="submit" name="slide" class="btn btn-primary submit" value="<?php echo lang('ADD_NEW');?>" />
											</div>
											
											</form>

										</div>
										
										<div class="col-md-7">
											<?php echo getAllImages(); ?>
										</div>
										
										</div>
										
									</div>
									<!-- Tab Slideshow Form -->
									<div role="tabpanel" class="tab-pane" id="particles">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="particles" >
										<?php $csrf->echoInputField(); ?>
										<br/>
											
											<div class="row">
												<fieldset>
												<!-- Image input-->
												<div class="col-md-12">
													<div class="form-group">
														<div class="col-md-9">
														<label class='col-md-8 control-label' for='bpcolor'><?php echo lang('BACKGROUND_COLOR');?></label>
															<div class='col-md-4'>
															  <div id="cp1" class="input-group colorpicker-component">
																<input type="text" name="bpcolor" value="<?php echo getbpcolor(); ?>" class="form-control" />
																<span class="input-group-addon"><i></i></span>
															</div>
														</div>
														<label class='col-md-8 control-label' for='dpcolor'><?php echo lang('DEGRADE_COLOR');?></label>
															<div class='col-md-4'>
															  <div id="cp2" class="input-group colorpicker-component">
																<input type="text" name="dpcolor" value="<?php echo getdpcolor(); ?>" class="form-control" />
																<span class="input-group-addon"><i></i></span>
															</div>
														</div>
														<label class='col-md-8 control-label' for='ppcolor'><?php echo lang('PARTICLES_COLOR');?></label>
															<div class='col-md-4'>
															  <div id="cp3" class="input-group colorpicker-component">
																<input type="text" name="ppcolor" value="<?php echo getppcolor(); ?>" class="form-control" />
																<span class="input-group-addon"><i></i></span>
															</div>
														</div>
														</div>
														<!-- Button -->
														<div class="form-group">
														  <label class="col-md-10 control-label" for="singlebutton"></label><br/>
														  <div class="col-md-2">
															<input type="submit" name="updateparticles" class="btn btn-primary submit" value="update" />
														  </div>
														</div>
														<script>
															$(function() {
																$('#cp1').colorpicker();
															});
															$(function() {
																$('#cp2').colorpicker();
															});
															$(function() {
																$('#cp3').colorpicker();
															});
														</script>
		
													</div>
													</div>
												</fieldset>
												 <?php
											  
												  if(isset($_POST['updateparticles']))
													{
														updateParticles($_POST['bpcolor'],$_POST['dpcolor'],$_POST['ppcolor']);
													}
												?>
				
											</div>
											
	
										</form>
									</div>
									<!-- Tab Youtube Video Form -->
									<div role="tabpanel" class="tab-pane" id="youtube">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="youtube" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											
											<div class="row">
												<fieldset>
												
													<!-- Image input-->
													<div class="form-group">
														
														<div class="col-md-8">
														<label class="col-md-3 control-label" for="video_id"><?php echo lang('YOUTUBE_VIDEO_ID');?></label>
															<input type="text" data-toggle="tooltip" data-placement="right" title="Just copy/paste the id from the youtube video you want to display. Select it after the '?v='." name="video_id" id="video_id">
														</div>
														<div class="col-md-1">
															<input type="submit" name="youtube" class="btn btn-primary submit" value="<?php echo lang('ADD_NEW');?>" />
														 </div>
														 <br/>
														 <br/><br/>
														<div class="col-md-9">
														<?php
																global $conection;
																$sql = mysqli_query($conection,"select type from youtube");
																echo "<div id='background-video' class='background-video'></div>";
															
														?>
														</div>
														
														
													</div>

												</fieldset>
											</div>
								
											<br/>

										</form>
										
									</div>
									<!-- Tab Own Video Form -->
									<div role="tabpanel" class="tab-pane" id="own">
										<form action="background.php" method="post" enctype="multipart/form-data">
											<?php $csrf->echoInputField(); ?>
											<br/>
												<div class="row">
													<div class="col-md-6">
														<input type="file"  data-toggle="tooltip" data-placement="right" title="The valid formats are webm, ogv and mp4" name="fileToUpload"/>
													</div>
													<div class="col-md-6">
														<input type="submit" name="upd" class="btn btn-primary submit" value="Upload Video" />
													</div>
												</div>
											</form>
											<?php
											global $conection;
											if (isset($_FILES["fileToUpload"]["name"]))
											{
												$fileToUpload = $_FILES["fileToUpload"]["name"];
											}
											if(!empty($fileToUpload)){
												extract($_POST);
												$target_dir = "../assets/img/uploads/video/";
												$target_file = $target_dir . $fileToUpload;
												if(isset($upd)){
													$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
													if($imageFileType != "mp4" && $imageFileType != "ogv" && $imageFileType != "webm"){
														echo "<script type='text/javascript'>sweetAlert('Oops...', '".lang('ERROR')."', 'error');</script>";
														echo '<meta http-equiv="refresh" content="1; background.php">'; 
														die();
													}if($imageFileType == "mp4" || $imageFileType == "ogv" || $imageFileType == "webm"){
														$video_path=$_FILES['fileToUpload']['name'];
														mysqli_query($conection,"insert into video(video_name, type) values('$video_path', 'own') ");
														move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$target_file);
														echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('VIDEO_UPLOADED')."', 'success');</script>";
														echo '<meta http-equiv="refresh" content="1; background.php#own">'; 
														die();
													}
												}
											//display all uploaded video
											}
											
											?>
											<?php echo getAllVideo(); ?>
											
											</div>
											
									</div>
								</div>
							</div>  
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

			<script>
	$('#myTabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});
</script>
</div>
<!-- /#page-wrapper -->

<?php
							  
  if(isset($_POST['youtube']))
	{
		updateYoutube($_POST['video_id']);
	}
?>


<?php
global $conection;
// Se o usuário clicou no botão cadastrar efetua as ações
if (!empty($_POST['static']))
 {
  
	// Recupera os dados dos campos
	$image = $_FILES['image'];

	// Se a foto estiver sido selecionada
	if (!empty($image["name"])) {
 
		// Largura máxima em pixels
		$largura = 1000000;
		// Altura máxima em pixels
		$altura = 1000000;
		// Tamanho máximo do arquivo em bytes
		$tamanho = 500000000000;
 
    	// Verifica se o arquivo é uma imagem
    	if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
     	   $error[1] = "".lang('NOT_A_IMAGE')."";
   	 	} 
 
		// Pega as dimensões da imagem
		$dimensoes = getimagesize($image["tmp_name"]);
 
		// Verifica se a largura da imagem é maior que a largura permitida
		if($dimensoes[0] > $largura) {
			$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}
 
		// Verifica se a altura da imagem é maior que a altura permitida
		if($dimensoes[1] > $altura) {
			$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}
 
		// Verifica se o tamanho da imagem é maior que o tamanho permitido
		if($image["size"] > $tamanho) {
   		 	$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
 
			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
 
        	// Gera um nome único para a imagem
        	$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
 
        	// Caminho de onde ficará a imagem
        	$caminho_imagem = "../assets/img/uploads/other/" . $nome_imagem;
 
			// Faz o upload da imagem para seu respectivo caminho
			move_uploaded_file($image["tmp_name"], $caminho_imagem);
 
			// Insere os dados no banco
			$sql = mysqli_query($conection,"INSERT INTO static VALUES (0, '".$nome_imagem."', 'static')");
 
			// Se os dados forem inseridos com sucesso			
			if (!$sql) {
			echo ("Can't insert into database: " . mysqli_error());
			return false;
			} else {
					echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; background.php#static">'; 
					die();
			}

		// Se houver mensagens de erro, exibe-as
		if (count($error) != 0) {
			foreach ($error as $erro) {
				echo $erro . "<br />";
			}
		}

}
}
?>

<?php
global $conection;
// Se o usuário clicou no botão cadastrar efetua as ações
if (!empty($_POST['slide']))
 {
	$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
	$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
	// Recupera os dados dos campos
	$image = $_FILES['image'];

	// Se a foto estiver sido selecionada
	if (!empty($image["name"])) {
 
		// Largura máxima em pixels
		$largura = 1000000;
		// Altura máxima em pixels
		$altura = 1000000;
		// Tamanho máximo do arquivo em bytes
		$tamanho = 500000000000;
 
    	// Verifica se o arquivo é uma imagem
    	if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
     	   $error[1] = "Isso não é uma imagem.";
   	 	} 
 
		// Pega as dimensões da imagem
		$dimensoes = getimagesize($image["tmp_name"]);
 
		// Verifica se a largura da imagem é maior que a largura permitida
		if($dimensoes[0] > $largura) {
			$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
		}
 
		// Verifica se a altura da imagem é maior que a altura permitida
		if($dimensoes[1] > $altura) {
			$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
		}
 
		// Verifica se o tamanho da imagem é maior que o tamanho permitido
		if($image["size"] > $tamanho) {
   		 	$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
		}
 
			// Pega extensão da imagem
			preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
 
        	// Gera um nome único para a imagem
        	$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
 
        	// Caminho de onde ficará a imagem
        	$caminho_imagem = "../assets/img/uploads/other/" . $nome_imagem;
 
			// Faz o upload da imagem para seu respectivo caminho
			move_uploaded_file($image["tmp_name"], $caminho_imagem);
 
			// Insere os dados no banco
			$sql = mysqli_query($conection,"INSERT INTO slide VALUES (0, '".$title."', '".$description."', '".$nome_imagem."', 'slide')");
 
			// Se os dados forem inseridos com sucesso			
			if (!$sql) {
			echo ("Can't insert into database: " . mysqli_error($conection));
			return false;
			} else {
					echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
					echo '<meta http-equiv="refresh" content="1; background.php#slide">'; 
					die();
			}

		// Se houver mensagens de erro, exibe-as
		if (count($error) != 0) {
			foreach ($error as $erro) {
				echo $erro . "<br />";
			}
		}

}
}
?>
		</div>
		<!-- /.row -->
	
    </div>
    <!-- /.container-fluid -->


</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>