<?php include 'header.php'; ?>
<?php 
error_reporting(0);
include("functions/config.php");
backup_tables($host,$username,$password,$dbname);//host-name,user-name,password,DB name

	echo "<script type='text/javascript'>swal('Good job!', 'Your Backup Was Successful!', 'success');</script>";
	echo '<meta http-equiv="refresh" content="1; settings.php">'; 
	die(); 

/* backup the db OR just a table */
function backup_tables($host,$username,$password,$dbname,$tables = '*')
{
$return = "";
// 1. Create a database connection
$conection=mysqli_connect($host,$username,$password,$dbname);
if (!$conection) {
	die("Database connection failed: " . mysqli_error());
}

// 2. Select a database to use 
$db_selected = mysqli_select_db($conection, $dbname);
if (!$db_selected) {
	die("Database selection failed: " . mysqli_error());
}
//get all of the tables
if($tables == '*')
{
$tables = array();
$result = mysqli_query($conection,'SHOW TABLES');
while($row = mysqli_fetch_row($result))
{
$tables[] = $row[0];
}
}
else
{
$tables = is_array($tables) ? $tables : explode(',',$tables);
}
//cycle through
foreach($tables as $table)
{
$result = mysqli_query($conection,'SELECT * FROM '.$table);
$num_fields = mysqli_num_fields($result);
$return.= 'DROP TABLE '.$table.';';
$row2 = mysqli_fetch_row(mysqli_query($conection,'SHOW CREATE TABLE '.$table));
$return.= "\n\n".$row2[1].";\n\n";
for ($i = 0; $i < $num_fields; $i++)
{
while($row = mysqli_fetch_row($result))
{
$return.= 'INSERT INTO '.$table.' VALUES(';
for($j=0; $j<$num_fields; $j++)
{
$row[$j] = addslashes($row[$j]);
$row[$j] = ereg_replace("\n","\\n",$row[$j]);
if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
if ($j<($num_fields-1)) { $return.= ','; }
}
$return.= ");\n";
}
}
$return.="\n\n\n";
}
//save file
$folder = 'assets/backup/';
$handle = fopen($folder . $dbname .'.sql','w+');
// echo $return;
fwrite($handle,$return);
fclose($handle);
}
?> 

<?php include 'footer.php'; ?>