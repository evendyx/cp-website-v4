<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('PORTFOLIO');?></span>
				 <a href="new_project.php" type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo lang('NEW_PROJECT');?></a>				
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li class="active"><?php echo lang('PORTFOLIO');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						<div id="portfolio" class="table-responsive">
						
							<div class="row">

								<?php echo listPortfolioBack();?>

							</div>    
						</div>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>

			<div class="col-md-4">
				
				<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('CATEGORIES');?><a class="heading-elements-toggle"><i class="icon-more"></i></a> <a href="new_category.php" type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo lang('NEW_CATEGORY');?></a></h4></h5> 
					</div>

					<div class="panel-body">
					
						<div id="categories" class="table-responsive">
							
								<div class="row">

									<?php echo listCategoriesBack();?>

								</div>    
							</div>
						
					</div>
				</div>
				<!-- /basic layout -->
				
			</div>
		</div>
		<!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>