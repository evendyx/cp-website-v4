<?php include 'header.php';

// Name of the file
$filename = 'assets/backup/josecarl_infinity.sql';
global $conection;

// 1. Create a database connection
$conection=mysqli_connect($host,$username,$password,$dbname);
if (!$conection) {
	die("Database connection failed: " . mysqli_error());
}

// 2. Select a database to use 
$db_selected = mysqli_select_db($conection, $dbname);
if (!$db_selected) {
	die("Database selection failed: " . mysqli_error());
	}
// Temporary variable, used to store current query
$templine = '';
// Read in entire file
$lines = file($filename);
// Loop through each line
foreach ($lines as $line)
{
// Skip it if it's a comment
if (substr($line, 0, 2) == '--' || $line == '')
    continue;

// Add this line to the current segment
$templine .= $line;
// If it has a semicolon at the end, it's the end of the query
if (substr(trim($line), -1, 1) == ';')
{
    // Perform the query
    mysqli_query($conection,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
    // Reset temp variable to empty
    $templine = '';
}
}
	echo "<script type='text/javascript'>swal('Good job!', 'Your Database Has been Restored Successfuly!', 'success');</script>";
	echo '<meta http-equiv="refresh" content="1; settings.php">'; 
	die(); 
?>

<?php include 'footer.php'; ?>

