<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('COUNTERS');?></span>
					 <a href="new_counter.php" type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo lang('NEW_COUNTER');?></a>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li class="active"><?php echo lang('COUNTERS');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
					
						<div id="categories" class="table-responsive">
							
								<div class="row">

									<?php echo listCountersBack();?>

								</div>    
							</div>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>
			
			<div class="col-md-4">
			<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('TOP_IMAGE');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					</div>
					
					<div class="panel-body">

						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="countersImg" >
							<fieldset>
							<?php $csrf->echoInputField(); ?>
						
							<?php 
								global $conection;
								$sql = mysqli_query($conection,"select * from counters_img");
								$row = mysqli_fetch_assoc($sql);
								
								$image = $row['image'];
								
								if ($image == ''){
									echo"
										<div class='form-group'>
											<div class='col-md-9'>
											<label class='col-md-12 control-label' for='image'>".lang('UPLOAD_IMAGE')."</label>
												<input type='file' name='image' id='image'>
											</div>
											
										</div>

										</fieldset>
										<div class='col-md-12'>
											<input type='submit' name='countersImg' class='btn btn-primary submit' value='".lang('ADD_NEW')."' />
										</div>
									";
								}
								else{
									echo"
										<div class='col-md-12'>
											<img class='img-responsive' src='../assets/img/uploads/parallax/".$row['image']."' alt='' />
										</div>
										
										<a id='del' class='btn btn-danger' href='javascript:EliminaCountersImageParallax(".$row['id'].")' title='click for delete' ><i class='fa fa-fw fa-trash'></i> ".lang('DELETE_IMAGE')."</a>
									";
								}
							?>

										
						<?php
							global $conection;
							// Se o usuário clicou no botão cadastrar efetua as ações
							if (!empty($_POST['countersImg']))
							 {
							  
								// Recupera os dados dos campos
								$image = $_FILES['image'];

								// Se a foto estiver sido selecionada
								if (!empty($image["name"])) {
							 
									// Largura máxima em pixels
									$largura = 1000000;
									// Altura máxima em pixels
									$altura = 1000000;
									// Tamanho máximo do arquivo em bytes
									$tamanho = 500000000000;
							 
									// Verifica se o arquivo é uma imagem
									if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
									   $error[1] = "Isso não é uma imagem.";
									} 
							 
									// Pega as dimensões da imagem
									$dimensoes = getimagesize($image["tmp_name"]);
							 
									// Verifica se a largura da imagem é maior que a largura permitida
									if($dimensoes[0] > $largura) {
										$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
									}
							 
									// Verifica se a altura da imagem é maior que a altura permitida
									if($dimensoes[1] > $altura) {
										$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
									}
							 
									// Verifica se o tamanho da imagem é maior que o tamanho permitido
									if($image["size"] > $tamanho) {
										$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
									}
							 
										// Pega extensão da imagem
										preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
							 
										// Gera um nome único para a imagem
										$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
							 
										// Caminho de onde ficará a imagem
										$caminho_imagem = "../assets/img/uploads/parallax/" . $nome_imagem;
							 
										// Faz o upload da imagem para seu respectivo caminho
										move_uploaded_file($image["tmp_name"], $caminho_imagem);
							 
										// Insere os dados no banco
										$sql = mysqli_query($conection,"INSERT INTO counters_img VALUES (0, '".$nome_imagem."')");
							 
										// Se os dados forem inseridos com sucesso			
										if (!$sql) {
										echo ("Can't insert into database: " . mysqli_error());
										return false;
										} else {
												echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
												echo '<meta http-equiv="refresh" content="1; counters.php">'; 
												die();
										}

									// Se houver mensagens de erro, exibe-as
									if (count($error) != 0) {
										foreach ($error as $erro) {
											echo $erro . "<br />";
										}
									}

							}
							}
						?>
					  </form>
				</div>
			</div>
		</div>

		</div>
		<!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>