<?php
	 include("functions/config.php");
	 
	 // open connection to mysql database
    $conection = mysqli_connect($host, $username, $password, $dbname) or die("Connection Error " . mysqli_error($conection));
    
    // fetch mysql table rows
    $sql = "select * from clients";
    $result = mysqli_query($conection, $sql) or die("Selection Error " . mysqli_error($conection));

    header('Content-Type: text/csv; charset=utf-8');  
	header('Content-Disposition: attachment; filename=data.csv');  
	$fp = fopen("php://output", "w"); 

    while($row = mysqli_fetch_assoc($result))
    {
        fputcsv($fp, $row);
    }
    
    fclose($fp);

    //close the db connection
    mysqli_close($conection);

?>