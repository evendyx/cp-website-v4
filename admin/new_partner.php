<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_PARTNER');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="partners.php"><?php echo lang('PARTNERS');?></a></li>
				<li class="active"><?php echo lang('NEW_PARTNER');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newPartner">
										<fieldset>
											<?php $csrf->echoInputField(); ?>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="name"><?php echo lang('NAME');?></label>
												<div class="col-md-6">
													<input id="name" name="name" type="text" class="form-control input-md" required>

												</div>
											</div>
											
											<!-- Text input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="website"><?php echo lang('WEBSITE');?></label>
												<div class="col-md-6">
													<input id="website" name="website" type="text" class="form-control input-md" required>

												</div>
											</div>
											
											<!-- Image input-->
											<div class="form-group">
												<label class="col-md-2 control-label" for="url"><?php echo lang('UPLOAD_LOGO_IMAGE');?></label>
												<div class="col-md-6">
													<input type="file" name="image" id="image">
												</div>
											</div>


											<!-- Button -->
											<div class="form-group">
												<label class="col-md-10 control-label" for="singlebutton"></label>
												<div class="col-md-2">
													<input type="submit" name="newPartner" class="btn btn-primary" value="<?php echo lang('NEW_PARTNER');?>" />
												</div>
											</div>

										</fieldset>
									</form>
									<?php

										// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
										if (!empty($_POST['newPartner']))
										 {
											global $conection;
											// Recupera os dados dos campos
											$image = $_FILES['image'];
											$name = htmlspecialchars($_POST['name'], ENT_QUOTES);
											$website = htmlspecialchars($_POST['website'], ENT_QUOTES);

											// Se a foto estiver sido selecionada
											if (!empty($image["name"])) {
										 
												// Largura m�xima em pixels
												$largura = 1000000;
												// Altura m�xima em pixels
												$altura = 1000000;
												// Tamanho m�ximo do arquivo em bytes
												$tamanho = 500000000000;
										 
												// Verifica se o arquivo � uma imagem
												if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
												   $error[1] = "Isso n�o � uma imagem.";
												} 
										 
												// Pega as dimens�es da imagem
												$dimensoes = getimagesize($image["tmp_name"]);
										 
												// Verifica se a largura da imagem � maior que a largura permitida
												if($dimensoes[0] > $largura) {
													$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
												}
										 
												// Verifica se a altura da imagem � maior que a altura permitida
												if($dimensoes[1] > $altura) {
													$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
												}
										 
												// Verifica se o tamanho da imagem � maior que o tamanho permitido
												if($image["size"] > $tamanho) {
													$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
												}
										 
													// Pega extens�o da imagem
													preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
										 
													// Gera um nome �nico para a imagem
													$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
										 
													// Caminho de onde ficar� a imagem
													$caminho_imagem = "../assets/img/uploads/partners/" . $nome_imagem;
										 
													// Faz o upload da imagem para seu respectivo caminho
													move_uploaded_file($image["tmp_name"], $caminho_imagem);
										 
													// Insere os dados no banco
													$sql = mysqli_query($conection,"INSERT INTO partners VALUES (0, '".$nome_imagem."', '".$name."', '".$website."')");
					
													// Se os dados forem inseridos com sucesso			
													if (!$sql) {
													echo ("Can't insert into database: " . mysqli_error());
													return false;
													} else {
													echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_PARTNER_CREATED')."', 'success');</script>";
															echo '<meta http-equiv="refresh" content="1; partners.php">'; 
															die();
													}		
													return true;

												// Se houver mensagens de erro, exibe-as
												if (count($error) != 0) {
													foreach ($error as $erro) {
														echo $erro . "<br />";
													}
												}

										}
										}
	
										?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>           
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>