				<!-- Page Heading -->

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-user fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo listClients(); ?></div>
                                        <div><?php echo lang('CLIENTS');?></div>
                                    </div>
                                </div>
                            </div>
                            <a href="clients.php">
                                <div class="panel-footer">
                                    <span class="pull-left"><?php echo lang('VIEW_DETAILS');?></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-info-circle fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo listServices(); ?></div>
                                        <div><?php echo lang('SERVICES');?></div>
                                    </div>
                                </div>
                            </div>
                            <a href="services.php">
                                <div class="panel-footer">
                                    <span class="pull-left"><?php echo lang('VIEW_DETAILS');?></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo listTestimonials(); ?></div>
                                        <div><?php echo lang('TESTIMONIALS');?></div>
                                    </div>
                                </div>
                            </div>
                            <a href="testimonials.php">
                                <div class="panel-footer">
                                    <span class="pull-left"><?php echo lang('VIEW_DETAILS');?></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-thumbs-up fa-4x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"><?php echo listPartners(); ?></div>
                                        <div><?php echo lang('PARTNERS');?></div>
                                    </div>
                                </div>
                            </div>
                            <a href="partners.php">
                                <div class="panel-footer">
                                    <span class="pull-left"><?php echo lang('VIEW_DETAILS');?></span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                <!-- /.row -->