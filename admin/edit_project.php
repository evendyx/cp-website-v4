<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_PROJECT');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="portfolio.php"><?php echo lang('PORTFOLIO');?></a></li>
				<li class="active"><?php echo lang('EDIT_PROJECT');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
		 
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
				
					<form id="editProject" method="post" enctype="multipart/form-data" class="form-horizontal" name="editProject">
										
						<?php $csrf->echoInputField(); ?>
						
						<fieldset>
						
						<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="category"><?php echo lang('CATEGORY');?></label>
								<div class='col-md-6'>
									<select class="form-control input-md" name="category">
									  <option value="<?php
										global $conection;
										$sql = mysqli_query($conection,"select slug from categories");
										$row = mysqli_fetch_assoc($sql);
										echo $row['slug'];
									?>">------</option>
										<?php echo listCategories(); ?>
									</select>
								</div>
							</div>
							
						<?php echo editProject($_GET['id']); ?>

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-9 control-label" for="singlebutton"></label>
							<div class="col-md-3">
								<input type="submit" name="editProject" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
							</div>
						</div>

						</fieldset>
					</form>
					<?php

					  if(isset($_POST['editProject']))
					  {
						updateProject($_GET['id'],$_POST['category'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['link'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
					  }

					?>
					
				</div>
			</div>
			<!-- /basic layout -->

		</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>