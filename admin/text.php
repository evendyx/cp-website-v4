<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('TEXT');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('BACKGROUND_SETTINGS');?></li>
				<li class="active"><?php echo lang('TEXT');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="text.php" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatetext" >
						<fieldset>
						
						<?php $csrf->echoInputField(); ?>
					
						<?php echo getTheText();?>

						</fieldset>
						
						<br/>
					  
					<!-- Button -->
					<div class="form-group">
					  <label class="col-md-10 control-label" for="singlebutton"></label>
					  <div class="col-md-2">
						<input type="submit" name="updatetext" class="btn btn-primary" value="update" />
					  </div>
					</div>
					<?php

					if(isset($_POST['updatetext']))
					{
						updateText(htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
					}

					?>
				  </form>
				</div>
			</div>
			<!-- /basic layout -->

		</div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>