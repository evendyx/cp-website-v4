<?php
    require("functions/config.php"); 
	
	// CSRF Protection
	require 'functions/CSRF_Protect.php';
	$csrf = new CSRF_Protect();
	
	// Error Reporting Active
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
		
    $submitted_username = ''; 
    if(!empty($_POST)){ 
        $query = " 
            SELECT 
                id, 
                username, 
                password, 
                salt, 
                email,
				lang
            FROM users 
            WHERE 
                username = :username 
        "; 
        $query_params = array( 
            ':username' => $_POST['username'] 
        ); 
         
        try{ 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage()); } 
		
		$login_ok = false; 
        $row = $stmt->fetch(); 
        if($row){ 
            $check_password = hash('sha256', $_POST['password'] . $row['salt']); 
            for($round = 0; $round < 65536; $round++){
                $check_password = hash('sha256', $check_password . $row['salt']);
            } 
            if($check_password === $row['password']){
                $login_ok = true;
            } 
        } 

        if($login_ok){ 
            unset($row['salt']); 
            unset($row['password']); 
            $_SESSION['user'] = $row;  
            header("Location: dashboard.php"); 
            die("Redirecting to: dashboard.php"); 			
		} 	
	}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>ADMIN - Login</title>
    <meta name="description" content="">
    <meta name="author" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="assets/images/favico.ico" type="image/x-icon" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="assets/js/sweetalert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
	<link href="assets/css/styles.css" rel="stylesheet" media="screen">
</head>

<body id="bg">

<div class="container hero-unit form-signin">
    <p><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Log in into your account</p>
     <form action="index.php" method="post"> 
	 <?php $csrf->echoInputField(); ?>
		<input type="text" name="username" class="form-control" placeholder="Username" required value="<?php echo $submitted_username; ?>" /> 
		<input type="password" name="password" class="form-control" placeholder="Password" required value="" />
		<br/>		
		<input type="submit" class="btn btn-info btn-flat" value="Login" /> 		
	</form>
		<?php
			global $conection;
			$sql = mysqli_query($conection,"select * from users");
			$row = mysqli_fetch_assoc($sql);
			
			$id = $row['id'];
			
			if($id == ""){
				echo "<span><a href='register.php'>Create an account</a> &bull;";
			}
			if($id != ""){
				echo "";
			}
		?>
	 <a href="forgot.php">Forgot password</a></span>
</div>

<?php


	if(!empty($_POST)){ 
	
		$submitted_username = $_POST['username'];
		if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $submitted_username))
		{
			echo "<script type='text/javascript'>sweetAlert('REALLY?!', 'Using special characters?!', 'error');</script>";	
			echo '<META HTTP-EQUIV=REFRESH CONTENT="100"; '.$url.'">'; 
			die; 
		}
		if(!$login_ok){ 
			echo "<script type='text/javascript'>swal('Oops...', 'Something went wrong!', 'error');</script>";
			echo '<META HTTP-EQUIV=REFRESH CONTENT="100"; index.php">'; 
			die;
					
		} 
			
	}
?>
</body>

</html>