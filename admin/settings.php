<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('SETTINGS');?></span> 
					<a href="backup.php">
						<button type="button" class="btn btn-success"><i class="fa fa-cloud-download"></i> <?php echo lang('BACKUP');?></button>
					</a>
					<a href="restore.php">
						<button type="button" class="btn btn-info"><i class="fa fa-cloud-upload"></i> <?php echo lang('RESTORE');?></button>
					</a>
				</h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li class="active"><?php echo lang('SETTINGS');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
		
		<div class="row">
		
			<div class="col-md-8">
				<!-- Basic layout-->
				<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php echo lang('SETTINGS');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						</div>

					<div class="panel-body">
					
						<div class="table-responsive">
						
							<div class="row">
							
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwelve">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
										  <?php echo lang('LOADERS_STYLE');?>
										</a>
									  </h6>
									</div>
									<div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateLoaderType" >
											<?php $csrf->echoInputField(); ?>
												<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">
											
											<?php echo activeLoaderType(); ?>

												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateLoaderType" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateLoaderType']))
											{
												updateLoaderType($_POST['type']);
											}
										?>
									  </div>
									</div>
								</div>
								
								  <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
									  <h6 class="panel-title">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										  <?php echo lang('LOGO_OPTIONS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="logo" >
											<br/>
											<div class="row">

											<fieldset>
											<?php $csrf->echoInputField(); ?>
												<?php 
													global $conection;
													$sql = mysqli_query($conection,"select * from logo");
													$row = mysqli_fetch_assoc($sql);
													
													$image = $row['image'];
													
													if ($image == ''){
														echo"
															<div class='form-group'>
																<div class='col-md-9'>
																<label class='col-md-3 control-label' for='logo'>".lang('UPLOAD_IMAGE')."</label>
																	<input type='file' name='image' id='image' data-toggle='tooltip' data-placement='right' title='Only one image will be uploaded. In order to add a new one, you`ll need to delete the existing one.'>
																</div>
																
															</div>

															<div class='col-md-3'>
																<input type='submit' name='logo' class='btn btn-primary submit' value='".lang('ADD_NEW')."' />
															</div>
														";
													}
													else{
														echo"
															<div class='col-md-5'>
																<img class='img-responsive' src='../assets/img/uploads/other/".$row['image']."' alt='' />
															</div>
															
															<a id='del' class='btn btn-danger' href='javascript:EliminaLogoImage(".$row['id'].")' title='click for delete' ><i class='fa fa-fw fa-trash'></i> ".lang('DELETE_IMAGE')."</a>
														";
													}
												?>
												

											</fieldset>
										</div>
									</form>
									<?php
										global $conection;
										// Se o usuário clicou no botão cadastrar efetua as ações
										if (!empty($_POST['logo']))
										 {
										  
											// Recupera os dados dos campos
											$image = $_FILES['image'];

											// Se a foto estiver sido selecionada
											if (!empty($image["name"])) {
										 
												// Largura máxima em pixels
												$largura = 1000000;
												// Altura máxima em pixels
												$altura = 1000000;
												// Tamanho máximo do arquivo em bytes
												$tamanho = 500000000000;
										 
												// Verifica se o arquivo é uma imagem
												if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
												   $error[1] = "".lang('NOT_A_IMAGE')."";
												} 
										 
												// Pega as dimensões da imagem
												$dimensoes = getimagesize($image["tmp_name"]);
										 
												// Verifica se a largura da imagem é maior que a largura permitida
												if($dimensoes[0] > $largura) {
													$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
												}
										 
												// Verifica se a altura da imagem é maior que a altura permitida
												if($dimensoes[1] > $altura) {
													$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
												}
										 
												// Verifica se o tamanho da imagem é maior que o tamanho permitido
												if($image["size"] > $tamanho) {
													$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
												}
										 
													// Pega extensão da imagem
													preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
										 
													// Gera um nome único para a imagem
													$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
										 
													// Caminho de onde ficará a imagem
													$caminho_imagem = "../assets/img/uploads/other/" . $nome_imagem;
										 
													// Faz o upload da imagem para seu respectivo caminho
													move_uploaded_file($image["tmp_name"], $caminho_imagem);
										 
													// Insere os dados no banco
													$sql = mysqli_query($conection,"INSERT INTO logo VALUES (0, 'logo', '".$nome_imagem."')");
										 
													// Se os dados forem inseridos com sucesso			
													if (!$sql) {
															echo "<script type='text/javascript'>swal('".lang('ERROR')."', '".lang('IMPOSSIBLE')."', 'error');</script>";
															echo '<meta http-equiv="refresh" content="3; settings.php">'; 
															die();
													return false;
													} else {
															echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
															echo '<meta http-equiv="refresh" content="1; settings.php">'; 
															die();
													}

												// Se houver mensagens de erro, exibe-as
												if (count($error) != 0) {
													foreach ($error as $erro) {
														echo $erro . "<br />";
													}
												}

										}
										}
									?>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTwo">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										 <?php echo lang('MENU_SETTINGS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateMenuType" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12 options_menu">											
											
											<?php echo activeMenuType(); ?>
																												
												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateMenuType" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateMenuType']))
											{
												updateMenuType($_POST['type']);
											}
										?>
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingThree">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										 <?php echo lang('MENU_ORDER');?>
										</a>
									  </h6>
									</div>
									<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="logo" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
												<div class="panel-body">
												<ul id="sortable">
													<?php $data=get_records(); ?> 
													<?php foreach($data as $record): ?>
														<li data-id="<?php echo $record['id'];  ?>" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $record['title']; ?></li>
													<?php endforeach; ?>
												</ul>
												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input id="save-reorder" type="submit" class="btn btn-primary" value="update" />
												  </div>
												</div>
													<script>
														$(document).ready(function(){
															$(document).on('click','#save-reorder',function(){
																var list = new Array();
																$('#sortable').find('.ui-state-default').each(function(){
																	 var id=$(this).attr('data-id');    
																	 list.push(id);
																});
																var data=JSON.stringify(list);
																$.ajax({
																url: 'save.php', // server url
																type: 'POST', //POST or GET 
																data: {token:'reorder',data:data}, // data to send in ajax format or querystring format
																datatype: 'json',
																success: function () {
																	swal("<?php echo lang('NICE');?>", "<?php echo lang('MENU_ORDER_UPDATED');?>", "success");
																	window.setTimeout(function(){ 
																		location.reload();
																	} ,2000);
																},
														  
															});
															});
															 
														});
													</script>
											</div>		
										</div>
										</form>
									  </div>
									</div>
								  </div>
								  
								  <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingTen">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
										 <?php echo lang('GDPR_SETTINGS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateGDPR" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">											
											
											<?php echo activeGDPR(); ?>
																												
												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateGDPR" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateGDPR']))
											{
												updateGDPR($_POST['type']);
											}
										?>
									  </div>
									</div>
								  </div>
								  
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingNine">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
										  <?php echo lang('PRICING_TABLES');?>
										</a>
									  </h6>
									</div>
									<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatePricingType" >
											<?php $csrf->echoInputField(); ?>
												<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">
											
											<?php echo activePricingType(); ?>

												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updatePricingType" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updatePricingType']))
											{
												updatePricingType($_POST['type']);
											}
										?>
									  </div>
									</div>
								</div>
									
									
								   <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFour">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
										  <?php echo lang('CONTACT_SETTINGS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateContactType" >
											<?php $csrf->echoInputField(); ?>
												<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">
												
												<?php echo activeContactType(); ?>

												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateContactType" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateContactType']))
											{
												updateContactType($_POST['type']);
											}
										?>
									  </div>
									</div>
								  </div>
								  
								  <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingEleven">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
										  <?php echo lang('BLOG_SETTINGS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateBlogType" >
											<?php $csrf->echoInputField(); ?>
												<br/>
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">
											
											<?php echo activeBlogType(); ?>
											
																	
								<script>
								
									$('.switch').change(function(){
										var x = document.getElementById('pagination');
										var y = document.getElementById('nopagination');
										if(this.checked) {
											 
											if (x.style.display === 'none') {
												x.style.display = 'block';
												y.style.display = 'none';
											} else {
												x.style.display = 'none';
												y.style.display = 'block';
											}
										}
										else {
											if (x.style.display === 'none') {
												x.style.display = 'block';
												y.style.display = 'none';
											} else {
												x.style.display = 'none';
												y.style.display = 'block';
											}
										}
									});
								</script>
							
								<div class="row">								
									
									<?php 
										global $conection;
										$sql = mysqli_query($conection,"select type from blog_type");
										$row = mysqli_fetch_assoc($sql);
										$type = $row['type'];
										
										if($type == "No Pagination")
										{
									?>
											<div id="nopagination"></div>
									<?php
										}
										if($type == "Pagination")
										{
											
									?>
											<div id="pagination" >
												<div class="row"></div>
													<div class="col-md-6"></div>
													
													<?php
														global $conection;
														$sql = mysqli_query($conection,"select number from blog_type");
														$row = mysqli_fetch_assoc($sql);
														echo
														"
														<div class='col-md-3'>
															".lang('CURRENT_BLOG_POSTS')." <br/> <input type='text' class='col-md-12' name='number' value='".$row['number']." ".lang('POSTS')."' disabled />
														</div>
														";
													?>

														<div class='col-md-3'>
															<?php echo lang('SELECT_POSTS_TOTAL');?><br/>
															<select class='col-md-12' name="number">
															  <option value="<?php
																global $conection;
																$sql = mysqli_query($conection,"select number from blog_type");
																$row = mysqli_fetch_assoc($sql);
																echo $row['number'];
															?>">------</option>
															  <option value="1">1 <?php echo lang('POSTS');?></option>
															  <option value="2">2 <?php echo lang('POSTS');?></option>
															  <option value="3">3 <?php echo lang('POSTS');?></option>
															  <option value="4">4 <?php echo lang('POSTS');?></option>
															  <option value="5">5 <?php echo lang('POSTS');?></option>
															  <option value="6">6 <?php echo lang('POSTS');?></option>
															  <option value="7">7 <?php echo lang('POSTS');?></option>
															  <option value="8">8 <?php echo lang('POSTS');?></option>
															</select>
														</div>
											</div>
											<?php
													
												}
											
											?>

										</div> 
											

												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-9 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateBlogType" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateBlogType']))
											{
												updateBlogType($_POST['type'],$_POST['number']);
											}
										?>
									  </div>
									</div>
								  </div>
								  
								   <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingFive">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
										  <?php echo lang('COUNTER_OPTIONS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateCounterSetting" >
											<?php $csrf->echoInputField(); ?>							
											
											<div class="row">
																		
											<fieldset>

											<div class="col-md-12">
											<?php
												global $conection;
												$sql = mysqli_query($conection,"select number from counter");
												$row = mysqli_fetch_assoc($sql);
												echo
												"
												<div class='col-md-12'>
													".lang('CURRENT_COUNTERS')." <br/> <input type='text' class='col-md-8' name='number' value='".$row['number']." ".lang('COUNTERS')."' disabled />
												</div>
												";
											?>

												<div class='col-md-12'>
													<?php echo lang('SELECT_COUNTER_TOTAL');?><br/>
													<select class='col-md-8' name="number">
													  <option value="<?php
														global $conection;
														$sql = mysqli_query($conection,"select number from counter");
														$row = mysqli_fetch_assoc($sql);
														echo $row['number'];
													?>">------</option>
													  <option value="2">2 <?php echo lang('COUNTERS');?></option>
													  <option value="3">3 <?php echo lang('COUNTERS');?></option>
													  <option value="4">4 <?php echo lang('COUNTERS');?></option>
													  <option value="6">6 <?php echo lang('COUNTERS');?></option>
													</select>
												</div>
												
												
												
												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-8 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateCounterSetting" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
											</div>

											</fieldset>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updateCounterSetting']))
											{
												updateCounterSetting($_POST['number']);
											}
										?>
									  </div>
									</div>
								  </div>
								  </div>
								   <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingSix">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
										  <?php echo lang('THEME_COLOR_OPTIONS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatedashboard" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
																		
											<fieldset>

											 <div class='form-group'>
												<label class='col-md-8 control-label' for='font'><?php echo lang('GOOGLE_FONT');?></label>
													<div class='col-md-4'>
													  <div id="font"  data-toggle="tooltip" data-placement="left" title="Use only Google Fonts. Just copy, and paste only the name, for ex: 'Quicksand'" class="input-group">
														<input type="text" name="font" value="<?php echo getFont(); ?>" class="form-control" />
														
													</div>
												</div>
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('LOADER_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp1" class="input-group colorpicker-component">
														<input type="text" name="lcolor" value="<?php echo getlColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('MENU_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp2" class="input-group colorpicker-component">
														<input type="text" name="menu_color" value="<?php echo getMenu_Color(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('MENU_BACKGROUND_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp3" class="input-group colorpicker-component">
														<input type="text" name="menu_bcolor" value="<?php echo getMenu_BColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('HOVER_MENU_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp4" class="input-group colorpicker-component">
														<input type="text" name="menu_colorhover" value="<?php echo getMenu_ColorHover(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('HOVER_MENU_BACKGROUND_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp5" class="input-group colorpicker-component">
														<input type="text" name="menu_bcolorhover" value="<?php echo getMenu_BColorHover(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('PRIMARY_SECTIONS_BACKGROUND_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp6" class="input-group colorpicker-component">
														<input type="text" name="background_color" value="<?php echo getBackgroundColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('PRIMARY_TEXT_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp7" class="input-group colorpicker-component">
														<input type="text" name="background_tcolor" value="<?php echo getBackgroundTColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('BACKGROUND_MENU_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp8" class="input-group colorpicker-component">
														<input type="text" name="background_menu_color" value="<?php echo getBackgroundMenuColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('ITEMS_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp9" class="input-group colorpicker-component">
														<input type="text" name="items_color" value="<?php echo getItemsColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
												
												<label class='col-md-8 control-label' for='wcolor'><?php echo lang('FOOTER_COLOR');?></label>
													<div class='col-md-4'>
													  <div id="cp10" class="input-group colorpicker-component">
														<input type="text" name="footer_color" value="<?php echo getFooterColor(); ?>" class="form-control" />
														<span class="input-group-addon"><i></i></span>
													</div>
												</div>
											  
											</div>
											<!-- Button -->
												<div class="form-group">
												  <label class="col-md-10 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updatedashboard" class="btn btn-primary submit" value="update" />
												  </div>
												</div>
												<script>
													$(function() {
														$('.colorpicker-component').colorpicker();
													});													
												</script>
												

											</fieldset>
											</div>
											
										</form>
										 <?php
													  
										  if(isset($_POST['updatedashboard']))
											{
												updateDashboard($_POST['font'],$_POST['lcolor'],$_POST['menu_color'],$_POST['menu_bcolor'],$_POST['menu_colorhover'],$_POST['menu_bcolorhover'],$_POST['background_color'],$_POST['background_tcolor'],$_POST['background_menu_color'],$_POST['items_color'],$_POST['footer_color']);
											}
										?>
									  </div>
									</div>
								  </div>
								   <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingSeven">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
										 <?php echo lang('SEO');?>
										</a>
									  </h6>
									</div>
									<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateSeo" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
												<fieldset>									
											
												<?php echo seo();?>

												</fieldset>
												
											  
												<!-- Button -->
												<div class="form-group">
												  <label class="col-md-10 control-label" for="singlebutton"></label>
												  <div class="col-md-2">
													<input type="submit" name="updateSeo" class="btn btn-primary" value="Update" />
												  </div>
												</div>
												
												 <?php
											  
												  if(isset($_POST['updateSeo']))
													{
														updateSeo(htmlspecialchars($_POST['keywords'], ENT_QUOTES),htmlspecialchars($_POST['title'], ENT_QUOTES), htmlspecialchars($_POST['description'], ENT_QUOTES));
													}
												?>
												
												</div>

										</form>
									  </div>
									</div>
								  </div>
								   <div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingEight">
									  <h6 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
										 <?php echo lang('CUSTOM_CSS');?>
										</a>
									  </h6>
									</div>
									<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
									  <div class="panel-body">
										<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateCss" >
											<?php $csrf->echoInputField(); ?>
											<br/>
											<div class="row">
												<fieldset>
											
												<?php echo getCss();?>

												</fieldset>
											
											  
											 <!-- Button -->
											<div class="form-group">
											  <label class="col-md-10 control-label" for="singlebutton"></label>
											  <div class="col-md-2">
												<input type="submit" name="updateCss" class="btn btn-primary" value="update" />
											  </div>
											</div>
											<?php

											if(isset($_POST['updateCss']))
											{
												updateCss($_POST['description']);
											}

											?>
											</div>
										</form>
									  </div>
									</div>
								  </div>
								</div>
								</div>

							</div>  						
					</div>
				</div>
				<!-- /basic layout -->

			</div>
			
			<div class="col-md-4">
				<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php echo lang('PAGES_TO_DISPLAY');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						</div>
						<div class="panel-body">
					
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateActive" >	
							<?php $csrf->echoInputField(); ?>
							<br/>
								<div class="row">				
							<fieldset>

							<div class="col-md-12">
							
							<?php echo activeSettings(); ?>
								
								<!-- Button -->
								<div class="form-group">
								  <label class="col-md-8 control-label" for="singlebutton"></label>
								  <div class="col-md-2">
									<input type="submit" name="updateActive" class="btn btn-primary submit" value="update" />
								  </div>
								</div>
							</div>

							</fieldset>
							
							</div>
							
						</form>
						 <?php
									  
						  if(isset($_POST['updateActive']))
							{
								updateSettings($_POST['about'],$_POST['contacts'],$_POST['services'],$_POST['partners'],$_POST['portfolio'],$_POST['blog'],$_POST['custom'],$_POST['privacy'],$_POST['calendar']);
							}
						?>
					
					</div>
				
				</div>
				
			</div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>