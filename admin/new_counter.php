<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_COUNTER');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="counters.php"><?php echo lang('COUNTERS');?></a></li>
				<li class="active"><?php echo lang('NEW_COUNTER');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newcounter">
						<fieldset>
							<?php $csrf->echoInputField(); ?>
							
							<div class="form-group">
								<label class='col-md-2 control-label' for='color'><?php echo lang('COUNTER_COLOR');?></label>
									<div class='col-md-2'>
									  <div id="cp1" class="input-group colorpicker-component">
										<input type="text" name="color" value="#000" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="icon">Icon</label>
								<div class="col-md-6">
									
									<?php
										$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"\\\\(.+)";\s+}/';
										$subject =  file_get_contents('assets/css/font-awesome.css');
										preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

										echo'<select name="icon" id="icon" class="fa-select col-md-6 control-label">
												<option selected="selected">'.lang('CHOOSE_ONE').'</option>';
										foreach ($matches as $match) {
										  echo '<option value="'.$match[1].'">'."&#x".$match[2].' '.$match[1].'</option>';
											}
										echo'</select> ';

										?>

								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="title"><?php echo lang('TITLE');?></label>
								<div class="col-md-3">
									<input id="title" name="title" type="text" class="form-control input-md" required>

								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-2 control-label" for="value"><?php echo lang('VALUE');?></label>
								<div class="col-md-3">
									<input id="value" name="value" type="text" class="form-control input-md" required>

								</div>
							</div>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-10 control-label" for="singlebutton"></label>
								<div class="col-md-2">
									<input type="submit" name="newcounter" class="btn btn-primary" value="<?php echo lang('NEW_COUNTER');?>" />
								</div>
							</div>
							
							<script>
								$(function() {
									$('#cp1').colorpicker();
								});
							</script>

						</fieldset>
					</form>
					<?php		
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['newcounter']))
						 {
							global $conection;
							// Recupera os dados dos campos

							$color = $_POST['color'];
							$icon = $_POST['icon'];
							$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
							$value = htmlspecialchars($_POST['value'], ENT_QUOTES);
						  
							// Insere os dados no banco
							$sql = mysqli_query($conection,"INSERT INTO counters VALUES (0, '".$color."','".$title."','".$value."','".$icon."')");
				 
							// Se os dados forem inseridos com sucesso			
							if (!$sql) {
							echo ("Can't insert into database: " . mysqli_error());
							return false;
							} else {
							echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_COUNTER_CREATED')."', 'success');</script>";
									echo '<meta http-equiv="refresh" content="1; counters.php">'; 
									die();
							}		
							return true;

						}
							
						?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>
		
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>