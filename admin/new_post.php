<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_POST');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="blog.php"><?php echo lang('BLOG_POSTS');?></a></li>
				<li class="active"><?php echo lang('NEW_POST');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-8">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newPost">
						<fieldset>
							<?php $csrf->echoInputField(); ?>
							
							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-12">
									<input id="title" name="title" type="text" class="form-control input-md" placeholder="<?php echo lang('TITLE');?>" required>

								</div>
							</div>
							
							<div class="form-group">
								<div class="input-group date form_date col-md-4" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="date" data-link-format="yyyy-mm-dd hh:ii">
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
								</div>
								<input id="date" name="date" type="hidden" value="" required>
								<div class="form-group">
									<!-- Image input-->
									<div class="col-md-8">
										<input type="file" name="image" id="image">
									</div>
								</div>

							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-12">
									 <textarea rows ='14' cols='10' id='summernote' name='description' type='text' class='form-control input-md'></textarea>

								</div>
							</div>
							
							<!-- Button -->
							<div class="form-group">
								<label class="col-md-10 control-label" for="singlebutton"></label>
								<div class="col-md-2">
									<input type="submit" name="newPost" class="btn btn-primary" value="<?php echo lang('NEW_POST');?>" />
								</div>
							</div>

						
				</div>
			</div>
			<!-- /basic layout -->

		</div>

			<div class="col-md-4">
				
			</div>
			
			</fieldset>
		</form>

		
        </div>
        <!-- /.row -->
		
		<?php

			// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
			if (!empty($_POST['newPost']))
			 {
				global $conection;
				// Recupera os dados dos campos
				$date = $_POST['date'];
				$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
				$slug = str_replace(' ', '-', Slug($_POST['title']));
				$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
				$image = $_FILES['image'];				

				// Se a foto estiver sido selecionada
				if (!empty($image["name"])) {
			 
					// Largura m�xima em pixels
					$largura = 1000000;
					// Altura m�xima em pixels
					$altura = 1000000;
					// Tamanho m�ximo do arquivo em bytes
					$tamanho = 500000000000;
			 
					// Verifica se o arquivo � uma imagem
					if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
					   $error[1] = "Isso n�o � uma imagem.";
					} 
			 
					// Pega as dimens�es da imagem
					$dimensoes = getimagesize($image["tmp_name"]);
			 
					// Verifica se a largura da imagem � maior que a largura permitida
					if($dimensoes[0] > $largura) {
						$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
					}
			 
					// Verifica se a altura da imagem � maior que a altura permitida
					if($dimensoes[1] > $altura) {
						$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
					}
			 
					// Verifica se o tamanho da imagem � maior que o tamanho permitido
					if($image["size"] > $tamanho) {
						$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
					}
			 
						// Pega extens�o da imagem
						preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
			 
						// Gera um nome �nico para a imagem
						$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
			 
						// Caminho de onde ficar� a imagem
						$caminho_imagem = "../assets/img/uploads/other/" . $nome_imagem;
			 
						// Faz o upload da imagem para seu respectivo caminho
						move_uploaded_file($image["tmp_name"], $caminho_imagem);
			 
						// Insere os dados no banco
						$sql = mysqli_query($conection,"INSERT INTO post VALUES (0, '".$date."', '".$title."', '".$slug."', '".$description."', '".$nome_imagem."')");

						// Se os dados forem inseridos com sucesso			
						if (!$sql) {
						echo ("Can't insert into database: " . mysqli_error());
						return false;
						} else {
						echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_POST_CREATED')."', 'success');</script>";
								echo '<meta http-equiv="refresh" content="1; blog.php">'; 
								die();
						}		
						return true;

					// Se houver mensagens de erro, exibe-as
					if (count($error) != 0) {
						foreach ($error as $erro) {
							echo $erro . "<br />";
						}
					}

			}
			}

			?>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>