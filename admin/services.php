<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('SERVICES');?></span>
				<a href="new_service.php" type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo lang('NEW_SERVICE');?></a></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li class="active"><?php echo lang('SERVICES');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						<div class="table-responsive">
							
								<div class="form-group">
									<label class="col-md-2 control-label" ><?php echo lang('ENABLE_IMAGE_LIST');?></label>
									<div class="col-md-6">
										<!-- Rounded switch -->
										<label class="switch" data-toggle="tooltip" data-placement="bottom" title="Enable Services List">
										  <input type="checkbox">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
							
								<script>
								
									$('.switch').change(function(){
										var x = document.getElementById('service_image');
										var y = document.getElementById('service_logo');
										if(this.checked) {
											 
											if (x.style.display === 'none') {
												x.style.display = 'block';
												y.style.display = 'none';
											} else {
												x.style.display = 'none';
												y.style.display = 'block';
											}
										}
										else {
											if (x.style.display === 'none') {
												x.style.display = 'block';
												y.style.display = 'none';
											} else {
												x.style.display = 'none';
												y.style.display = 'block';
											}
										}
									});
								</script>
							
								<div class="row">
									
									<div id="service_logo">
										<?php echo listServicesBack();?>
									</div>
									<div id="service_image" style="display:none;">
										<?php echo listServicesImagesBack();?>
									</div>

								</div>    
							</div>
					</div>
				</div>
				<!-- /basic layout -->

			</div>

			<div class="col-md-4">

			</div>
		</div>
		<!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>