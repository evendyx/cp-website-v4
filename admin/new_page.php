<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('NEW_CUSTOM_PAGE');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="custom.php"><?php echo lang('CUSTOM_PAGES');?></a></li>
				<li class="active"><?php echo lang('NEW_CUSTOM_PAGE');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-8">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="newpage">
						<fieldset>
							<?php $csrf->echoInputField(); ?>

							
							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-12">
									<input id="title" name="title" type="text" class="form-control input-md" placeholder="<?php echo lang('TITLE');?>"required>
								</div>
							</div>
							
							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-12">
									  <textarea rows ='14' cols='10' id='summernote' name='content' type='text' class='form-control input-md'></textarea>
								</div>
							</div>

				</div> 
				
				
				<!-- Button -->
				<div class="form-group">
					<label class="col-md-10 control-label" for="singlebutton"></label>
					<div class="col-md-2">
						<input type="submit" name="newpage" class="btn btn-primary" value="<?php echo lang('NEW_PAGE');?>" />
					</div>
				</div>
				<br/><br/>	
							
				</div>
			</div>
			<!-- /basic layout -->
			<div class="col-md-4">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('PARALLAX');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
						
							
								<div class="col-md-12">

									<!-- Trigger the modal with a button -->
									<button type='button' class='btn btn-info' data-toggle='modal' data-target='#myModal'><?php echo lang('SELECT_IMAGE');?></button>

									
								<?php 
									global $conection;
									$sql = mysqli_query($conection,"select * from media");
									$row = mysqli_num_rows($sql);
									
									$image = $row['image'];
									
									echo "
										<!-- Modal -->
										<div id='myModal' class='modal fade' role='dialog'>
										  <div class='modal-dialog'>

											<!-- Modal content-->
											<div class='modal-content'>
											  <div class='modal-header'>
												<button type='button' class='close' data-dismiss='modal'>&times;</button>
												<h4 class='modal-title'>".lang('MEDIA')."</h4>
											  </div>
											  <div class='modal-body'>
												<div class='options'>";
									
									  while ($row = mysqli_fetch_array($sql)) {
										  echo "
												<label title='Media Image'>
													<input type='radio' name='image' value='".$row['image']."'/>
													<img src='../assets/img/uploads/other/".$row['image']."'/>
												</label>
											";
									  }
									echo " 
												</div>
												</div>
											  <div class='modal-footer'>
												<button type='button' class='btn btn-primary' data-dismiss='modal'>".lang('SAVE_SELECTION')."</button>
											  </div>
											</div>

										  </div>
										</div>";
												 

								?>
								 
								<?php

									// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
									if (!empty($_POST['newpage']))
									 {
										global $conection;															
										// Recupera os dados dos campos											
										$title = htmlspecialchars($_POST['title'], ENT_QUOTES);
										$slug = str_replace(' ', '-', str_without_accents($_POST['title']));
										$content = htmlspecialchars($_POST['content'], ENT_QUOTES);
										$image = '';
										$display_order = 0;

										if (!empty($_POST['image']))
										{
											$image = $_POST['image'];
											// Insere os dados no banco
											$sql = mysqli_query($conection,"INSERT INTO custom VALUES (0, '".$title."','".$slug."','".$content."','".$image."','".$display_order."')");	
										}	
										if (empty($_POST['image']))
										{
											// Insere os dados no banco
											$sql = mysqli_query($conection,"INSERT INTO custom VALUES (0, '".$title."','".$slug."','".$content."','','".$display_order."')");	
										}
										
										// Se os dados forem inseridos com sucesso			
										if (!$sql) {
										echo ("Can't insert into database: " . mysqli_error());
										return false;
										} else {
											echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_PAGE_CREATED')."', 'success');</script>";
											echo '<meta http-equiv="refresh" content="1; custom.php">'; 
											die();
										}		
										return true;
									}

								?> 

							
							</div>
						</div>
					</div>
				</div>

		</form>	

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>