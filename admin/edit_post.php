<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_POST');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="blog.php"><?php echo lang('BLOG');?></a></li>
				<li class="active"><?php echo lang('EDIT_POST');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
					
						<form id="editPost" method="post" enctype="multipart/form-data" class="form-horizontal" name="editPost">
										
							<?php $csrf->echoInputField(); ?>
							
							<fieldset>
								
							<?php echo editPost($_GET['id']); ?>

							<!-- Button -->
							<div class="form-group">
								<label class="col-md-9 control-label" for="singlebutton"></label>
								<div class="col-md-3">
									<input type="submit" name="editPost" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
								</div>
							</div>

							</fieldset>
						</form>
						<?php

						  if(isset($_POST['editPost']))
						  {
							updatePost($_GET['id'],$_POST['date'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
						  }

						?>
						
					</div>
				</div>
				<!-- /basic layout -->

			</div>
			
			<div class="col-md-8">
			
			</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<script>
	$(document).ready(function() {
		$('#summernote').summernote({
		  height: 300,                 // set editor height
		  minHeight: null,             // set minimum height of editor
		  maxHeight: null,             // set maximum height of editor
		  focus: true,                // set focus to editable area after initializing summernote
		  toolbar: [
			// [groupName, [list of button]]
			['codeview', ['codeview']],
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['link', ['link']],
			['table', ['table']],
			['hr', ['hr']],					
		  ]
		});
	});
</script>

<?php include 'footer.php'; ?>