<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EVENTS');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('PAGES');?></li>
				<li class="active"><?php echo lang('EVENTS');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">						
						 <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#event">
						 <i class="fa fa-calendar-plus-o" aria-hidden="true"></i> <?php echo lang('NEW_EVENT');?>
						</button>
                        <!-- Modal -->
                        <div class="modal fade" id="event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo lang('CLOSE');?>"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?php echo lang('NEW_EVENT');?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">

                                            <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="novoevento">
                                                <fieldset>
													<?php $csrf->echoInputField(); ?>
                                                    <!-- Text input-->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="title"><?php echo lang('TITLE');?></label>
                                                        <div class="col-md-6">
                                                            <select name='title' class="form-control input-md">
																<option value=" "><?php echo lang('SELECT_TYPE');?></option>
																<?php 
																global $conection;
																$query = mysqli_query($conection,"select * from type ORDER BY id DESC");
																

																while ($row = mysqli_fetch_assoc($query))
																  {
																	  
																	echo "
																	
																	<option value='".$row['title']."'>".$row['title']."</option>
																	
																	";
																						
																  }
															
																?>
															</select>
                                                        </div>
                                                    </div>

                                                    <!-- Text input-->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="url"><?php echo lang('LINK');?></label>
                                                        <div class="col-md-9">
                                                            <input id="url" name="url" type="text" class="form-control input-md" required>

                                                        </div>
                                                    </div>

                                                    <!-- Text input-->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="description"><?php echo lang('DESCRIPTION');?></label>
                                                        <div class="col-md-9">
                                                            <textarea class="form-control" rows="5" name="description" id="description"></textarea>
                                                        </div>
                                                    </div>

													<div class="form-group">
														<label class="col-md-3 control-label" for="start"><?php echo lang('START_DATE');?></label>
														<div class="input-group date form_date col-md-6" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="start" data-link-format="yyyy-mm-dd hh:ii">
															<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
														</div>
														<input id="start" name="start" type="hidden" value="" required>

													</div>

													<div class="form-group">
														<label class="col-md-3 control-label" for="end"><?php echo lang('END_DATE');?></label>
														<div class="input-group date form_date col-md-6" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="end" data-link-format="yyyy-mm-dd hh:ii">
															<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
														</div>
														<input id="end" name="end" type="hidden" value="" required>

													</div>

                                                    <!-- Button -->
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label" for="singlebutton"></label>
                                                        <div class="col-md-4">
                                                            <input type="submit" name="novoevento" class="btn btn-success" value="<?php echo lang('NEW_EVENT');?>" />
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#type">
							<i class="fa fa-globe" aria-hidden="true"></i> <?php echo lang('NEW_TYPE');?>
						</button>
                        <!-- Modal -->
                        <div class="modal fade" id="type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo lang('CLOSE');?>"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?php echo lang('NEW_TYPE');?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">

                                            <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="novotipo">
                                                <fieldset>
													<?php $csrf->echoInputField(); ?>
                                                    <!-- Text input-->
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="title"><?php echo lang('TITLE');?></label>
                                                        <div class="col-md-9">
                                                            <input id="title" name="title" type="text" class="form-control input-md" required>

                                                        </div>
                                                    </div>

                                                    <!-- Button -->
                                                    <div class="form-group">
                                                        <label class="col-md-12 control-label" for="singlebutton"></label>
                                                        <div class="col-md-4">
                                                            <input type="submit" name="novotipo" class="btn btn-success" value="<?php echo lang('NEW_TYPE');?>" />
                                                        </div>
                                                    </div>

                                                </fieldset>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#delevent">
							<i class="fa fa-close" aria-hidden="true"></i> <?php echo lang('DELETE_EVENT');?>
						</button>

                        <!-- Modal -->
                        <div class="modal fade" id="delevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo lang('CLOSE');?>"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?php echo lang('DELETE_EVENT');?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">

                                            <?php echo listAllEvents(); ?>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deltype">
							<i class="fa fa-close" aria-hidden="true"></i> <?php echo lang('DELETE_TYPE');?>
						</button>

                        <!-- Modal -->
                        <div class="modal fade" id="deltype" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo lang('CLOSE');?>"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel3"> <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <?php echo lang('DELETE_TYPE');?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">

                                            <?php echo listAllTypes(); ?>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE');?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="panel-body">
                        <div class="table-responsive">

                            <div class="col-lg-12">
                                <div id="events"></div>
                            </div>
                        </div>
                    </div>
					</div>
					 <!-- /.panel-heading -->
               
				<?php echo listEvents(); ?>
				<?php echo modalEvents(); ?>
				
				<?php	
						global $conection;
						
						if (!empty($_POST['novoevento']))
						 {
							// Recupera os dados dos campos
							$title = $_POST['title'];
							$description = trim(preg_replace('/\s+/', ' ',nl2br($_POST['description'])));
							$url = $_POST['url'];
							$start = $_POST['start'];
							$end = $_POST['end'];
						  
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO events VALUES (0, '".$title."','".str_replace( "'", "�", $description)."','".$start."','".$end."','".$url."')");
						 
									// Se os dados forem inseridos com sucesso			
									if (!$sql) {
									echo ("Can't insert into database: " . mysqli_error());
									return false;
									} else {
									echo "<script type='text/javascript'>swal({   title: '".lang('AWESOME')."',   text: '".lang('NEW_EVENT_CREATED')."',   timer: 1000,   showConfirmButton: false });</script>";
											echo '<meta http-equiv="refresh" content="1; eventspage.php">'; 
											die();
									}		
									return true;
						}

					
						// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
						if (!empty($_POST['novotipo']))
						 {
						 
							// Recupera os dados dos campos
							$title = $_POST['title'];
						  
									// Insere os dados no banco
									$sql = mysqli_query($conection,"INSERT INTO type VALUES (0, '".$title."')");
						 
									// Se os dados forem inseridos com sucesso			
									if (!$sql) {
									echo ("Can't insert into database: " . mysqli_error());
									return false;
									} else {
									echo "<script type='text/javascript'>swal({   title: '".lang('AWESOME')."',   text: '".lang('NEW_TYPE_CREATED')."',  timer: 1000,   showConfirmButton: false });</script>";
											echo '<meta http-equiv="refresh" content="1; eventspage.php">'; 
											die();
									}		
									return true;
						}
						
				?>
			</div>

			<div class="col-md-4">

				<!-- Basic layout-->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><?php echo lang('TOP_IMAGE');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					</div>
					
					<div class="panel-body">

						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="calendarImg" >
							<fieldset>
							<?php $csrf->echoInputField(); ?>	
							<?php 
								global $conection;
								$sql = mysqli_query($conection,"select * from calendar_img");
								$row = mysqli_fetch_assoc($sql);
								
								$image = $row['image'];
								
								if ($image == ''){
									echo"
										<div class='form-group'>
											<div class='col-md-9'>
											<label class='col-md-12 control-label' for='image'>".lang('UPLOAD_IMAGE')."</label>
												<input type='file' name='image' id='image'>
											</div>
											
										</div>

										</fieldset>
										<div class='col-md-12'>
											<input type='submit' name='calendarImg' class='btn btn-primary submit' value='".lang('ADD_NEW')."' />
										</div>
									";
								}
								else{
									echo"
										<div class='col-md-12'>
											<img class='img-responsive' src='../assets/img/uploads/parallax/".$row['image']."' alt='' />
										</div>
										
										<a id='del' class='btn btn-danger' href='javascript:EliminaCalendarImageParallax(".$row['id'].")' title='click for delete' ><i class='fa fa-fw fa-trash'></i> ".lang('DELETE_IMAGE')."</a>
									";
								}
							?>
				
							<?php
								global $conection;
								// Se o usu�rio clicou no bot�o cadastrar efetua as a��es
								if (!empty($_POST['calendarImg']))
								 {
								  
									// Recupera os dados dos campos
									$image = $_FILES['image'];

									// Se a foto estiver sido selecionada
									if (!empty($image["name"])) {
								 
										// Largura m�xima em pixels
										$largura = 1000000;
										// Altura m�xima em pixels
										$altura = 1000000;
										// Tamanho m�ximo do arquivo em bytes
										$tamanho = 500000000000;
								 
										// Verifica se o arquivo � uma imagem
										if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
										   $error[1] = "Isso n�o � uma imagem.";
										} 
								 
										// Pega as dimens�es da imagem
										$dimensoes = getimagesize($image["tmp_name"]);
								 
										// Verifica se a largura da imagem � maior que a largura permitida
										if($dimensoes[0] > $largura) {
											$error[2] = "A largura da imagem n�o deve ultrapassar ".$largura." pixels";
										}
								 
										// Verifica se a altura da imagem � maior que a altura permitida
										if($dimensoes[1] > $altura) {
											$error[3] = "Altura da imagem n�o deve ultrapassar ".$altura." pixels";
										}
								 
										// Verifica se o tamanho da imagem � maior que o tamanho permitido
										if($image["size"] > $tamanho) {
											$error[4] = "A imagem deve ter no m�ximo ".$tamanho." bytes";
										}
								 
											// Pega extens�o da imagem
											preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
								 
											// Gera um nome �nico para a imagem
											$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
								 
											// Caminho de onde ficar� a imagem
											$caminho_imagem = "../assets/img/uploads/parallax/" . $nome_imagem;
								 
											// Faz o upload da imagem para seu respectivo caminho
											move_uploaded_file($image["tmp_name"], $caminho_imagem);
								 
											// Insere os dados no banco
											$sql = mysqli_query($conection,"INSERT INTO calendar_img VALUES (0, '".$nome_imagem."')");
								 
											// Se os dados forem inseridos com sucesso			
											if (!$sql) {
											echo ("Can't insert into database: " . mysqli_error());
											return false;
											} else {
													echo "<script type='text/javascript'>swal('".lang('NICE')."', '".lang('NEW_IMAGE_UPLOADED')."', 'success');</script>";
													echo '<meta http-equiv="refresh" content="1; eventspage.php">'; 
													die();
											}

										// Se houver mensagens de erro, exibe-as
										if (count($error) != 0) {
											foreach ($error as $erro) {
												echo $erro . "<br />";
											}
										}

								}
								}
							?>
						  </form>
				</div>
			</div>
			
			<!-- Active sections Total-->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php echo lang('WEBSITE_SECTIONS');?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						</div>
						<div class="panel-body">
						
						<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updateCalendar" ><?php $csrf->echoInputField(); ?>					
						<fieldset>

						<div class="col-md-12">
						
						<?php echo activeCalendar(); ?>
							
							<!-- Button -->
							<div class="form-group">
							  <label class="col-md-8 control-label" for="singlebutton"></label>
							  <div class="col-md-2">
								<input type="submit" name="updateCalendar" class="btn btn-primary submit" value="update" />
							  </div>
							</div>
						</div>

						</fieldset>
						
						
					</form>
					 <?php
								  
					  if(isset($_POST['updateCalendar']))
						{
							updateActiveCalendar($_POST['services'],$_POST['testimonials'],$_POST['partners'],$_POST['subscribe'],$_POST['team'],$_POST['pricing'],$_POST['skills2']);
						}
					?>
						 
						</div>
					</div>
			
			
		</div>
	

  </div>
  <!-- /.row -->

</div>
 <!-- /.container-fluid -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>