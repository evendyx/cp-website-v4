<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold">Form Layouts</span> - Horizontal</h4>
			<a class="heading-elements-toggle"></a></div>

		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="index.html"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
				<li><a href="form_layout_horizontal.html">Form layouts</a></li>
				<li class="active">Horizontal</li>
			</ul>

		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
	
		<div class="row">
			<div class="col-md-8">

				<!-- Basic layout-->
				<form action="#" class="form-horizontal">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Basic layout<a class="heading-elements-toggle"></a></h5>
						</div>

						<div class="panel-body">
							<div class="form-group">
								<label class="col-lg-3 control-label">Name:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" placeholder="Eugene Kopyov">
								</div>
							</div>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit form</button>
							</div>
						</div>
					</div>
				</form>
				<!-- /basic layout -->

			</div>

			<div class="col-md-4">

				<!-- Basic layout-->
				<form action="#" class="form-horizontal">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Basic layout<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
						</div>

						<div class="panel-body">
							<div class="form-group">
								<label class="col-lg-3 control-label">Name:</label>
								<div class="col-lg-9">
									<input type="text" class="form-control" placeholder="Eugene Kopyov">
								</div>
							</div>


							<div class="text-right">
								<button type="submit" class="btn btn-primary">Submit form</button>
							</div>
						</div>
					</div>
				</form>
				<!-- /basic layout -->

			</div>
		</div>



    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>