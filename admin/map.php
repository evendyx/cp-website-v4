<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('MAP');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li class="active"><?php echo lang('MAP');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatemap" >
						<fieldset>
						<?php $csrf->echoInputField(); ?>
						<?php echo googleMaps();?>

						  <div id="map_canvas" style="width:50%; height:50%"></div>
						  
						  <div class="col-md-4">
						   <!-- Text input-->
						  <div class='form-group'>
							<label class='col-sm-4 control-label' for='latitude'><?php echo lang('LATITUDE') ?>:</label>
							<div class='col-sm-2'>
							   <input size="20" type="text" id="latbox" name="latitude" >
							</div>
						  </div>
								  
							<!-- Text input-->
						  <div class='form-group'>
							<label class='col-sm-4 control-label' for='longitude'><?php echo lang('LONGITUDE') ?>:</label>
							<div class='col-sm-2'>
							   <input size="20" type="text" id="lngbox" name="longitude" >
							</div>
						  </div>
						  </div>
						  
						  <?php
							global $conection;
							$sql = mysqli_query($conection,"select * from map WHERE id='1'");
							$row = mysqli_fetch_assoc($sql);
							
								$lat = $row['latitude'];
								$lng = $row['longitude'];
								$zoom = $row['zoom'];

							?>

						<cfoutput>
							<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAvOhtbl4wK5vOq3YZuc-gkldJTxI13eY4&sensor=false"></script>
						</cfoutput>

						<script type="text/javascript">
						//<![CDATA[

							// global "map" variable
							var map = null;
							var marker = null;

							// popup window for pin, if in use
							var infowindow = new google.maps.InfoWindow({ 
								size: new google.maps.Size(150,50)
								});

							// A function to create the marker and set up the event window function 
							function createMarker(latlng, name, html) {

							var contentString = html;

							var marker = new google.maps.Marker({
								position: latlng,
								map: map,
								zIndex: Math.round(latlng.lat()*-100000)<<5
								});

							google.maps.event.addListener(marker, 'click', function() {
								infowindow.setContent(contentString); 
								infowindow.open(map,marker);
								});

							google.maps.event.trigger(marker, 'click');    
							return marker;


						}

						function initialize() {

							// the location of the initial pin
							var myLatlng = new google.maps.LatLng(<?php echo $lat; ?>,<?php echo $lng; ?>);

							// create the map
							var myOptions = {
								zoom: <?php echo $zoom; ?>,
								center: myLatlng,
								mapTypeControl: true,
								mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
								navigationControl: true,
								mapTypeId: google.maps.MapTypeId.ROADMAP
							}

							map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

							// establish the initial marker/pin
							var image = '../assets/img/map-marker.png';  
							marker = new google.maps.Marker({
							  position: myLatlng,
							  map: map,
							  icon: image,
							  title:"Property Location",
							  draggable:true,
							  animation: google.maps.Animation.DROP
							});

							// establish the initial div form fields
							formlat = document.getElementById("latbox").value = myLatlng.lat();
							formlng = document.getElementById("lngbox").value = myLatlng.lng();

							// close popup window
							google.maps.event.addListener(map, 'click', function() {
								infowindow.close();
								});

							// removing old markers/pins
							google.maps.event.addListener(map, 'click', function(event) {
								//call function to create marker
								 if (marker) {
									marker.setMap(null);
									marker = null;
								 }

								// Information for popup window if you so chose to have one
								/*
								 marker = createMarker(event.latLng, "name", "<b>Location</b><br>"+event.latLng);
								*/

								var image = '../assets/img/map-marker.png';
								var myLatLng = event.latLng ;
								/*  
								var marker = new google.maps.Marker({
									by removing the 'var' subsquent pin placement removes the old pin icon
								*/
								marker = new google.maps.Marker({   
									position: myLatLng,
									map: map,
									icon: image,
									title:"Property Location",
									draggable:true,
									animation: google.maps.Animation.DROP
								});

								// populate the form fields with lat & lng 
								formlat = document.getElementById("latbox").value = event.latLng.lat();
								formlng = document.getElementById("lngbox").value = event.latLng.lng();

							});
							
							google.maps.event.addListener(marker, 'dragend', function (event) {
								// populate the form fields with lat & lng 
									formlat = document.getElementById("latbox").value = event.latLng.lat();
									formlng = document.getElementById("lngbox").value = event.latLng.lng();
							});

						}
						//]]>

						</script> 

					 <!-- Button -->
					<div class="form-group">
					  <label class="col-md-10 control-label" for="singlebutton"></label>
					  <div class="col-md-2">
						<input type="submit" name="updatemap" class="btn btn-primary" value="Update" />
					  </div>
					</div>
					
					</fieldset>
					
					 <?php
				  
					  if(isset($_POST['updatemap']))
						{
							updateMap($_POST['latitude'],$_POST['longitude'],htmlspecialchars($_POST['title'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES),$_POST['zoom'],$_POST['api']);
						}
					?>

				  </form>
				</div>
			</div>
			<!-- /basic layout -->

		</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>