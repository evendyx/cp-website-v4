<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('PROGRESS_BAR');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('BACKGROUND_SETTINGS');?></li>
				<li class="active"><?php echo lang('PROGRESS_BAR');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">

			<div class="row">
			<div class="col-md-12">

				<!-- Basic layout-->
				<div class="panel panel-flat">

					<div class="panel-body">
						 <form action="<?php $_PHP_SELF ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="updatebar" >
							<fieldset>
							<?php $csrf->echoInputField(); ?>
							<?php echo progressBar();?>
							
							  <!-- Text input-->
							  <div class='form-group'>
								<label class='col-sm-2 control-label' for='color'><?php echo lang('BACKGROUND_COLOR');?></label>
									<div class='col-sm-4'>
									  <div id="cp2" class="input-group colorpicker-component">
										<input type="text" name="color" value="<?php echo getColor(); ?>" class="form-control" />
										<span class="input-group-addon"><i></i></span>
									</div>
								</div>
							  </div>
							
							<script>
								$(function() {
									$('#cp2').colorpicker();
								});
							</script>

							</fieldset>
							
							<br/>
						  
						 <!-- Button -->
						<div class="form-group">
						  <label class="col-md-10 control-label" for="singlebutton"></label>
						  <div class="col-md-2">
							<input type="submit" name="updatebar" class="btn btn-primary" value="Update" />
						  </div>
						</div>
						
						 <?php
					  
						  if(isset($_POST['updatebar']))
							{
								updateBar(htmlspecialchars($_POST['percent'], ENT_QUOTES), $_POST['height'], $_POST['color']);
							}
						?>

					  </form>
					</div>
				</div>
				<!-- /basic layout -->

			</div>

		</div>
		<!-- /.row -->
        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>