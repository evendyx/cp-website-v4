<?php include 'header.php'; ?>

<?php include 'sidebar.php'; ?>

	<div class="page-header page-header-default">
		<div class="page-header-content">
			<div class="page-title">
				<h4><span class="text-semibold"><?php echo lang('EDIT_TESTIMONIAL');?></span></h4>
			</div>
		</div>

		<div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"></a>
			<ul class="breadcrumb">
				<li><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i> <?php echo lang('DASHBOARD');?></a></li>
				<li><?php echo lang('WEBSITE_SECTIONS');?></li>
				<li><a href="testimonials.php"><?php echo lang('TESTIMONIALS');?></a></li>
				<li class="active"><?php echo lang('EDIT_TESTIMONIAL');?></li>
			</ul>
		</div>
	</div>

<div id="page-wrapper">

    <div class="container-fluid">
         <div class="row">
			<div class="col-md-12">

			<!-- Basic layout-->
			<div class="panel panel-flat">

				<div class="panel-body">
					<form id="editTestimonial" method="post" enctype="multipart/form-data" class="form-horizontal" name="editTestimonial">
										
						<?php $csrf->echoInputField(); ?>
						
						<fieldset>
							
						<?php echo editTestimonial($_GET['id']); ?>

						<!-- Button -->
						<div class="form-group">
							<label class="col-md-9 control-label" for="singlebutton"></label>
							<div class="col-md-3">
								<input type="submit" name="editTestimonial" class="btn btn-primary" value="<?php echo lang('UPDATE_INFO');?>" />
							</div>
						</div>

						</fieldset>
					</form>
					<?php

					  if(isset($_POST['editTestimonial']))
					  {
						updateTestimonial($_GET['id'],htmlspecialchars($_POST['name'], ENT_QUOTES),htmlspecialchars($_POST['website'], ENT_QUOTES),htmlspecialchars($_POST['description'], ENT_QUOTES));
					  }

					?>
				</div>
			</div>
			<!-- /basic layout -->

		</div>

        </div>
        <!-- /.row -->


    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<?php include 'footer.php'; ?>